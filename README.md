# modmedLog

The open source modmedLog C++ library allows producing analyzable traces from 
existing standard C++ tracepoints, and then improving them while refactoring the
source code. 

It is designed to provide JSON traces compatible with ParTraP tools and is 
provided AS-IS according to the BSD license.

It is not actively developed anymore, but there is still ongoing work to 
integrate modmedLog features to the [Qt framework](https://www.qt.io/) based on 
the [QBind proof-of-concept](/tests/QBind). 
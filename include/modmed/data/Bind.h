/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <vector>
#include <map>
#include <set>
#include <list>
#include <array>

#include <modmed/data/BindDeclaration.h>

#include <modmed/modmed_global.h>
#include <QtCore/qstring.h>
#include <QtCore/qlogging.h>
#include <QtCore/qvector.h>

QT_WARNING_PUSH
QT_WARNING_DISABLE_CLANG("-Wmicrosoft-enum-value")
#include <QtCore/qdatetime.h>
#include <QtCore/quuid.h>
#include <QtCore/qsharedpointer.h>
#include <QtCore/qlinkedlist.h>
#include <QtCore/qvector.h>
#include <QtCore/qlist.h>
#include <QtCore/qmap.h>
#include <QtCore/qhash.h>
#include <QtCore/qset.h>
#include <QtCore/qvariant.h>
#include <QtCore/qlocale.h>
#include <QtCore/qcoreapplication.h>
#include <QtCore/qversionnumber.h>
#include <QtCore/qprocess.h>

#include <QtNetwork/qhostinfo.h>
#include <QtNetwork/qhostaddress.h>
QT_WARNING_POP

namespace modmed {
namespace data {

class MODMED_API Hex
{
public:
    template<typename T, class = typename std::enable_if<std::is_integral<T>::value>::type>
    Hex(T& p_value);

    template<typename T, class = typename std::enable_if<std::is_integral<T>::value>::type>
    Hex(const T& p_value);

    Hex(void* p_ptr);

    void bind(Item<Writer> p_data) const;

    void bind(Item<Reader> p_data);

private:
    quint64 m_value;
    enum ValueType {
        Bool = 0,
        U8 = 1,
        U16 = 2,
        U32 = 4,
        U64 = 8
    } m_valueType;
    void *m_valuePtr;
};

/// <summary> Default const implementation that produces a static_assert</summary>
template<typename T> struct Bind < Reader, const T >
{ static void bind(Item<Reader> data, const T& userData); };
/// <summary> Default const implementation that calls bind with a const_cast on userData</summary>
template<typename T> struct Bind < Writer, const T >
{ static void bind(Item<Writer> data, const T& userData); };

/// <summary> Default implementation for c-array that binds its contents in a sequence </summary>
//! An empty c-array is bound as an empty sequence
template<Operator Op, typename T, size_t Size> struct Bind < Op, T[Size] >
{ static void bind(Item<Op> data, T(&userData)[Size]); };

/// <summary> Default const implementation for c-array that produces a static_assert</summary>
template<typename T, size_t Size> struct Bind < Reader, const T[Size] >
{ static void bind(Item<Reader> data, const T(&userData)[Size]); };
/// <summary> Default const implementation for c-array that calls bind with a const_cast on userData</summary>
template<typename T, size_t Size> struct Bind < Writer, const T[Size] >
{ static void bind(Item<Writer> data, const T(&userData)[Size]); };

#ifdef Q_COMPILER_NULLPTR
template<Operator Op> struct Bind < Op, std::nullptr_t >
{ static void bind(Item<Op> data, std::nullptr_t& ); };
#endif

template<Operator Op> struct Bind < Op, void* >
{ static void bind(Item<Op> data, void*& userData); };

template<> struct Bind < Writer, const void* >
{ static void bind(Item<Writer> data, const void*& userData); };

/// <summary> Bind null if userData is null and *userData otherwise </summary>
/// \warning The function allocates a userData object. It is the responsibility of the calling code to ensure the destruction of the object.
template<typename T> struct Bind < Reader, T* >
{ static void bind(Item<Reader> data, T*& userData); };
/// <summary> Bind null if userData is null and *userData otherwise </summary>
template<typename T> struct Bind < Writer, T* >
{ static void bind(Item<Writer> data, T*& userData); };

template<Operator Op> struct Bind < Op, QLatin1String >
{
    static void bind(Item<Op>, QLatin1String&);
};

//-----------------------------------------------------------------------------
// Item

/// <summary> Bind a readable Item \p p_src with a non-readable Item \p p_dst </summary>
template<> struct MODMED_API Bind < Reader, Item<Writer> >
{ static void bind(Item<Reader> p_src, Item<Writer>& p_dst); };

template<Operator Op, size_t Size> struct Bind < Op, char[Size] >
{
    static void bind(Item<Op> data, char(&userData)[Size]);
};

//-----------------------------------------------------------------------------
// C++ STL

template<> struct MODMED_API Bind < Reader, std::string >
{ static void bind(Item<Reader>, std::string&); };
template<> struct MODMED_API Bind < Writer, std::string >
{ static void bind(Item<Writer>, std::string&); };

template<> struct MODMED_API Bind < Reader, std::wstring >
{ static void bind(Item<Reader>, std::wstring&); };
template<> struct MODMED_API Bind < Writer, std::wstring >
{ static void bind(Item<Writer>, std::wstring&); };

/// <summary> Bind the \p userArray into a sequence </summary>
//! An empty std::array is bound as an empty sequence
template<Operator Op, class T, size_t Size> struct Bind < Op, std::array<T, Size> >
{ static void bind(Item<Op> data, std::array<T, Size>& userArray); };

/// <summary> Bind the \p userVector into a sequence </summary>
template<Operator Op, class T> struct Bind < Op, std::vector<T> >
{ static void bind(Item<Op> data, std::vector<T>& userVector); };

/// <summary> Bind the \p userVector into a sequence </summary>
template<Operator Op, class T> struct Bind < Op, QVector<T> >
{ static void bind(Item<Op> data, QVector<T>& userVector); };

/// <summary> Bind the \p userMap into a sequence containing a record for each item. </summary>
//! Each record contains a "key" and a "value" item
//! An empty std::map is bound as an empty sequence
template<typename TKey, typename T> struct Bind < Reader, std::map<TKey, T> >
{ static void bind(Item<Reader> d, std::map<TKey, T>& userMap); };
template<typename TKey, typename T> struct Bind < Writer, std::map<TKey, T> >
{ static void bind(Item<Writer> d, std::map<TKey, T>& userMap); };

/// <summary> Bind the \p userMap into a record indexed by the userMap keys </summary>
//! An empty std::map<std::string, ...> is bound as an empty record
template<typename T> struct Bind < Reader, std::map<std::string, T> >
{ static void bind(Item<Reader> d, std::map<std::string, T>& userMap); };
template<typename T> struct Bind < Writer, std::map<std::string, T> >
{ static void bind(Item<Writer> d, std::map<std::string, T>& userMap); };

/// <summary> Bind the \p userList into a sequence </summary>
//! An empty std::list is bound as an empty sequence
template<Operator Op, class T> struct Bind < Op, std::list<T> >
{ static void bind(Item<Op> data, std::list<T>& userList); };

/// <summary> Bind the \p userSet into a sequence </summary>
//! An empty std::set is bound as an empty sequence
template<class T> struct Bind < Reader, std::set<T> >
{ static void bind(Item<Reader> data, std::set<T>& userSet); };
template<class T> struct Bind < Writer, std::set<T> >
{ static void bind(Item<Writer> data, std::set<T>& userSet); };

template<Operator Op> struct Bind < Op, QtMsgType >
{
    static void bind (Item<Op>, QtMsgType&);
};
template<>
void MODMED_API Bind<Reader, QtMsgType>::bind(Item<Reader> data, QtMsgType& userItm);
template<>
void MODMED_API Bind<Writer, QtMsgType>::bind(Item<Writer> data, QtMsgType& userItm);

//-----------------------------------------------------------------------------
// Qt IItem

/// <summary> Bind the \p userList into a sequence </summary>
//! An empty QList is bound as an empty sequence
template<Operator Op, typename T> struct Bind < Op, QList<T> >
{ static void bind(Item<Op> d, QList<T>& userList); };

/// <summary> Bind the \p userList into a sequence </summary>
//! An empty QStringList is bound as an empty sequence
template<Operator Op> struct Bind < Op, QStringList >
{ static void bind(Item<Op> d, QStringList& userList); };

/// <summary> Bind the \p userLinkedList into a sequence </summary>
//! An empty QLinkedList is bound as an empty sequence
template<Operator Op, typename T> struct Bind < Op, QLinkedList<T> >
{ static void bind(Item<Op> d, QLinkedList<T>& userLinkedList); };

/// <summary> Bind the \p userSet into a sequence </summary>
//! An empty QSet is bound as an empty sequence
template<Operator Op, typename T> struct Bind < Op, QSet<T> >
{ static void bind(Item<Op> d, QSet<T>& userSet); };

/// <summary> Bind the \p userMap into a record indexed by the userMap keys </summary>
//! An empty QMap<QString, ...> is bound as an empty record
template<Operator Op, typename T> struct Bind < Op, QMap<QString,T> >
{ static void bind(Item<Op> d, QMap<QString,T>& userMap); };

/// <summary> Bind the \p userMap into a record indexed by the userMap keys </summary>
//! An empty QHash<QString, ...> is bound as an empty record
template<Operator Op, typename T> struct Bind < Op,QHash<QString ,T> >
{ static void bind(Item<Op> d, QHash<QString,T>& userMap);};

/// <summary> Bind the \p userMap into a sequence containing a record for each item. </summary>
//! Each record contains a "key" and a "value" item
//! An empty QMap is bound as an empty sequence
template<Operator Op, typename TKey, typename T> struct Bind < Op, QMap<TKey, T> >
{ static void bind(Item<Op> d, QMap<TKey, T>& userMap);};

/// <summary> Bind the \p userMap into a sequence containing a record for each item. </summary>
//! Each record contains a "key" and a "value" item
//! An empty QHash is bound as an empty sequence
template<Operator Op, typename TKey, typename T> struct Bind < Op, QHash<TKey ,T> >
{ static void bind(Item<Op> d, QHash<TKey,T>& userMap);};

/// <summary> Bind null if userData is null and *userData otherwise </summary>
template<typename T> struct Bind < Reader, QSharedPointer<T> >
{ static void bind(Item<Reader> data, QSharedPointer<T>& userData); };
/// <summary> Bind null if userData is null and *userData otherwise </summary>
template<typename T> struct Bind < Writer, QSharedPointer<T> >
{ static void bind(Item<Writer> data, QSharedPointer<T>& userData); };

template<Operator Op> struct Bind < Op, QDate >
{
    //! format Date as descrubed by ISO-8601 (aka RFC-3339)
    static void bind (Item<Op>, QDate&);
};
template<>
void MODMED_API Bind<Reader, QDate>::bind(Item<Reader> data, QDate& userItm);
template<>
void MODMED_API Bind<Writer, QDate>::bind(Item<Writer> data, QDate& userItm);

template<Operator Op> struct Bind < Op, QTime >
{
    //! format Date as descrubed by ISO-8601 (aka RFC-3339)
    static void bind (Item<Op>, QTime&);
};
template<>
void MODMED_API Bind<Reader, QTime>::bind(Item<Reader> data, QTime& userItm);
template<>
void MODMED_API Bind<Writer, QTime>::bind(Item<Writer> data, QTime& userItm);

template<Operator Op> struct Bind < Op, QUuid >
{
    static void bind (Item<Op>, QUuid&);
};
template<>
void MODMED_API Bind<Reader, QUuid>::bind(Item<Reader> data, QUuid& userItm);
template<>
void MODMED_API Bind<Writer, QUuid>::bind(Item<Writer> data, QUuid& userItm);

template<Operator Op> struct Bind < Op, QLocale >
{
    static void bind (Item<Op>, QLocale&);
};

template<>
struct MODMED_API Bind < Writer, QCoreApplication >
{
    static void bind (Item<Writer>, QCoreApplication&);
};

template<Operator Op> struct Bind < Op, QHostInfo >
{
    static void bind (Item<Op>, QHostInfo&);
};

template<Operator Op> struct Bind < Op, QHostAddress >
{
    static void bind (Item<Op>, QHostAddress&);
};
template<>
void MODMED_API Bind<Reader, QHostAddress>::bind(Item<Reader> data, QHostAddress& userItem);
template<>
void MODMED_API Bind<Writer, QHostAddress>::bind(Item<Writer> data, QHostAddress& userItem);

template<Operator Op> struct Bind < Op, QVersionNumber >
{
    static void bind (Item<Op>, QVersionNumber&);
};
template<>
void MODMED_API Bind<Reader, QVersionNumber>::bind(Item<Reader> data, QVersionNumber& userItem);
template<>
void MODMED_API Bind<Writer, QVersionNumber>::bind(Item<Writer> data, QVersionNumber& userItem);

template<>
struct MODMED_API Bind < Reader, QProcessEnvironment >
{
    static void bind (Item<Reader>, QProcessEnvironment&);
};

template<>
struct MODMED_API Bind < Writer, QProcessEnvironment >
{
    static void bind (Item<Writer>, QProcessEnvironment&);
};

} // data
} // modmed

#include "Bind.hpp"



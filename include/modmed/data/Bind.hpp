/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>
#include <string.h>

#include <QtCore/qtextcodec.h>

#include <modmed/data/Data.h>

namespace modmed {
namespace data {

//==============================================================================
template<typename T, class> // std::enable_if<std::is_integral<T>::value>::type
Hex::Hex(T& p_value)
{
    static_assert(sizeof(T) == 1 ||
                  sizeof(T) == 2 ||
                  sizeof(T) == 4 ||
                  sizeof(T) == 8, "Can only handle integral type of 8, 16, 32 and 64 bits");
    if (std::is_same<T, bool>::value)
        m_valueType = Bool;
    else
        m_valueType = ValueType(sizeof(T));
    m_valuePtr = &p_value;
}

//==============================================================================
template<typename T, class> // std::enable_if<std::is_integral<T>::value>::type
Hex::Hex(const T& p_value)
{
    m_value = p_value;
    m_valueType = ValueType(sizeof(uint64_t));
    m_valuePtr = &m_value;
}

//==============================================================================
template<Operator Op, typename T>
void Bind<Op, T>::bind(Item<Op> p_data, T& p_userData)
{
    p_userData.bind(p_data); // If you get here, you can either define a global bind function with the following prototype data::Bind<Op, T>::bind(Item<Op>, T&) or a class method T::bind(Item<Op>)
}

//==============================================================================
template<typename T>
void Bind<Reader, const T>::bind(Item<Reader>, const T&)
{
    static_assert(std::is_void<T>::value, "Cannot read a Item<Reader> and store its result in a const variable");
}

//==============================================================================
template<typename T>
void Bind<Writer, const T>::bind(Item<Writer> data, const T& userData)
{
    bind(data, const_cast<T&>(userData));
}

//==============================================================================
template<Operator Op, typename T, size_t Size>
void Bind<Op, T[Size]>::bind(Item<Op> data, T(&userData)[Size])
{
    auto s = data.sequence();
    for (unsigned i = 0; i < Size; ++i)
    { s = s.bind(userData[i]); }
}

//==============================================================================
template<typename T, size_t Size>
void Bind<Reader, const T[Size]>::bind(Item<Reader>, const T(&)[Size])
{
    static_assert(std::is_void<T>::value, "Cannot read a Item<Reader> and store its result in a const variable");
}

//==============================================================================
template<typename T, size_t Size>
void Bind<Writer, const T[Size]>::bind(Item<Writer> data, const T(&userData)[Size])
{
    bind(data, const_cast<T(&)[Size]>(userData));
}

#ifdef Q_COMPILER_NULLPTR
//==============================================================================
template<Operator Op>
void Bind<Op, std::nullptr_t>::bind(Item<Op> data, std::nullptr_t&)
{
    data.null();
}
#endif

//==============================================================================
template<Operator Op>
void Bind<Op, void*>::bind(Item<Op> data, void*& userData)
{
    data.bind(reinterpret_cast<uintptr_t>(userData));
}

//==============================================================================
template<typename T>
void Bind<Reader, T*>::bind(Item<Reader> data, T*& userData)
{
    if (data.isNull()) { userData = nullptr; }
    else
    {
        delete userData; // CR This is to avoid leaks... but we cannot be sure to point to the heap
        userData = new T(); // If you get an error here, you may write a more specialized Bind<T*> using another constructor
        // try not to eat the data to support more flexible reading
        BindErrors err;
        if (data.state()->errors() == nullptr)
        { data.state()->trapErrors(&err); }
        if (!data.bind(*userData).noError())
        {
            delete userData;
            userData = nullptr;
        }
    }
}

//==============================================================================
template<typename T>
void Bind<Writer, T*>::bind(Item<Writer> data, T*& userData)
{
    if (userData) { data.bind(*userData); }
    else { data.null(); }
}

//==============================================================================
template<Operator Op, size_t Size>
void Bind<Op, char[Size]>::bind(Item<Op> data, char(&userData)[Size])
{
    QString s(userData);
    data.bind(s);
    if (Op == Reader)
    {
        std::string ss = s.toStdString();
        if (ss.size() < Size)
            strcpy(userData, ss.c_str());
        else
            data.state()->addError(FailBind("char c-array is not big enough to contain read value", true));
    }
}

//==============================================================================
template<Operator Op>
void Bind<Op, QLatin1String>::bind(Item<Op> data, QLatin1String& userData)
{
    QString s(userData);
    data.bind(s);
    if (Op == Reader)
    {
      userData = QLatin1String(s.toLatin1());
    }
}

//==============================================================================
template<Operator Op, class T, size_t Size>
void Bind<Op, std::array<T, Size>>::bind(Item<Op> data, std::array<T, Size>& userArray)
{
    auto s = data.sequence();
    if (!s.state()->isOk()) { return; }
    for (unsigned i = 0; i < Size; ++i)
    { s = s.bind(userArray[i]); }
}

//==============================================================================
template<typename TCollection>
inline void bindCollectionWithPushBackAsSequence(Item<Reader> d, TCollection& userCollection)
{
    using T = typename TCollection::value_type;
    Sequence<Reader> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    try
    {
        userCollection.clear();
        for (Item<Reader> i(s.item()); i.state()->isOk(); i = s.item())
        {
            T readItem;
            i.bind(readItem);
            userCollection.push_back(readItem);
        }
    }
    catch (std::bad_alloc) {
        // TODO remove ?
    }
}

//==============================================================================
template<typename TCollection>
inline void bindCollectionWithPushBackAsSequence(Item<Writer> d, TCollection& userCollection)
{
    Sequence<Writer> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    for (typename TCollection::iterator userItem = userCollection.begin(); userItem != userCollection.end(); ++userItem)
    {
        s.bind(*userItem);
    }
}

//==============================================================================
template<Operator Op, class T>
void Bind<Op, std::vector<T>>::bind(Item<Op> data, std::vector<T>& userVector)
{
    bindCollectionWithPushBackAsSequence(data, userVector);
}

//==============================================================================
template<Operator Op, class T>
void Bind<Op, QVector<T>>::bind(Item<Op> data, QVector<T>& userVector)
{
    bindCollectionWithPushBackAsSequence(data, userVector);
}

//==============================================================================
template<typename TKey, typename T>
void Bind<Reader, std::map<TKey, T>>::bind(Item<Reader> d, std::map<TKey, T>& userMap)
{
    Sequence<Reader> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    TKey k;
    userMap.clear();
    for (Item<Reader> i(s.item()); i.state()->isOk(); i = s.item())
    {
        auto r = i.record().bind(QStringLiteral("key"), k);
        // Splitting in 2 statements prevents compiler optimizations that would use k before it is read!
        r.bind(QStringLiteral("value"), userMap[k]);
    }
}

//==============================================================================
template<typename TKey, typename T>
void Bind<Writer, std::map<TKey, T>>::bind(Item<Writer> d, std::map<TKey, T>& userMap)
{
    Sequence<Writer> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    for (typename std::map<TKey, T>::iterator userItem = userMap.begin(); userItem != userMap.end(); ++userItem)
    {
        s.record()
                .bind(QStringLiteral("key"  ), userItem->first )
                .bind(QStringLiteral("value"), userItem->second);
    }
}

//==============================================================================
template<typename T>
void Bind<Reader, std::map<std::string, T>>::bind(Item<Reader> d, std::map<std::string, T>& userMap)
{
    Record<Reader> r(d.record());
    if (!r.state()->isOk()) { return; }
    QString k;
    userMap.clear();
    for (Item<Reader> i(r.item(k)); i.state()->isOk(); i = r.item(k))
    {
        T readItem;
        i.bind(readItem);
        userMap[k.toStdString()] = readItem;
    }
}

//==============================================================================
template<typename T>
void Bind<Writer, std::map<std::string, T>>::bind(Item<Writer> d, std::map<std::string, T>& userMap)
{
    Record<Writer> r(d.record());
    if (!r.state()->isOk()) { return; }
    QString k;
    for (typename std::map<std::string, T>::iterator userItem = userMap.begin(); userItem != userMap.end(); ++userItem)
    {
        k = QString::fromStdString(userItem->first);
        r.bind(k, userItem->second);
    }
}

//==============================================================================
template<Operator Op, class T>
void Bind<Op, std::list<T>>::bind(Item<Op> data, std::list<T>& userList)
{
    bindCollectionWithPushBackAsSequence(data, userList);
}

//==============================================================================
template<class T>
void Bind<Reader, std::set<T>>::bind(Item<Reader> d, std::set<T>& userSet)
{
    Sequence<Reader> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    try
    {
        userSet.clear();
        for (Item<Reader> i(s.item()); i.state()->isOk(); i = s.item())
        {
            T readItem;
            i.bind(readItem);
            userSet.insert(readItem);
        }
    }
    catch (std::bad_alloc) {
        // TODO remove ?
    }
}

//==============================================================================
template<class T>
void Bind<Writer, std::set<T>>::bind(Item<Writer> d, std::set<T>& userSet)
{
    Sequence<Writer> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    for (typename std::set<T>::iterator userItem = userSet.begin(); userItem != userSet.end(); ++userItem)
    {
        s.bind(*userItem);
    }
}

template<Operator Op>
void Bind<Op, QStringList>::bind(Item<Op> d, QStringList& userList)
{
    bindQtCollectionAsSequence(d, userList);
}

template<Operator Op>
void Bind<Op, QLocale>::bind(Item<Op> data, QLocale& userData)
{
    data.record()
    .bind(QStringLiteral("bcp47_name"      ), userData.bcp47Name())
    .bind(QStringLiteral("ui_languages"    ),userData.uiLanguages())
    .bind(QStringLiteral("timestamp_format"),userData.dateTimeFormat())
    ;
}

template<Operator Op>
void Bind<Op, QHostInfo>::bind(Item<Op> data, QHostInfo& userData)
{
    data.record()
    .bind(QStringLiteral(        "addresses"), userData.addresses      ())
    .bind(QStringLiteral(        "host_name"), userData.hostName       ())
    .bind(QStringLiteral("local_domain_name"), userData.localDomainName())
    .bind(QStringLiteral(  "local_host_name"), userData.localHostName  ())
    ;
}

template<typename TCollection>
inline void bindQtCollectionAsSequence(Item<Reader> d, TCollection& userCollection)
{
    typedef typename TCollection::value_type T;
    Sequence<Reader> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    userCollection.clear();
    for (Item<Reader> i(s.item()); i.state()->isOk(); i = s.item())
    {
        T userData;
        i.bind(userData);
        userCollection << userData;
    }
}

template<typename TCollection>
inline void bindQtCollectionAsSequence(Item<Writer> d, TCollection& userCollection)
{
    Sequence<Writer> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    Q_FOREACH (auto v, userCollection)
    {
        s.bind(v);
    }
}

template<Operator Op, typename T>
inline void Bind<Op, QList<T>>::bind(Item<Op> d, QList<T>& userList)
{
    bindQtCollectionAsSequence(d, userList);
}

template<Operator Op, typename T>
inline void Bind<Op, QLinkedList<T>>::bind(Item<Op> d, QLinkedList<T>& userLinkedList)
{
    bindQtCollectionAsSequence(d, userLinkedList);
}

template<Operator Op, typename T>
inline void Bind<Op, QSet<T>>::bind(Item<Op> d, QSet<T>& userSet)
{
    bindQtCollectionAsSequence(d, userSet);
}


template<typename TDictionary>
inline void bindQtDictAsRecord(Item<Reader> d, TDictionary& userDict)
{
    Record<Reader> r(d.record());
    if (!r.state()->isOk()) { return; }
    QString k;
    userDict.clear();
    for (Item<Reader> i(r.item(k)); i.state()->isOk(); i = r.item(k))
    {
        i.bind(userDict[k]);
    }
}

template<typename TDictionary>
inline void bindQtDictAsRecord(Item<Writer> d, TDictionary& userDict)
{
    Record<Writer> r(d.record());
    if (!r.state()->isOk()) { return; }
    for (typename TDictionary::iterator userItems = userDict.begin(); userItems != userDict.end(); ++userItems)
    {
        r.bind(userItems.key(), userItems.value());
    }
}

template<typename TDictionary>
inline void bindQtDictAsSequenceOfRecord(Item<Reader> d, TDictionary& userDict)
{
    Sequence<Reader> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    typename TDictionary::key_type k;
    userDict.clear();
    for (Item<Reader> i(s.item()); i.state()->isOk(); i = s.item())
    {
        auto r = i.record().bind(QStringLiteral("key"), k);
        // Splitting in 2 statements prevents compiler optimizations that would use k before it is read!
        r.bind(QStringLiteral("value"), userDict[k]);
    }
}

template<typename TDictionary>
inline void bindQtDictAsSequenceOfRecord(Item<Writer> d, TDictionary& userDict)
{
    Sequence<Writer> s(d.sequence());
    if (!s.state()->isOk()) { return; }
    for (typename TDictionary::iterator userItems = userDict.begin(); userItems != userDict.end(); ++userItems)
    {
        s.record()
                .bind(QStringLiteral("key"  ), userItems.key  ())
                .bind(QStringLiteral("value"), userItems.value());
    }
}

template<Operator Op, typename T>
inline void Bind<Op, QMap<QString, T>>::bind(Item<Op> d, QMap<QString, T>& userMap)
{
    bindQtDictAsRecord(d, userMap);
}

template<Operator Op, typename T>
inline void Bind<Op, QHash<QString, T>>::bind(Item<Op> d, QHash<QString, T>& userHash)
{
    bindQtDictAsRecord(d, userHash);
}

template<Operator Op, typename TKey, typename T>
inline void Bind<Op, QMap<TKey, T>>::bind(Item<Op> d, QMap<TKey, T>& userMap)
{
    bindQtDictAsSequenceOfRecord(d, userMap);
}

template<Operator Op, typename TKey, typename T>
inline void Bind<Op, QHash<TKey, T>>::bind(Item<Op> d, QHash<TKey, T>& userHash)
{
    bindQtDictAsSequenceOfRecord(d, userHash);
}

template<typename T>
inline void Bind<Reader, QSharedPointer<T>>::bind(Item<Reader> data, QSharedPointer<T>& userData)
{
    if (data.isNull()) { userData.reset(nullptr); }
    else
    {
        userData.reset(new T()); // avoiding this prior allocation would require a copy ctor
        // try not to eat the data to support more flexible reading
        data.bind(*userData); // see issue #625
    }
}

template<typename T>
inline void Bind<Writer, QSharedPointer<T>>::bind(Item<Writer> data, QSharedPointer<T>& userData)
{
    if (userData) { data.bind(*userData); }
    else { data.null(); }
}

} // data
} // modmed

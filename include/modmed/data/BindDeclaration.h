/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qstring.h>

namespace modmed {
namespace data {

/// <summary> Specifies what to do when binding 2 data structures following a common logical "model" </summary>
//!
//! \remark In addition to Reader and Writer, one may imagine an Operator::Merge that would Read/Write both data structures to make them identical.
//! This information can be used at compile time for instance to:
//! - try and match some const T bound to a Item&lt;Reader&gt;
//! - iterate through Item or T collections depending on Reader/Writer operator (and truncate the other side...)
//!
enum Operator
{
    /// <summary> Bind &lt; Item&lt;Reader&gt;, T &gt; reads some Item and constructs the corresponding T (which cannot be const) </summary>
    Reader = 1,
    /// <summary> Bind &lt; Item&lt;Writer&gt;, T &gt; reads some T (possibly const) and writes the corresponding Item </summary>
    Writer = 2,
};

// Break an include cycle with Data.h

template<Operator Op> class Result;
template<class T_> class Itm;
template<Operator Op> using Item = Itm<Result<Op>>;

/// <summary> Default implementation that calls userData.bind and displays a comment to help the user define the missing method or specialization </summary>
template<Operator Op, typename T> struct Bind
{
    static void bind(Item<Op> data, T& userData);
};

} // data
} // modmed

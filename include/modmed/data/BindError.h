/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qmap.h>
#include <QtCore/qpair.h>
#include <QtCore/qstring.h>
#include <QtCore/qscopedpointer.h>
#include <QtCore/qsharedpointer.h>

#include <modmed/data/BindDeclaration.h>

#include <modmed/modmed_global.h>

QT_WARNING_PUSH
QT_WARNING_DISABLE_CLANG("-Wunused-private-field") // defined in lieu of more elaborate Q_D()

namespace modmed {
namespace data {

struct BindErrorPrivate;

/// <summary> Base class that represents an error occurring during the bind process </summary>
class MODMED_API BindError
{
public:
    /// <summary> Returns the description of the error </summary>
    virtual QString toString() const;

    /// <summary> Returns true if the bind process was reading and false if it was writing </summary>
    bool isReader() const;

    /// <summary> Destructor </summary>
    virtual ~BindError();

    /// <summary> Method used to deep copy the BindError </summary>
    virtual QSharedPointer<BindError> clone() const = 0; // CR Use SharedFromThis ?

protected:
    /// <summary> Construct the BindError with a description and a read/write status </summary>
    BindError(const QString& p_description, bool p_isReader);

private:
    QScopedPointer<BindErrorPrivate> m_d;
};

/// <summary> Binds toString() </summary>
template<>
struct Bind<Writer, BindError>
{
    static void bind(Item<Writer> d, BindError& t);
};

class MODMED_API Unknown : public BindError // CR Rename UnknownError
{
public:
    Unknown(const QString& p_description, bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring during a call to bind </summary>
class MODMED_API FailBind : public BindError // CR Rename BindError
{
public:
    FailBind(const QString& p_description, bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring during a call to bindFrom </summary>
class MODMED_API FailBindFrom : public BindError
{
public:
    FailBindFrom(QString p_read, QString p_type);
    QSharedPointer<BindError> clone() const;

    /// <summary> Returns the QString that was read </summary>
    QString getRead() const;

    /// <summary> Returns the type that was bound </summary>
    QString getType() const;
private:
    QScopedPointer<QPair<QString,QString>> m_d; // no Class##Private struct
};

/// <summary> Fail occurring during a call to IItem::null </summary>
class MODMED_API FailNull : public BindError
{
public:
    FailNull(bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring during a call to IItem::sequence </summary>
class MODMED_API FailSequence : public BindError
{
public:
    FailSequence(bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring during a call to ISequence::sequenceItem </summary>
class MODMED_API FailSequenceItem : public BindError
{
public:
    FailSequenceItem(bool p_isReader, int p_idx);
    QSharedPointer<BindError> clone() const;

    /// <summary> Returns the index of the item that fails </summary>
    int getIdx() const;
private:
    QScopedPointer<int> m_idx;
};

/// <summary> Fail occurring during a call to ISequence::sequenceOut </summary>
class MODMED_API FailSequenceOut : public BindError
{
public:
    FailSequenceOut(bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring during a call to IItem::record </summary>
class MODMED_API FailRecord : public BindError
{
public:
    FailRecord(bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring during a call to IRecord::recordItem </summary>
class MODMED_API FailRecordItem : public BindError
{
public:
    FailRecordItem(bool p_isReader, QString p_key);
    QSharedPointer<BindError> clone() const;

    /// <summary> Returns the key of the item that fails </summary>
    QString getKey() const;
private:
    QScopedPointer<QString> m_key;
};

/// <summary> Fail occurring during a call to IRecord::recordOut </summary>
class MODMED_API FailRecordOut : public BindError
{
public:
    FailRecordOut(bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring when IRecord::recordItem is called with an empty key </summary>
class MODMED_API FailRecordItemEmptyKey : public BindError
{
public:
    FailRecordItemEmptyKey (bool p_isReader);
    QSharedPointer<BindError> clone() const;
private:
    void* _RESERVED_FOR_FUTURE_USE_;
};

/// <summary> Fail occurring when IRecord::recordItem is called with key but the key read is not the same </summary>
class MODMED_API FailRecordItemWrongKey : public BindError
{
public:
    FailRecordItemWrongKey (QString p_expectedKey, QString p_readKey);
    QSharedPointer<BindError> clone() const;

    /// <summary> Returns the key that was expected </summary>
    QString getExpectedKey() const;

    /// <summary> Returns the key that was read </summary>
    QString getReadKey() const;
private:
    QScopedPointer<QPair<QString,QString>> m_d; // no Class##Private struct
};

/// <summary> Fail occurring during a call to IMessage::text </summary>
class MODMED_API FailText : public BindError
{
public:
    FailText(bool p_isReader, QString p_txt);
    QSharedPointer<BindError> clone() const;

    /// <summary> Returns the text that was bound </summary>
    QString getText() const;
private:
    QScopedPointer<QString> m_txt;
};

/// <summary> Fail occurring during a call to IMessage::bindText </summary>
class MODMED_API FailBindText : public BindError
{
public:
    FailBindText(bool p_isReader, QString p_txt);
    QSharedPointer<BindError> clone() const;

    /// <summary> Returns the text that was bound </summary>
    QString getText() const;
private:
    QScopedPointer<QString> m_txt;
};

struct BindErrors : public QMap<QString, QSharedPointer<BindError>>
{
    template <typename T> QSharedPointer<T> find() const
    {
        for (auto err : *this)
            if (err.dynamicCast<T>())
                return err.dynamicCast<T>();
        return nullptr;
    }

    void bind(Item<Writer> data);
};

} // data
} // modmed

QT_WARNING_POP

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once
#include <modmed/modmed_global.h>

#include <memory>

#include <QtCore/qglobal.h>

#include <QtCore/qsharedpointer.h>
#include <QtCore/qstring.h>
#include <QtCore/qdatetime.h>
#include <QtCore/qvector.h>

#include <modmed/data/IData.h>
#include <modmed/data/BindError.h>

namespace modmed {
namespace data {

template<Operator Op> class IData;
template<class TDataWriter> class StringResult;

/// <summary> Base class for all BindState node types </summary>
//!
//! \warning BindState MAY only be used on changing structures if the referenced parts can always be dereferenced with the guarantee it was not deleted (by using QSharedPointer for instance)
//! \n\b Responsibilities:
//! - Point to a place in the data tree (\ref dataPath)
//! - Store current level information error tree, parent, attachmentsDir, currentness (\ref dataPathAttDir, \ref dataPathErrSys)
template<Operator Op>
class BindState
{
    Q_DISABLE_COPY(BindState)
public:
    virtual ~BindState();

    /// <summary> Returns !isOk() </summary>
    bool operator!() const;

    /// <summary> Returns true if the BindState is current and has a concrete implementation </summary>
    bool isOk() const;

    /// <summary> Returns the depth from the concrete implementation root </summary>
    unsigned int level() const;

    /// <summary> Returns a string representation of the current path in the data </summary>
    virtual QString toString() const = 0;

    /// <summary> Returns the parent path </summary>
    virtual QSharedPointer<const BindState> parent() const;

    /// <summary> Set the attachments directory </summary>
    void setAttachmentsDir(QString p_dir);

    /// <summary> Get the attachments directory </summary>
    QString getAttachmentsDir() const;

    /// <summary> Get the BindErrors if any </summary>
    virtual BindErrors* errors() const;

    /// <summary> Add \p p_error at current path given by toString() </summary>
    virtual void addError(const BindError& p_error) const;

    ///// <summary> Traps all BindErrors happening within this State to p_errors or forward them to parent() if p_errors is null </summary>
    virtual void trapErrors(BindErrors* p_errors);

protected:
    BindState(IData<Op>* impl, QSharedPointer<BindState<Op>> parent);

    // used to call setCurrent on the top level path
    friend Itm<Result<Reader>> IItem<Reader>::read(BindErrors *);
    friend Itm<Result<Writer>> IItem<Writer>::write(BindErrors *);
    template<class TDataWriter> friend class StringResult;

    // make the path current
    bool setCurrent(bool c = true);

    // if current, give its currentness to \p c
    void makeCurrent(QSharedPointer<BindState> c);

    // make its parent current
    virtual void out();

    // convenient method that check isOk() and dynamic_cast the impl
    template<class T> T* getCurrent() const;

    QSharedPointer<BindState<Op>>  m_parent;
    IData<Op>*                     m_impl;
    bool                           m_isCurrent; //!< step of the walk (the only one that can step to another IData*)
    QString                        m_attachmentsDir;
    mutable BindErrors*            m_errors = nullptr;
};

template<Operator Op> class SeqState;
template<Operator Op> class RecState;

template<Operator Op>
class ItmState : public BindState<Op>, public QEnableSharedFromThis<ItmState<Op>>
{
    Q_DISABLE_COPY(ItmState)
public:
    virtual ~ItmState();

    /// <summary> Returns parent()->toString() </summary>
    QString toString() const;

    ItmState<Op>(IItem<Op>* impl, QSharedPointer<BindState<Op>> parent);

    /// <summary> Get the BindErrors if any </summary>
    virtual BindErrors* errors() const;

private:
    // Only Itm<T_> can access ItmState<Op> navigation methods
    // Partial specification is not possible in friend declaration => Cannot limit friendship to same OperatorValue
    template<class T_> friend class Itm; // used by Itm<T_>::operator Itm<>()
    void                         out     ();
    QSharedPointer<SeqState<Op>> sequence();
    QSharedPointer<RecState<Op>> record  ();
    void                         null    ();
    bool                         isNull  ();

    void                                     text          (const char*);
    template<typename T>                void bind          (      T& d);
    template<typename T>                void bind          (const T& d);
    template<typename T, unsigned Size> void bind          (const T(&d)[Size]);
};

extern template MODMED_API ItmState<Reader>::~ItmState();
extern template MODMED_API ItmState<Writer>::~ItmState();

template<Operator Op>
class SeqState : public BindState < Op >, public QEnableSharedFromThis<SeqState < Op > >
{
    Q_DISABLE_COPY(SeqState)
public:
    virtual ~SeqState();

    /// <summary> Returns true until we add an element in the sequence </summary>
    bool isFirst() const;

    /// <summary> Returns the index of the current element </summary>
    int currentIndex() const;

    /// <summary> Returns parent()->toString() + "[" + currentIndex() </summary>
    QString toString() const;

    /// <summary> Get the BindErrors if any </summary>
    virtual BindErrors* errors() const;

private:
    // Only ItmState<Op> can create a new SeqState<Op>
    friend class ItmState<Op>;
    SeqState<Op>(ISequence<Op>* impl, QSharedPointer<BindState<Op>> parent);

    // Only Seq<T_> can access SeqState<Op> navigation methods
    // Partial specification is not possible in friend declaration => Cannot limit friendship to same OperatorValue
    template<class T_> friend class Seq; // used by Itm<T_>::operator Itm<>()
    template<class T_> friend class BindableSeq; // used by Itm<T_>::operator Itm<>()
    void                         out();
    QSharedPointer<ItmState<Op>> item();

    int m_idx;
};

template<Operator Op>
class RecState : public BindState < Op >, public QEnableSharedFromThis<RecState < Op > >
{
    Q_DISABLE_COPY(RecState)
public:
    virtual ~RecState();

    /// <summary> Returns true until we add a key in the record </summary>
    bool isFirst() const;

    /// <summary> Returns the read/write position of the current element </summary>
    int currentIndex() const;

    /// <summary> Returns the key of the current element </summary>
    QString currentKey  () const;

    /// <summary> Returns parent()->toString() + "{" + currentKey() </summary>
    QString toString    () const;

    /// <summary> Get the BindErrors if any </summary>
    virtual BindErrors* errors() const;

private:
    // Only ItmState<Op> can create a new RecState<Op>
    friend class ItmState<Op>;
    RecState<Op>(IRecord<Op>* impl, QSharedPointer<BindState<Op>> parent);

    // Only Rec<T_> can access RecState<Op> navigation methods
    // Partial specification is not possible in friend declaration => Cannot limit friendship to same OperatorValue
    template<class T_> friend class Rec; // used by Itm<T_>::operator Itm<>()
    template<class T_> friend class BindableRec; // used by Itm<T_>::operator Itm<>()
    void                         out();
    QSharedPointer<ItmState<Op>> item(      QString& key);
    QSharedPointer<ItmState<Op>> item(const QString& key);

    QVector<QString> m_keys;
};

} // data
} // modmed

#include "BindState.hpp"

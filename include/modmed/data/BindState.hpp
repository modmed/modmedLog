/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <typeinfo>
#include <type_traits>
#include <limits>

#include <modmed/data/BindError.h>
#include <modmed/data/Data.h>

namespace modmed {
namespace data {

template <Operator Op, typename T>
struct BindDispatch
{
    static void bind(IItem<Op>*, QSharedPointer<ItmState<Op>> state, T& d)
    { Bind<Op, T>::bind(Item<Op>(state), d); }
};

template <Operator Op>
struct BindDispatch<Op, QString>
{
    static void bind(IItem<Op>* i, QSharedPointer<ItmState<Op>> state, QString& txt)
    {
        if (!i->bindText(*state, txt))
        {
            state->addError(FailBindText(Op == Reader, txt));
        }
    }
};

template<> struct MODMED_API BindDispatch<Reader, QDateTime>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, QDateTime& b); };
template<> struct MODMED_API BindDispatch<Writer, QDateTime>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, QDateTime& b); };

template <Operator Op>
struct BindDispatch<Op, QByteArray>
{
    static void bind(IItem<Op>* i, QSharedPointer<ItmState<Op>> state, QByteArray& b)
    {
        if (!i->bindBytes(*state, b))
        {
            state->addError(FailBind(QString("Fail to ") + (Op == Reader ? "read" : "write") + " a bytes", Op == Reader));
        }
    }
};

template<> struct MODMED_API BindDispatch<Reader, bool>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, bool& b); };
template<> struct MODMED_API BindDispatch<Writer, bool>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, bool& b); };
template<> struct MODMED_API BindDispatch<Reader, char>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, char& b); };
template<> struct MODMED_API BindDispatch<Writer, char>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, char& b); };
template<> struct MODMED_API BindDispatch<Reader, unsigned char>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned char& b); };
template<> struct MODMED_API BindDispatch<Writer, unsigned char>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned char& b); };
template<> struct MODMED_API BindDispatch<Reader, short>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, short& b); };
template<> struct MODMED_API BindDispatch<Writer, short>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, short& b); };
template<> struct MODMED_API BindDispatch<Reader, unsigned short>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned short& b); };
template<> struct MODMED_API BindDispatch<Writer, unsigned short>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned short& b); };
template<> struct MODMED_API BindDispatch<Reader, int>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, int& b); };
template<> struct MODMED_API BindDispatch<Writer, int>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, int& b); };
template<> struct MODMED_API BindDispatch<Reader, unsigned int>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned int& b); };
template<> struct MODMED_API BindDispatch<Writer, unsigned int>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned int& b); };
template<> struct MODMED_API BindDispatch<Reader, long>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, long& b); };
template<> struct MODMED_API BindDispatch<Writer, long>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, long& b); };
template<> struct MODMED_API BindDispatch<Reader, unsigned long>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned long& b); };
template<> struct MODMED_API BindDispatch<Writer, unsigned long>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned long& b); };
template<> struct MODMED_API BindDispatch<Reader, long long>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, long long& b); };
template<> struct MODMED_API BindDispatch<Writer, long long>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, long long& b); };
template<> struct MODMED_API BindDispatch<Reader, unsigned long long>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned long long& b); };
template<> struct MODMED_API BindDispatch<Writer, unsigned long long>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned long long& b); };
template<> struct MODMED_API BindDispatch<Reader, float>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, float& b); };
template<> struct MODMED_API BindDispatch<Writer, float>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, float& b); };
template<> struct MODMED_API BindDispatch<Reader, double>
{ static void bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, double& b); };
template<> struct MODMED_API BindDispatch<Writer, double>
{ static void bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, double& b); };

//==============================================================================
template<Operator Op>
template<class T> T* BindState<Op>::getCurrent() const
{ return !isOk() ? nullptr : static_cast<T*>(m_impl); }

//==============================================================================
template<>
template<typename T>
void ItmState<Reader>::bind(T& d)
{
    IItem<Reader>* i = this->template getCurrent<IItem<Reader>>();
    if (i)
    { BindDispatch<Reader, T>::bind(i, sharedFromThis(), d); }
    out();
}

//==============================================================================
template<>
template<typename T>
void ItmState<Writer>::bind(T& d)
{
    IItem<Writer>* i = this->template getCurrent<IItem<Writer>>();
    if (i)
    { BindDispatch<Writer, T>::bind(i, sharedFromThis(), d); }
    out();
}

//==============================================================================
template<>
template<typename T>
void ItmState<Reader>::bind(const T&)
{
    static_assert(std::is_void<T>::value, "Writing to a const T& is not possible. See T type below:");
}

//==============================================================================
template<>
template<typename T>
void ItmState<Writer>::bind(const T& d)
{
    bind(const_cast<T&>(d));
}

//==============================================================================
template<>
template<typename T, unsigned Size>
void ItmState<Reader>::bind(const T(&)[Size])
{
    static_assert(std::is_void<T>::value, "Writing to a const T& is not possible. See T type below:");
}

//==============================================================================
template<>
template<typename T, unsigned Size>
void ItmState<Writer>::bind(const T(&d)[Size])
{
    bind(const_cast<T(&)[Size]>(d));
}

//==============================================================================
template<Operator Op>
void BindState<Op>::addError(const BindError &p_error) const
{
    if (m_errors)
        m_errors->insert(toString(), p_error.clone());
}

//==============================================================================
template<Operator Op>
void BindState<Op>::trapErrors(BindErrors* p_errors)
{
    m_errors = p_errors;
}

//==============================================================================
template<Operator Op>
BindState<Op>::BindState(IData<Op>* impl, QSharedPointer<BindState<Op>> parent) :
  m_parent(parent),
  m_impl(impl),
  m_isCurrent(false)
{
  if (m_parent)
    m_errors = m_parent->m_errors;
}

//==============================================================================
template<Operator Op>
BindState<Op>::~BindState() = default;

//==============================================================================
template<Operator Op>
bool BindState<Op>::setCurrent(bool c) { return m_isCurrent = c; }

//==============================================================================
template<Operator Op>
void BindState<Op>::setAttachmentsDir(QString p_dir) { m_attachmentsDir = p_dir; }

//==============================================================================
template<Operator Op>
QString BindState<Op>::getAttachmentsDir() const
{
    if (!m_attachmentsDir.isNull() || !m_parent)
    {
        return m_attachmentsDir;
    }
    return  m_parent->getAttachmentsDir();
}

//==============================================================================
template<Operator Op>
void BindState<Op>::out() { makeCurrent(m_parent); }

//==============================================================================
template<Operator Op>
QSharedPointer<const BindState<Op>> BindState<Op>::parent() const
{ return m_parent.template dynamicCast<const BindState<Op>>(); }

//==============================================================================
template<Operator Op>
bool BindState<Op>::operator!() const { return !isOk(); }

//==============================================================================
template<Operator Op>
bool BindState<Op>::isOk() const { return m_isCurrent && m_impl; }

//==============================================================================
template<Operator Op>
unsigned int BindState<Op>::level() const
{
    unsigned int level = 0;
    for (QSharedPointer<const BindState> p = m_parent; p; p = p->m_parent)
    {
        if (dynamic_cast<IItem<Op>*>(p->m_impl)) { level++; }
    }
    return level;
}

//==============================================================================
template<Operator Op>
void BindState<Op>::makeCurrent(QSharedPointer<BindState> c)
{
    if (m_isCurrent && (!c || c->m_impl))
    {
        m_isCurrent = false;
        if (c) { c->setCurrent(); }
    }
}

//==============================================================================
template<Operator Op>
ItmState<Op>::ItmState(IItem<Op>* impl, QSharedPointer<BindState<Op>> parent)
    : BindState<Op>(impl, parent)
{
}

//==============================================================================
template<Operator Op>
SeqState<Op>::SeqState(ISequence<Op>* impl, QSharedPointer<BindState<Op>> parent)
    : BindState<Op>(impl, parent)
    , m_idx(0)
{
}

//==============================================================================
template<Operator Op>
RecState<Op>::RecState(IRecord<Op>* impl, QSharedPointer<BindState<Op>> parent)
    : BindState<Op>(impl, parent)
    , m_keys()
{
}

//==============================================================================
template<Operator Op>
SeqState<Op>::~SeqState() { out(); }

//==============================================================================
template<Operator Op>
RecState<Op>::~RecState() { out(); }

//==============================================================================
template<Operator Op>
QSharedPointer<SeqState<Op>> ItmState<Op>::sequence()
{
    IItem<Op>* i = this->template getCurrent<IItem<Op>>();
    ISequence<Op>* is = i ? i->sequence(*this) : nullptr;
    if (!is) { this->addError(FailSequence(Op == Reader)); }

    QSharedPointer<SeqState<Op>> s(new SeqState<Op>(is, this->m_parent));
    s->m_errors = this->m_errors;
    s->setAttachmentsDir(this->getAttachmentsDir());
    this->makeCurrent(s);
    return s;
}

//==============================================================================
template<Operator Op>
QSharedPointer<RecState<Op>> ItmState<Op>::record()
{
    IItem<Op>* i = this->template getCurrent<IItem<Op>>();
    IRecord<Op>* ir = i ? i->record(*this) : nullptr;
    if (!ir) { this->addError(FailRecord(Op == Reader)); }

    QSharedPointer<RecState<Op>> r(new RecState<Op>(ir, this->m_parent));
    r->m_errors = this->m_errors;
    r->setAttachmentsDir(this->getAttachmentsDir());
    this->makeCurrent(r);
    return r;
}

//==============================================================================
template<Operator Op>
QSharedPointer<ItmState<Op>> SeqState<Op>::item()
{
    ISequence<Op>* s = this->template getCurrent<ISequence<Op>>();
    IItem<Op>* id = s ? s->sequenceItem(*this) : nullptr;
    if (!id && !(Op == Reader)) { BindState<Op>::addError(FailSequenceItem(Op == Reader, m_idx)); }
    if (s) { m_idx++; }
    QSharedPointer<ItmState<Op>> d(new ItmState<Op>(id, this->sharedFromThis()));
    this->makeCurrent(d);
    return d;
}

//==============================================================================
template<Operator Op>
QSharedPointer<ItmState<Op>> RecState<Op>::item(QString& key)
{
    if (!(Op == Reader)) { return item(const_cast<const QString&>(key)); }
    IRecord<Op>* r = this->template getCurrent<IRecord<Op>>();
    IItem<Op>* id = r ? r->recordItem(*this, key) : nullptr;
    if (!id && !(Op == Reader)) { BindState<Op>::addError(FailRecordItem(Op == Reader, key)); }
    if (r) { m_keys.push_back(key); }
    QSharedPointer<ItmState<Op>> d(new ItmState<Op>(id, this->sharedFromThis()));
    this->makeCurrent(d);
    return d;
}

//==============================================================================
template<Operator Op>
QSharedPointer<ItmState<Op>> RecState<Op>::item(const QString& key)
{
    IItem<Op>* id = nullptr;
    if (key.isEmpty())
    {
        BindState<Op>::addError(FailRecordItemEmptyKey(Op == Reader));
    }
    else
    {
        IRecord<Op>* r = this->template getCurrent<IRecord<Op>>();
        QString readKey(key);
        id = r ? r->recordItem(*this, readKey) : nullptr;
        if (!id) { BindState<Op>::addError(FailRecordItem(Op == Reader, key)); }
        else if (Op == Reader && key != readKey)
        {
            BindState<Op>::addError(FailRecordItemWrongKey(key, readKey));
            id = nullptr;
        }
        if (r) { m_keys.push_back(key); }
    }
    QSharedPointer<ItmState<Op>> d(new ItmState<Op>(id, this->sharedFromThis()));
    this->makeCurrent(d);
    return d;
}

//==============================================================================
template<Operator Op>
void ItmState<Op>::text(const char* var)
{
    IItem<Op>* t = this->template getCurrent<IItem<Op>>();
    if (t)
    {
        QString v(var);
        if (!t->text(*this, v))
        {
            this->addError(FailText(Op == Reader, v));
        }
    }
    out();
}

//==============================================================================
template<Operator Op>
void ItmState<Op>::null()
{
    IItem<Op>* i = this->template getCurrent<IItem<Op>>();
    if (i)
    {
        if (!i->null(*this))
        {
            this->addError(FailNull(Op == Reader));
        }
    }
}

//==============================================================================
template<Operator Op>
bool ItmState<Op>::isNull()
{
    IItem<Op>* i = this->template getCurrent<IItem<Op>>();
    if (i)
    {
        Q_ASSERT(Op == Reader);
        if (Op == Reader)
        { return i->null(*this); }
    }
    return false;
}

//==============================================================================
template<Operator Op>
void ItmState<Op>::out()
{
    BindState<Op>::out();
}

//==============================================================================
template<Operator Op>
void SeqState<Op>::out()
{
    ISequence<Op>* s = this->template getCurrent<ISequence<Op>>();
    if (s)
    {
        if (!s->sequenceOut(*this))
        {
            this->addError(FailSequenceOut(Op == Reader));
        }
    }
    BindState<Op>::out();
}

//==============================================================================
template<Operator Op>
void RecState<Op>::out()
{
    IRecord<Op>* r = this->template getCurrent<IRecord<Op>>();
    if (r)
    {
        if (!r->recordOut(*this))
        {
            this->addError(FailRecordOut(Op == Reader));
        }
    }
    BindState<Op>::out();
}

//==============================================================================
template<Operator Op>
bool SeqState<Op>::isFirst () const { return currentIndex() == 0; }

//==============================================================================
template<Operator Op>
bool RecState<Op>::isFirst () const { return currentIndex() == 0; }

//==============================================================================
template<Operator Op>
int SeqState<Op>::currentIndex() const
{
    return m_idx;
}

//==============================================================================
template<Operator Op>
int RecState<Op>::currentIndex() const
{
    return m_keys.size();
}

//==============================================================================
template<Operator Op>
QString RecState<Op>::currentKey () const
{
    return m_keys.size() < 1 ? QString() : m_keys.back();
}

//==============================================================================
template<Operator Op>
QString RecState<Op>::toString() const { return (this->parent()?this->parent()->toString():QString("root"))+QChar('{')+currentKey  (); }

//==============================================================================
template<Operator Op>
QString SeqState<Op>::toString() const { return (this->parent()?this->parent()->toString():QString("root"))+QChar('[')+QString::number(currentIndex()); }

//==============================================================================
template<Operator Op>
QString ItmState<Op>::toString() const { return (this->parent()?this->parent()->toString():QString("root")); }

//==============================================================================
template<Operator Op>
BindErrors* BindState<Op>::errors() const
{
    return m_errors;
}

template<Operator Op>
BindErrors* ItmState<Op>::errors() const
{
    return BindState<Op>::errors();
}

template<Operator Op>
BindErrors* SeqState<Op>::errors() const
{
    return BindState<Op>::errors();
}

template<Operator Op>
BindErrors* RecState<Op>::errors() const
{
    return BindState<Op>::errors();
}

} // data
} // modmed

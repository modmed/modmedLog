/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <modmed/data/Data.h>

#include <modmed/modmed_global.h>
#include <QtCore/qstringlist.h>
#include <QtCore/qscopedpointer.h>

namespace modmed {
namespace data {

/// <summary> Interface for classes that can be bound with any Item </summary>
class MODMED_API IBindable
{
public:
    virtual ~IBindable();
    virtual void bind(Item<Writer> data) = 0;
};

template<typename T>
class BindableRef : public modmed::data::IBindable
{
    //! \warning DO NOT change inheritance or add a virtual to keep \b reinterpret_cast<IBindable*>(this) safe!
public:
    /// <summary> Constructor from T </summary>
    BindableRef(const T& p_data);

    /// <summary> IBindable::bind specialization that calls Bind<T>::bind(p_data, m_data) </summary>
    virtual void bind(modmed::data::Item<modmed::data::Writer> p_data);
protected:
    const T& m_data;
};

/// <summary> IBindable copy of T that can be bound any time later </summary>
//! \n\b Responsibility: Encapsulate a bind object with its bind function for later bind call
template<typename T>
class BindableCopy : public IBindable
{
    //! \warning DO NOT change inheritance or add a virtual to keep \b reinterpret_cast<IBindable*>(this) safe!
public:
    /// <summary> Constructor from T if it is not a c-array </summary>
    template<typename T_ = T,
              class = typename std::enable_if<!std::is_array<T_>::value>::type>
    BindableCopy(const T& p_data);

    /// <summary> Constructor from T if it is a c-array </summary>
    template<unsigned Size,
              typename T_ = T,
              class = typename std::enable_if<std::is_array<T_>::value>::type>
    BindableCopy(const typename std::remove_all_extents<T>::type (&p_data)[Size]);

    /// <summary> IBindable::bind specialization that calls Bind<T>::bind(p_data, m_data) </summary>
    virtual void bind(Item<Writer> p_data);
protected:
    T m_data;
};

template<>
class BindableRef<const char*> : public modmed::data::IBindable
{
    //! \warning DO NOT change inheritance or add a virtual to keep \b reinterpret_cast<IBindable*>(this) safe!
public:
    /// <summary> Constructor from T </summary>
    BindableRef(const char* p_data);

    /// <summary> IBindable::bind specialization that calls Bind<T>::bind(p_data, m_data) </summary>
    virtual void bind(modmed::data::Item<modmed::data::Writer> p_data);
protected:
    const char* m_data;
};

/// <summary> IBindable copy of T that can be bound any time later </summary>
//! \n\b Responsibility: Encapsulate a bind object with its bind function for later bind call
template<>
class BindableCopy<const char*> : public IBindable
{
    //! \warning DO NOT change inheritance or add a virtual to keep \b reinterpret_cast<IBindable*>(this) safe!
public:
    /// <summary> Constructor from T if it is not a c-array </summary>
    BindableCopy(const char* p_data);

    /// <summary> IBindable::bind specialization that calls Bind<T>::bind(p_data, m_data) </summary>
    virtual void bind(Item<Writer> p_data);
protected:
    QScopedArrayPointer<char> m_data;
};

class BindableData;
class BindableSequence;
class BindableRecord;

class MODMED_API BindableResult
{
public:
    static const Operator OperatorValue = Writer;
    using TResult = BindableResult;
    using TItem = BindableData*;
    using TSequence = BindableSequence*;
    using TRecord = BindableRecord*;
};

enum BindableMode
{
  BindableModeCopy = 0, // default
  BindableModeRef = 1
};

// ************** Bindable Containers **************
class MODMED_API BindableData : public IBindable
{
public:

    /// <summary> Constructor </summary>
    BindableData(BindableMode p_mode = BindableModeCopy);

    /// <summary> Copy constructor </summary>
    BindableData(const BindableData&);

    /// <summary> Assignment operator </summary>
    BindableData& operator=(const BindableData&);

    Itm<BindableResult> write();

    /// <summary> IBindable::bind specialization that binds the stored object </summary>
    void bind(data::Item<Writer> data); // IBindable implementation

private:
    // Only Itm<T_> can access BindableData navigation methods
    // Partial specification is not possible in friend declaration => Cannot limit friendship to same OperatorValue
    template<class T_> friend class Itm;

    /// <summary> Returns a new BindableSequence and store it </summary>
    BindableSequence* sequence();

    /// <summary> Returns a new BindableRecord and store it </summary>
    BindableRecord* record();

    /// <summary> Reset the stored BindableData </summary>
    void null();

    /// <summary> Copy and stores the p_txt text </summary>
    void text(const char* p_txt);

    /// <summary> Does nothing </summary>
    void out();

    /// <summary> Copy and stores the object \p p_var </summary>
    template<typename T>
    void bind(const T& p_var);

    // FIXME Need pimpl
    QScopedPointer<QSharedPointer<IBindable>> m_data;
    BindableMode m_mode;
};

class MODMED_API BindableSequence : public IBindable
{
public:
    /// <summary> IBindable::bind specialization that creates a sequence and binds all inserted items in it </summary>
    void bind(Item<Writer> data);

private:
    // Only Seq<T_> can access BindableSequence navigation methods
    // Partial specification is not possible in friend declaration => Cannot limit friendship to same OperatorValue
    template<class T_> friend class Seq;
    friend class BindableData;

    /// <summary> Constructor </summary>
    BindableSequence(BindableMode p_mode = BindableModeCopy);

    /// <summary> Add a new Result in the sequence </summary>
    BindableData* item();

    /// <summary> Does nothing </summary>
    void out();

    // FIXME Need pimpl
    QScopedPointer<QVector<BindableData>> m_data;
    BindableMode m_mode;
};

class MODMED_API BindableRecord: public IBindable
{
public:
    /// <summary> IBindable::bind specialization that creates a sequence and binds all inserted items in it </summary>
    void bind(Item<Writer> data);

private:
    // Only Rec<T_> can access BindableRecord navigation methods
    // Partial specification is not possible in friend declaration => Cannot limit friendship to same OperatorValue
    template<class T_> friend class Rec;
    friend class BindableData;

    /// <summary> Constructor </summary>
    BindableRecord(BindableMode p_mode = BindableModeCopy);

    /// <summary> Add a new Result in the sequence </summary>
    BindableData* item(const QString& p_key);

    /// <summary> Does nothing </summary>
    void out();

    // FIXME Need pimpl
    QScopedPointer<QVector<QPair<QString, BindableData>>> m_data;
    BindableMode m_mode;
};

} // data
} // modmed

#include "Bindable.hpp"

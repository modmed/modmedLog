/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/

#include <modmed/data/BindError.h>

namespace modmed {
namespace data {

// ************** Bindable Containers **************

template<class T>
BindableRef<T>::BindableRef(const T& p_data) : m_data(p_data) {}

template<class T>
void BindableRef<T>::bind(modmed::data::Item<modmed::data::Writer> p_data)
{
    p_data.bind(const_cast<T&>(m_data));
}

//==============================================================================
template<typename T>
template<typename T_, class>
BindableCopy<T>::BindableCopy(const T& p_data) : m_data(p_data) {}

//==============================================================================
template<typename T>
template<unsigned Size, typename T_, class>
BindableCopy<T>::BindableCopy(const typename std::remove_all_extents<T>::type (&p_data)[Size])
{
    for (unsigned i = 0; i < Size; ++i)
    { m_data[i] = p_data[i]; }
}

//==============================================================================
template<typename T>
void BindableCopy<T>::bind(Item<Writer> p_data)
{ p_data.bind(m_data); }

//==============================================================================
template<typename T>
void BindableData::bind(const T& var)
{
  if (m_mode == BindableModeCopy)
    m_data->reset(new BindableCopy<typename std::remove_const<T>::type>(var)); // std::remove_const needed by MSVC
  else if (m_mode == BindableModeRef)
    m_data->reset(new BindableRef<typename std::remove_const<T>::type>(var)); // std::remove_const needed by MSVC
}

} // data
} // modmed

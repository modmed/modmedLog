/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qscopedpointer.h>
#include <QtCore/qiodevice.h>
#include <QtCore/qstring.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>

#include <modmed/modmed_global.h>

namespace modmed {
namespace data {

struct CborWriterPrivate;

/// <summary> Serialize using the CBOR format </summary>
//!
//! - Sequence  is represented by CBOR indefinite-length array       (major type 4)
//! - Record    is represented by CBOR indefinite-length map         (major type 5)
//! - Message   is represented by CBOR text string                   (major type 3)
//! - Null      is represented by CBOR value 22 (Null)               (major type 7)
//! - Boolean   is represented by CBOR value 20 (False) or 21 (True) (major type 7)
//! - Integer   is represented by CBOR integer (major type 0 or 1 depending on sign with appropriate 5-bit value followed by appropriate integer type)
//! - Decimal   is represented by CBOR double precision float (major type 7 with 5-bit value 27 followed by double)
//! - Timestamp is represented by CBOR tag 0 (major type 6) followed by definite-length text string (major type 3)
//! - \todo Bytes is represented by CBOR byte string (major type 2)
//!
//! \b Responsibility: Read and write data structures using the CBOR format \see http://cbor.io/spec.html
//!
class MODMED_API CborWriter : public virtual IItem<Writer>, public virtual ISequence<Writer>, public virtual IRecord<Writer>
{
    Q_DISABLE_COPY(CborWriter)
public:
    /// <summary> Construct an CBOR serializer or deserializer from a QIODevice </summary>
    /// <param name="p_s"> QIODevice that is read or written </param>
    /// <param name="p_documentInformation"> CBOR preamble that is passed to the #document method </param>
    CborWriter(QIODevice* p_s, const QString& p_documentInformation = QString());

    /// <summary> Destructor </summary>
    virtual ~CborWriter();

    /// <summary> Returns true if the internal QIODevice::isValid method returns \todo ??? </summary>
    bool isValid();

    /// <summary> Write a '[' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    ISequence<Writer>* sequence(const ItmState<Writer>&);

    /// <summary> Write a '{' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IRecord<Writer>* record(const ItmState<Writer>&);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, qlonglong& p_numb);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, double& p_numb);

    /// <summary> Write the \p p_bool item </summary>
    //! \return False if \p p_bool cannot be write and true otherwise
    bool bindBoolean(const ItmState<Writer>&, bool& p_bool);

    /// <summary> Write the \p p_datetime item </summary>
    //! \return False if \p p_datetime cannot be write and true otherwise
    bool bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime);

    /// <summary> Write the \p p_bytes item </summary>
    //! \return False if \p p_bytes cannot be write and true otherwise
    bool bindBytes(const ItmState<Writer>&, QByteArray& p_bytes);

    /// <summary> Write a '\c null' token </summary>
    //! \return true if the write succeeded false otherwise
    bool null(const ItmState<Writer>&);

    /// <summary> Write a ']' token </summary>
    //! \return true if the write succeeded false otherwise
    bool sequenceOut(const SeqState<Writer>&);

    /// <summary> Write a ',' token if it is not the first item </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IItem<Writer>* sequenceItem(const SeqState<Writer>&);

    /// <summary> Write a '}' token </summary>
    //! \return true if the write succeeded false otherwise
    bool recordOut(const RecState<Writer>&);

    /// <summary> Write a ',' token if it is not the first item followed by \p p_key and the ':' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IItem<Writer>* recordItem(const RecState<Writer>&, QString& p_key);

    /// <summary> Write \p p_txt </summary>
    //! \return true if the write succeeded, false otherwise
    bool text(const ItmState<Writer>&, const QString& p_txt);

    /// <summary> Write \p p_dat </summary>
    //! \return true if the write succeeded, false otherwise
    bool bindText(const ItmState<Writer>&, QString& p_dat);

    /// <summary> Writes document information at the beginning of the stream </summary>
    //! \warning Does nothing if the underlying stream position is not 0
    //! \remark isDocument() allows checking that document information was written
    //!
    //! \pre !information.isNull()
    //! \pre !m_isDocument
    //! \pre m_stream.pos() == 0
    //!
    //! CBOR document information is added as { "document":information, "data":... } which means
    //! it can consist in any well-formed CBOR like:
    //! "log file"
    //!
    CborWriter&      document(const QString& information = QString("\"\""));

    /// <summary> Returns true if a call to #document succeeded </summary>
    bool       isDocument();

private:
    QScopedPointer<CborWriterPrivate> m_d;
};

} // data
} // modmed

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <memory>

#include <QtCore/qdebug.h>
#include <QtCore/qstring.h>

#include <modmed/data/IData.h>
#include <modmed/data/BindError.h>
#include <modmed/modmed_global.h>

namespace modmed {
namespace data {

// //////////////////////////////////////////////////////////////////////////
// Fluent interface classes

/// <summary> This class stores the final result of a Bind<Operator> </summary>
//! The user can test whether Result contains noError() or look for errors()
//!
//! \n\b Responsibility:
//! - Extend the life a the QSharedPointer<Error>
template<Operator Op>
class Result
{
public:
    static const Operator OperatorValue = Op;
    using TResult   = Result<Op>;
    using TItem     = QSharedPointer<ItmState<Op>>;
    using TSequence = QSharedPointer<SeqState<Op>>;
    using TRecord   = QSharedPointer<RecState<Op>>;

    /// <summary> Default constructor </summary>
    Result();

    /// <summary> Constructor taking the \p p_error that must be kept alive until Result destruction </summary>
    Result(BindErrors* p_error);

    /// <summary> Copy constructor </summary>
    Result(const Result& p_other);

    /// <summary> Assignment operator </summary>
    Result& operator=(const Result& p_other);

    /// <summary> Returns !errors() || errors().isEmpty() </summary>
    bool noError() const;

    /// <summary> Return the errors </summary>
    const BindErrors* errors() const;

private:
    BindErrors* m_errors;
};

template<class T_> class Seq;
template<class T_> class Rec;

/// <summary> Item that can be specialized into a sequence, a record or a message </summary>
template<class T_> class Itm
{
public:
    using TParent = T_;
    using TResult = typename T_::TResult;
    using TItem   = typename TResult::TItem;
    static const Operator OperatorValue = TResult::OperatorValue;

    Itm(TItem);
    Itm(TItem, const T_& parent);

    /// <summary> Conversion into a Itm<Result> with a dynamic context but a fluent interface that will not allow getting out of the current Itm<> </summary>
    operator Itm<TResult>();

    ~Itm();

    /// <summary> Specialize into a sequence </summary>
    Seq<T_> sequence();

    /// <summary> Specialize into a record </summary>
    Rec<T_> record();

    /// <summary> Specialize into a null </summary>
    T_ null();

    /// <summary> Specialize into a constant text value </summary>
    T_ text(const char* v);

    /// <summary> Binds d calling the appropriate Bind::bind(Item, T&amp; d) </summary>
#ifdef Q_CC_MSVC
    template<typename T, class = typename std::enable_if<OperatorValue == Reader>::type>
#else
    template<typename T, Operator O = OperatorValue, class = typename std::enable_if<O == Reader>::type>
#endif
    T_ bind(T& d);

    /// <summary> Binds d calling the appropriate Bind::bind(Item, T&amp; d) </summary>
#ifdef Q_CC_MSVC
    template<typename T, class = typename std::enable_if<OperatorValue == Writer>::type>
#else
    template<typename T, Operator O = OperatorValue, class = typename std::enable_if<O == Writer>::type>
#endif
    T_ bind(const T& d);

    /// <summary> Returns true if successfully read a null item, returns false in any other case. </summary>
#ifdef Q_CC_MSVC
    template<class = typename std::enable_if<OperatorValue == Reader>::type>
#else
    template<Operator O = OperatorValue, class = typename std::enable_if<O == Reader>::type>
#endif
    bool isNull();

    /// <summary> Returns the manipulated state </summary>
    TItem state() const; // CR Rename operator* as iterators? (+ operator->, operator)

private:
    /// <summary> PROTECTED since it would bypass all subsequent Record or Sequence items </summary>
    T_ out();

    TItem m_data;
    T_ m_parent;
};

/// <summary> Top level unspecialized data out method returns a Result<> that keeps the error tree alive so the user can check it </summary>
template<> Result<Reader> MODMED_API Itm<Result<Reader>>::out();
template<> Result<Writer> MODMED_API Itm<Result<Writer>>::out();

/// <summary> Result that represent a sequence </summary>
template<class T_> class Seq
{
public:
    using TParent = T_;
    using TResult = typename T_::TResult;
    using TSequence = typename TResult::TSequence;
    static const Operator OperatorValue = TResult::OperatorValue;

    Seq(TSequence);
    Seq(TSequence, const T_& parent);

    /// <summary> this operator makes all necessary call to out() to get the TResult </summary>
    operator TResult();

    /// <summary> Explicit TResult conversion call </summary>
    TResult end();

    ~Seq();

    /// <summary> Close the sequence </summary>
    T_ out();

    /// <summary> Add an unspecialized item in the sequence </summary>
    Itm<Seq<T_>> item();

    /// <summary> Returns item().sequence() </summary>
    Seq<Seq<T_>> sequence();

    /// <summary> Returns item().record() </summary>
    Rec<Seq<T_>> record();

    /// <summary> Returns item().null() </summary>
    Seq<T_> null();

    /// <summary> Returns item().text(v) </summary>
    Seq<T_> text(const char* v);

    /// <summary> Returns item().bind(d) </summary>
#ifdef Q_CC_MSVC
    template<typename T, class = typename std::enable_if<OperatorValue == Reader>::type>
#else
    template<typename T, Operator O = OperatorValue, class = typename std::enable_if<O == Reader>::type>
#endif
    Seq<T_> bind(T& d);

    /// <summary> Returns item().bind(d) </summary>
#ifdef Q_CC_MSVC
    template<typename T, class = typename std::enable_if<OperatorValue == Writer>::type>
#else
    template<typename T, Operator O = OperatorValue, class = typename std::enable_if<O == Writer>::type>
#endif
    Seq<T_> bind(const T& d);

    /// <summary> Returns the manipulated state </summary>
    TSequence state() const;

private:
    TSequence m_sequence;
    T_ m_parent;
};

/// <summary> Top level sequence out method returns a Result<> that keeps the error tree alive so the user can check it </summary>
template<> Result<Reader> MODMED_API Seq<Result<Reader>>::out();
template<> Result<Writer> MODMED_API Seq<Result<Writer>>::out();

/// <summary> Result that represent a record </summary>
template<class T_> class Rec
{
public:
    using TParent = T_;
    using TResult = typename T_::TResult;
    using TRecord = typename TResult::TRecord;
    static const Operator OperatorValue = TResult::OperatorValue;

    Rec(TRecord);
    Rec(TRecord, const T_& parent);

    /// <summary> this operator makes all necessary call to out() to get the TResult </summary>
    operator TResult();

    /// <summary> Explicit TResult conversion call </summary>
    TResult end();

    ~Rec();

    /// <summary> Close the record </summary>
    T_ out();

    /// <summary> Add an unspecialized item associated to \p key in the record </summary>
    Itm<Rec<T_>> item(QString& key);

    /// <summary> Add an unspecialized item associated to \p key in the record </summary>
    Itm<Rec<T_>> item(const QString& key);

    /// <summary> Returns item(key).sequence() </summary>
    Seq<Rec<T_>> sequence(const QString& key);

    /// <summary> Returns item(key).record() </summary>
    Rec<Rec<T_>> record(const QString& key);

    /// <summary> Returns item(key).null() </summary>
    Rec<T_> null(const QString& key);

    /// <summary> Returns item(key).text(v) </summary>
    Rec<T_> text(const QString& key, const char* v);

    /// <summary> Returns item(key).bind(d) </summary>
#ifdef Q_CC_MSVC
    template<typename T, class = typename std::enable_if<OperatorValue == Reader>::type>
#else
    template<typename T, Operator O = OperatorValue, class = typename std::enable_if<O == Reader>::type>
#endif
    Rec<T_> bind(const QString& key, T& d);

    /// <summary> Returns item(key).bind(d) </summary>
#ifdef Q_CC_MSVC
    template<typename T, class = typename std::enable_if<OperatorValue == Writer>::type>
#else
    template<typename T, Operator O = OperatorValue, class = typename std::enable_if<O == Writer>::type>
#endif
    Rec<T_> bind(const QString& key, const T& d);

    /// <summary> Returns the manipulated state </summary>
    TRecord state() const;

private:
    TRecord m_record;
    T_ m_parent;
};

/// <summary> Top level record out method returns a Result<> that keeps the error tree alive so the user can check it </summary>
template<> Result<Reader> MODMED_API Rec<Result<Reader>>::out();
template<> Result<Writer> MODMED_API Rec<Result<Writer>>::out();

/// <summary> Top level sequence </summary>
template<Operator Op> using Sequence = Seq<Result<Op>>;

/// <summary> Top level record </summary>
template<Operator Op> using Record = Rec<Result<Op>>;

} // data
} // modmed

#include "Data.hpp"

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <modmed/data/BindState.h>

namespace modmed {
namespace data {

//==============================================================================
template<Operator Op>
Result<Op>::Result() : m_errors(nullptr) {}

//==============================================================================
template<Operator Op>
Result<Op>::Result(BindErrors* p_errors) : m_errors(p_errors) {}

//==============================================================================
template<Operator Op>
Result<Op>::Result(const Result&) = default;

//==============================================================================
template<Operator Op>
Result<Op>& Result<Op>::operator=(const Result<Op>&) = default;

//==============================================================================
template<Operator Op>
const BindErrors* Result<Op>::errors() const
{ return m_errors; }

//==============================================================================
template<Operator Op>
bool Result<Op>::noError() const
{
    return !m_errors || m_errors->isEmpty();
}

// ************** Fluent Interface **************

//==============================================================================
template<class T_>
Itm<T_>::Itm(TItem data) :
    m_data(data),
    m_parent()
{}

//==============================================================================
template<class T_>
Itm<T_>::Itm(TItem data, const T_& parent) :
    m_data(data),
    m_parent(parent)
{
    Q_ASSERT(m_data);
    Q_ASSERT(static_cast<const void*>(&parent) != this);
}

//==============================================================================
template<class T_>
Itm<T_>::operator Itm<typename Itm<T_>::TResult>()
{
    Itm<TResult> d(m_data);
    return d;
}

//==============================================================================
template<class T_>
Itm<T_>::~Itm() = default;

//==============================================================================
template<class T_>
Seq<T_> Itm<T_>::sequence()
{
    return Seq<T_>(m_data->sequence(), m_parent);
}

//==============================================================================
template<class T_>
Rec<T_> Itm<T_>::record()
{
    return Rec<T_>(m_data->record(), m_parent);
}

//==============================================================================
template<class T_>
T_ Itm<T_>::null()
{
    m_data->null();
    return out();
}

//==============================================================================
template<class T_>
T_ Itm<T_>::text(const char* v)
{
    m_data->text(v);
    return out();
}

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<class> // std::enable_if<OperatorValue == Reader>>
#else
template<Operator, class> // std::enable_if<OperatorValue == Reader>>
#endif
bool Itm<T_>::isNull()
{ return m_data->isNull(); }

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<typename T, class> // std::enable_if<OperatorValue == Reader>>
#else
template<typename T, Operator, class> // std::enable_if<OperatorValue == Reader>>
#endif
T_ Itm<T_>::bind(T& d)
{
    m_data->bind(d);
    return out();
}

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<typename T, class> // std::enable_if<OperatorValue == Writer>>
#else
template<typename T, Operator, class> // std::enable_if<OperatorValue == Writer>>
#endif
T_ Itm<T_>::bind(const T& d)
{
    m_data->bind(d);
    return out();
}

//==============================================================================
template<class T_>
typename Itm<T_>::TItem Itm<T_>::state() const
{
    return m_data;
}

//==============================================================================
template<class T_>
T_ Itm<T_>::out()
{
    m_data->out();
    return m_parent;
}

//==============================================================================
template<class T_>
Seq<T_>::Seq(TSequence sequence) :
    m_sequence(sequence),
    m_parent()
{}

//==============================================================================
template<class T_>
Seq<T_>::Seq(TSequence sequence, const T_& parent) :
    m_sequence(sequence),
    m_parent(parent)
{
    Q_ASSERT(m_sequence);
    Q_ASSERT(static_cast<const void*>(&parent) != this);
}

//==============================================================================
template<class T_>
Seq<T_>::operator TResult()
{
    return out();
}

//==============================================================================
template<class T_>
typename Seq<T_>::TResult Seq<T_>::end()
{
    return *this;
}

//==============================================================================
template<class T_>
Seq<T_>::~Seq() = default;

//==============================================================================
template<class T_>
T_ Seq<T_>::out()
{
    m_sequence->out();
    return m_parent;
}

//==============================================================================
template<class T_>
Itm<Seq<T_>> Seq<T_>::item()
{
    return Itm<Seq<T_>>(m_sequence->item(), *this);
}

//==============================================================================
template<class T_>
Seq<Seq<T_>> Seq<T_>::sequence()
{
    return item().sequence();
}

//==============================================================================
template<class T_>
Rec<Seq<T_>> Seq<T_>::record()
{
    return item().record();
}

//==============================================================================
template<class T_>
Seq<T_> Seq<T_>::null()
{
    return item().null();
}

//==============================================================================
template<class T_>
Seq<T_> Seq<T_>::text(const char* txt)
{
    return item().text(txt);
}

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<typename T, class> // std::enable_if<OperatorValue == Reader>>
#else
template<typename T, Operator, class> // std::enable_if<OperatorValue == Reader>>
#endif
Seq<T_> Seq<T_>::bind(T& d)
{
    return item().bind(d);
}

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<typename T, class> // std::enable_if<OperatorValue == Writer>>
#else
template<typename T, Operator, class> // std::enable_if<OperatorValue == Writer>>
#endif
Seq<T_> Seq<T_>::bind(const T& d)
{
    return item().bind(d);
}

//==============================================================================
template<class T_>
typename Seq<T_>::TSequence Seq<T_>::state() const
{
    return m_sequence;
}

//==============================================================================
template<class T_>
Rec<T_>::Rec(TRecord record) :
    m_record(record),
    m_parent()
{}

//==============================================================================
template<class T_>
Rec<T_>::Rec(TRecord record, const T_& parent) :
    m_record(record),
    m_parent(parent)
{
    Q_ASSERT(m_record);
    Q_ASSERT(static_cast<const void*>(&parent) != this);
}

//==============================================================================
template<class T_>
Rec<T_>::operator TResult()
{
    return out();
}

//==============================================================================
template<class T_>
typename Rec<T_>::TResult Rec<T_>::end()
{
    return *this;
}

//==============================================================================
template<class T_>
Rec<T_>::~Rec() = default;

//==============================================================================
template<class T_>
T_ Rec<T_>::out()
{
    m_record->out();
    return m_parent;
}

//==============================================================================
template<class T_>
Itm<Rec<T_>> Rec<T_>::item(QString& key)
{
    return Itm<Rec<T_>>(m_record->item(key), *this);
}

//==============================================================================
template<class T_>
Itm<Rec<T_>> Rec<T_>::item(const QString& key)
{
    return Itm<Rec<T_>>(m_record->item(key), *this);
}

//==============================================================================
template<class T_>
Seq<Rec<T_>> Rec<T_>::sequence(const QString& key)
{
    return item(key).sequence();
}

//==============================================================================
template<class T_>
Rec<Rec<T_>> Rec<T_>::record(const QString& key)
{
    return item(key).record();
}

//==============================================================================
template<class T_>
Rec<T_> Rec<T_>::null(const QString& key)
{
    return item(key).null();
}

//==============================================================================
template<class T_>
Rec<T_> Rec<T_>::text(const QString& key, const char* txt)
{
    return item(key).text(txt);
}

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<typename T, class> // std::enable_if<OperatorValue == Reader>>
#else
template<typename T, Operator, class> // std::enable_if<OperatorValue == Reader>>
#endif
Rec<T_> Rec<T_>::bind(const QString& key, T& d)
{
    return item(key).bind(d);
}

//==============================================================================
template<class T_>
#ifdef Q_CC_MSVC
template<typename T, class> // std::enable_if<OperatorValue == Writer>>
#else
template<typename T, Operator, class> // std::enable_if<OperatorValue == Writer>>
#endif
Rec<T_> Rec<T_>::bind(const QString& key, const T& d)
{
    return item(key).bind(d);
}

//==============================================================================
template<class T_>
typename Rec<T_>::TRecord Rec<T_>::state() const
{
    return m_record;
}

} // data
} // modmed

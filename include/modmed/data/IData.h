/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qstring.h>
#include <QtCore/qdatetime.h>

#include <modmed/modmed_global.h>
#include <modmed/data/BindDeclaration.h>
#include <modmed/data/BindError.h>

namespace modmed {
namespace data {

enum Operator;

// Break an include cycle with Data.h

template<Operator Op> class Result;
template<class T_> class Itm;

// Break an include cycle with BindState.h

template<Operator Op> class BindState;
template<Operator Op> class ItmState;
template<Operator Op> class SeqState;
template<Operator Op> class RecState;
template<Operator Op> class MsgState;

/// <summary> Base interface for all physical data implementations that can be bound </summary>
//! \remark Operator Op may be used to support other bind operations in the future: : iterate, build, parse, format, filter, etc.
template<Operator Op>
class IData
{
public:
    virtual ~IData() {}
};

template<Operator Op> class ISequence;
template<Operator Op> class IRecord;
template<Operator Op> class IItem;

/// <summary> Policy that gives semantic to the choice of sequence(), record(), message() or null() </summary>
//! \remark ItmState takes responsibility for not calling this interface in illegal state w.r.t. to Item grammar
//! \warning IItem implementations must be very cautious and explicit on their side effects!
template<>
class MODMED_API IItem<Reader> : public IData<Reader>
{
public:
    virtual ~IItem() {}

    /// <summary> Read a sequence opening token </summary>
    //! \return Null if the read fails and a valid ISequence* otherwise
    //! \warning data will not delete the returned ISequence* the user
    //! must keep it alive at least until sequenceOut call
    virtual ISequence<Reader>* sequence(const ItmState<Reader>&) = 0;

    /// <summary> Read a record opening token </summary>
    //! \return Null if the read fails and a valid IRecord* otherwise
    //! \warning data will not delete the returned IRecord* the user
    //! must keep it alive at least until recordOut call
    virtual IRecord<Reader>* record(const ItmState<Reader>&) = 0;

    /// <summary> Read/Write the \p expected constant text item </summary>
    //! \return False if \p expected cannot be read/write and true otherwise
    virtual bool text(const ItmState<Reader>&, const QString& expected) = 0;

    /// <summary> Read/Write the \p data text item </summary>
    //! \return False if \p data cannot be read/write and true otherwise
    virtual bool bindText(const ItmState<Reader>&, QString& data) = 0;

    // When bindNumber or bindBoolean are used, the implementation MAY choose optimized representations (like JSON number or sqlite numeric types)

    /// <summary> Read the \p integer item </summary>
    //! \return False if \p integer cannot be read and true otherwise
    virtual bool bindNumber(const ItmState<Reader>&, qlonglong& integer) = 0;

    /// <summary> Read the \p floatingPoint item </summary>
    //! \return False if \p floatingPoint cannot be read and true otherwise
    virtual bool bindNumber(const ItmState<Reader>&, double& floatingPoint) = 0;

    /// <summary> Read the \p boolean item </summary>
    //! \return False if \p boolean cannot be read and true otherwise
    virtual bool bindBoolean(const ItmState<Reader>&, bool& boolean) = 0;

    /// <summary> Read the \p datetime item </summary>
    //! \return False if \p datetime cannot be read and true otherwise
    virtual bool bindTimeStamp(const ItmState<Reader>&, QDateTime& datetime) = 0;

    /// <summary> Read the \p bytes item </summary>
    //! \return False if \p bytes cannot be read and true otherwise
    virtual bool bindBytes(const ItmState<Reader>&, QByteArray& bytes) = 0;

    /// <summary> Read a null item </summary>
    //! \return False if null cannot be read and true otherwise
    virtual bool null (const ItmState<Reader>&) = 0;

    /// <summary> Start reading </summary>
    /// This method returns the starting point of the fluent interface
    /// for the reading of the modmedLog.
    Item<Reader> read(BindErrors* p_errors = nullptr);
};

template<>
class MODMED_API IItem<Writer> : public IData<Writer>
{
public:
    virtual ~IItem() {}

    /// <summary> Write a sequence opening token </summary>
    //! \return Null if the write fails and a valid ISequence* otherwise
    //! \warning data will not delete the returned ISequence* the user
    //! must keep it alive at least until sequenceOut call
    virtual ISequence<Writer>* sequence(const ItmState<Writer>&) = 0;

    /// <summary> Write a record opening token </summary>
    //! \return Null if the write fails and a valid IRecord* otherwise
    //! \warning data will not delete the returned IRecord* the user
    //! must keep it alive at least until recordOut call
    virtual IRecord<Writer>* record (const ItmState<Writer>&) = 0;

    /// <summary> Read/Write the \p expected constant text item </summary>
    //! \return False if \p expected cannot be read/write and true otherwise
    virtual bool text(const ItmState<Writer>&, const QString& expected) = 0;

    /// <summary> Read/Write the \p data text item </summary>
    //! \return False if \p data cannot be read/write and true otherwise
    virtual bool bindText(const ItmState<Writer>&, QString& data) = 0;

    // When bindNumber or bindBoolean are used, the implementation MAY choose optimized representations (like JSON number or sqlite numeric types)

    /// <summary> Write the \p integer item </summary>
    //! \return False if \p integer cannot be written and true otherwise
    virtual bool bindNumber(const ItmState<Writer>&, qlonglong& integer) = 0;

    /// <summary> Write the \p floatingPoint item </summary>
    //! \return False if \p floatingPoint cannot be written and true otherwise
    virtual bool bindNumber(const ItmState<Writer>&, double& floatingPoint) = 0;

    /// <summary> Write the \p boolean item </summary>
    //! \return False if \p boolean cannot be written and true otherwise
    virtual bool bindBoolean(const ItmState<Writer>&, bool& boolean) = 0;

    /// <summary> Write the \p datetime item </summary>
    //! \return False if \p datetime cannot be written and true otherwise
    virtual bool bindTimeStamp(const ItmState<Writer>&, QDateTime& datetime) = 0;

    /// <summary> Read the \p bytes item </summary>
    //! \return False if \p bytes cannot be read and true otherwise
    virtual bool bindBytes(const ItmState<Writer>&, QByteArray& bytes) = 0;

    /// <summary> Write a null item </summary>
    //! \return False if null cannot be written and true otherwise
    virtual bool null (const ItmState<Writer>&) = 0;

    /// <summary> Start writing </summary>
    /// This method returns the starting point of the fluent interface
    /// for the writting of the modmedLog.
    Item<Writer> write(BindErrors* p_errors = nullptr);
};

/// <summary> Policy that gives semantic to the sequence of sequenceItem() or sequenceOut() </summary>
//! \remark SeqState<Op> takes responsibility for not calling this interface in illegal state w.r.t. to Item grammar
template<Operator Op>
class ISequence : public IData<Op>
{
public:
    virtual ~ISequence() {}

    /// <summary> Prepare to read/write next item </summary>
    //! \return Null if the step fails and a valid IItem* otherwise
    //! \warning data will not delete the returned IItem* the user
    //! must keep it alive at least until sequenceItem or sequenceOut call
    virtual IItem<Op>* sequenceItem(const SeqState<Op>&) = 0;

    /// <summary> Read/Write the sequence closing token </summary>
    //! \remark In reading mode, all the unread item are skipped
    //! \return False if the sequence closing token cannot be read/write and true otherwise
    virtual bool sequenceOut (const SeqState<Op>&) = 0;
};

/// <summary> Policy that gives semantic to the sequence of recordItem() or recordOut() </summary>
//! \remark RecState<Op> takes responsibility for not calling this interface in illegal state w.r.t. to Item grammar
template<Operator Op>
class IRecord : public IData<Op>
{
public:
    virtual ~IRecord() {}

    /// <summary> Read/Write next item key, store it in \p key and prepare to read/write the associated item </summary>
    //! \return Null if the step fails and a valid IItem* otherwise
    //! \warning data will not delete the returned IItem* the user
    //! must keep it alive at least until sequenceItem or sequenceOut call
    virtual IItem<Op>* recordItem(const RecState<Op>&, QString& key) = 0;

    /// <summary> Read/Write the record closing token </summary>
    //! \remark In reading mode, all the unread item are skipped
    //! \return False if the record closing token cannot be read/write and true otherwise
    virtual bool recordOut (const RecState<Op>&) = 0;
};

} // data
} // modmed


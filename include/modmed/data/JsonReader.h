/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qscopedpointer.h>
#include <QtCore/qtextstream.h>
#include <QtCore/qstring.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>

#include <modmed/modmed_global.h>

namespace modmed {
namespace data {

struct JsonReaderPrivate;

/// <summary> Deserialize using the JSON format </summary>
//!
//! - Sequence is represented by items between '[' ']' separated by ','
//! - Record   is represented by items indexed by strings between '{' '}' separated by ','
//! - Null     is represented by the null item
//!
//! \b Responsibility: Read data structures using the Json format (\ref dataJson)
class MODMED_API JsonReader : public virtual IItem<Reader>, public virtual ISequence<Reader>, public virtual IRecord<Reader>
{
    Q_DISABLE_COPY(JsonReader)
public:
    /// <summary> Construct an JSON serializer or deserializer from a QTextStream </summary>
    /// <param name="p_s"> QTextStream that is read or written </param>
    /// <param name="p_documentInformation"> JSON preamble that is passed to the #document method </param>
    JsonReader(QTextStream* p_s, const QString& p_documentInformation = QString());

    /// <summary> Destructor </summary>
    virtual ~JsonReader();

    /// <summary> Call flush on the internal QTextStream </summary>
    void flush();

    /// <summary> Returns true if the internal QTextStream::isValid method returns QTextStream::Ok </summary>
    bool isValid();

    /// <summary> Read a '[' token </summary>
    //! \return \p this if the read succeeded nullptr otherwise
    ISequence<Reader>* sequence(const ItmState<Reader>&);

    /// <summary> Read a '{' token </summary>
    //! \return \p this if the read succeeded nullptr otherwise
    IRecord<Reader>* record(const ItmState<Reader>&);

    /// <summary> Read the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be read and true otherwise
    bool bindNumber(const ItmState<Reader>&, qlonglong& p_numb);

    /// <summary> Read the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be read and true otherwise
    bool bindNumber(const ItmState<Reader>&, double& p_numb);

    /// <summary> Read the \p p_bool constant boolean item </summary>
    //! \return False if \p p_bool cannot be read and true otherwise
    bool bindBoolean(const ItmState<Reader>&, bool& p_bool);

    /// <summary> Read the \p p_datetime constant datetime item </summary>
    //! \return False if \p p_datetime cannot be read and true otherwise
    bool bindTimeStamp(const ItmState<Reader>&, QDateTime& p_datetime);

    /// <summary> Write the \p p_bytes item </summary>
    //! \return False if \p p_bytes cannot be write and true otherwise
    bool bindBytes(const ItmState<Reader>&, QByteArray& p_bytes);

    /// <summary> Read \p p_txt </summary>
    //! \return true if the read succeeded and the read text is equal to
    //! \p p_txt, false otherwise \n
    bool text(const ItmState<Reader>&, const QString& p_txt);

    /// <summary> Read \p p_dat </summary>
    //! \return true if the read succeeded and stores the read text in
    //! \p p_dat, false otherwise \n
    bool bindText(const ItmState<Reader>&, QString& p_dat);

    /// <summary> Read a '\c null' token </summary>
    //! \return true if the read succeeded false otherwise
    bool null(const ItmState<Reader>&);

    /// <summary> Read a ']' token </summary>
    //! \return true if the read succeeded false otherwise
    bool sequenceOut(const SeqState<Reader>&);

    /// <summary> Read a ',' token if it is not the first item </summary>
    //! \return \p this if the read succeeded nullptr otherwise
    IItem<Reader>* sequenceItem(const SeqState<Reader>&);

    /// <summary> Read a '}' token </summary>
    //! \return true if the read succeeded false otherwise
    bool recordOut(const RecState<Reader>&);

    /// <summary> Read a ',' token if it is not the first item followed by \p p_key and the ':' token </summary>
    //! \return \p this if the read succeeded nullptr otherwise
    IItem<Reader>* recordItem(const RecState<Reader>&, QString& p_key);

    /// <summary> Writes document information at the beginning of the stream </summary>
    //! \warning Does nothing if the underlying stream position is not 0
    //! \remark isDocument() allows checking that document information was written
    //!
    //! \pre !information.isNull()
    //! \pre !m_isDocument
    //! \pre m_stream.pos() == 0
    //!
    //! JSON document information is added as { "document":information, "data":... } which means
    //! it can consist in any well-formed JSON like:
    //! "log file"
    //!
    JsonReader&      document(const QString& information = QString("\"\""));

    /// <summary> Returns true if a call to #document succeeded </summary>
    bool       isDocument();

private:
    QScopedPointer<JsonReaderPrivate> m_d;
};

} // data
} // modmed

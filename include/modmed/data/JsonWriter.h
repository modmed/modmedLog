/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qscopedpointer.h>
#include <QtCore/qtextstream.h>
#include <QtCore/qstring.h>
class QIODevice;
class QByteArray;

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>
#include <modmed/data/StringResult.h>

#include <modmed/modmed_global.h>

namespace modmed {
namespace data {

struct JsonWriterPrivate;

/// <summary> Serialize using the JSON format </summary>
//!
//! - Sequence is represented by items between '[' ']' separated by ','
//! - Record   is represented by items indexed by strings between '{' '}' separated by ','
//! - Null     is represented by the null item
//!
//! \b Responsibility: Read and write data structures using the Json format (\ref dataJson)
class MODMED_API JsonWriter : public virtual IItem<Writer>, public virtual ISequence<Writer>, public virtual IRecord<Writer>
{
    Q_DISABLE_COPY(JsonWriter)
public:
    static void escape(QTextStream& jsonStream, const QString& t);
    static void escape(QTextStream& jsonStream, unsigned ucs4);
    static void escapeUtf8(QIODevice &jsonUtf8, QByteArray utf8);

    /// <summary> Construct an JSON serializer from a QTextStream </summary>
    /// <param name="p_s"> QTextStream that is written </param>
    /// <param name="p_documentInformation"> JSON preamble that is passed to the #document method </param>
    /// <param name="p_maxIndentLevel"> maximum JSON nesting level that will be written on a new line and indented </param>
    /// <param name="p_maxIndentLevelSeparator"> if not '\0', the character is used to separate items at p_maxIndentLevel JSON nesting level </param>
    JsonWriter(QTextStream* p_s
               , const QString& p_documentInformation = QString()
               , unsigned short p_maxIndentLevel = 1
               , char p_maxIndentLevelSeparator = '\t');

    /// <summary> Construct an JSON serializer from a QIODevice </summary>
    /// <param name="p_io"> QIODevice where to write utf-8 JSON </param>
    /// <param name="p_documentInformation"> JSON preamble that is passed to the #document method </param>
    /// <param name="p_maxIndentLevel"> maximum JSON nesting level that will be written on a new line and indented </param>
    /// <param name="p_maxIndentLevelSeparator"> if not '\0', the character is used to separate items at p_maxIndentLevel JSON nesting level </param>
    JsonWriter(QIODevice* p_io
               , const QString& p_documentInformation = QString()
               , unsigned short p_maxIndentLevel = 1
               , char p_maxIndentLevelSeparator = '\t');

    /// <summary> Construct an JSON serializer from a File* </summary>
    /// <param name="p_file"> File handler that is read or written </param>
    /// <param name="p_documentInformation"> JSON preamble that is passed to the #document method </param>
    /// <param name="p_maxIndentLevel"> maximum JSON nesting level that will be written on a new line and indented </param>
    /// <param name="p_maxIndentLevelSeparator"> if not '\0', the character is used to separate items at p_maxIndentLevel JSON nesting level </param>
    JsonWriter(FILE* p_file
               , const QString& p_documentInformation = QString()
               , unsigned short p_maxIndentLevel = 1
               , char p_maxIndentLevelSeparator = '\t');

    /// <summary> Construct an JSON serializer from a QString </summary>
    /// <param name="p_string"> QString that is read or written </param>
    /// <param name="p_documentInformation"> JSON preamble that is passed to the #document method </param>
    /// <param name="p_maxIndentLevel"> maximum JSON nesting level that will be written on a new line and indented </param>
    /// <param name="p_maxIndentLevelSeparator"> if not '\0', the character is used to separate items at p_maxIndentLevel JSON nesting level </param>
    JsonWriter(QString* p_string
               , const QString& p_documentInformation = QString()
               , unsigned short p_maxIndentLevel = 1
               , char p_maxIndentLevelSeparator = '\t');

    /// <summary> Destructor </summary>
    virtual ~JsonWriter();

    /// <summary> Call flush on the internal QTextStream </summary>
    void flush();

    /// <summary> Returns true if the internal QTextStream::isValid method returns QTextStream::Ok </summary>
    bool isValid();

    /// <summary> Write a '[' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    ISequence<Writer>* sequence(const ItmState<Writer>&);

    /// <summary> Write a '{' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IRecord<Writer>* record(const ItmState<Writer>&);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, qlonglong& p_numb);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, double& p_numb);

    /// <summary> Write the \p p_bool item </summary>
    //! \return False if \p p_bool cannot be write and true otherwise
    bool bindBoolean(const ItmState<Writer>&, bool& p_bool);

    /// <summary> Write the \p p_datetime item </summary>
    //! \return False if \p p_datetime cannot be write and true otherwise
    bool bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime);

    /// <summary> Write the \p p_bytes item </summary>
    //! \return False if \p p_bytes cannot be write and true otherwise
    bool bindBytes(const ItmState<Writer>&, QByteArray& p_bytes);

    /// <summary> Write \p p_txt </summary>
    //! \return true if the write succeeded, false otherwise
    bool text(const ItmState<Writer>&, const QString& p_txt);

    /// <summary> Write \p p_dat </summary>
    //! \return true if the write succeeded, false otherwise
    bool bindText(const ItmState<Writer>&, QString& p_dat);

    /// <summary> Write a '\c null' token </summary>
    //! \return true if the write succeeded false otherwise
    bool null(const ItmState<Writer>&);

    /// <summary> Write a ']' token </summary>
    //! \return true if the write succeeded false otherwise
    bool sequenceOut(const SeqState<Writer>&);

    /// <summary> Write a ',' token if it is not the first item </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IItem<Writer>* sequenceItem(const SeqState<Writer>&);

    /// <summary> Write a '}' token </summary>
    //! \return true if the write succeeded false otherwise
    bool recordOut(const RecState<Writer>&);

    /// <summary> Write a ',' token if it is not the first item followed by \p p_key and the ':' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IItem<Writer>* recordItem(const RecState<Writer>&, QString& p_key);

    /// <summary> Writes document information at the beginning of the stream </summary>
    //! \warning Does nothing if the underlying stream position is not 0
    //! \remark isDocument() allows checking that document information was written
    //!
    //! \pre !information.isNull()
    //! \pre !m_isDocument
    //! \pre m_stream.pos() == 0
    //!
    //! JSON document information is added as { "document":information, "data":... } which means
    //! it can consist in any well-formed JSON like:
    //! "log file"
    //!
    JsonWriter&      document(const QString& information = QString("\"\""));

    /// <summary> Returns true if a call to #document succeeded </summary>
    bool       isDocument();

private:
    QScopedPointer<JsonWriterPrivate> m_d;
};

using JsonString = StringResult<JsonWriter>;

} // data
} // modmed

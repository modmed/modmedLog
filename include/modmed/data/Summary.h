/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qstring.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>

#include <modmed/modmed_global.h>

namespace modmed {
namespace data {

struct SummaryPrivate;

/// <summary> Serialize using a JSON like write only format </summary>
//!
//! - Sequence is represented by items between '[' ']' separated by ' | '
//! - Record   is represented by items indexed by strings between '{' '}' separated by ' | '
//! - Null     is represented by the null item
//!
//! \b Responsibility: Write data structures using a human readable format (\ref dataSummary)
class MODMED_API Summary : public virtual IItem<Writer>, public virtual ISequence<Writer>, public virtual IRecord<Writer>
{
    Q_DISABLE_COPY(Summary)
public:

    /// <summary> Construct a Summary serializer from a QIODevice </summary>
    /// <param name="p_ioDevice"> Device that is written </param>
    Summary(QIODevice* p_ioDevice);

    /// <summary> Construct a Summary serializer from a FILE* </summary>
    /// <param name="p_file"> File handler that is written </param>
    Summary(FILE* p_file);

    /// <summary> Construct a Summary serializer from a QString </summary>
    /// <param name="p_string"> QString that is written </param>
    Summary(QString* p_string);

    /// <summary> Construct a Summary serializer from a QTextStream* </summary>
    /// <param name="p_s"> QTextStream written </param>
    Summary(QTextStream* p_s);

    virtual ~Summary();

    /// <summary> Call flush on the internal QTextStream </summary>
    void flush();

    /// <summary> Returns true if the internal QTextStream::isValid method returns QTextStream::Ok </summary>
    bool isValid();

    /// <summary> Truncate the output after \p p_numberOfCompleteLevels levels, displaying \p p_numberOfItemsInLastLevel items in last level sequences/records </summary>
    void setLevelOfDetails(unsigned p_numberOfCompleteLevels, unsigned p_numberOfItemsInLastLevel);

    /// <summary> Write a '[' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    ISequence<Writer>* sequence(const ItmState<Writer>&);

    /// <summary> Write a '{' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IRecord<Writer>* record(const ItmState<Writer>&);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, qlonglong& p_numb);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, double& p_numb);

    /// <summary> Write the \p p_bool item </summary>
    //! \return False if \p p_bool cannot be write and true otherwise
    bool bindBoolean(const ItmState<Writer>&, bool& p_bool);

    /// <summary> Write the \p p_datetime item </summary>
    //! \return False if \p p_datetime cannot be write and true otherwise
    bool bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime);

    /// <summary> Write the \p p_bytes item </summary>
    //! \return False if \p p_bytes cannot be write and true otherwise
    bool bindBytes(const ItmState<Writer>&, QByteArray& p_bytes);

    /// <summary> Write \p p_txt </summary>
    //! \return true if the write succeeded, false otherwise
    bool text(const ItmState<Writer>&, const QString& p_txt);

    /// <summary> Write \p p_dat </summary>
    //! \return true if the write succeeded, false otherwise
    bool bindText(const ItmState<Writer>&, QString& p_dat);

    /// <summary> Write a '\c null' token </summary>
    //! \return true if the write succeeded false otherwise
    bool null(const ItmState<Writer>&);

    /// <summary> Write a ']' token </summary>
    //! \return true if the write succeeded false otherwise
    bool sequenceOut(const SeqState<Writer>&);

    /// <summary> Write a ' | ' token if it is not the first item </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IItem<Writer>* sequenceItem(const SeqState<Writer>&);

    /// <summary> Write a '}' token </summary>
    //! \return true if the write succeeded false otherwise
    bool recordOut(const RecState<Writer>&);

    /// <summary> Write a ' | ' token if it is not the first item followed by \p p_key and the ':' token </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IItem<Writer>* recordItem(const RecState<Writer>&, QString& p_key);

private:
    QScopedPointer<SummaryPrivate> m_d;
};

using SummaryString = StringResult<Summary>;

} // data
} // modmed

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qscopedpointer.h>
#include <QtCore/qtextstream.h>
#include <QtCore/qstring.h>

class QIODevice;
class QByteArray;

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>

#include <modmed/modmed_global.h>

namespace modmed {
namespace data {

struct XmlWriterPrivate;

/// <summary> Serialize using the XML format </summary>
//!
//! - Sequence is represented by the \<s\> tag
//! - Record   is represented by the \<r\> tag
//! - Message  is encoded as text represented by the \<t\> tag (Message structure is not explicit in the physical data, only in code that reads the text)
//! - Null     is represented by the \<n /\> tag
//! - Tags inside a record have a key attribute.
//!
//! \b Responsibility: Write data structures using the XmlWriter format (\ref dataXml)
class MODMED_API XmlWriter : public virtual IItem<Writer>, public virtual ISequence<Writer>, public virtual IRecord<Writer>
{
    Q_DISABLE_COPY(XmlWriter)
public:
    /// <summary> Construct an XML serializer from a QIODevice </summary>
    /// <param name="p_ioDevice"> Device that is written </param>
    /// <param name="p_documentInformation"> XML preamble that is passed to the #document method </param>
    XmlWriter(QIODevice* p_ioDevice, const QString& p_documentInformation = QString());

    /// <summary> Construct an XML serializer from a FILE* </summary>
    /// <param name="p_file"> File handler that is written </param>
    /// <param name="p_documentInformation"> XML preamble that is passed to the #document method </param>
    XmlWriter(FILE* p_file, const QString& p_documentInformation = QString());

    /// <summary> Construct an XML serializer from a QString </summary>
    /// <param name="p_string"> QString that is written </param>
    /// <param name="p_documentInformation"> XML preamble that is passed to the #document method </param>
    XmlWriter(QString* p_string, const QString& p_documentInformation = QString());

    /// <summary> Construct an XML serializer from a QTextString </summary>
    /// <param name="p_s"> QTextStream that is written </param>
    /// <param name="p_documentInformation"> XML preamble that is passed to the #document method </param>
    XmlWriter(QTextStream* p_s, const QString& p_documentInformation = QString());

    /// <summary> Destructor </summary>
    virtual ~XmlWriter();

    /// <summary> Call flush on the internal QTextStream </summary>
    void flush();

    /// <summary> Returns true if the internal QTextStream::isValid method returns QTextStream::Ok </summary>
    bool isValid();

    /// <summary> Return the underlying QTextStream codecName even when iconv was used </summary>
    QString codecName();

    /// <summary> Write a &lt;s&gt; tag </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    ISequence<Writer>* sequence(const ItmState<Writer>&);

    /// <summary> Write a &lt;r&gt; tag </summary>
    //! \return \p this if the write succeeded nullptr otherwise
    IRecord<Writer>* record(const ItmState<Writer>&);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, qlonglong& p_numb);

    /// <summary> Write the \p p_numb item </summary>
    //! \return False if \p p_numb cannot be write and true otherwise
    bool bindNumber(const ItmState<Writer>&, double& p_numb);

    /// <summary> Write the \p p_bool item </summary>
    //! \return False if \p p_bool cannot be write and true otherwise
    bool bindBoolean(const ItmState<Writer>&, bool& p_bool);

    /// <summary> Write the \p p_datetime item </summary>
    //! \return False if \p p_datetime cannot be write and true otherwise
    bool bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime);

    /// <summary> Write the \p p_bytes item </summary>
    //! \return False if \p p_bytes cannot be write and true otherwise
    bool bindBytes(const ItmState<Writer>&, QByteArray& p_bytes);

    /// <summary> Write a &lt;n /&gt; tag </summary>
    //! \return true if the write succeeded false otherwise
    bool null(const ItmState<Writer>&);

    /// <summary> Write a &lt;/s&gt; tag </summary>
    //! \return true if the write succeeded false otherwise
    bool sequenceOut(const SeqState<Writer>&);

    /// <summary> Write nothing </summary>
    //! \return \p this
    IItem<Writer>* sequenceItem(const SeqState<Writer>&);

    /// <summary> Write a &lt;/r&gt; tag </summary>
    //! \return true if the write succeeded false otherwise
    bool recordOut(const RecState<Writer>&);

    /// <summary> Write nothing, next opening tag will contains \p p_key as "key" attribute </summary>
    //! \return \p this
    IItem<Writer>* recordItem(const RecState<Writer>&, QString& p_key);

    /// <summary> Write \p p_txt </summary>
    //! \return
    //! - Write: true if the write succeeded, false otherwise
    bool text(const ItmState<Writer>&, const QString& p_txt);

    /// <summary> Write \p p_dat </summary>
    //! \return
    //! Write: true if the write succeeded, false otherwise
    bool bindText(const ItmState<Writer>&, QString& p_dat);

    /// <summary> Writes document information at the beginning of the stream </summary>
    //! \warning Does nothing if the underlying stream position is not 0
    //! \remark isDocument() allows checking that document information was written
    //!
    //! \pre !information.isNull()
    //! \pre !m_isDocument
    //! \pre m_stream.pos() == 0
    //!
    //! XML document information is added between the XML preamble and root document element which means
    //! it can consist in well-formed XML processing instructions and comments like:
    //! \<!-- log file --\>\<?xml-stylesheet href="my.css" type="text/css"?\>
    XmlWriter& document(const QString& information = QString(""));

    /// <summary> Returns true if a call to #document succeeded </summary>
    bool isDocument();

private:
    QScopedPointer<XmlWriterPrivate> m_d;
};

/// <summary> Fail occurring when writing a character that is not valid in an Xml Document </summary>
/// See http://www.w3.org/TR/2004/REC-xml-20040204/#NT-Char
class MODMED_API InvalidXmlChar : public BindError
{
public:
    InvalidXmlChar(QChar c);
    QSharedPointer<BindError> clone() const;
    QChar getInvalidChar() const;
private:
    QScopedPointer<QChar> m_d; // no Class##Private struct
};

using XmlString = StringResult<XmlWriter>;

} // data
} // modmed

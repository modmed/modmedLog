/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once
#include <modmed/modmed_global.h>

#ifdef Q_OS_WIN
#include <QtCore/qt_windows.h>
#endif

#include <QtCore/qstring.h>
#include <QtCore/qfile.h>
#include <QtCore/qbuffer.h>

#include <modmed/log/LogEventData.h>
#include <modmed/log/ILogOutput.h>

#include <modmed/data/Summary.h>
namespace md = ::modmed::data;

namespace modmed {
namespace log {

//==============================================================================

/// <summary> Writes to stderr (or OutputDebugString on windows) </summary>
//!
struct ConsoleLogWriter : public ILogOutput
{
    bool m_useStderrOutput = false;

    static const char* severity(QtMsgType p_type)
    {
        switch (p_type) {
        case QtFatalMsg:    return "X";
        case QtCriticalMsg: return "!";
        case QtWarningMsg:  return "?";
        case QtInfoMsg:     return "i";
        default:
        case QtDebugMsg:    return "_";
        }
    }
    static void formatMessageTo(QIODevice& s, const LogEventData& p_eventData)
    {
        QByteArray format = p_eventData.format();
        QByteArrayList textParts = {""};
        for (int start=0, end=format.indexOf('%'); -1 == end || end < format.size(); end=format.indexOf('%',start))
        {
            static const auto flags       = QByteArrayLiteral("-+0 #");
            static const auto digits      = QByteArrayLiteral("0123456789");
            static const auto lengths     = QByteArrayLiteral("hljztLwI");
            static const auto conversions = QByteArrayLiteral("cCdiouxXeEfFgGaAnpsSZ");

            if (-1 == end) {
                textParts.last().append(format.mid(start));
                break;
            }
            else if (format.size() <= end+1 || format[end+1]=='%') {
                textParts.last().append(format.mid(start,end+1-start));
                start=end+1;
            }
            else {
                auto formatter = end+1;
                while          (formatter < format.size() && flags      .contains(format[formatter]))   formatter++;

                if             (formatter < format.size() && '*'         ==       format[formatter] )   formatter++;
                else while     (formatter < format.size() && digits     .contains(format[formatter]))   formatter++;

                if             (formatter < format.size() && '.'         ==       format[formatter] ) { formatter++;
                    if         (formatter < format.size() && '*'         ==       format[formatter] )   formatter++;
                    else while (formatter < format.size() && digits     .contains(format[formatter]))   formatter++;
                                                                                                      }
                if             (formatter < format.size() && lengths    .contains(format[formatter]))   formatter++;
                else if        (formatter < format.size() && format     .mid(formatter,2)=="hh"     )   formatter+=2;
                else if        (formatter < format.size() && format     .mid(formatter,2)=="ll"     )   formatter+=2;
                else if        (formatter < format.size() && format     .mid(formatter,3)=="I32"    )   formatter+=3;
                else if        (formatter < format.size() && format     .mid(formatter,3)=="I64"    )   formatter+=3;

                if             (formatter < format.size() && conversions.contains(format[formatter]))   formatter++;
                else                                                                                    formatter=end+1; // unknown formatter

                if (end+1 == formatter) {
                    textParts.last().append(format.mid(start,end+1-start)); // including previous '%'
                    start=end+1;
                }
                else {
                    textParts.last().append(format.mid(start,end  -start)); // without '%'
                    textParts.append("");
                    start=formatter; // skipped
                }
            }
        }
        for (int i=0; i<textParts.size() || i<p_eventData.args().size(); ++i)
        {
            if (i<textParts.size())
                s.write(textParts[i]);
            else
                s.write(" ");

            if (i<p_eventData.args().size())
            {
                md::Summary(&s).write().bind(p_eventData.args()[i]);
            }
        }
    }

    ConsoleLogWriter(bool useStderr = false) { m_useStderrOutput = useStderr;}

    /// <summary> Writes a fixed-length header with as much category and function characters as possible </summary>
    //! \remark Limits category to the first 4 characters and function to the first 14 characters after return type if necessary
    virtual bool output(const LogEventData& p_eventData)
    {
        QBuffer s;
        s.open(QIODevice::WriteOnly);
        QByteArray category(p_eventData.category());
        QByteArrayList categoryNames(category.split('.'));
        QByteArray function;
        QByteArray functionReturnType;
//        bool stripNamespaceParts = true;
        Q_FOREACH(QByteArray part, QByteArray(p_eventData.function()).split(' '))
        {
            // TODO Fix contructor names removed
            if (functionReturnType.isEmpty())
                functionReturnType = part;
            else
            {
#   ifdef Q_CC_MSVC
                if (part != "__cdecl" && part != "__stdcall")
//                    continue;
#   endif
// TODO
//                if (stripNamespaceParts)
//                {
//                    stripNamespaceParts = false;
//                    QStringList functionNamespace(part.split("::"));
//                    int i=0;
//                    while (i<categoryNames.length() && i<functionNamespace.length() && functionNamespace[i]==categoryNames[i])
//                        ++i;

//                    part.clear();
//                    for (int nonRedundantPart=i; nonRedundantPart<functionNamespace.length(); ++nonRedundantPart)
//                        part.append(functionNamespace[nonRedundantPart]);
//                }
                function.append(part);
            }
        }

        const int C =  9;
        const int F = 19;
        const int separatorPos =
            category.length() <= C && function.length() <= F /* both      are ok       */ ? C                                                  :
            category.length() >  C && function.length() >  F /* both      are too long */ ? C                                                  :
            category.length() <= C                           /* function   is too long */ ? qMax(category.length(), C+F-function.length()) :
            /**/                                             /* category   is too long */   qMin(C+F-function.length(), category.length()) ;
        const int categoryWidth =     separatorPos;
        const int functionWidth = F+C-separatorPos;
        const int categoryPad = categoryWidth-category.length();
        const int functionPad = functionWidth-function.length();

        static const auto space = QByteArrayLiteral(" ");

        s.write("\n");
        s.write(severity(p_eventData.type()));
        s.write("|");
        s.write(category.left(categoryWidth));
        s.write(space.repeated(categoryPad));
        s.write(separatorPos < C ? "[" : separatorPos == C ? "|" : "]");
        s.write(space.repeated(functionPad));
        s.write(function.left(functionWidth));
        s.write("| ");
        formatMessageTo(s, p_eventData);

#ifdef Q_OS_WIN
        if (!m_useStderrOutput)
        {
            ::OutputDebugStringA(qPrintable(s.buffer()));
            return true;
        }
#endif
        fprintf(stderr, qPrintable(s.buffer()));
        fflush(stderr);
        return true;
    }

    virtual void flush() {}
};

} // log
} // modmed

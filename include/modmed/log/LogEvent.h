/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once
#include <modmed/modmed_global.h>

#include <QtCore/qglobal.h>
#include <QtCore/qlogging.h>
#include <QtCore/qloggingcategory.h>
#include <QtCore/qdebug.h>
#include <QtCore/qatomic.h>
#include <QtCore/qstring.h>
#include <QtCore/qbytearray.h>
#include <QtCore/qvector.h>
#include <QtCore/qelapsedtimer.h>

#include <modmed/data/JsonWriter.h>

#include <modmed/log/LogMessage.h>
#include <modmed/log/LogEventData.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bindable.h>
#include <modmed/data/Bind.h>

namespace modmed {
namespace log {

// CR Store a QStringRef when __func__ is a part of __PRETTY_FUNCTION__ for later processing? Beware of names with class (=ctor) and namespace though...

/// <summary> Smart log event maintaining event classification and sampling information for a single trace point </summary>
//!
//! \remark LogEvent is used by macros which can be made source compatible with qlogging.h and qloggingcategory.h macros
class MODMED_API LogEvent
{
    Q_DISABLE_COPY(LogEvent)
public:
    static void noMessage(const char *, ...) Q_ATTRIBUTE_FORMAT_PRINTF(1, 2) {}
    static LogMessage noMessage() Q_DECL_NOTHROW { return LogMessage(); }

    LogEvent(QtMsgType type, const char *file = nullptr, int line = 0, const char *function = nullptr, const QLoggingCategory& category = *QLoggingCategory::defaultCategory()) :
        m_type(type),
        m_file(file),
        m_line(line),
        m_function(function),
        m_categoryName(category.categoryName()),
        m_category(category),
        m_id(0),
        m_count(0)
    {}

    LogMessage message(LogMessage::Options options = LogMessage::NoOptions, const char *emptyVariadicMacroExpr = nullptr);

    //! \warning This method must have the same name as message() for use by macros (with C-style format and args)
    template<typename... TArgs>
    LogMessage message(LogMessage::Options options, const char *variadicMacroExpr, const char *format, const TArgs&... args)
    {
        if (!m_category.isEnabled(m_type))
            return LogMessage();

        if (!m_format.isNull())
            options |= LogMessage::NoLiterals;

        return LogMessage(this, options, variadicMacroExpr, format, args...);
    }

    virtual void log(QByteArray format = QByteArray(), QVector<QSharedPointer<data::IBindable>> args = QVector<QSharedPointer<data::IBindable>>(),
                     QVector<QByteArray> argTypes = QVector<QByteArray>(), QVector<QByteArray> argNames = QVector<QByteArray>());

    int id() const;
    int count() const;
    const QLoggingCategory& category() const;

protected:
    static QAtomicInt s_newId;

    const QtMsgType m_type;
    const char * m_file;
    const int m_line;
    const char * m_function;
    const char * m_categoryName;
    const QLoggingCategory& m_category;
    QByteArray m_format;
    QVector<QByteArray> m_argTypes;
    QVector<QByteArray> m_argNames;

    mutable int m_id;
    mutable int m_count; // mutable allows making log() const
};

class MODMED_API LogSample : public LogEvent
{
    Q_DISABLE_COPY(LogSample)
public:
    LogSample(QtMsgType type = QtMsgType::QtDebugMsg, const char *file = nullptr, int line = 0, const char *function = nullptr, const QLoggingCategory& category = *QLoggingCategory::defaultCategory(), int countInterval = 0, int nsecsInterval = 0) :
        LogEvent(type, file, line, function, category),
        m_countInterval(countInterval),
        m_nsecsInterval(nsecsInterval),
        m_lastCount(-m_countInterval)
    {
        m_lastTime.start();
    }

    LogMessage message(LogMessage::Options options = LogMessage::NoOptions, const char *emptyVariadicMacroExpr = nullptr) { return this->LogEvent::message(options, emptyVariadicMacroExpr); }

    //! \warning This method must have the same name as message() for use by macros (with C-style format and args)
    template<typename... TArgs>
    LogMessage message(LogMessage::Options options, const char *variadicMacroExpr, const char *format, const TArgs&... args)
    {
        if (!isSampled())
            return LogMessage();

        return this->LogEvent::message(options, variadicMacroExpr, format, args...);
    }

    bool isSampled() const;
    virtual void log(QByteArray format = QByteArray(), QVector<QSharedPointer<data::IBindable>> args = QVector<QSharedPointer<data::IBindable>>(),
                     QVector<QByteArray> argTypes= QVector<QByteArray>(), QVector<QByteArray> argNames = QVector<QByteArray>());

private:
    const int m_countInterval;
    const int m_nsecsInterval;
    mutable int m_lastCount;
    QElapsedTimer m_lastTime; // CR make thread-safe
};

// PRIVATE macros
#define mSCFDEvent(severity, category, function) \
    ([](const char* callingFuncInfo)->::modmed::log::LogEvent&{ \
    static ::modmed::log::LogEvent e(severity, QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, callingFuncInfo, (category)()); \
    return e;})(function)

// BEWARE these macros are statements that must be followed by either .message()<<expr or .message(...)
#define mSCFEventIfEnabled(severity, category, function) \
    for (bool qt_category_enabled = (category)().isEnabled(severity); qt_category_enabled; qt_category_enabled = false) \
        mSCFDEvent(severity, category, function)

#define mSCCEventOutIfEnabled(severity, category, counter) \
    ::modmed::log::LogMessage mPendingLogMessage##counter; \
    for (bool qt_category_enabled = (category)().isEnabled(severity); qt_category_enabled; qt_category_enabled = false) \
        mPendingLogMessage##counter = mSCFDEvent(severity, category, Q_FUNC_INFO)

#define mSCCTSampleIfEnabled(severity, category, countInterval, timeInterval) \
    for (bool qt_category_enabled = (category)().isEnabled(severity); qt_category_enabled; qt_category_enabled = false) \
        ([](const char* callingFuncInfo, int c, int t)->::modmed::log::LogSample&{ \
        static ::modmed::log::LogSample e(severity, QT_MESSAGELOG_FILE, QT_MESSAGELOG_LINE, callingFuncInfo, (category)(), c, t); \
        return e;})(Q_FUNC_INFO, countInterval, timeInterval)

#define mSCEventOutIfEnabled(severity, category) mSCCEventOutIfEnabled(severity, category, __COUNTER__)
// end of PRIVATE macros

// Contrary to qCDebug, qDebug is always an expression since there was no way to disable it and so no need to check whether it was disabled
// So, for source compatibility with qDebug, mDebug cannot use mCDebug
#define mDebug(...) mSCFDEvent(QtMsgType::QtDebugMsg, *QLoggingCategory::defaultCategory(), Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mInfo(...) mSCFDEvent(QtMsgType::QtInfoMsg, *QLoggingCategory::defaultCategory(), Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mWarning(...) mSCFDEvent(QtMsgType::QtWarningMsg, *QLoggingCategory::defaultCategory(), Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mCritical(...) mSCFDEvent(QtMsgType::QtCriticalMsg, *QLoggingCategory::defaultCategory(), Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)

#define mCDebugModulo(category, modulo, ...) mSCCTSampleIfEnabled(QtMsgType::QtDebugMsg, category, modulo, 0).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mCDebugNSecs(category, nsecs, ...) mSCCTSampleIfEnabled(QtMsgType::QtDebugMsg, category, 0, nsecs).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mCDebugOut(category, ...) mSCEventOutIfEnabled(QtMsgType::QtDebugMsg, category).message(::modmed::log::LogMessage::GetArgRefs, "" #__VA_ARGS__, ##__VA_ARGS__)

#define mCDebug(category, ...) mSCFEventIfEnabled(QtMsgType::QtDebugMsg, category, Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mCInfo(category, ...) mSCFEventIfEnabled(QtMsgType::QtInfoMsg, category, Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mCWarning(category, ...) mSCFEventIfEnabled(QtMsgType::QtWarningMsg, category, Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)
#define mCCritical(category, ...) mSCFEventIfEnabled(QtMsgType::QtCriticalMsg, category, Q_FUNC_INFO).message(::modmed::log::LogMessage::NoOptions, "" #__VA_ARGS__, ##__VA_ARGS__)

#define MODMED_NO_MESSAGE_MACRO ::modmed::log::LogEvent::noMessage

#if defined(QT_NO_DEBUG_OUTPUT)
#  undef mCDebugModulo
#  define mCDebugModulo(category, modulo, ...) MODMED_NO_MESSAGE_MACRO()
#  undef mCDebugNSecs
#  define mCDebugNSecs(category, modulo, ...) MODMED_NO_MESSAGE_MACRO()
#  undef mCDebugOut
#  define mCDebugOut(category, ...) MODMED_NO_MESSAGE_MACRO()
#  undef mCDebug
#  define mCDebug(category, ...) MODMED_NO_MESSAGE_MACRO()
#  undef mDebug
#  define mDebug(...) MODMED_NO_MESSAGE_MACRO()
#endif
#if defined(QT_NO_INFO_OUTPUT)
#  undef mCInfo
#  define mCInfo(category, ...) MODMED_NO_MESSAGE_MACRO()
#  undef mInfo
#  define mInfo(...) MODMED_NO_MESSAGE_MACRO()
#endif
#if defined(QT_NO_WARNING_OUTPUT)
#  undef mCWarning
#  define mCWarning(category, ...) MODMED_NO_MESSAGE_MACRO()
#  undef mWarning
#  define mWarning(...) MODMED_NO_MESSAGE_MACRO()
#endif

#define mExpr(expression) modmed::log::NamedArg<decltype(expression)>(#expression, expression)

#define mExpr2(e1,e2) mExpr(e1)<<mExpr(e2)
#define mExpr3(e1,e2,e3) mExpr(e1)<<mExpr2(e2,e3)
#define mExpr4(e1,e2,e3,e4) mExpr(e1)<<mExpr3(e2,e3,e4)
#define mExpr5(e1,e2,e3,e4,e5) mExpr(e1)<<mExpr4(e2,e3,e4,e5)
#define mExpr6(e1,e2,e3,e4,e5,e6) mExpr(e1)<<mExpr5(e2,e3,e4,e5,e6)
#define mExpr7(e1,e2,e3,e4,e5,e6,e7) mExpr(e1)<<mExpr6(e2,e3,e4,e5,e6,e7)
#define mExpr8(e1,e2,e3,e4,e5,e6,e7,e8) mExpr(e1)<<mExpr7(e2,e3,e4,e5,e6,e7,e8)
#define mExpr9(e1,e2,e3,e4,e5,e6,e7,e8,e9) mExpr(e1)<<mExpr8(e2,e3,e4,e5,e6,e7,e8,e9)

#define mCTrace0(category) mCDebug(category)<<::modmed::log::ACTION_TRACE
#define mCTrace(category,expression) mCTrace0(category)<<mExpr(expression)

#define mCTrace2(category,e1,e2) mCTrace0(category)<<mExpr2(e1,e2)
#define mCTrace3(category,e1,e2,e3) mCTrace0(category)<<mExpr3(e1,e2,e3)
#define mCTrace4(category,e1,e2,e3,e4) mCTrace0(category)<<mExpr4(e1,e2,e3,e4)
#define mCTrace5(category,e1,e2,e3,e4,e5) mCTrace0(category)<<mExpr5(e1,e2,e3,e4,e5)
#define mCTrace6(category,e1,e2,e3,e4,e5,e6) mCTrace0(category)<<mExpr6(e1,e2,e3,e4,e5,e6)
#define mCTrace7(category,e1,e2,e3,e4,e5,e6,e7) mCTrace0(category)<<mExpr7(e1,e2,e3,e4,e5,e6,e7)
#define mCTrace8(category,e1,e2,e3,e4,e5,e6,e7,e8) mCTrace0(category)<<mExpr8(e1,e2,e3,e4,e5,e6,e7,e8)
#define mCTrace9(category,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTrace0(category)<<mExpr9(e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mCTraceOut0(category) mCDebugOut(category)<<::modmed::log::ACTION_TRACE<<::modmed::log::STATUS_FINISHED
#define mCTraceOut(category,reference) mCTraceOut0(category)<<mExpr(reference)

#define mCTraceOut2(category,r1,r2) mCTraceOut0(category)<<mExpr2(r1,r2)
#define mCTraceOut3(category,r1,r2,r3) mCTraceOut0(category)<<mExpr3(r1,r2,r3)
#define mCTraceOut4(category,r1,r2,r3,r4) mCTraceOut0(category)<<mExpr4(r1,r2,r3,r4)
#define mCTraceOut5(category,r1,r2,r3,r4,r5) mCTraceOut0(category)<<mExpr5(r1,r2,r3,r4,r5)
#define mCTraceOut6(category,r1,r2,r3,r4,r5,r6) mCTraceOut0(category)<<mExpr6(r1,r2,r3,r4,r5,r6)
#define mCTraceOut7(category,r1,r2,r3,r4,r5,r6,r7) mCTraceOut0(category)<<mExpr7(r1,r2,r3,r4,r5,r6,r7)
#define mCTraceOut8(category,r1,r2,r3,r4,r5,r6,r7,r8) mCTraceOut0(category)<<mExpr8(r1,r2,r3,r4,r5,r6,r7,r8)
#define mCTraceOut9(category,r1,r2,r3,r4,r5,r6,r7,r8,r9) mCTraceOut0(category)<<mExpr9(r1,r2,r3,r4,r5,r6,r7,r8,r9)

#define mCTraceQObject(category,qobject) mCTrace0(category)<<qPrintable((qobject)->metaObject()->className())<<modmed::data::Hex(uintptr_t(qobject))<<"objectName"<<(qobject)->objectName()

#define mCTraceQObject2(category,qobject,e2) mCTraceQObject(category,qobject)<<mExpr(e2)
#define mCTraceQObject3(category,qobject,e2,e3) mCTraceQObject(category,qobject)<<mExpr2(e2,e3)
#define mCTraceQObject4(category,qobject,e2,e3,e4) mCTraceQObject(category,qobject)<<mExpr3(e2,e3,e4)
#define mCTraceQObject5(category,qobject,e2,e3,e4,e5) mCTraceQObject(category,qobject)<<mExpr4(e2,e3,e4,e5)
#define mCTraceQObject6(category,qobject,e2,e3,e4,e5,e6) mCTraceQObject(category,qobject)<<mExpr5(e2,e3,e4,e5,e6)
#define mCTraceQObject7(category,qobject,e2,e3,e4,e5,e6,e7) mCTraceQObject(category,qobject)<<mExpr6(e2,e3,e4,e5,e6,e7)
#define mCTraceQObject8(category,qobject,e2,e3,e4,e5,e6,e7,e8) mCTraceQObject(category,qobject)<<mExpr7(e2,e3,e4,e5,e6,e7,e8)
#define mCTraceQObject9(category,qobject,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceQObject(category,qobject)<<mExpr8(e2,e3,e4,e5,e6,e7,e8,e9)

#define mCTraceModulo0(category,modulo) mCDebugModulo(category,modulo)<<::modmed::log::ACTION_TRACE
#define mCTraceModulo(category,modulo,expression) mCTraceModulo0(category,modulo)<<mExpr(expression)

#define mCTraceModulo2(category,modulo,e1,e2) mCTraceModulo0(category,modulo)<<mExpr2(e1,e2)
#define mCTraceModulo3(category,modulo,e1,e2,e3) mCTraceModulo0(category,modulo)<<mExpr3(e1,e2,e3)
#define mCTraceModulo4(category,modulo,e1,e2,e3,e4) mCTraceModulo0(category,modulo)<<mExpr4(e1,e2,e3,e4)
#define mCTraceModulo5(category,modulo,e1,e2,e3,e4,e5) mCTraceModulo0(category,modulo)<<mExpr5(e1,e2,e3,e4,e5)
#define mCTraceModulo6(category,modulo,e1,e2,e3,e4,e5,e6) mCTraceModulo0(category,modulo)<<mExpr6(e1,e2,e3,e4,e5,e6)
#define mCTraceModulo7(category,modulo,e1,e2,e3,e4,e5,e6,e7) mCTraceModulo0(category,modulo)<<mExpr7(e1,e2,e3,e4,e5,e6,e7)
#define mCTraceModulo8(category,modulo,e1,e2,e3,e4,e5,e6,e7,e8) mCTraceModulo0(category,modulo)<<mExpr8(e1,e2,e3,e4,e5,e6,e7,e8)
#define mCTraceModulo9(category,modulo,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceModulo0(category,modulo)<<mExpr9(e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mCTraceNSecs0(category,nsecs) mCDebugNSecs(category, nsecs)<<::modmed::log::ACTION_TRACE
#define mCTraceNSecs(category,nsecs,expression) mCTraceNSecs0(category, nsecs)<<mExpr(expression)

#define mCTraceNSecs2(category,nsecs,e1,e2) mCTraceNSecs0(category,nsecs)<<mExpr2(e1,e2)
#define mCTraceNSecs3(category,nsecs,e1,e2,e3) mCTraceNSecs0(category,nsecs)<<mExpr3(e1,e2,e3)
#define mCTraceNSecs4(category,nsecs,e1,e2,e3,e4) mCTraceNSecs0(category,nsecs)<<mExpr4(e1,e2,e3,e4)
#define mCTraceNSecs5(category,nsecs,e1,e2,e3,e4,e5) mCTraceNSecs0(category,nsecs)<<mExpr5(e1,e2,e3,e4,e5)
#define mCTraceNSecs6(category,nsecs,e1,e2,e3,e4,e5,e6) mCTraceNSecs0(category,nsecs)<<mExpr6(e1,e2,e3,e4,e5,e6)
#define mCTraceNSecs7(category,nsecs,e1,e2,e3,e4,e5,e6,e7) mCTraceNSecs0(category,nsecs)<<mExpr7(e1,e2,e3,e4,e5,e6,e7)
#define mCTraceNSecs8(category,nsecs,e1,e2,e3,e4,e5,e6,e7,e8) mCTraceNSecs0(category,nsecs)<<mExpr8(e1,e2,e3,e4,e5,e6,e7,e8)
#define mCTraceNSecs9(category,nsecs,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceNSecs0(category,nsecs)<<mExpr9(e1,e2,e3,e4,e5,e6,e7,e8,e9)

// For internal use only
#define mCAssumptionStmt_(category,expression,function) mSCFEventIfEnabled(QtMsgType::QtWarningMsg, category, function).message()<<::modmed::log::ACTION_TRACE<<::modmed::log::OBJECT_ASSUMPTION<<::modmed::log::STATUS_FAILURE<<(#expression)
#define mCRequirementStmt_(category,expression,function) mSCFEventIfEnabled(QtMsgType::QtWarningMsg, category, function).message()<<::modmed::log::ACTION_TRACE<<::modmed::log::OBJECT_REQUIREMENT<<::modmed::log::STATUS_FAILURE<<(#expression)

// NB: 'expression' is used first as a literal, and a second time as code to be evaluated
#define mCAssumption(category,expression) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function); return false; }})(expression, Q_FUNC_INFO)

#define mCAssumption2(category,expression,e2) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr(e2); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption3(category,expression,e2,e3) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr2(e2,e3); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption4(category,expression,e2,e3,e4) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr3(e2,e3,e4); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption5(category,expression,e2,e3,e4,e5) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr4(e2,e3,e4,e5); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption6(category,expression,e2,e3,e4,e5,e6) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr5(e2,e3,e4,e5,e6); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption7(category,expression,e2,e3,e4,e5,e6,e7) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr6(e2,e3,e4,e5,e6,e7); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption8(category,expression,e2,e3,e4,e5,e6,e7,e8) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr7(e2,e3,e4,e5,e6,e7,e8); return false; }})(expression, Q_FUNC_INFO)
#define mCAssumption9(category,expression,e2,e3,e4,e5,e6,e7,e8,e9) ([&](bool b, const char* function){ if (b) { return true; } else { mCAssumptionStmt_(category,expression,function)<<mExpr8(e2,e3,e4,e5,e6,e7,e8,e9); return false; }})(expression, Q_FUNC_INFO)

#define mCRequirement(category,expression) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function); return false; }})(expression, Q_FUNC_INFO)

#define mCRequirement2(category,expression,e2) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr(e2); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement3(category,expression,e2,e3) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr2(e2,e3); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement4(category,expression,e2,e3,e4) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr3(e2,e3,e4); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement5(category,expression,e2,e3,e4,e5) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr4(e2,e3,e4,e5); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement6(category,expression,e2,e3,e4,e5,e6) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr5(e2,e3,e4,e5,e6); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement7(category,expression,e2,e3,e4,e5,e6,e7) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr6(e2,e3,e4,e5,e6,e7); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement8(category,expression,e2,e3,e4,e5,e6,e7,e8) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr7(e2,e3,e4,e5,e6,e7,e8); return false; }})(expression, Q_FUNC_INFO)
#define mCRequirement9(category,expression,e2,e3,e4,e5,e6,e7,e8,e9) ([&](bool b, const char* function){ if (b) { return true; } else { mCRequirementStmt_(category,expression,function)<<mExpr8(e2,e3,e4,e5,e6,e7,e8,e9); return false; }})(expression, Q_FUNC_INFO)

// Variants using mSoftwareUnitLogCategory

// Simple way to categorize all trace points from this compilation unit using mUxxx macros if namespace-based mNxxx macros are inappropriate
#define M_UNIT_LOG_CATEGORY(categoryName, ...) namespace{ Q_LOGGING_CATEGORY(mSoftwareUnitLogCategory,categoryName, ##__VA_ARGS__) }

#define mUDebugModulo(modulo,...) mCDebugModulo(mSoftwareUnitLogCategory(),modulo, ##__VA_ARGS__)
#define mUDebugNSecs(nsecs,...) mCDebugNSecs(mSoftwareUnitLogCategory(),nsecs, ##__VA_ARGS__)
#define mUDebugOut(...) mCDebugOut(mSoftwareUnitLogCategory(), ##__VA_ARGS__)
#define mUDebug(...) mCDebug(mSoftwareUnitLogCategory(), ##__VA_ARGS__)
#define mUInfo(...) mCInfo(mSoftwareUnitLogCategory(), ##__VA_ARGS__)
#define mUWarning(...) mCWarning(mSoftwareUnitLogCategory(), ##__VA_ARGS__)
#define mUCritical(...) mCCritical(mSoftwareUnitLogCategory(), ##__VA_ARGS__)

#define mUTrace0() mCTrace0(mSoftwareUnitLogCategory())
#define mUTrace(expression) mCTrace(mSoftwareUnitLogCategory(),expression)

#define mUTrace2(e1,e2) mCTrace2(mSoftwareUnitLogCategory(),e1,e2)
#define mUTrace3(e1,e2,e3) mCTrace3(mSoftwareUnitLogCategory(),e1,e2,e3)
#define mUTrace4(e1,e2,e3,e4) mCTrace4(mSoftwareUnitLogCategory(),e1,e2,e3,e4)
#define mUTrace5(e1,e2,e3,e4,e5) mCTrace5(mSoftwareUnitLogCategory(),e1,e2,e3,e4,e5)
#define mUTrace6(e1,e2,e3,e4,e5,e6) mCTrace6(mSoftwareUnitLogCategory(),e1,e2,e3,e4,e5,e6)
#define mUTrace7(e1,e2,e3,e4,e5,e6,e7) mCTrace7(mSoftwareUnitLogCategory(),e1,e2,e3,e4,e5,e6,e7)
#define mUTrace8(e1,e2,e3,e4,e5,e6,e7,e8) mCTrace8(mSoftwareUnitLogCategory(),e1,e2,e3,e4,e5,e6,e7,e8)
#define mUTrace9(e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTrace9(mSoftwareUnitLogCategory(),e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mUTraceOut0() mCTraceOut0(mSoftwareUnitLogCategory())
#define mUTraceOut(reference) mCTraceOut(mSoftwareUnitLogCategory(),reference)

#define mUTraceOut2(r1,r2) mCTraceOut2(mSoftwareUnitLogCategory(),r1,r2)
#define mUTraceOut3(r1,r2,r3) mCTraceOut3(mSoftwareUnitLogCategory(),r1,r2,r3)
#define mUTraceOut4(r1,r2,r3,r4) mCTraceOut4(mSoftwareUnitLogCategory(),r1,r2,r3,r4)
#define mUTraceOut5(r1,r2,r3,r4,r5) mCTraceOut5(mSoftwareUnitLogCategory(),r1,r2,r3,r4,r5)
#define mUTraceOut6(r1,r2,r3,r4,r5,r6) mCTraceOut6(mSoftwareUnitLogCategory(),r1,r2,r3,r4,r5,r6)
#define mUTraceOut7(r1,r2,r3,r4,r5,r6,r7) mCTraceOut7(mSoftwareUnitLogCategory(),r1,r2,r3,r4,r5,r6,r7)
#define mUTraceOut8(r1,r2,r3,r4,r5,r6,r7,r8) mCTraceOut8(mSoftwareUnitLogCategory(),r1,r2,r3,r4,r5,r6,r7,r8)
#define mUTraceOut9(r1,r2,r3,r4,r5,r6,r7,r8,r9) mCTraceOut9(mSoftwareUnitLogCategory(),r1,r2,r3,r4,r5,r6,r7,r8,r9)

#define mUTraceQObject(qobject) mCTraceQObject(mSoftwareUnitLogCategory(),qobject)

#define mUTraceQObject2(qobject,e2) mCTraceQObject2(mSoftwareUnitLogCategory(),qobject,e2)
#define mUTraceQObject3(qobject,e2,e3) mCTraceQObject3(mSoftwareUnitLogCategory(),qobject,e2,e3)
#define mUTraceQObject4(qobject,e2,e3,e4) mCTraceQObject4(mSoftwareUnitLogCategory(),qobject,e2,e3,e4)
#define mUTraceQObject5(qobject,e2,e3,e4,e5) mCTraceQObject5(mSoftwareUnitLogCategory(),qobject,e2,e3,e4,e5)
#define mUTraceQObject6(qobject,e2,e3,e4,e5,e6) mCTraceQObject6(mSoftwareUnitLogCategory(),qobject,e2,e3,e4,e5,e6)
#define mUTraceQObject7(qobject,e2,e3,e4,e5,e6,e7) mCTraceQObject7(mSoftwareUnitLogCategory(),qobject,e2,e3,e4,e5,e6,e7)
#define mUTraceQObject8(qobject,e2,e3,e4,e5,e6,e7,e8) mCTraceQObject8(mSoftwareUnitLogCategory(),qobject,e2,e3,e4,e5,e6,e7,e8)
#define mUTraceQObject9(qobject,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceQObject9(mSoftwareUnitLogCategory(),qobject,e2,e3,e4,e5,e6,e7,e8,e9)

#define mUTraceModulo0(modulo) mCTraceModulo0(mSoftwareUnitLogCategory(),modulo)
#define mUTraceModulo(modulo,expression) mCTraceModulo(mSoftwareUnitLogCategory(),modulo,expression)

#define mUTraceModulo2(modulo,e1,e2) mCTraceModulo2(mSoftwareUnitLogCategory(),modulo,e1,e2)
#define mUTraceModulo3(modulo,e1,e2,e3) mCTraceModulo3(mSoftwareUnitLogCategory(),modulo,e1,e2,e3)
#define mUTraceModulo4(modulo,e1,e2,e3,e4) mCTraceModulo4(mSoftwareUnitLogCategory(),modulo,e1,e2,e3,e4)
#define mUTraceModulo5(modulo,e1,e2,e3,e4,e5) mCTraceModulo5(mSoftwareUnitLogCategory(),modulo,e1,e2,e3,e4,e5)
#define mUTraceModulo6(modulo,e1,e2,e3,e4,e5,e6) mCTraceModulo6(mSoftwareUnitLogCategory(),modulo,e1,e2,e3,e4,e5,e6)
#define mUTraceModulo7(modulo,e1,e2,e3,e4,e5,e6,e7) mCTraceModulo7(mSoftwareUnitLogCategory(),modulo,e1,e2,e3,e4,e5,e6,e7)
#define mUTraceModulo8(modulo,e1,e2,e3,e4,e5,e6,e7,e8) mCTraceModulo8(mSoftwareUnitLogCategory(),modulo,e1,e2,e3,e4,e5,e6,e7,e8)
#define mUTraceModulo9(modulo,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceModulo9(mSoftwareUnitLogCategory(),modulo,e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mUTraceNSecs0(nsecs) mCTraceNSecs0(mSoftwareUnitLogCategory(),nsecs)
#define mUTraceNSecs(nsecs,expression) mCTraceNSecs(mSoftwareUnitLogCategory(),nsecs,expression)

#define mUTraceNSecs2(nsecs,e1,e2) mCTraceNSecs2(mSoftwareUnitLogCategory(),nsecs,e1,e2)
#define mUTraceNSecs3(nsecs,e1,e2,e3) mCTraceNSecs3(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3)
#define mUTraceNSecs4(nsecs,e1,e2,e3,e4) mCTraceNSecs4(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3,e4)
#define mUTraceNSecs5(nsecs,e1,e2,e3,e4,e5) mCTraceNSecs5(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3,e4,e5)
#define mUTraceNSecs6(nsecs,e1,e2,e3,e4,e5,e6) mCTraceNSecs6(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3,e4,e5,e6)
#define mUTraceNSecs7(nsecs,e1,e2,e3,e4,e5,e6,e7) mCTraceNSecs7(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3,e4,e5,e6,e7)
#define mUTraceNSecs8(nsecs,e1,e2,e3,e4,e5,e6,e7,e8) mCTraceNSecs8(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3,e4,e5,e6,e7,e8)
#define mUTraceNSecs9(nsecs,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceNSecs9(mSoftwareUnitLogCategory(),nsecs,e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mUAssumption(expression) mCAssumption(mSoftwareUnitLogCategory(),expression)

#define mUAssumption2(expression,e2) mCAssumption2(mSoftwareUnitLogCategory(),expression,e2)
#define mUAssumption3(expression,e2,e3) mCAssumption3(mSoftwareUnitLogCategory(),expression,e2,e3)
#define mUAssumption4(expression,e2,e3,e4) mCAssumption4(mSoftwareUnitLogCategory(),expression,e2,e3,e4)
#define mUAssumption5(expression,e2,e3,e4,e5) mCAssumption5(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5)
#define mUAssumption6(expression,e2,e3,e4,e5,e6) mCAssumption6(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6)
#define mUAssumption7(expression,e2,e3,e4,e5,e6,e7) mCAssumption7(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6,e7)
#define mUAssumption8(expression,e2,e3,e4,e5,e6,e7,e8) mCAssumption8(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8)
#define mUAssumption9(expression,e2,e3,e4,e5,e6,e7,e8,e9) mCAssumption9(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8,e9)

#define mURequirement(expression) mCRequirement(mSoftwareUnitLogCategory(),expression)

#define mURequirement2(expression,e2) mCRequirement2(mSoftwareUnitLogCategory(),expression,e2)
#define mURequirement3(expression,e2,e3) mCRequirement3(mSoftwareUnitLogCategory(),expression,e2,e3)
#define mURequirement4(expression,e2,e3,e4) mCRequirement4(mSoftwareUnitLogCategory(),expression,e2,e3,e4)
#define mURequirement5(expression,e2,e3,e4,e5) mCRequirement5(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5)
#define mURequirement6(expression,e2,e3,e4,e5,e6) mCRequirement6(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6)
#define mURequirement7(expression,e2,e3,e4,e5,e6,e7) mCRequirement7(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6,e7)
#define mURequirement8(expression,e2,e3,e4,e5,e6,e7,e8) mCRequirement8(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8)
#define mURequirement9(expression,e2,e3,e4,e5,e6,e7,e8,e9) mCRequirement9(mSoftwareUnitLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8,e9)

// Variants using mNamespaceLogCategory

MODMED_API QByteArray categoryFromFunctionNamespace(const char* functionSignature, const char* functionName);

#if !defined(__func__) && defined(__FUNCTION__)
#define __func__ __FUNCTION__
#endif

#define M_NAMESPACE_LOG_CATEGORY(...) \
    inline const QLoggingCategory& mNamespaceLogCategory() \
    { \
        static const QByteArray categoryName(::modmed::log::categoryFromFunctionNamespace(Q_FUNC_INFO,__func__)); \
        static const QLoggingCategory category(categoryName, ##__VA_ARGS__); \
        return category; \
    }

#define mNDebugModulo(modulo,...) mCDebugModulo(mNamespaceLogCategory(),modulo, ##__VA_ARGS__)
#define mNDebugNSecs(nsecs,...) mCDebugNSecs(mNamespaceLogCategory(),nsecs, ##__VA_ARGS__)
#define mNDebugOut(...) mCDebugOut(mNamespaceLogCategory(), ##__VA_ARGS__)
#define mNDebug(...) mCDebug(mNamespaceLogCategory(), ##__VA_ARGS__)
#define mNInfo(...) mCInfo(mNamespaceLogCategory(), ##__VA_ARGS__)
#define mNWarning(...) mCWarning(mNamespaceLogCategory(), ##__VA_ARGS__)
#define mNCritical(...) mCCritical(mNamespaceLogCategory(), ##__VA_ARGS__)

#define mNTrace0() mCTrace0(mNamespaceLogCategory())
#define mNTrace(expression) mCTrace(mNamespaceLogCategory(),expression)

#define mNTrace2(e1,e2) mCTrace2(mNamespaceLogCategory(),e1,e2)
#define mNTrace3(e1,e2,e3) mCTrace3(mNamespaceLogCategory(),e1,e2,e3)
#define mNTrace4(e1,e2,e3,e4) mCTrace4(mNamespaceLogCategory(),e1,e2,e3,e4)
#define mNTrace5(e1,e2,e3,e4,e5) mCTrace5(mNamespaceLogCategory(),e1,e2,e3,e4,e5)
#define mNTrace6(e1,e2,e3,e4,e5,e6) mCTrace6(mNamespaceLogCategory(),e1,e2,e3,e4,e5,e6)
#define mNTrace7(e1,e2,e3,e4,e5,e6,e7) mCTrace7(mNamespaceLogCategory(),e1,e2,e3,e4,e5,e6,e7)
#define mNTrace8(e1,e2,e3,e4,e5,e6,e7,e8) mCTrace8(mNamespaceLogCategory(),e1,e2,e3,e4,e5,e6,e7,e8)
#define mNTrace9(e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTrace9(mNamespaceLogCategory(),e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mNTraceOut0() mCTraceOut0(mNamespaceLogCategory())
#define mNTraceOut(reference) mCTraceOut(mNamespaceLogCategory(),reference)

#define mNTraceOut2(r1,r2) mCTraceOut2(mNamespaceLogCategory(),r1,r2)
#define mNTraceOut3(r1,r2,r3) mCTraceOut3(mNamespaceLogCategory(),r1,r2,r3)
#define mNTraceOut4(r1,r2,r3,r4) mCTraceOut4(mNamespaceLogCategory(),r1,r2,r3,r4)
#define mNTraceOut5(r1,r2,r3,r4,r5) mCTraceOut5(mNamespaceLogCategory(),r1,r2,r3,r4,r5)
#define mNTraceOut6(r1,r2,r3,r4,r5,r6) mCTraceOut6(mNamespaceLogCategory(),r1,r2,r3,r4,r5,r6)
#define mNTraceOut7(r1,r2,r3,r4,r5,r6,r7) mCTraceOut7(mNamespaceLogCategory(),r1,r2,r3,r4,r5,r6,r7)
#define mNTraceOut8(r1,r2,r3,r4,r5,r6,r7,r8) mCTraceOut8(mNamespaceLogCategory(),r1,r2,r3,r4,r5,r6,r7,r8)
#define mNTraceOut9(r1,r2,r3,r4,r5,r6,r7,r8,r9) mCTraceOut9(mNamespaceLogCategory(),r1,r2,r3,r4,r5,r6,r7,r8,r9)

#define mNTraceQObject(qobject) mCTraceQObject(mNamespaceLogCategory(),qobject)

#define mNTraceQObject2(qobject,e2) mCTraceQObject2(mNamespaceLogCategory(),qobject,e2)
#define mNTraceQObject3(qobject,e2,e3) mCTraceQObject3(mNamespaceLogCategory(),qobject,e2,e3)
#define mNTraceQObject4(qobject,e2,e3,e4) mCTraceQObject4(mNamespaceLogCategory(),qobject,e2,e3,e4)
#define mNTraceQObject5(qobject,e2,e3,e4,e5) mCTraceQObject5(mNamespaceLogCategory(),qobject,e2,e3,e4,e5)
#define mNTraceQObject6(qobject,e2,e3,e4,e5,e6) mCTraceQObject6(mNamespaceLogCategory(),qobject,e2,e3,e4,e5,e6)
#define mNTraceQObject7(qobject,e2,e3,e4,e5,e6,e7) mCTraceQObject7(mNamespaceLogCategory(),qobject,e2,e3,e4,e5,e6,e7)
#define mNTraceQObject8(qobject,e2,e3,e4,e5,e6,e7,e8) mCTraceQObject8(mNamespaceLogCategory(),qobject,e2,e3,e4,e5,e6,e7,e8)
#define mNTraceQObject9(qobject,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceQObject9(mNamespaceLogCategory(),qobject,e2,e3,e4,e5,e6,e7,e8,e9)

#define mNTraceModulo0(modulo) mCTraceModulo0(mNamespaceLogCategory(),modulo)
#define mNTraceModulo(modulo,expression) mCTraceModulo(mNamespaceLogCategory(),modulo,expression)

#define mNTraceModulo2(modulo,e1,e2) mCTraceModulo2(mNamespaceLogCategory(),modulo,e1,e2)
#define mNTraceModulo3(modulo,e1,e2,e3) mCTraceModulo3(mNamespaceLogCategory(),modulo,e1,e2,e3)
#define mNTraceModulo4(modulo,e1,e2,e3,e4) mCTraceModulo4(mNamespaceLogCategory(),modulo,e1,e2,e3,e4)
#define mNTraceModulo5(modulo,e1,e2,e3,e4,e5) mCTraceModulo5(mNamespaceLogCategory(),modulo,e1,e2,e3,e4,e5)
#define mNTraceModulo6(modulo,e1,e2,e3,e4,e5,e6) mCTraceModulo6(mNamespaceLogCategory(),modulo,e1,e2,e3,e4,e5,e6)
#define mNTraceModulo7(modulo,e1,e2,e3,e4,e5,e6,e7) mCTraceModulo7(mNamespaceLogCategory(),modulo,e1,e2,e3,e4,e5,e6,e7)
#define mNTraceModulo8(modulo,e1,e2,e3,e4,e5,e6,e7,e8) mCTraceModulo8(mNamespaceLogCategory(),modulo,e1,e2,e3,e4,e5,e6,e7,e8)
#define mNTraceModulo9(modulo,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceModulo9(mNamespaceLogCategory(),modulo,e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mNTraceNSecs0(nsecs) mCTraceNSecs0(mNamespaceLogCategory(),nsecs)
#define mNTraceNSecs(nsecs,expression) mCTraceNSecs(mNamespaceLogCategory(),nsecs,expression)

#define mNTraceNSecs2(nsecs,e1,e2) mCTraceNSecs2(mNamespaceLogCategory(),nsecs,e1,e2)
#define mNTraceNSecs3(nsecs,e1,e2,e3) mCTraceNSecs3(mNamespaceLogCategory(),nsecs,e1,e2,e3)
#define mNTraceNSecs4(nsecs,e1,e2,e3,e4) mCTraceNSecs4(mNamespaceLogCategory(),nsecs,e1,e2,e3,e4)
#define mNTraceNSecs5(nsecs,e1,e2,e3,e4,e5) mCTraceNSecs5(mNamespaceLogCategory(),nsecs,e1,e2,e3,e4,e5)
#define mNTraceNSecs6(nsecs,e1,e2,e3,e4,e5,e6) mCTraceNSecs6(mNamespaceLogCategory(),nsecs,e1,e2,e3,e4,e5,e6)
#define mNTraceNSecs7(nsecs,e1,e2,e3,e4,e5,e6,e7) mCTraceNSecs7(mNamespaceLogCategory(),nsecs,e1,e2,e3,e4,e5,e6,e7)
#define mNTraceNSecs8(nsecs,e1,e2,e3,e4,e5,e6,e7,e8) mCTraceNSecs8(mNamespaceLogCategory(),nsecs,e1,e2,e3,e4,e5,e6,e7,e8)
#define mNTraceNSecs9(nsecs,e1,e2,e3,e4,e5,e6,e7,e8,e9) mCTraceNSecs9(mNamespaceLogCategory(),nsecs,e1,e2,e3,e4,e5,e6,e7,e8,e9)

#define mNAssumption(expression) mCAssumption(mNamespaceLogCategory(),expression)

#define mNAssumption2(expression,e2) mCAssumption2(mNamespaceLogCategory(),expression,e2)
#define mNAssumption3(expression,e2,e3) mCAssumption3(mNamespaceLogCategory(),expression,e2,e3)
#define mNAssumption4(expression,e2,e3,e4) mCAssumption4(mNamespaceLogCategory(),expression,e2,e3,e4)
#define mNAssumption5(expression,e2,e3,e4,e5) mCAssumption5(mNamespaceLogCategory(),expression,e2,e3,e4,e5)
#define mNAssumption6(expression,e2,e3,e4,e5,e6) mCAssumption6(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6)
#define mNAssumption7(expression,e2,e3,e4,e5,e6,e7) mCAssumption7(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6,e7)
#define mNAssumption8(expression,e2,e3,e4,e5,e6,e7,e8) mCAssumption8(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8)
#define mNAssumption9(expression,e2,e3,e4,e5,e6,e7,e8,e9) mCAssumption9(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8,e9)

#define mNRequirement(expression) mCRequirement(mNamespaceLogCategory(),expression)

#define mNRequirement2(expression,e2) mCRequirement2(mNamespaceLogCategory(),expression,e2)
#define mNRequirement3(expression,e2,e3) mCRequirement3(mNamespaceLogCategory(),expression,e2,e3)
#define mNRequirement4(expression,e2,e3,e4) mCRequirement4(mNamespaceLogCategory(),expression,e2,e3,e4)
#define mNRequirement5(expression,e2,e3,e4,e5) mCRequirement5(mNamespaceLogCategory(),expression,e2,e3,e4,e5)
#define mNRequirement6(expression,e2,e3,e4,e5,e6) mCRequirement6(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6)
#define mNRequirement7(expression,e2,e3,e4,e5,e6,e7) mCRequirement7(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6,e7)
#define mNRequirement8(expression,e2,e3,e4,e5,e6,e7,e8) mCRequirement8(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8)
#define mNRequirement9(expression,e2,e3,e4,e5,e6,e7,e8,e9) mCRequirement9(mNamespaceLogCategory(),expression,e2,e3,e4,e5,e6,e7,e8,e9)

} // log
} // modmed

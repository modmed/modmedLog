/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qlogging.h>
#include <QtCore/qstring.h>
#include <QtCore/qdatetime.h>
#include <QtCore/qsharedpointer.h>

#include <modmed/data/Bindable.h>

namespace modmed {
namespace log {

/// <summary> Structured LogEventData separating the constant message format() from its corresponding data() </summary>
//!
//! Event metadata collected at the trace point is also available
//!
class MODMED_API LogEventData
{
public:
    LogEventData(const QtMsgType& type, const char *file, int line, const char *function, const char *category);

    const QtMsgType& type() const;

    const char* file() const;       //!< Same as QMessageLogContext
    int line() const;               //!< Same as QMessageLogContext
    const char* function() const;   //!< Same as QMessageLogContext
    const char* category() const;   //!< Same as QMessageLogContext

    /// More structured version of QtMessageHandler message
    const QByteArray& format() const;
    /// IBindable data in the format() appearance order
    const QVector<QSharedPointer<data::IBindable>>& args() const;
    const QVector<QByteArray>& argTypes() const;
    const QVector<QByteArray>& argNames() const;

    const QByteArray& id() const;      //!< Same as LogEvent
    quint64 count() const;   //!< Same as LogEvent or QString() if last logged LogEvent was actually notified and not filtered out
    qint64 nsecsElapsed() const;
    const QDateTime& dateTime() const;

    const QByteArray& threadId() const;

protected:
    friend class LogEvent;
    friend class LogSample;
    void setId(const QByteArray&);
    void setCount(quint64);

    void setFormat(const QByteArray&);
    void setArgs(const QVector<QSharedPointer<data::IBindable>>&);
    void setArgTypes(const QVector<QByteArray>&);
    void setArgNames(const QVector<QByteArray>&);

    friend class LogManager;
    void setNSecsElapsed(qint64);

private:
    QtMsgType m_type;
    const char* m_file;
    int m_line;
    const char* m_function;
    const char* m_category;
    // specific to LogEvent contrary to QMessageLogger
    QByteArray m_format;
    mutable QByteArray m_threadId;
    QByteArray m_id;
    quint64 m_count;
    qint64  m_nsecsElapsed;
    mutable QDateTime m_dateTime;
    QVector<QSharedPointer<data::IBindable>> m_args;
    QVector<QByteArray> m_argTypes;
    QVector<QByteArray> m_argNames;
};

} // log
} // modmed

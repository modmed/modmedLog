﻿#pragma once

#include <modmed/modmed_global.h>
#include <modmed/log/TsvJsonLogWriter.h>
#include <modmed/log/ConsoleLogWriter.h>
#include <modmed/log/LogEventData.h>
#include <modmed/data/Bindable.h>

#include <QtCore/qlogging.h>
#include <QtCore/qvector.h>
#include <QtCore/qmutex.h>
#include <QtCore/qatomic.h>
#include <QtCore/qqueue.h>
#include <QtCore/qreadwritelock.h>

namespace modmed {
namespace log {

class MODMED_API LogManager
{
    Q_DISABLE_COPY(LogManager)
public:
    static void qtMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);
    static LogManager& getInstance();

    ~LogManager();

    void process(LogEventData&);

    /// <summary> </summary>
    //! \remark 0 resets the default output
    void setOutput(ILogOutput*);

    const QElapsedTimer& startWriteTime();

private:    
    LogManager();

    void output(const LogEventData& m);

    static QScopedPointer<LogManager> m_instance;

    ILogOutput*    m_output;
    QReadWriteLock m_outputMutex;

    ConsoleLogWriter m_defaultOutput;

    QElapsedTimer m_startWriteTime;
};

} // log
} // modmed

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include<type_traits>

#include <QtCore/qchar.h>
#include <QtCore/qstring.h>
#include <QtCore/qstringlist.h>
#include <QtCore/qdebug.h>

#include <modmed/modmed_global.h>
#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>
#include <modmed/data/Bindable.h>
#include <modmed/data/JsonWriter.h>

namespace modmed {
namespace log {

class LogEvent;
class LogSample;

// See CEE core taxonomy https://cee.mitre.org/language/1.0-beta1/core-profile.html

// "action" tags for use in LogMessage
extern MODMED_API const char* ACTION_CREATE;
extern MODMED_API const char* ACTION_COPY;
extern MODMED_API const char* ACTION_MOVE;
extern MODMED_API const char* ACTION_REMOVE;

extern MODMED_API const char* ACTION_OPEN;
extern MODMED_API const char* ACTION_READ;
extern MODMED_API const char* ACTION_WRITE;
extern MODMED_API const char* ACTION_EXECUTE;
extern MODMED_API const char* ACTION_ACCESS; // when neither R/W/X is appropriate
extern MODMED_API const char* ACTION_CLOSE;

extern MODMED_API const char* ACTION_LOGIN;
extern MODMED_API const char* ACTION_ALLOW;
extern MODMED_API const char* ACTION_BLOCK;
extern MODMED_API const char* ACTION_LOGOUT;

extern MODMED_API const char* ACTION_ALLOCATE;
extern MODMED_API const char* ACTION_INITIALIZE;
extern MODMED_API const char* ACTION_LOCK;
extern MODMED_API const char* ACTION_UNLOCK;
extern MODMED_API const char* ACTION_FREE;

extern MODMED_API const char* ACTION_BIND;
extern MODMED_API const char* ACTION_CONNECT;
extern MODMED_API const char* ACTION_DOWNLOAD;
extern MODMED_API const char* ACTION_UPLOAD;
extern MODMED_API const char* ACTION_DISCONNECT;

extern MODMED_API const char* ACTION_START;
extern MODMED_API const char* ACTION_SUSPEND;
extern MODMED_API const char* ACTION_RESUME;
extern MODMED_API const char* ACTION_STOP;

extern MODMED_API const char* ACTION_INSTALL;
extern MODMED_API const char* ACTION_UPDATE;
extern MODMED_API const char* ACTION_UPGRADE;
extern MODMED_API const char* ACTION_UNINSTALL;

// "object" tags for use in LogMessage
extern MODMED_API const char* OBJECT_ACCOUNT;
extern MODMED_API const char* OBJECT_APP;
extern MODMED_API const char* OBJECT_CONNECTION;
extern MODMED_API const char* OBJECT_DRIVER;
extern MODMED_API const char* OBJECT_EMAIL;
extern MODMED_API const char* OBJECT_FILE;
extern MODMED_API const char* OBJECT_HOST; // CEE field
extern MODMED_API const char* OBJECT_IPV4; // CEE type
extern MODMED_API const char* OBJECT_IPV6; // CEE type
extern MODMED_API const char* OBJECT_LIBRARY;
extern MODMED_API const char* OBJECT_MEMORY;
extern MODMED_API const char* OBJECT_PROCESS;
extern MODMED_API const char* OBJECT_SYSTEM;
extern MODMED_API const char* OBJECT_THREAD;

// "status" tags for use in LogMessage
extern MODMED_API const char* STATUS_ONGOING;
extern MODMED_API const char* STATUS_CANCEL;
extern MODMED_API const char* STATUS_FINISHED;
extern MODMED_API const char* STATUS_FAILURE; // more precise than error which may mean "fault"
extern MODMED_API const char* STATUS_SUCCESS;
extern MODMED_API const char* STATUS_UNKNOWN;

// MODMED tags

// "action" tags for use in LogMessage
extern MODMED_API const char* ACTION_TRACE;

// "object" tags for use in LogMessage
extern MODMED_API const char* OBJECT_ASSUMPTION;
extern MODMED_API const char* OBJECT_REQUIREMENT;

template <typename T>
struct NamedArg
{
    NamedArg(const char *p_literal, const T& p_value) : m_literal(p_literal), m_value(p_value) {}

    const char* m_literal;
    const T& m_value;
};

/// <summary> Structured log message source compatible with QDebug </summary>
class MODMED_API LogMessage
{
public:
    enum Option
    {
        NoOptions   = 0,
        GetArgRefs  = 1,
        NoLiterals = 2
    };
    Q_DECLARE_FLAGS(Options, Option)
private:
    friend class LogEvent;
    friend class LogMessageStateSaverPrivate;

    struct Stream
    {
        enum { DefaultVerbosity = 2, VerbosityShift = 29, VerbosityMask = 0x7 };

        Stream(QTextStream* backend) : ts(backend), pEvent(nullptr) {}
        Stream(LogEvent* event, Options o) : ts(nullptr), pEvent(event), options(o) {}

        QTextStream* ts;
        QByteArray utf8Format;
        int ref = 1;
        // Best handled by pEvent: QtMsgType type;
        bool space = true;
        // Replaced with pEvent: bool message_output;
        // Best handled by pEvent: QMessageLogContext context;

        enum FormatFlag   // Note: Bits 29..31 are reserved for the verbose level introduced in 5.6.
        {
            NoQuotes = 0x1
        };

        bool testFlag(FormatFlag flag) const { return flags & flag; }
        void setFlag(FormatFlag flag) { flags |= flag; }
        void unsetFlag(FormatFlag flag) { flags &= ~flag; }
        int verbosity() const
        { return (flags >> VerbosityShift) & VerbosityMask; }
        void setVerbosity(int v)
        {
            flags &= ~(VerbosityMask << VerbosityShift);
            flags |= (v & VerbosityMask) << VerbosityShift;
        }
        // added in 5.4
        int flags = DefaultVerbosity << VerbosityShift;

        // specific to LogMessage
        LogEvent* pEvent;
        Options options = NoOptions;
        QVector<QSharedPointer<data::IBindable>> args;
        QVector<QByteArray> argTypes;
        QVector<QByteArray> argNames;

    } *stream;

    void putUcs4(unsigned ucs4);
    void putString(const QString& t);
    void putLatin1String(QLatin1String s);
    void putByteArray(const QByteArray& ba);

    // considered as Item
    template<class T>
    inline void putItem(const T& t, bool noFormatter = false)
    {
        stream->args.push_back(QSharedPointer<data::IBindable>(
            stream->options & GetArgRefs ? (data::IBindable*) new data::BindableRef<T>(t)
            : new data::BindableCopy<T>(t)));

        // It is not possible to use data::BindableRef in general because the message may capture temporary results of expressions
        // However, for most Qt "value" and container types, the copy costs almost nothing thanks to implicit sharing
        // See http://doc.qt.io/qt-5/implicit-sharing.html
        // In general, value types may implement similar strategies to reduce the cost of "pass by value" copies

        if (!stream->options.testFlag(NoLiterals))
        {
            if (!noFormatter)
                stream->utf8Format.append("%s");

            // TODO add constexpr c++14/17
            if (std::is_same<bool, T>::value)
                stream->argTypes.append("_Boolean");
            else if (std::is_integral<T>::value)
                stream->argTypes.append("_Integer");
            else if (std::is_floating_point<T>::value)
                stream->argTypes.append("_Decimal");
            else if (std::is_same<QByteArray, T>::value)
                stream->argTypes.append("_Bytes");
            else if (std::is_same<QDateTime, T>::value)
                stream->argTypes.append("_TimeStamp");
            else
                stream->argTypes.append(QByteArray());

            if (stream->argNames.size() < stream->args.size()) // operator<<(NamedArg) may have already appended to argNames
            {
                stream->argNames.append(QByteArray());
            }
        }
    }
public:
    inline LogMessage(const LogMessage& o) : stream(o.stream) { ++stream->ref; }

    inline LogMessage(QTextStream* backend) : stream(new Stream(backend)) {}
    inline LogMessage(LogEvent* event = nullptr, Options options = NoOptions, const char *emptyVariadicMacroExpr = nullptr) : stream(new Stream(event, options)) { Q_UNUSED(emptyVariadicMacroExpr); }
    template<typename... TArgs>
    inline LogMessage(LogEvent* event, Options options, const char* variadicMacroExpr, const char* format, const TArgs&... args) : stream(new Stream(event, options))
    {
        if (!stream->options.testFlag(NoLiterals))
        {
            // _format
            stream->utf8Format = format;
            // _arg_names
            QByteArrayList names = QByteArray::fromRawData(variadicMacroExpr, strlen(variadicMacroExpr)).split(',');
            names.takeFirst(); // it is a duplicate of the format coming from macro mDebug(...) etc.
            Q_FOREACH(QByteArray name, names)
            {
                if (name.trimmed().startsWith('"'))
                    stream->argNames.append(QByteArray());
                else
                    stream->argNames.append(toName(name.constData()));
            }
        }
        // _args + _arg_types
        putItems(args...);
    }

    QByteArray toName(const char* text);

    inline LogMessage& operator=(const LogMessage& other);
    ~LogMessage();
    inline void swap(LogMessage& other) Q_DECL_NOTHROW { std::swap(stream, other.stream); }

    LogMessage& resetFormat();

    inline LogMessage& space() { stream->space = true; stream->utf8Format.append(' '); return *this; }
    inline LogMessage& nospace() { stream->space = false; return *this; }
    inline LogMessage& maybeSpace() { if (stream->space && !stream->options.testFlag(NoLiterals)) stream->utf8Format.append(' '); return *this; }
    int verbosity() const { return stream->verbosity(); }
    void setVerbosity(int verbosityLevel) { stream->setVerbosity(verbosityLevel); }

    bool autoInsertSpaces() const { return stream->space; }
    void setAutoInsertSpaces(bool b) { stream->space = b; }

    inline LogMessage& quote() { stream->unsetFlag(Stream::NoQuotes); return *this; }
    inline LogMessage& noquote() { stream->setFlag(Stream::NoQuotes); return *this; }
    inline LogMessage& maybeQuote(char asciiQuote = '"') { if (!(stream->testFlag(Stream::NoQuotes))) stream->utf8Format.append(asciiQuote); return *this; }

    // considered as Item if !Stream::NoQuotes
    inline LogMessage& operator<<(QChar t)
    {
        putUcs4(t.unicode());
        maybeSpace();
        return *this;
    }
#ifdef Q_COMPILER_UNICODE_STRINGS
    inline LogMessage& operator<<(char16_t t) { return *this << QChar(t); }
    inline LogMessage& operator<<(char32_t t)
    {
        putUcs4(t);
        maybeSpace();
        return *this;
    }
#endif

    // not considered as Item
    inline LogMessage& operator<<(const char* utf8Format)
    {
        if (!stream->options.testFlag(NoLiterals))
        {
            stream->utf8Format.append(utf8Format);
            maybeSpace();
        }
        return *this;
    }

    // considered as Item if !Stream::NoQuotes
    inline LogMessage& operator<<(const QString& t)
    {
        putString(t);
        maybeSpace();
        return *this;
    }
    // TODO inline LogMessage &operator<<(const QStringRef & t) { putString(t); return maybeSpace(); }
    inline LogMessage& operator<<(QLatin1String t)
    {
        putLatin1String(t);
        maybeSpace();
        return *this;
    }
    inline LogMessage& operator<<(const QByteArray& t)
    {
        putByteArray(t);
        maybeSpace();
        return *this;
    }

    // considered as Item
    template <typename T>
    inline LogMessage& operator<<(const NamedArg<T>& t)
    {
        if (!stream->options.testFlag(NoLiterals))
        {
            stream->argNames.append(toName(t.m_literal)); // usual operator<<(T) must be called after!
            operator <<(t.m_literal);
        }
        operator <<(t.m_value);
        return *this;
    }

    template<class T>
    inline LogMessage& operator<<(const T& t)
    {
        putItem(t);
        maybeSpace();
        return *this;
    } // BEWARE putItem is necessary to other operator<< to avoid infinite recursive calls

    inline LogMessage& operator<<(QTextStreamFunction f) { if (stream->ts)      f(*stream->ts); return *this; }
    inline LogMessage& operator<<(QTextStreamManipulator m) { if (stream->ts) m.exec(*stream->ts); return *this; }

    /// <summary> for use by putItems(...) </summary>
    inline void putItems() {}

    /// <summary> for use by multiple __VA_ARGS__ </summary>
    template<typename TFirstArg, typename... TArgs>
    void putItems(const TFirstArg& first, const TArgs&... rest)
    {
        putItem(first, true);
        putItems(rest...); // recursive call to one of the 3 templates using pack expansion syntax
    }

    QByteArray format() const;
    inline QVector<QSharedPointer<data::IBindable>> args() const { return stream->args; }
    inline QVector<QByteArray> argTypes() const { return stream->argTypes; }
    inline QVector<QByteArray> argNames() const { return stream->argNames; }
};

class LogMessageStateSaverPrivate;
class MODMED_API LogMessageStateSaver
{
public:
    LogMessageStateSaver(LogMessage& dbg);
    ~LogMessageStateSaver();
private:
    Q_DISABLE_COPY(LogMessageStateSaver)
    QScopedPointer<LogMessageStateSaverPrivate> d;
};

inline LogMessage& LogMessage::operator=(const LogMessage& other)
{
    if (this != &other)
    {
        LogMessage copy(other);
        std::swap(stream, copy.stream);
    }
    return *this;
}

#if MODMED_USERS_REALLY_NEED_IT
// TODO in Bind.h

template<typename T>
inline LogMessage operator<<(LogMessage debug, const QFlags<T>& flags)
{
    // see qlogging.h
}

#endif

} // log
} // modmed

Q_DECLARE_OPERATORS_FOR_FLAGS(::modmed::log::LogMessage::Options)

Q_DECLARE_SHARED(::modmed::log::LogMessage)

MODMED_API QDebug operator<<(QDebug debug, const modmed::log::LogMessage& message);

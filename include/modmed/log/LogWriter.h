#pragma once

#include <QtCore/qscopedpointer.h>

#include <modmed/modmed_global.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>
#include <modmed/data/BindError.h>
#include <modmed/log/Log.h>

#include <modmed/log/ILogOutput.h>
#include <modmed/log/LogEventData.h>

namespace modmed {
namespace log {

template<class TDataWriter> class LogWriter : public ILogOutput
{
    Q_DISABLE_COPY(LogWriter)
public:
    LogWriter(QIODevice*   p_ioDevice, const QString& p_documentInformation = QString());
    LogWriter(FILE*        p_file    , const QString& p_documentInformation = QString());
    LogWriter(QString*     p_string  , const QString& p_documentInformation = QString());
    LogWriter(QTextStream* p_stream  , const QString& p_documentInformation = QString());

    virtual ~LogWriter();

    // TODO Refactor with TsvJson into a modmed::log function
    /// <summary> syslog positive integer values with 0 being the most severe </summary>
    //! \see https://en.wikipedia.org/wiki/Syslog#Severity_level
    static int severity(QtMsgType p_type);

    virtual bool output(const LogEventData& p_eventData);

    virtual void flush();

    void close();

private:
    // TODO Pimpl to batch 3 new in one
    // Keep this order for the destruction
    QScopedPointer<QTextStream> m_output;
    QScopedPointer<TDataWriter> m_writer;
    QScopedPointer<data::Sequence<data::Writer>> m_current;
};

template<class TDataWriter> inline LogWriter<TDataWriter>::LogWriter(QIODevice* p_ioDevice, const QString& p_documentInformation /*= QString()*/)
{
    m_output.reset(new QTextStream(p_ioDevice));
    m_output->setCodec("UTF-8");
    m_writer.reset(new TDataWriter(m_output.data(), p_documentInformation));
    m_current.reset(new data::Sequence<data::Writer>(m_writer->write().sequence()));
}

template<class TDataWriter> inline LogWriter<TDataWriter>::LogWriter(FILE* p_file, const QString& p_documentInformation /*= QString()*/)
{
    m_output.reset(new QTextStream(p_file, QIODevice::WriteOnly));
    m_output->setCodec("UTF-8");
    m_writer.reset(new TDataWriter(m_output.data(), p_documentInformation));
    m_current.reset(new data::Sequence<data::Writer>(m_writer->write().sequence()));
}

template<class TDataWriter> inline LogWriter<TDataWriter>::LogWriter(QString* p_string, const QString& p_documentInformation /*= QString()*/)
{
    m_output.reset(new QTextStream(p_string));
    m_output->setCodec("UTF-8");
    m_writer.reset(new TDataWriter(m_output.data(), p_documentInformation));
    m_current.reset(new data::Sequence<data::Writer>(m_writer->write().sequence()));
}

template<class TDataWriter> inline LogWriter<TDataWriter>::LogWriter(QTextStream* p_stream, const QString& p_documentInformation /*= QString()*/)
{
    if (p_stream->string())
        m_output.reset(new QTextStream(p_stream->string()));
    else if (p_stream->device())
        m_output.reset(new QTextStream(p_stream->device()));
    else
        m_output.reset(new QTextStream(new QString));
    m_output->setCodec("UTF-8");
    m_writer.reset(new TDataWriter(m_output.data(), p_documentInformation));
    m_current.reset(new data::Sequence<data::Writer>(m_writer->write().sequence()));
}

template<class TDataWriter> inline LogWriter<TDataWriter>::~LogWriter() {}

template<class TDataWriter> inline bool LogWriter<TDataWriter>::output(const LogEventData& p_eventData)
{
    auto logData = m_current->record()
                   .bind("_elapsed_s", (double)p_eventData.nsecsElapsed()/1000000000)
                   .bind("_timestamp", p_eventData.dateTime().toString(Qt::DateFormat::ISODate))
                   .bind("_severity",QString::number(p_eventData.type()))
                   .bind("_category", p_eventData.category())
                   .bind("_function", QLatin1String(p_eventData.function()))
                   .bind("_id", QLatin1String(p_eventData.id()))
                   .bind("_count", p_eventData.count())
                   .bind("_path", QLatin1String(p_eventData.file()))
                   .bind("_line", p_eventData.line())
                   .bind("_thread_id", QLatin1String(p_eventData.threadId()))
                   .bind("_format", QLatin1String(p_eventData.format()))
                   .bind("_args", p_eventData.args());

    auto seqData = logData.sequence("_arg_types");

    for (auto it = p_eventData.argTypes().cbegin(); it != p_eventData.argTypes().cend(); ++it)
    {
        seqData = seqData.bind(QLatin1String(*it));
    }

    seqData = seqData.out().sequence("_arg_names");

    for (auto it = p_eventData.argNames().cbegin(); it != p_eventData.argNames().cend(); ++it)
    {
        seqData = seqData.bind(QLatin1String(*it));
    }
    *m_current = seqData.out().out();

    data::BindErrors err;
    m_current->state()->trapErrors(&err);

    return err.isEmpty();
}

template<class TDataWriter> inline void LogWriter<TDataWriter>::flush()
{
    m_writer->flush();
    m_output->flush();
}

template<class TDataWriter> inline void LogWriter<TDataWriter>::close()
{
    flush();
    m_current.reset();
    m_writer.reset();
    m_output.reset();
}

template<class TDataWriter> inline int LogWriter<TDataWriter>::severity(QtMsgType p_type)
{
    switch (p_type)
    {
    case QtFatalMsg:    return 0;
    case QtCriticalMsg: return 2;
    case QtWarningMsg:  return 4;
    default:
    case QtInfoMsg:     return 6;
    case QtDebugMsg:    return 7;
    }
}


} // log
} // modmed



/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once
#include <modmed/modmed_global.h>

#include <QtCore/qdebug.h>

#include <modmed/log/ILogOutput.h>

#include <modmed/data/JsonWriter.h>

namespace modmed {
namespace log {

//! \warning Using this output with qInstallMessageHandler(ml::LogManager::qtMessageHandler); results in an infinite loop!
struct QtLogOutput : public ILogOutput
{
    QtLogOutput() {}

    virtual bool output(const LogEventData& p_eventData)
    {
        QString qtMsg = QString("%1\t%2\t%3\t%4")
                .arg(eventData.threadId())
                .arg(eventData.id())
                .arg(eventData.count())
                .arg(eventData.format());

        for (auto it = eventData.data().cbegin(); it != eventData.data().cend(); ++it)
            qtMsg += '\t' + data::JsonString().write().bind(*it).toString();

        if (isFatal(eventData.type()))
        {
            // Do not try to remove the warning about %ls and wchar_t*, its a "false positive".
            // See https://bugreports.qt.io/browse/QTBUG-37155
            // QString::sprintf doc says:
            // The %ls escape sequence expects a pointer to a zero-terminated array of unicode
            // characters of type char16_t, or ushort (as returned by QString::utf16()). This
            // is at odds with the printf() in the standard C++ library, which defines %lc to
            // print a wchar_t and %ls to print a wchar_t*, and might also produce compiler
            /// warnings on platforms where the size of wchar_t is not 16 bits.
            QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).fatal("%ls", qtMsg.utf16());
        }
        else
        {
            switch (eventData.type())
            {
            case QtDebugMsg:
                QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).debug("%ls", qtMsg.utf16());
                break;
            case QtWarningMsg:
                QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).warning("%ls", qtMsg.utf16());
                break;
            case QtCriticalMsg:
                QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).critical("%ls", qtMsg.utf16());
                break;
            case QtFatalMsg:
                QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).fatal("%ls", qtMsg.utf16());
                break;
            case QtInfoMsg:
                QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).info("%ls", qtMsg.utf16());
                break;
            default:
                QMessageLogger(eventData.file(), eventData.line(), eventData.function(), eventData.category()).info("%ls", qtMsg.utf16());
            }
        }
    }

    virtual void flush() {}
};

} // log
} // modmed

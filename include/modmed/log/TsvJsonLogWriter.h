/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <modmed/modmed_global.h>

#include <QtCore/qstring.h>
#include <QtCore/qfile.h>
#include <QtCore/qelapsedtimer.h>
#include <QtCore/qflags.h>
#include <QtCore/qdatetime.h>
#include <QtCore/qtextstream.h>
#include <QtCore/qstring.h>
class QIODevice;
class QByteArray;

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>
#include <modmed/data/JsonWriter.h>

#include <modmed/log/ILogOutput.h>
#include <modmed/log/LogEventData.h>

namespace modmed {
namespace log {

/// <summary> TSV+JSON-based log file format both human and software-readable </summary>
//!
//! The first line is a TSV header line identifying event data and metadata
//! \remark Columns headers between square braces denote columns where redundant values from a line and its previous line may have been removed
//! \warning This class is not thread-safe!
//!
//! Other lines contain a single event.
//! Possible event data are:
//! - _elapsed_s: elapsed seconds from log start in decimal format
//!   - decimal point may be changed from system to any arbitrary locale to facilitate reading with TSV readers such as Excel
//!   \see QElapsedTimer for monotonicity and precision
//! - _date_time: local date time in ISO format with offset from UTC
//!   - considered redundant if equal to previous dateTime + elapsedTime delta (up to 1 second)
//! - _severity: \see severity()
//! - _category: QLoggingCategory name
//! - _function: \a Q_FUNC_INFO
//! - _id: positive integer followed by ' (new)' text on 1st occurence
//!   - event severity, function, source path and line is considered redundant on future occurences
//! - _source_path, _source_line: QMessageLogContext
//! - _thread_id: system thread integer identifier in decimal format
//! - _metadata: JSON data coming from LogEvent
//! - _format: printf-like format string
//! - _0 _n: variable number of JSON message arguments (one per column)
//!
class MODMED_API TsvJsonLogWriter : public ILogOutput
{
    Q_DISABLE_COPY(TsvJsonLogWriter)
public:
    enum LogOption{
        HideCategory = 1,
        HideDateTime = 2,
        HideFileName = 4,
        HideFunction = 8,
        HideLine = 16,
        KeepRedundancy = 32, // CR May be changed to uint m_removeRedundantLines to set a number of lines after which some redundancy is useful (say 20 to always get one full line on screen)
    };
    Q_DECLARE_FLAGS(LogOptions, LogOption)

    static const qint64 DateTimeIntervalNSecs = 60000000000; /* 1min */

    // TODO Add a field with common QElapsedTimer::msecsSinceReference() to facilitate later merging of log files (an immediately following restart()<1000 should be given too so that both are within 1sec?)

    /// <summary> syslog positive integer values with 0 being the most severe </summary>
    //! \see https://en.wikipedia.org/wiki/Syslog#Severity_level
    static int severity(QtMsgType p_type);

    static const QByteArray s_nullString;

    /// <summary> Construct an TSVJSON serializer or deserializer from a QTextStream </summary>
    /// <param name="p_stream"> QTextStream that is written </param>
    /// <param name="p_options" Log options to hide catagories or to keep redundancy </param>
    TsvJsonLogWriter(QTextStream* p_stream, LogOptions p_options = LogOptions());

    /// <summary> Construct an TSVJSON serializer or deserializer from a QIODevice </summary>
    /// <param name="p_io"> QIODevice that is written </param>
    /// <param name="p_options" Log options to hide catagories or to keep redundancy </param>
    TsvJsonLogWriter(QIODevice* p_io, LogOptions p_options = LogOptions());

    /// <summary> Construct an TSVJSON serializer or deserializer from a FILE* </summary>
    /// <param name="p_file"> File handler that is written </param>
    /// <param name="p_options" Log options to hide catagories or to keep redundancy </param>
    TsvJsonLogWriter(FILE* p_file, LogOptions p_options = LogOptions());

    /// <summary> Construct an TSVJSON serializer or deserializer from a QString </summary>
    /// <param name="p_string"> QString that is written </param>
    /// <param name="p_options" Log options to hide catagories or to keep redundancy </param>
    TsvJsonLogWriter(QString* p_string, LogOptions p_options = LogOptions());

    ~TsvJsonLogWriter();

    /// <summary> Logs a single TSV+JSON line depending on locale and options </summary>
    //! \return false if writeStart failed
    virtual bool output(const LogEventData& p_eventData);

    virtual void flush() { m_streamPointer->flush(); }

    bool isValid() {return m_streamPointer->status() == QTextStream::Ok; }

private:
    void init();

    // TODO Pimpl to hide impl
    QTextStream* m_streamPointer;
    LogOptions   m_options;
    qint64       m_previousDateTimeNSecs = -DateTimeIntervalNSecs;

    // For redundancy check
    int     m_previousSeverity = -1;
    QString m_previousFunction;
    QString m_previousPath;
    int     m_previousLine = 999999999; // CR Choose better invalid value
    QString m_previousThreadId;
    bool    m_changedFile = false;      // CR Better define it locally in write?!
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TsvJsonLogWriter::LogOptions)

} // log
} // modmed

QT += network
QT -= gui widgets

TEMPLATE = lib
CONFIG += c++11 precompile_header debug_and_release
PRECOMPILED_HEADER = source/Precompiled.h
# source/log path needed by lttng :/
INCLUDEPATH += include source/log
DEFINES += BUILD_MODMED_LOG
unix:LIBS += -llttng-ust -ldl
unix:CONFIG += link_pkgconfig
unix:PKGCONFIG += libsystemd
msvc:LIBS += Advapi32.lib

QMAKE_CXXFLAGS += -pedantic -Wall

CONFIG(debug, debug|release) { DESTDIR = $$_PRO_FILE_PWD_/lib/Debug }
else { DESTDIR = $$_PRO_FILE_PWD_/lib/Release }

HEADERS += \
    include/modmed/data/Bind.h \
    include/modmed/data/Bind.hpp \
    include/modmed/data/Bindable.h \
    include/modmed/data/Bindable.hpp \
    include/modmed/data/BindDeclaration.h \
    include/modmed/data/BindState.h \
    include/modmed/data/BindState.hpp \
    include/modmed/data/Data.h \
    include/modmed/data/Data.hpp \
    include/modmed/data/IData.h \
    include/modmed/data/Summary.h \
    include/modmed/data/JsonReader.h \
    include/modmed/data/JsonWriter.h \
    include/modmed/data/XmlWriter.h \
    include/modmed/data/StringResult.h \
    include/modmed/data/StringResult.hpp \
    include/modmed/modmed_global.h \
    include/modmed/log/LogMessage.h \
    include/modmed/log/LogEvent.h \
    include/modmed/log/ILogOutput.h \
    include/modmed/log/ConsoleLogWriter.h \
    include/modmed/log/LogEventData.h \
    include/modmed/log/TsvJsonLogWriter.h \
    include/modmed/log/QtLogOutput.h \
    include/modmed/data/CborWriter.h \
    include/modmed/data/BindError.h \
    include/modmed/log/LogManager.h \
    include/modmed/log/LogWriter.h \
    include/modmed/log/log.h \
    source/data/cbor.h

unix {
HEADERS += \
    source/log/LogEventData_c.h \
    source/log/LttngTracepointProvider.h \
    include/modmed/log/LttngLogWriter.h
    include/modmed/log/JournaldWriter.h \
}

msvc {
HEADERS += \
    include/modmed/log/EtlLogWriter.h
}

SOURCES += \
    source/data/Bind.cpp \
    source/data/Bindable.cpp \
    source/data/BindError.cpp \
    source/data/BindState.cpp \
    source/data/Data.cpp \
    source/data/IData.cpp \
    source/data/Summary.cpp \
    source/data/JsonReader.cpp \
    source/data/JsonWriter.cpp \
    source/data/XmlWriter.cpp \
    source/log/LogMessage.cpp \
    source/log/LogEvent.cpp \
    source/log/TsvJsonLogWriter.cpp \
    source/log/LogEventData.cpp \
    source/data/CborWriter.cpp \
    source/log/LogManager.cpp

unix {
SOURCES += \
    source/log/LttngTracepointProvider.c \
    source/log/JournaldWriter.cpp \
    source/log/LttngLogWriter.cpp
}

msvc {
SOURCES += \
    source/log/EtlLogWriter.cpp
}

include(modmedLog.pri)

TEMPLATE=subdirs

SUBDIRS += modmedLog \
        TestData \
        TestLogger \
        TestLogger_exe \
        1_HowToLog \
        2_HowToLogUserData \
        3_Benchmark

CONFIG += debug_and_release

modmedLog.file = modmedLog.pro

TestData.subdir = tests/TestData
TestData.depends = modmedLog

TestLogger.subdir = tests/TestLogger
TestLogger.depends = modmedLog

TestLogger_exe.subdir = tests/TestLogger_exe
TestLogger_exe.depends = modmedLog

1_HowToLog.subdir = samples/1_HowToLog
1_HowToLog.depends = modmedLog

2_HowToLogUserData.subdir = samples/2_HowToLogUserData
2_HowToLogUserData.depends = modmedLog

3_Benchmark.subdir = samples/3_Benchmark
3_Benchmark.depends = modmedLog

torrent.subdir = samples/torrent
torrent.depends = modmedLog

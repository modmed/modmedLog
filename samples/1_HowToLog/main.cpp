#include <errno.h>
#include <iostream>
#include <limits>
#include <cmath>

#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QLibraryInfo>
#include <QtCore/QStorageInfo>
#include <QtCore/QThread>
#include <QtCore/QMutex>
#include <QtCore/QMutexLocker>

#include <QtCore/qstring.h>
#include <QtCore/qlocale.h>
#include <QtCore/qtextstream.h>
#include <QtCore/qiodevice.h>

#include <modmed/data/Bind.h>

#include <modmed/log/TsvJsonLogWriter.h>
//#include <modmed/log/LttngLogWriter.h>
#include <modmed/log/LogEvent.h>
#include <modmed/log/LogMessage.h>
#include <modmed/log/LogManager.h>
namespace ml = ::modmed::log;
namespace md = ::modmed::data;

#include "module/service.h"

M_NAMESPACE_LOG_CATEGORY()

static QTextStream mOut(stdout);

//! Prints some T toPrint
template<typename T>
void print(const T& toPrint)
{
    mNTrace(toPrint); // one id per T (with identical file and line)
    mOut <<endl << toPrint;
}

//! Computes goldenRatio up to epsilon (with a trace for every step when debugEnabled)
double goldenRatio(bool debugEnabled = true)
{
    mNTrace(debugEnabled);

    double result = 0.;
    mNTraceOut(result); // however the function exits...

    // Fibonacci terms
    unsigned int previous = 1, current = 1;
    while (std::abs((double(current) / previous) - result) > std::numeric_limits<double>::epsilon() // max precision not reached
           && previous <= current) // no integer overflow
    {
        if (debugEnabled)
            mNTrace3(current, previous, result);

        result = double(current) / previous;
        unsigned int next = previous + current;
        previous = current;
        current = next;
    }
    return result;
}

int main(int argc, char *argv[])
{
    Q_UNUSED(argc);
    mDebug() << "Hi there!"; // log to stderr by default

    // Configure log
    QFile logFile("log.tsv");
    if (!logFile.open(QIODevice::WriteOnly|QIODevice::Truncate))
        return -1;

    QTextStream sfile(&logFile);
    ml::TsvJsonLogWriter myLog(&sfile); // , ml::TsvJsonLogWriter::KeepRedundancy | ml::TsvJsonLogWriter::HideFileName);
//    ml::LttngLogWriter myLog;
    ml::LogManager::getInstance().setOutput(&myLog);

    // Or on UNIX platforms...
    //ml::JournaldWriter myJournal;
    //ml::LogManager::getInstance().setOutput(&myJournal);

    qInstallMessageHandler(ml::LogManager::qtMessageHandler); // to capture qDebug() and the like

    QCoreApplication app(argc, argv);

    QDir logDir(logFile.fileName()+"_attachments");
    logDir.removeRecursively();

    mNTrace(QString(argv[0])); // useful convention for first event logged, see below for explanations

    QLocale myLocale(QLocale::French, QLocale::France);

    // TsvJsonLogWriter our own data using various styles

    // Old-style:
    mNDebug("C-style logging is %s and %s", "not type-safe (may crash!)", "not extensible to user types");
    mNDebug() << "Hello" << myLocale << QString("people!"); // usual way but less convenient and precise than below...

    // For general use:
    mNInfo()     << "started demonstration to the users"; // for auditability purposes!
    mNWarning()  << "unexpected or badly evolving condition:"
                 << "free storage left (MB)" << QStorageInfo::root().bytesFree()/1024/1024;
    mNCritical() << "failure affecting the user:" << QString("unable to make coffee!");

    // For developer use:
    mNTrace(md::Hex(&sfile)); // sfile address is logged using hexadecimal notation
    mNTrace(myLocale);
    // Wherever you could throw an exception, return an error code, or set a global error number
    // You can at least trace an error (with severity=Warning):
    errno = EACCES;
    mNAssumption(errno != 0);

    // All trace points get a short unique id that will help filtering and exploiting logs
    print(10);
    print(QString("plop"));
    print(QString("blip"));
    print(42);

    mOut <<endl << "Beware trace formatting in CPU-bound loops severely degrades performance as can be seen in logs";
    mOut <<endl << goldenRatio()     ;
    mOut <<endl << goldenRatio(false);

    for (int i=0; i<250; ++i)
        mNTraceModulo(100, i);

    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    for (double elapsed(0); elapsed<1000*ms; elapsed=t.nsecsElapsed()) {
        mNTraceNSecs(20*ms, std::sin(elapsed/(100*ms)));
        QThread::msleep(19); // slightly faster than sampling
    }

    // Logging information about the host, operating system and application is easy thanks to <modmed/data/Bind.h>
    mNInfo() <<ml::OBJECT_HOST    <<mExpr2( QSysInfo::machineHostName()         , QHostInfo::localDomainName()             );
    mNInfo() <<ml::OBJECT_SYSTEM  <<mExpr2( QSysInfo::productType()             , QSysInfo::productVersion()               );
    mNInfo() <<ml::OBJECT_PROCESS <<mExpr2( QCoreApplication::applicationPid()  , QProcessEnvironment::systemEnvironment() );
    mNInfo() <<ml::OBJECT_APP     <<mExpr ( qApp                                                                           );
    mNInfo() <<ml::OBJECT_LIBRARY <<mExpr ( QLibraryInfo::location(QLibraryInfo::BinariesPath)                             );

    // Now, let us use a service that will log its own things
    // Let us say we need to make sure the Service processing does not block our own processing
    auto worker = new QThread(qApp);
    worker->start();

    auto service = new acme::Service(); // deletion will be scheduled below
    service->moveToThread(worker);

    auto connect1 = QObject::connect(service, SIGNAL(processed(QString,QString)), service, SLOT(process(QString))/*, Qt::QueuedConnection*/);
    // uncomment Qt::QueuedConnection to fix recursive process calls

    auto connect21 = QObject::connect(service          , SIGNAL(rejected())         , service          , SLOT(deleteLater()));
    auto connect22 = QObject::connect(service          , SIGNAL(destroyed(QObject*)), service->thread(), SLOT(quit()));
    auto connect23 = QObject::connect(service->thread(), SIGNAL(finished())         , qApp             , SLOT(quit()));

    auto start = QMetaObject::invokeMethod(service, "process", Qt::DirectConnection, Q_ARG(QString, "."));
    // change to Qt::AutoConnection to fix thread assumption failure

    if (!mNRequirement(start && connect1 && connect21 && connect22 && connect23))
    {
        // Contrary to Q_ASSERT, failing mRequirement is always logged
        // and it let you do something useful to your user!
        mOut <<endl << "Please contact your support with information below!";
    }
    else
    {
        mNTrace(app.exec());
    }

    mOut <<endl << "See log file:" << logFile.fileName();
    mOut <<endl;
    return 0;
}

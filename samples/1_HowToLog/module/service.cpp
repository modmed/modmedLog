#include "service.h"

#include <modmed/log/LogMessage.h>
#include <modmed/log/LogEvent.h>
#include <QtCore/QThread>
namespace ml = ::modmed::log;

namespace acme {

M_NAMESPACE_LOG_CATEGORY()

Service::Service(QObject *parent) : QObject(parent)
    , m_submitted(0), m_processed(0), m_rejected(0)
{
    Q_ASSERT(m_submitted == 0 && m_rejected  == 0 && m_processed == 0);
}

Service::~Service()
{
    mNRequirement4(m_submitted == m_processed + m_rejected, m_processed, m_rejected, m_submitted);
    // This dynamic requirement cannot be verified statically or by only testing!
    // It depends a lot on the lifetime and thread affinity which is controlled by Service user
}

void Service::process(QString input)
{
    mNTraceQObject2(this,input);
    mNTraceOut3(m_submitted, m_rejected, m_processed);

    mNAssumption(QThread::currentThread()==thread());
    // Service is not thread-safe and should only be called from this->thread()!

    ++m_submitted;
    if (!mNAssumption2(input.size() <= 32, input)) {
        ++m_rejected;
        emit rejected(); // emitting this is a requirement that cannot be checked in code but can be checked in logs...
    }
    else {
        // ++m_processed; // uncomment to fullfill requirements

        QString result(input.repeated(2));
        mNRequirement2(result.size() == 2*input.size(), input);
        mNRequirement2(result.contains(input), input);

        emit processed(result, input);
    }
}

} // acme

#pragma once

#include <QObject>

#include <modmed/data/Bind.h>

namespace acme {

class Service : public QObject
{
    Q_OBJECT
public:
    explicit Service(QObject *parent = 0);
    ~Service();

signals:
    void processed(QString output, QString forInput);
    void rejected();

public slots:
    void process(QString input);

private:
    uint m_submitted;
    uint m_processed;
    uint m_rejected;
};

}

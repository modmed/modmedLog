#include <iostream>

#include <QtCore/qstring.h>
#include <QtCore/qtextstream.h>
#include <QtCore/qdatetime.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>
#include <modmed/data/JsonWriter.h>
#include <modmed/data/JsonReader.h>
#include <modmed/data/XmlWriter.h>
namespace md = ::modmed::data;

// The following include contains some user-defined data types with appropriate bind() functions defined
#include "userdata.h"

static QTextStream mOut(stdout);

int main()
{
    mOut <<endl << md::JsonString().write()
        .sequence()
            .record()
                .bind("number", 2)
        .end().toString();

    // 1st, let us write some Json data structure fluently in source code
    mOut <<endl << md::JsonString().write()
        .sequence()
            .bind("free text")
            .record()
                .bind("number", 2)
                .bind("the number", 42)
                .sequence("sequence")
                    .bind(true)
                    .bind(Meters{1.23f})
        .end().toString(); // let data automagically format valid Json for us

    QVector<Person> persons;
    persons.push_back( Person { "John", "Doe", 1953, 1.78f } );
    persons.push_back( Person { "Jane", "Doe", 2013, 1.12f } );

    //! \remark Bind<Op, T> works identically for JSON, XML, etc. "physical" IItem implementations
    mOut <<endl << md::JsonString().write().bind(persons.front()).toString();
    //! \remark // JSON numbers are not used since they do not trivially map to a single type in most languages (C++, Python, ...)

    // JsonWriter and XmlWriter, which are based on QTextStream provide flexibility
    // can write into any FILE, QIODevice, or QString
    QString s;
    QTextStream buffer(&s);
    {
        md::JsonWriter jsonWriter(&buffer);
        auto iter = jsonWriter.write().sequence();
        // write() is equivalent to getting an iterator, and gives access to the fluent interface
        Q_FOREACH(Person p, persons)
        {
            iter = iter.record()
                .bind("id", md::Hex(&p))
                .bind("person", p)
                .bind("canVote", 18 < QDateTime::currentDateTime().date().year() - p.m_yearOfBirth)
                .out()
                // NB: the fluent interface tracks nested structures in C++ types
                // so we cannot reassign iter before getting out of the record()!
                ;
        };
    }
    mOut <<endl << s;
    mOut <<endl;

    // Reading our Json buffer and translate the date structure to XML is a one-liner
    // Since each "logical" data structure follow a common model, they can be bound together:
//    buffer.seek(0);
//    md::XmlWriter(&mOut).write().bind(
//        md::JsonReader(&buffer).read());

//    mOut <<endl;


    mOut <<endl << md::XmlString().write()
        .sequence()
            .record()
                .bind("texttemoin", "thetemoin")
                .bind("number", 2)
                .bind("booleantest", false)
        .end().toString();

    // 1st, let us write some Json data structure fluently in source code
    mOut <<endl << md::XmlString().write()
        .sequence()
            .bind("free text")
            .record()
                .bind("number", 2)
                .bind("the number", 42)
                .sequence("sequence")
                    .bind(true)
                    .bind(Meters{1.23f})
        .end().toString(); // let data automagically format valid Json for us

    QString s2;
    QTextStream buffer2(&s2);
    {
        md::JsonWriter jsonWriter(&buffer2);
        auto iter = jsonWriter.write().sequence();
        iter = iter.record()
                .bind("atxt", "withtxt")
                .bind("canVote", true)
                .bind("cannotVote", false)
                .out()
                // NB: the fluent interface tracks nested structures in C++ types
                // so we cannot reassign iter before getting out of the record()!
                ;
    }
    mOut <<endl << s2;
    mOut <<endl;

    // Reading our Json buffer and translate the date structure to XML is a one-liner
    // Since each "logical" data structure follow a common model, they can be bound together:
    buffer2.seek(0);
    md::XmlWriter(&mOut).write().bind(
        md::JsonReader(&buffer2).read());

    mOut <<endl;

    return 0;
}

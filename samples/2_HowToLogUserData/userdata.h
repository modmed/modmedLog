#pragma once

#include <modmed/data/BindError.h>

/// <summary> Atomic data types can mean more than raw number types and \b data will allow to convey this meaning </summary>
//! \warning template specialization does not work with simple typedefs, so...
struct Meters { float meters; };

/// <summary> A constant text used for reading/writing Meters </summary>
static const QString MetersTag("m");

namespace modmed {
namespace data {

//! \see BindAtom, Operator
//! \remark External definition of BindAtom::bind is possible with any type!
//! \warning Atomic data can almost never be bound the same way for Reader and Writer as it generally uses specialized APIs for reading or writing
//! \remark The point of defining BindAtom<Reader> once for all is to:
//!  - provide robust consistency checks
//!  - allow defining flexible Reader definitions as below
//!  - remove boilerplate code everywhere else
//! \warning No space allowed in \b atom!
template<>
void Bind<Reader,Meters>::bind(Item<Reader> item, Meters& value)
{
    QString atom;
    item.bind(atom);
    QTextStream reader(&atom);
    reader >> value.meters;
    if (reader.status() != QTextStream::Ok) {
        item.state()->addError(FailBindFrom(atom,QString("Misses a number")));
    }
    else {
        QString tag;
        reader >> tag;
        if (reader.status() != QTextStream::Ok || tag != MetersTag) {
            item.state()->addError(FailBindFrom(atom,QString("Misses ")+MetersTag+" at the end"));
        }
    }
}

/// <summary> Bind Meters as an Atom inside messages </summary>
//! \remark The point of defining BindAtom<Writer> once for all is to:
//!  - remove boilerplate code everywhere else
//!  - provide consistent formatting
//! \warning It is best to define seemingly trivial BindAtom<Writer> after BindAtom<Reader> to be sure it can be read again unambiguously
template<>
void Bind<Writer,Meters>::bind(Item<Writer> item, Meters& value)
{
    item.bind(QString::number(value.meters,'f',2) + MetersTag);
}

} // data
} // modmed

struct Person
{
    QString m_firstName;
    QString m_lastName;
    int m_yearOfBirth;
    Meters m_height;

    /// \remark Internal definition of Item::bind is simpler than external ones
    //! \remark As is the case for most user-defined types, Operator can be left unspecified as Item is designed to implement \b Reader and \b Writer exactly the same
    //! \remark Item ensures robust reading and will return all bind errors that happened at any level
    template<modmed::data::Operator Op>
    void bind(modmed::data::Item<Op> data) {
        // Item provides a fluent interface that helps a lot for defining Reader/Writer as it allows
        // writing data structures directly in C++, reusing already defined Bind<Reader/Writer, T>
        data.record()
            .sequence("name")
                .bind(m_firstName)
                .bind(m_lastName)
                .out()                           // closes the message and get out to the enclosing record
            .bind("birth_year", m_yearOfBirth)   // uses pre-defined Bind<> as is the case for most std and Qt types
            .bind("height"      , m_height)      // uses BindAtom<Reader/Writer> previously defined
            ; // automagically closes all opened structures
    }
};

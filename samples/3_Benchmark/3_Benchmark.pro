QT += core network
QT -= gui

TEMPLATE = app
TARGET = 3_Benchmark
CONFIG += c++11 debug_and_release
CONFIG -= app_bundle console

INCLUDEPATH += ../../include

CONFIG(debug, debug|release) { DESTDIR = $$_PRO_FILE_PWD_/bin/Debug }
else { DESTDIR = $$_PRO_FILE_PWD_/bin/Release }

CONFIG(debug, debug|release) {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Debug
} else {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Release
}

CONFIG(debug, debug|release) { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Debug }
else { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Release }

CONFIG(release, debug|release) { DEFINES += QT_MESSAGELOGCONTEXT}

LIBS += -lmodmedLog

SOURCES += main.cpp

HEADERS += ConsoleLogWriterBenchmark.h\

include(../../modmedLog.pri)

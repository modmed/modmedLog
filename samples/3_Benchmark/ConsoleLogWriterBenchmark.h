/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once
#include <modmed/modmed_global.h>

#ifdef Q_OS_WIN
#include <QtCore/qt_windows.h>
#endif

#include <modmed/log/LogEventData.h>
#include <modmed/log/ILogOutput.h>

#include <modmed/data/Summary.h>
namespace md = ::modmed::data;

namespace modmed {
namespace log {

//==============================================================================

/// <summary> Does the same as qDefaultMessageHandler with structured LogEventData </summary>
//!
struct ConsoleLogWriterBenchmark : public ILogOutput
{
    /// <summary> Writes a fixed-length header with as much category and function characters as possible </summary>
    //! \remark Limits category to the first 4 characters and function to the first 14 characters after return type if necessary
    virtual bool output(const LogEventData& p_eventData)
    {
        QString message = QString::fromUtf8(p_eventData.format()+' ');
        {
            md::Summary(&message).write().bind(p_eventData.args());
        }
        message = qFormatLogMessage(p_eventData.type(), QMessageLogContext(p_eventData.file(), p_eventData.line(), p_eventData.function(), p_eventData.category()), message);

        // print nothing if message pattern didn't apply / was empty.
        // (still print empty lines, e.g. because message itself was empty)
        if (message.isNull())
            return false;

#if defined(Q_OS_WIN)
        message.append(QLatin1Char('\n'));
        OutputDebugString(reinterpret_cast<const wchar_t *>(message.utf16()));
#else
        fprintf(stderr, "%s\n", message.toLocal8Bit().constData());
        fflush(stderr);
#endif
        return true;
    }

    virtual void flush() {}
};

} // log
} // modmed

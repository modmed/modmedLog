// NB: It is not possible to use QBENCHMARK to item qDebug because it installs a specific handler

#include <QtCore/qtemporarydir.h>
#include <QtCore/qbuffer.h>
#include <QtCore/qstring.h>

#include <modmed/data/Bind.h>

#include <modmed/data/JsonWriter.h>
#include <modmed/data/XmlWriter.h>
#include <modmed/data/CborWriter.h>

#include <modmed/log/LogEvent.h>
#include <modmed/log/LogMessage.h>
#include <modmed/log/LogManager.h>

#include <modmed/log/TsvJsonLogWriter.h>
#include <modmed/log/LogWriter.h>
#ifdef Q_OS_UNIX
#include <modmed/log/LttngLogWriter.h>
#include <modmed/log/JournaldWriter.h>
#endif
#ifdef Q_CC_MSVC
#include <modmed/log/EtlLogWriter.h>
#endif

#include <ConsoleLogWriterBenchmark.h>
QFile  qtc( "qtc.txt");
QFile fqtc("fqtc.txt");
void qtcMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    qtc.write(qFormatLogMessage(type, context, msg).toUtf8()+'\n');
}
void fqtcMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    fqtc.write(qFormatLogMessage(type, context, msg).toUtf8()+'\n');
}

// NB: Add QT_NO_DEBUG_OUTPUT to DEFINES in pro files to test max disabled output

using namespace modmed;
using namespace log;

#define GROUP(group) { \
    qint64 previousTotal=0, groupTotal=0; \
    bool groupWarmup; \
    do { \
    groupWarmup=(previousTotal==0 || abs(groupTotal-previousTotal)*100/previousTotal > 1); \
    previousTotal=groupTotal; \
    groupTotal=0; \
    if (!previousTotal) { fprintf(results,"group"); fprintf(samples, "=== " group " ========================================\n"); } else fprintf(results,group);

#define GROUP_STOP \
    if (!previousTotal) fprintf(results,",total(usecs),variation(%%)\n"); else fprintf(results,",%10lld,%3lld\n", groupTotal, previousTotal ? abs(groupTotal-previousTotal)*100/previousTotal : 0); \
    } while (groupWarmup); }

QElapsedTimer item;
#define START item.start(); for (int i=0; i<123; ++i)
#define STOP(label,result) { \
    auto usecs=item.nsecsElapsed()/1000/123; groupTotal+=usecs; \
    if (!previousTotal) { fprintf(results,"," label); fprintf(samples, "--- " label " ---------------------------------------\n%s\n", qPrintable(result)); } else fprintf(results,",%10lld", usecs); }

int main()
{
    // Generated files
    FILE* results = fopen("results.csv", "w");
    FILE* samples = fopen("samples.txt", "w");
    if (!results) return -1;
    if (!samples) return -1;

    QFile  tsv ("modmed.tsv");
    QFile  json("modmed.json");
    QFile  xml ("modmed.xml");
    QFile ftsv ("fmodmed.tsv");
    QFile fjson("fmodmed.json");
    QFile fxml ("fmodmed.xml");

    if (! qtc .open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (! tsv .open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (! json.open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (! xml .open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (!fqtc .open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (!ftsv .open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (!fjson.open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;
    if (!fxml .open(QIODevice::ReadWrite|QIODevice::Truncate)) return -1;

    // Read-only args
    QString text("..ascii characters + U+A4 ¤ U+B0 ° U+D8 Ø U+FF ÿ..");
    QMap<char,int> map;
    Q_FOREACH(char c, QByteArray("123456789")) {
        map[c] = c-'0';
    }
    double transform[] = {1./3, 2./3, 1./3, 1.,
                          2./3, 1./3, 2./3, 1.,
                          1./3, 2./3, 1./3, 1.,
                          0.  , 0.  , 0.  , 1.};

    // Temporary buffers
    QString s;
    QBuffer b;
    b.open(QIODevice::WriteOnly);

    GROUP("Format << atoms")
    {
        START {
            s.clear();
            QDebug(&s)
                << '['
                << ',' << true
                << ',' << 12
                << ',' << 1.333333333333f
                << ',' << text
                << ']'
                ;
        }
        STOP("QDebug",s);
        START {
            b.seek(0); b.buffer().clear();
            data::JsonWriter data(&b, QString(), 0, '\0');
            data.write().sequence()
                .bind(true)
                .bind(2)
                .bind(1.23f)
                .bind(text)
                ;
        }
        STOP("Json",QString::fromUtf8(b.buffer()));
        START {
            b.seek(0); b.buffer().clear();
            data::XmlWriter data(&b);
            data.write().sequence()
                .bind(true)
                .bind(2)
                .bind(1.23f)
                .bind(text)
                ;
        }
        STOP("Xml",QString::fromUtf8(b.buffer().replace("\n   ","")))
        START {
            b.seek(0); b.buffer().clear();
            data::CborWriter data(&b);
            data.write().sequence()
                .bind(true)
                .bind(2)
                .bind(1.23f)
                .bind(text)
                ;
        }
        STOP("Cbor",b.buffer().toHex());
    }
    GROUP_STOP;
    GROUP("Format << map")
    {
        START {
            s.clear();
            QDebug(&s)
                << map
                ;
        }
        STOP("QDebug",s);
        START {
            b.seek(0); b.buffer().clear();
            data::JsonWriter data(&b, QString(), 0, '\0');
            data.write()
                .bind(map)
                ;
        }
        STOP("Json",QString::fromUtf8(b.buffer()));
        START {
            b.seek(0); b.buffer().clear();
            data::XmlWriter data(&b);
            data.write()
                .bind(map)
                ;
        }
        STOP("Xml",QString::fromUtf8(b.buffer().replace("\n   ","")))
        START {
            b.seek(0); b.buffer().clear();
            data::CborWriter data(&b);
            data.write()
                .bind(map)
                ;
        }
        STOP("Cbor",b.buffer().toHex());
    }
    GROUP_STOP;
    GROUP("Format << floats")
    {
        START {
            s.clear();
            QDebug data(&s);
            data << '[';
            for (int i=0; i < 16; i++) {
            data << transform[i] << '|';
            };
        }
        STOP("QDebug",s);
        START {
            b.seek(0); b.buffer().clear();
            data::JsonWriter data(&b, QString(), 0, '\0');
            data.write()
                .bind(transform);
        }
        STOP("Json",b.buffer())
        START {
            b.seek(0); b.buffer().clear();
            data::XmlWriter data(&b);
            data.write()
                .bind(transform);
        }
        STOP("Xml",b.buffer().replace("\n",""))
        START {
            b.seek(0); b.buffer().clear();
            data::CborWriter data(&b);
            data.write()
                .bind(transform);
        }
        STOP("Cbor",b.buffer().toHex())
    }
    GROUP_STOP
    GROUP("LogMessage << literal")
    {
        START {
            s.clear();
            QDebug(&s) << "only fixed literal text";
        }
        STOP("QDebug",s)
        START {
            auto m = LogMessage() << "only fixed literal text";
            b.buffer() = m.format();
        }
        STOP("BindableCopy",b.buffer())
        START {
            b.seek(0); b.buffer().clear();
            auto m = LogMessage() << "only fixed literal text";
            data::JsonWriter json(&b, QString(), 0, '\0');
            json.write().bind(m.args());
        }
        STOP("Json",b.buffer())
        START {
            b.seek(0); b.buffer().clear();
            auto m = LogMessage() << "only fixed literal text";
            data::XmlWriter json(&b);
            json.write().bind(m.args());
        }
        STOP("Xml",b.buffer().replace("\n",""))
        START {
            b.seek(0); b.buffer().clear();
            auto m = LogMessage() << "only fixed literal text";
            data::CborWriter json(&b);
            json.write().bind(m.args());
        }
        STOP("Cbor",b.buffer().toHex())
    }
    GROUP_STOP
    GROUP("LogMessage << args")
    {
        START {
            s.clear();
            QDebug(&s)
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("QDebug",s)
        START {
            auto m = LogMessage()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
            b.buffer() = m.format();
        }
        STOP("BindableCopy",b.buffer())
        START {
            auto m = LogMessage()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
            b.seek(0); b.buffer().clear();
            data::JsonWriter json(&b, QString(), 0, '\0');
            json.write().bind(m.args());
        }
        STOP("Json",b.buffer())
        START {
            auto m = LogMessage()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
            b.seek(0); b.buffer().clear();
            data::XmlWriter json(&b);
            json.write().bind(m.args());
        }
        STOP("Xml",b.buffer().replace("\n",""))
        START {
            auto m = LogMessage()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
            b.seek(0); b.buffer().clear();
            data::CborWriter json(&b);
            json.write().bind(m.args());
        }
        STOP("Cbor",b.buffer().toHex())
    }
    GROUP_STOP
    GROUP("LogMessage << floats")
    {
        START {
            s.clear();
            QString formattedList('[');
            for (size_t i=0; i<16; i++) {
                 formattedList.append(QString::number(transform[i])).append('|');
            }
            QDebug(&s)
                << "a transform" << formattedList
                ;
        }
        STOP("QDebug",s)
        START {
            auto m = LogMessage()
                << "a transform" << transform;
            b.buffer() = m.format();
        }
        STOP("BindableCopy",b.buffer())
        START {
            auto m = LogMessage()
                << "a transform" << transform
                ;
            b.seek(0); b.buffer().clear();
            data::JsonWriter json(&b, QString(), 0, '\0');
            json.write().bind(m.args());
        }
        STOP("Json",b.buffer())
        START {
            auto m = LogMessage()
                << "a transform" << transform
                ;
            b.seek(0); b.buffer().clear();
            data::XmlWriter json(&b);
            json.write().bind(m.args());
        }
        STOP("Xml",b.buffer().replace("\n",""))
        START {
            auto m = LogMessage()
                << "a transform" << transform
                ;
            b.seek(0); b.buffer().clear();
            data::CborWriter json(&b);
            json.write().bind(m.args());
        }
        STOP("Cbor",b.buffer().toHex())
    }
    GROUP_STOP
    GROUP("LogEvent << literal")
    {
        qtc.resize(0);
        qSetMessagePattern("%{time} %{type} %{category} %{file}(%{line}) %{function} %{threadid} |");
        START {
            QDebug(&qtc)
                << qFormatLogMessage(QtDebugMsg, QMessageLogContext(__FILE__, __LINE__, Q_FUNC_INFO, ""), "")
                << "only fixed literal text"
                << endl;
        }
        qtc.seek(0);
        STOP("QDebug+Context",qtc.readLine())

        static LogEvent e(QtDebugMsg); // stores fixed context once and for all (including literals but not thread_id...)

        tsv.resize(0);
        TsvJsonLogWriter tsvw(&tsv);
        LogManager::getInstance().setOutput(&tsvw);
        mDebug() << "write header before START to avoid counting it";
        tsvw.flush();
        START {
            e.message()
                << "only fixed literal text"
                ;
        }
        STOP("TsvJson",qPrintable(tsv.fileName()))

        json.resize(0);
        LogWriter<data::JsonWriter> jsonw(&json);
        LogManager::getInstance().setOutput(&jsonw);
        START {
            e.message()
                << "only fixed literal text"
                ;
        }
        jsonw.flush();
        json.seek(0);
        STOP("Json",qPrintable(json.fileName()))

        xml.resize(0);
        LogWriter<data::XmlWriter> xmlw(&xml);
        LogManager::getInstance().setOutput(&xmlw);
        START {
            e.message()
                << "only fixed literal text"
                ;
        }
        xmlw.flush();
        xml.seek(0);
        STOP("Xml",qPrintable(xml.fileName()))

#ifdef Q_OS_UNIX
        log::LttngLogWriter lttngw;
        LogManager::getInstance().setOutput(&lttngw);
        START {
            e.message()
                << "only fixed literal text"
                ;
        }
        STOP("LTTng","see system log")

        log::JournaldWriter journaldw;
        LogManager::getInstance().setOutput(&journaldw);
        START {
            e.message()
                << "only fixed literal text"
                ;
        }
        STOP("Journald","see system log")
#elif Q_CC_MSVC
        log::EtlLogWriter etlw;
        LogManager::getInstance().setOutput(&etlw);
        START {
            e.message()
                << "only fixed literal text"
                ;
        }
        STOP("ETL","see system log")
#endif

        LogManager::getInstance().setOutput(nullptr);
    }
    GROUP_STOP
    GROUP("mDebug << literal")
    {
        qSetMessagePattern("%{type} %{category} %{function} | %{message}");
        START {
            qDebug() << "only fixed literal text";
        }
        STOP("qDebug","debug | only fixed literal text")

        ConsoleLogWriterBenchmark defaultw;
        LogManager::getInstance().setOutput(&defaultw);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("~qDefaultMessageHandler","debug | only fixed literal text []")

        ConsoleLogWriter consolew;
        LogManager::getInstance().setOutput(&consolew);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("Summary","_|default  |         main(void)| only fixed literal text")
    }
    GROUP_STOP
    GROUP("mDebug << args")
    {
        qSetMessagePattern("%{type} %{category} %{function} | %{message}");
        START {
            qDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("qDebug","debug | a integer 12 a boolean true a decimal 1.33333 a text \"..ascii characters + U+A4 ¤ U+B0 ° U+D8 Ø U+FF ÿ..\" a map QMap((1, 1)(2, 2)(3, 3)(4, 4)(5, 5)(6, 6)(7, 7)(8, 8)(9, 9))")

        ConsoleLogWriterBenchmark defaultw;
        LogManager::getInstance().setOutput(&defaultw);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("~qDefaultMessageHandler","debug | a integer %s a boolean %s a decimal %s a text %s a map %s [12 | true | 2 more | value:9]]")

        log::ConsoleLogWriter consolew;
        LogManager::getInstance().setOutput(&consolew);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("Summary","_|default  |         main(void)| a integer 12 a boolean true a decimal 1.33333 a text ..ascii characters + U+A4 ¤ U+B0 ° U+D8 Ø U+FF ÿ.. a map [{} | {} | 6 more | value:9}]")
    }
    GROUP_STOP
    GROUP("mDebug << floats")
    {
        qSetMessagePattern("%{type} %{category} %{function} | %{message}");
        START {
            QString formattedList('[');
            for (size_t i=0; i<16; i++) {
                 formattedList.append(QString::number(transform[i])).append('|');
            }
            qDebug()
                << "a transform" << formattedList
                ;
        }
        STOP("qDebug","debug | a transform \"[0.333333|0.666667|0.333333|1|0.666667|0.333333|0.666667|1|0.333333|0.666667|0.333333|1|0|0|0|1|\"")

        ConsoleLogWriterBenchmark defaultw;
        LogManager::getInstance().setOutput(&defaultw);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("~qDefaultMessageHandler","debug | a transform %s [[]]") // FIXME

        ConsoleLogWriter consolew;
        LogManager::getInstance().setOutput(&consolew);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("Summary","_|default  |         main(void)| a transform [0.333333 | 0.666667 | 13 more | 1]")
    }
    GROUP_STOP
    GROUP("mDebug+File << literal")
    {
        qtc.resize(0);
        qInstallMessageHandler(qtcMessageHandler);
        qSetMessagePattern("%{time} %{type} %{category} %{file}(%{line}) %{function} %{threadid} | %{message}");
        START {
            qDebug()
                << "only fixed literal text"
                ;
        }
        qtc.seek(0);
        STOP("qDebug+Context",qtc.readLine())

        tsv.resize(0);
        TsvJsonLogWriter tsvw(&tsv);
        LogManager::getInstance().setOutput(&tsvw);
        mDebug() << "write header before START to avoid counting it";
        tsvw.flush();
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("TsvJson",qPrintable(tsv.fileName()))

        json.resize(0);
        LogWriter<data::JsonWriter> jsonw(&json);
        LogManager::getInstance().setOutput(&jsonw);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("Json",qPrintable(json.fileName()))

        xml.resize(0);
        LogWriter<data::XmlWriter> xmlw(&xml);
        LogManager::getInstance().setOutput(&xmlw);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("Xml",qPrintable(xml.fileName()))

#ifdef Q_OS_UNIX
        log::LttngLogWriter lttngw;
        LogManager::getInstance().setOutput(&lttngw);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("Lttng","see system log")

        log::JournaldWriter journaldw;
        LogManager::getInstance().setOutput(&journaldw);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("Journald","see system log")
#elif Q_CC_MSVC
        log::EtlLogWriter etlw;
        LogManager::getInstance().setOutput(&etlw);
        START {
            mDebug() << "only fixed literal text";
        }
        STOP("ETL","see system log")
#endif

        LogManager::getInstance().setOutput(nullptr);
    }
    GROUP_STOP
    GROUP("mDebug+File << args")
    {
        qtc.resize(0);
        qInstallMessageHandler(qtcMessageHandler);
        qSetMessagePattern("%{time} %{type} %{category} %{file}(%{line}) %{function} %{threadid} | %{message}");
        START {
            qDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        qtc.seek(0);
        STOP("qDebug+Context",qtc.readLine())

        tsv.resize(0);
        TsvJsonLogWriter tsvw(&tsv);
        LogManager::getInstance().setOutput(&tsvw);
        mDebug() << "write header before START to avoid counting it";
        tsvw.flush();
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("TsvJson",qPrintable(tsv.fileName()))

        json.resize(0);
        LogWriter<data::JsonWriter> jsonw(&json);
        LogManager::getInstance().setOutput(&jsonw);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("Json",qPrintable(json.fileName()))

        xml.resize(0);
        LogWriter<data::XmlWriter> xmlw(&xml);
        LogManager::getInstance().setOutput(&xmlw);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("Xml",qPrintable(xml.fileName()))

#ifdef Q_OS_UNIX
        log::LttngLogWriter lttngw;
        LogManager::getInstance().setOutput(&lttngw);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("Lttng","see system log")

        log::JournaldWriter journaldw;
        LogManager::getInstance().setOutput(&journaldw);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("Journald","see system log")
#elif Q_CC_MSVC
        log::EtlLogWriter etlw;
        LogManager::getInstance().setOutput(&etlw);
        START {
            mDebug()
                << "a integer" << 12
                << "a boolean" << true
                << "a decimal" << 1.333333333333f
                << "a text" << text
                << "a map" << map
                ;
        }
        STOP("ETL","see system log")
#endif

        LogManager::getInstance().setOutput(nullptr);
    }
    GROUP_STOP
    GROUP("mDebug+File << floats")
    {
        fqtc.resize(0);
        qInstallMessageHandler(fqtcMessageHandler);
        qSetMessagePattern("%{time} %{type} %{category} %{file}(%{line}) %{function} %{threadid} | %{message}");
        START {
            QString formattedList('[');
            for (size_t i=0; i<16; i++) {
                 formattedList.append(QString::number(transform[i])).append('|');
            }
            qDebug()
                << "a transform" << formattedList
                ;
        }
        fqtc.seek(0);
        STOP("qDebug+Context",qPrintable(fqtc.fileName()))

        ftsv.resize(0);
        TsvJsonLogWriter tsvw(&ftsv);
        LogManager::getInstance().setOutput(&tsvw);
        mDebug() << "write header before START to avoid counting it";
        tsvw.flush();
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("TsvJson", qPrintable(ftsv.fileName()))

        fjson.resize(0);
        LogWriter<data::JsonWriter> jsonw(&fjson);
        LogManager::getInstance().setOutput(&jsonw);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("Json", qPrintable(fjson.fileName()))

        fxml.resize(0);
        LogWriter<data::XmlWriter> xmlw(&fxml);
        LogManager::getInstance().setOutput(&xmlw);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("Xml", qPrintable(fxml.fileName()))

#ifdef Q_OS_UNIX
        log::LttngLogWriter lttngw;
        LogManager::getInstance().setOutput(&lttngw);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("LTTng","see system log")

        log::JournaldWriter journaldw;
        LogManager::getInstance().setOutput(&journaldw);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("Journald","see system log")
#elif Q_CC_MSVC
        log::EtlLogWriter etlw;
        LogManager::getInstance().setOutput(&etlw);
        START {
            mDebug()
                << "a transform" << transform
                ;
        }
        STOP("ETL","see system log")
#endif

        LogManager::getInstance().setOutput(nullptr);
    }
    GROUP_STOP

    return fclose(samples);
    return fclose(results);
}

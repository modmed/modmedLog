/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include <QApplication>
#include <QtCore>

#include "modmed/log/Log.h"
#include "modmed/log/LogMessage.h"

#include "mainwindow.h"

#include "iostream" // REMOVE

using namespace modmed; // ADD

mLogCategory("Torrent")

Q_GLOBAL_STATIC(log::Log, my_log) // ADD

// ADD Message Handler
void FormatMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    my_log->write(type, context, msg);
}

// ADD click Event filter
M_WARNING_DISABLE_GCC("-Wswitch")
class MyEventFilter: public QObject
{
    public:
    MyEventFilter():QObject(){}
    ~MyEventFilter(){}

    bool eventFilter(QObject *object, QEvent *event)
    {
        switch(event->type()){
        case QEvent::MouseButtonPress:
            if (QString(object->metaObject()->className()) == QString("QMenu")
                    || QString(object->metaObject()->className()) == QString("QToolButton")
                    || QString(object->metaObject()->className()) == QString("QPushButton"))
            {
                QString guiClick = QString(event->type());
                mTrace(guiClick)mExpr(QString(object->metaObject()->className()));
            }
            break;
        case QEvent::Close:
            QString guiClose = QString(event->type());
            mTrace(guiClose)mExpr2(QString(object->metaObject()->className()), object->objectName());
            break;
        }
        return false;
    }
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

    // ADD Remove previous log file
    QFile::remove(QCoreApplication::applicationDirPath() + "/logtorrent.tsv");
    if (!QDir(QCoreApplication::applicationDirPath() + "/logtorrent.tsv_attachments").removeRecursively()) { Q_ASSERT(false); }

    my_log->startWrite(QString(QCoreApplication::applicationDirPath() + "/logtorrent.tsv"));

    // ADD Install line-based log writer
    qInstallMessageHandler(FormatMessageOutput);

    // ADD
    mTrace(app.instance());
    mTrace(QProcessEnvironment::systemEnvironment());
    mTrace(QLocale());

    Q_INIT_RESOURCE(icons);

    MainWindow window;
    window.show();
    app.installEventFilter(new MyEventFilter()); // ADD
    return app.exec();
}

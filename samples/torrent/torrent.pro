QT += network widgets
CONFIG += c++11 console debug_and_release
CONFIG -= app_bundle

INCLUDEPATH += ../../include

CONFIG(debug, debug|release) { DESTDIR = $$_PRO_FILE_PWD_/bin/Debug }
else { DESTDIR = $$_PRO_FILE_PWD_/bin/Release }

CONFIG(debug, debug|release) {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Debug
} else {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Release
}

CONFIG(debug, debug|release) { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Debug }
else { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Release }

CONFIG(release, debug|release) { DEFINES += QT_MESSAGELOGCONTEXT }

# Forms and resources
FORMS += forms/addtorrentform.ui
RESOURCES += icons.qrc

# install
target.path = $$[QT_INSTALL_EXAMPLES]/network/torrent
INSTALLS += target

LIBS += -lmodmedLog

HEADERS += addtorrentdialog.h \
           bencodeparser.h \
           connectionmanager.h \
           mainwindow.h \
           metainfo.h \
           peerwireclient.h \
           ratecontroller.h \
           filemanager.h \
           torrentclient.h \
           torrentserver.h \
           trackerclient.h

SOURCES += main.cpp \
           addtorrentdialog.cpp \
           bencodeparser.cpp \
           connectionmanager.cpp \
           mainwindow.cpp \
           metainfo.cpp \
           peerwireclient.cpp \
           ratecontroller.cpp \
           filemanager.cpp \
           torrentclient.cpp \
           torrentserver.cpp \
           trackerclient.cpp

include( ../../modmedLog.pri)

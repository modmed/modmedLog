#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

from modmed import cxx
from modmed import trace

class Test(unittest.TestCase):
    def test_getCStringLiteralValue(self):
        tests=[(r'""','')
              ,(r'" "',' ')
              ,(r'"\""', '\"')
              ,(r'"\\\'\?\a\b\f\n\r\t\v"', '\\\'?\a\b\f\n\r\t\v')
              ,(r'"\u0020-"', ' -')
              ,(r'"\U00000020-"', ' -')
              ,(r'"\x20-"', ' -')
              ,(r'"\040-"', ' -')
              ,(r'"\0-"', '\0-')
              ]
        for spelling, value in tests:
            self.assertEqual(cxx.getCStringLiteralValue(spelling), value)
        
        with self.assertRaises(SyntaxError):
            for s in ['',r'"\!"']:
                cxx.getCStringLiteralValue(s)
        
        with self.assertRaises(NotImplementedError):
            for s in [r'R""']:
                cxx.getCStringLiteralValue(s)

    def test_Parser(self):
        tests=[(r'''/**/// newlines
                qDebug /* whitespaces */ // comments
                () << "they" /* */ //
                << "are" << "ignored" ; //'''
               ,'qDebug'
               ,['they are ignored'])
              ,(r'qCInfo(cat,"cat is %s and C format is %d",ignored,parsed)'
               ,'qCInfo'
               ,['cat is ',('%s','ignored',None),' and C format is ',('%d','parsed','_Integer')])
              ,(r'''qCWarning(cat) << "cat_is ignored" << "\"'quotes" 
                << "are str in format";'''
               ,'qCWarning'
               ,['cat_is ignored \"\'quotes are str in format'])
              ,(r'''qFatal() << "str" // juxtaposed literals split on several lines are supported
                "str" << "str" NE FORMAT ;'''
               ,'qFatal'
               ,['strstr ',('%s','str_NE_FORMAT',None)])
              ,(r'''qCritical() << common < ops && are >= explicited;'''
               ,'qCritical'
               ,[('%s','common_lt_ops_and_are_ge_explicited',None)])
              ,(r'''qCDebug(cat) << foo<=this->tmpl<array<int,(1000>>2)>>(qstr+"bla")>2.5
                << "template arguments are approximately supported";'''
               ,'qCDebug'
               ,[('%s','foo_le_this_tmpl_array_int_1000_2_qstr_bla_gt_2_5',None),' template arguments are approximately supported'])
              ]
        for src, stmt, fmt in tests:
            tp = next(cxx.TracepointParser(trace.qtTracepointIds,templatedIds=trace.cxxTemplatedIds+['tmpl','array'],alternateSpellings=trace.cxxAlternateSpellings).parseTracepoints(src))
            self.assertEqual        (tp.statement , stmt)
            self.assertSequenceEqual(tp.formatArgs, fmt )
        
        formatterTests=[(r'''qDebug() .space() << "is" << "default"'''
                        ,'qDebug'
                        ,['is default'])
                       ,(r'''qDebug() .nospace() << "is" << "detected"'''
                        ,'qDebug'
                        ,['isdetected'])
                       ,(r'''qDebug() << endl << "is detected"'''
                        ,'qDebug'
                        ,['\nis detected'])
                       ]
        for src, stmt, fmt in formatterTests:
            tp = next(cxx.TracepointParser(trace.qtTracepointIds,formatters=trace.qtFormatters,formatMethods=trace.qtFormatMethods).parseTracepoints(src))
            self.assertEqual        (tp.statement , stmt)
            self.assertSequenceEqual(tp.formatArgs, fmt )
        
        # same with parsing all tracepoint concatenated as individual statements
        src = '\n;'.join([src for src, _, _ in tests]) # \n terminates pending line comments
        for (_, stmt, fmt), tp in zip(tests, cxx.TracepointParser(ids=trace.qtTracepointIds,templatedIds=trace.cxxTemplatedIds+['tmpl','array'],alternateSpellings=trace.cxxAlternateSpellings).parseTracepoints(src)):
            self.assertEqual        (tp.statement , stmt)
            self.assertSequenceEqual(tp.formatArgs, fmt )


if __name__ == '__main__':
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    unittest.main()

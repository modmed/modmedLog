#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

import os
from glob   import glob
from modmed import trace
from modmed import cxx
from modmed import incas

class Test(unittest.TestCase):
    def setUp(self):
        os.chdir(os.path.dirname(os.path.realpath(__file__)))

        self.tracepoints = trace.Tracepoints('tests/tracepoints.temp.tps')
        self.tracepoints.addTracepoints("../samples", trace.modmedTracepointIds + incas.incasTracepointIds,
                                        formatMethods      = incas.incasFormatMethods,
                                        templatedIds       = trace.cxxTemplatedIds,
                                        alternateSpellings = trace.cxxAlternateSpellings)
        self.tracepoints.addTracepoints("tests/incas.tps")

    def test_saveLoadTracepoints(self):
        self.tracepoints.save()
        with open(os.path.splitext(self.tracepoints.csvPath)[0]+'.tps', 'r') as expected, open(self.tracepoints.csvPath, 'r') as parsed:
            self.assertSequenceEqual(expected.readlines(), parsed.readlines())
        trace.Tracepoints(self.tracepoints.csvPath).save()
        with open(os.path.splitext(self.tracepoints.csvPath)[0]+'.tps', 'r') as expected, open(self.tracepoints.csvPath, 'r') as read:
            self.assertSequenceEqual(expected.readlines(), read.readlines())
        # TODO test parsing from loaded source

    def test_read(self):
        src    = 'tests/Test_readIncasCoreLog.json'
        events = incas.readIncasLogJson(src, self.tracepoints)
        trace.writeTsvJson(src+'.temp.tsv', events)
        with open(src+'.temp.tsv', 'r') as expected, open(src+'.tsv', 'r') as obtained:
            self.assertSequenceEqual(expected.readlines(), obtained.readlines())


if __name__ == '__main__':
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    unittest.main()

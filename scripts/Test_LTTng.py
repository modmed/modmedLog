# -*- coding: utf-8 -*-

import os
from modmed import trace
from modmed import lttng

import unittest


class Test(unittest.TestCase):
    def test_read():
        for src in ['tests/Test_readModmedLTTngCbor1','tests/Test_readModmedLTTngCbor2']:
            events = lttng.readLTTngCbor(src)
            trace.writeTsvJson(src+'.temp.tsv', events)
            with open(src+'.tsv', 'r') as expected, open(src+'.temp.tsv', 'r') as obtained:
                self.assertSequenceEqual(expected.readlines(), obtained.readlines())


if __name__ == '__main__':
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    unittest.main()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

import os
from glob        import glob
from datetime    import datetime
from collections import OrderedDict

from modmed import trace

class Test(unittest.TestCase):
    def test_parseCFormat(self):
        tests=[('Worker name is %s and id is %d', ['Worker name is ',('%s',None,None),' and id is ',('%d',None,'_Integer')])
              ,('That is %i%%'                  , ['That is ',('%i',None,'_Integer'),'%'])
              ,('%c'                            , [('%c',None,None)])
              ,('Decimal: %d  Justified: %.6d'  , ['Decimal: ',('%d',None,'_Integer'),'  Justified: ',('%.6d',None,'_Integer')])
              ,('%10c%5hc%5C%5lc'               , [('%10c',None,None),('%5hc',None,None), ('%5C',None,None), ('%5lc',None,None)])
              ,('The temp is %.*f'              , ['The temp is ',('%.*f' ,None,'_Decimal')])
              ,('%ss%lii'                       , [('%s'   ,None,None),'s',('%li',None,'_Integer'),'i'])
              ,('%*.*s | %.3d | %lC | %s%%%02d' , [('%*.*s',None,None),' | ',('%.3d',None,'_Integer'),' | ',('%lC',None,None),' | ',('%s',None,None),'%',('%02d',None,'_Integer')])
              ]
        for cformat, format in tests:
            self.assertSequenceEqual(trace.parseCFormat(cformat), format)

    def test_unquotedTsvValue(self):
        tests = [(' " "" "" " ' ,' " " '        )
                ,(' " \" \" " ' ,' " \" \" " '  )
                ,('" "" "" "'   ,' " " '        )
                ,('" \" \" "'   ,'" \" \" "'    )
                ,('" "" "" '    ,None           )
                ,('""" """'     ,'" "'          )
                ,(' " " '       ,' " " '        )
                ,(' " '         ,' " '          )
                ,(''            ,''             )
                ]
        for test, expected in tests:
            if expected is not None:
                self.assertEqual(trace.unquotedTsvValue(test), expected)

    def test_baseDataType(self):
        t = datetime(2017,12,26)
        tests = [([]            ,[]            ,'Sequence' )
                ,(OrderedDict() ,OrderedDict() ,'Record'   )
                ,({}            ,OrderedDict() ,'Record'   )
                ,(0/1           ,0/1           ,'Decimal'  )
                ,(0             ,0             ,'Integer'  )
                ,(t             ,t             ,'Timestamp')
                ,(False         ,False         ,'Boolean'  )
                ,(True          ,True          ,'Boolean'  )
                ,(None          ,None          ,'Null'     )
                ,(''            ,''            ,'Text'     )
                ,(' true '      ,' true '      ,'Text'     )
                ,('_name_0'     ,'_name_0'     ,'Name'     )
                ,('a'           ,'a'           ,'Name'     )
                ,(' '           ,' '           ,'Character')
                ,('_Decimal'    ,'_Decimal'    ,'Base_Type')
                ,('5'           ,5             ,'Integer'  )
                ,('-123'        ,-123          ,'Integer'  )
                ,('-1e-9'       ,-1e-9         ,'Decimal'  )
                ,('-inf'        ,float('-inf') ,'Decimal'  )
                ,('tRue'        ,True          ,'Boolean'  )
                ,('falSe'       ,False         ,'Boolean'  )
                ]
        for value, expectedData, expectedType in tests:
            self.assertEqual(trace.baseType(value)                      , '_'+expectedType)
            self.assertEqual(trace.baseData(value)                      ,     expectedData)
            self.assertEqual(trace.baseData(value,type='_'+expectedType),     expectedData)

    def test_readWriteTsvJson(self):
        file = 'tests/test_readWriteTsvJson'
        events = list(trace.readTsvJson(file+'.tsv'))
        trace.writeTsvJson(file+'.temp.tsv', events)
        with open(file+'.tsv', 'r') as expected, open(file+'.temp.tsv', 'r') as obtained:
            self.assertSequenceEqual(expected.readlines(), obtained.readlines())


if __name__ == '__main__':
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    unittest.main()

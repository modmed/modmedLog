#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

import os
import traceCmd

class Test(unittest.TestCase):
    def test_convert2Txt(self):
        dst = 'tests/Test_readModmedLogTsvJson'
        traceCmd.convert('tsv+modmed',
            'tests/Test_readModmedLogTsvJson.tsv',
            dst+'.temp.txt',
            '%(_elapsed_s)4.6f %(_severity_symbol)1.1s %(_function)25.25s | %(_message)s')
        with open(dst+'.txt', 'r') as expected, open(dst+'.temp.txt', 'r') as obtained:
            self.assertSequenceEqual(expected.readlines(), obtained.readlines())

    def test_convert2Tsv(self):
        dst = 'tests/Test_readModmedLogTsvJson'
        traceCmd.convert('tsv+modmed',
            'tests/Test_readModmedLogTsvJson.tsv',
            dst+'.temp.tsv',
            'tsv+modmed') # TODO implement trace.writeTsvJson to fix test
        with open(dst+'.tsv', 'r') as expected, open(dst+'.temp.tsv', 'r') as obtained:
            self.assertSequenceEqual(expected.readlines(), obtained.readlines())


if __name__ == '__main__':
    import sys
    import logging
    logging.basicConfig(stream=sys.stderr, level=logging.WARNING)
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    unittest.main()

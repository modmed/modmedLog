# -*- coding: utf-8 -*-
'''
Requires Clang Python bindings

Parses parts of C++ source code associated to traces to help analysing them
Tracepoint statements supported are in the form:
    <log>( ... ) << "format part" << arg_name ;
    <log>( "format %s" , arg_name ) ;

Where ... may include: "(()/*)*/)() /*(*/" and arg_name is an expression simplified to a MODMED _Name

Warning: many C++ peculiarities may prevent us from finding the tracepoint format and arg_names (macros, template parameters with '<<' set<array<int,2<<8>> ...)
'''

import os
import logging
import traceback
import collections

import clang.cindex as cl   # for C++ tokens
import string               # for C/C++ string literal value
import re                   # for C format string

from modmed import trace


def getCStringLiteralValue(spelling):
    prefix, delimiter, encoded = spelling.partition('"')
    if delimiter == '':
        raise SyntaxError('unexpected:'+delimiter)
    if prefix[-1:] == 'R':
        raise NotImplementedError('RawString encoded as in https://github.com/antlr/grammars-v4/blob/master/cpp/CPP14.g4')

    value = ''
    chars = iter(encoded)
    char = next(chars)
    while True:
        if char == '"':
            return value    
        elif char == '\\':
            char = next(chars)
            if   char=='\\': value += '\\'; char = next(chars) 
            elif char=='\'': value += '\''; char = next(chars)
            elif char== '"': value +=  '"'; char = next(chars)
            elif char== '?': value +=  '?'; char = next(chars)
            elif char== 'a': value += '\a'; char = next(chars)
            elif char== 'b': value += '\b'; char = next(chars)
            elif char== 'f': value += '\f'; char = next(chars)
            elif char== 'n': value += '\n'; char = next(chars)
            elif char== 'r': value += '\r'; char = next(chars)
            elif char== 't': value += '\t'; char = next(chars)
            elif char== 'v': value += '\v'; char = next(chars)
            # universal character name
            elif char== 'u': value += chr(int(next(chars)+next(chars)+next(chars)+next(chars), 16));                                                 char = next(chars)
            elif char== 'U': value += chr(int(next(chars)+next(chars)+next(chars)+next(chars)+next(chars)+next(chars)+next(chars)+next(chars), 16)); char = next(chars)
            # hex sequence
            elif char== 'x':
                hexValue = ''
                while True:
                    char = next(chars)
                    if char in string.hexdigits:
                        hexValue += char
                    else:
                        value += chr(int(hexValue, 16))
                        break
            # oct sequence
            elif char in string.octdigits:
                octValue = char
                while True:
                    char = next(chars)
                    if char in string.octdigits and len(octValue)<3:
                        octValue += char
                    else:
                        value += chr(int(octValue, 8))
                        break
            else:           
                raise SyntaxError('unexpected:'+char+' after:'+value)
        else:
            value += char; char = next(chars)


class TracepointParser:
    '''
    Extract simple structure from a valid C++ source or fragment
    Assumes:
    - the C++ is valid (notably, string escape sequences are legal)
    '''
    def __init__(self, ids, autoInsertSpace=True, formatters=[], formatMethods=[], templatedIds=trace.cxxTemplatedIds, alternateSpellings={}):
        assert isinstance(ids[0]            , str)
        assert isinstance(templatedIds      , collections.Sequence)
        assert isinstance(alternateSpellings, collections.Mapping)
        self.ids                = ids
        self.templatedIds       = templatedIds
        self.alternateSpellings = alternateSpellings
        self.autoInsertSpace    = autoInsertSpace
        self.formatters         = formatters
        self.formatMethods      = formatMethods

    def parseTracepoints(self, filePathOrSrc, clangArgs=''):
        '''
        Yields a Tracepoint for each expression matching a simple LL(1) grammar using recursive descent parsing:
          Tracepoints ::= ( id ['(' CFormatArgs ')'] CxxFormatArgs (';'|')') | token )*
        '''
        self.nextToken    = None
        self.tokenScanned = self._scanTokens(filePathOrSrc, clangArgs)
        self.source       = ''
        try:
            while True:
                startToken = next(self.tokenScanned)
                if startToken.spelling not in self.ids:
                    continue
                
                self.source = startToken.spelling # erases previous source assembled by _scanTokens
                self.format = trace.Message() # A sequence of format's str or arg's (format,name,type) tuple
                self.templateLevel= '' # stacks template delimiters to handle '>>' as '>','>' when plausible
                
                if self._get('(') : 
                    self._getCFormatArgsAnd(')')
                
                if not self.format:
                    self.format.autoInsertSpace = self.autoInsertSpace
                    self._getCxxFormatArgsUntil([',',';',':',')',']','}']) # log statement or expression for m?Assumption and the like

                yield trace.Tracepoint(startToken.location.file.name, startToken.location.line, self.format, statement=startToken.spelling, source=self.source)

        except StopIteration:
            pass
        except Exception as e:
            logging.warning(traceback.format_exc())
        finally:
            return

    def _scanTokens(self, filePathOrSrc, clangArgs):
        '''
        Parses filePathOrSrc using clangArgs and encapsulates current token but :
        1. set self.nextToken (or None) to enable parsing with a single token lookahead ;
        2. yield the current token
        '''
        index = cl.Index.create()
        translationUnit = index.parse(filePathOrSrc                                                , args=clangArgs, options=cl.TranslationUnit.PARSE_INCOMPLETE) if os.path.isfile(filePathOrSrc) else \
                          index.parse('unsaved.cpp', unsaved_files=[('unsaved.cpp', filePathOrSrc)], args=clangArgs, options=cl.TranslationUnit.PARSE_INCOMPLETE)
        tokens = [t for t in translationUnit.cursor.get_tokens()
                    if t.kind != cl.TokenKind.COMMENT]
        # Warning: Clang cannot totally parse C++ fragments, so we only use the scanner
        # Warning: The scanner returns '>>' for 2 '>' as in: "set<array<int,10>>"

        def tokenStr(t):
            return "%s at %s" % (t if t is None else "'"+t.spelling+"'", t.location)

        for current in range(0,len(tokens)):
            self.nextToken = tokens[current+1] if current+1 < len(tokens) else None
            logging.debug('scanned:%s',tokenStr(tokens[current]))

            # save source with whitespaces and comments normalized to a single space
            if 0 < current and tokens[current-1].extent.end.offset < tokens[current].extent.start.offset:
                self.source += ' '
            self.source += tokens[current].spelling

            yield tokens[current];

        return

    def _getCFormatArgsAnd(self, end):
        '''
        Sets self.format to a sequence of str or (format,name,type) matching a simple grammar with a single token lookahead:
          CFormatArgs ::= (end <break> | (Expr | Format | ArgName) [','] )*
        Warning: Expr are ignored until Format is found, after which everything is an ArgName
        '''
        cFormat   = None
        cArgNames = []
        try:
            delims = [',']+[end]
            while not self._get(end):
                try:
                    if not cFormat:
                        expr, cFormat = self.__getExprUntil(delims, maybeStrings=True)
                        if not cFormat: logging.debug('skipped:%s',expr)
                    else:
                        expr, _       = self.__getExprUntil(delims)
                        cArgNames.append(trace.name(expr))
                except Exception as e:
                    logging.warning(traceback.format_exc())

                self._get(',') # or end
        
        except Exception as e:
            logging.warning(traceback.format_exc())
        finally:
            if cFormat: # Merge format, name and type from cFormat and self.format
                merged = []
                names  = iter(cArgNames)
                for cf in trace.parseCFormat(cFormat):
                    if isinstance(cf, str):
                        merged.append(cf)
                    else:
                        formatter, _, type = cf
                        merged.append((formatter,next(names),type))

                self.format = trace.Message(merged)

    def _getCxxFormatArgsUntil(self, ends):
        '''
        Sets self.format to a sequence of str or (format,name,type) matching a simple grammar using recursive descent parsing with a single token lookahead:
          CxxFormatArgs ::= ( LOOKAHEAD(ends) <break> | '<<' (Format | ArgName | Formatter) | '.' Formatter | '>>' ArgName )*
        Warning: Does not understand Formatters, and assumes the stream automatically insert spaces
        '''
        delims = ['.','<<','>>']+ends
        while self.nextToken and self.nextToken.spelling not in ends:
            if self._get('<<'):
                self._getExprUntil(delims, maybeFormat=True , maybeArgName=True , formatters=self.formatters) # TODO support C++11 put_money(m,), put_time(t,), quoted(s,,) http://en.cppreference.com/w/cpp/io/manip
            elif self._get('>>'):
                self._getExprUntil(delims, maybeFormat=False, maybeArgName=True ) # may be used in InCAS log statements for readable args
            elif self._get('.'): 
                self._getExprUntil(delims, maybeFormat=False, maybeArgName=False, formatters=self.formatMethods) # TODO support message(), sequence(), record(), text(), bind(), bindText(), Qt maybeQuote(char)
            else:
                self._getExprUntil(delims, maybeFormat=False, maybeArgName=False) # in case other operators are used (like && for mNAssumption), they are just ignored

    def _getExprUntil(self, ends, maybeFormat, maybeArgName, formatters=[]):
        '''
        Appends a C++ expression's str or ('%s',name,None) (depending on maybeFormat, maybeArgName) until nextToken is in [ends] using recursive descent parsing of sub-expressions delimited by () [] {} (and <...> parameters following a C++ cast)
        Warning: does not handle <...> delimited template arguments
        '''
        try:
            expr, stringValue = self.__getExprUntil(ends, maybeFormat)
            if maybeFormat and stringValue:
                self.format.append(stringValue)
            else:
                firstToken, _, _ = expr.partition(' ')
                if firstToken in formatters:
                    if firstToken=='endl':
                        self.format.appendNewline()
                    elif firstToken=='ends':
                        self.format.append('\0')
                    elif firstToken=='reset':
                        self.format.autoInsertSpace = True
                    elif firstToken=='space':
                        self.format.autoInsertSpace = True
                        self.format.maybeSpace()
                    elif firstToken=='maybeSpace':
                        self.format.maybeSpace()
                    elif firstToken=='nospace':
                        self.format.autoInsertSpace = False
                    else:
                        logging.debug('skipped:%s',expr) # not usable
                elif maybeArgName:
                    self.format.append(('%s',trace.name(expr),None))
                else:
                    logging.debug('skipped:%s',expr)
        except Exception as e:
            logging.warning(traceback.format_exc())

    def __getExprUntil(self, ends, maybeStrings=False):
        assert isinstance(ends[0], str), ends

        # TODO support http://en.cppreference.com/w/cpp/language/operator_alternative
        starts = '([{'
        def endOf(start):
            return ')]}'[starts.find(start)]

        exprs   = []
        strings = [] if maybeStrings else None
        while self.nextToken and self.nextToken.spelling not in ends:
            spelling = next(self.tokenScanned).spelling
            # Assuming the C++ fragment is already checked by a compiler, we just need to track some of the parse state to avoid splitting formats on inner ',' or '<<'
            # This is fundamentally limited by the use of '<' '>' as binary operators AND template argument delimiters!
            # For instance, we are not able to decide between the template call map<QString,int>() and the comparison map<QString without parsing all the fragment (and maybe the QString declaration)
            if spelling in starts:
                end = endOf(spelling)
                exprs += [self.__getExprUntil([end])[0]]
                if not self._get(end): assert False
            else:
                try:
                    strings += [getCStringLiteralValue(spelling)]
                    exprs   += [strings[-1]] # we are interested in string values in expr
                except:
                    strings  = None
                    exprs   += [self.alternateSpellings.get(spelling, spelling)]
                    if spelling in self.templatedIds:
                        if self._get('<'):
                            self.templateLevel += '<'
                            exprs += [self.__getExprUntil(['>','>>'])[0]] # as we cannot change self.nextToken.spelling from '>>' to '>', we always need to process them specially
                            if self.nextToken.spelling == '>>':
                                # It is more probably correct to handle '>>' as '>','>'
                                if self.templateLevel[-2:]=='<<':
                                    self.templateLevel = self.templateLevel[:-2]+'_'
                                elif self.templateLevel[-1:] == '_':
                                    self.templateLevel = self.templateLevel[:-1]
                                    if not self._get('>>'): assert False
                                else: # '>>' cannot be the end, so
                                    exprs += [self.__getExprUntil(['>'])[0]]
                            else:
                                if not self._get('>'): assert False
                                assert self.templateLevel[-1:] == '<', self.templateLevel
                                self.templateLevel = self.templateLevel[:-1]

        return ' '.join(filter(None,exprs)) if exprs else '', ''.join(strings) if strings else None

    def _get(self, expected):
        assert isinstance(expected, str), expected
        if not self.nextToken or self.nextToken.spelling != expected:
            return False
        return next(self.tokenScanned).spelling == expected

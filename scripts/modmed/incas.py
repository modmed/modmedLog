# -*- coding: utf-8 -*-
'''
Reads InCAS JSON files as MODMED _Trace
'''

import json
from collections import OrderedDict
import logging
import traceback

from modmed.trace import Tracepoints, Event, timestamp, name as modmedName, severity, baseData

incasTracepointIds = ['INCAS_'+sev+cat for sev in ['CRITICAL','WARNING','DEBUG'] for cat in ['','_C']]

incasFormatMethods = ['message','sequence','record','text','bind','bindText','item'] # TODO support others...

def mapUserData(incas, event, tracepoints=None):
    '''
    incas.userData -> _format, _args, _arg_names, _arg_types
    '''
    assert isinstance(incas , dict)
    assert isinstance(event, Event)

    userData   = incas['userData']
    formatArgs = None
    if tracepoints:
        tracepoint = tracepoints.bestMatch(event)
        if tracepoint:
            formatArgs = tracepoint.formatArgs

    if isinstance(userData, str):
        if formatArgs:
            try:
                # extract _format and _args from userData matching formatArgs
                for i, item in enumerate(formatArgs):
                    if isinstance(item, str):
                        if not userData.startswith(item):
                            raise KeyError(item)
                        event._format   += item
                        userData         = userData[len(item):]
                    else:
                        format, name, type = item
                        value = userData
                        if i+1<len(formatArgs):
                            argEnd = userData.find(formatArgs[i+1])
                            if argEnd != -1:
                                value    = userData[:argEnd]
                                userData = userData[argEnd:]

                        try:
                            event._args      += [baseData(value, type)]
                            event._format    += format  
                            event._arg_names[len(event._args)-1] = name
                            event._arg_types[len(event._args)-1] = type
                        except Exception as e:               
                            event._args      += [value]
                            event._format    += '%s'
            except Exception as e:            
                event            ._args      += [userData]
                event            ._format    += '%s'
        else:
            event._format    = '%s'
            event._args      = [userData]
            #static _arg_type is unknown and _Text is the default

    elif isinstance(userData, list):
        # TODO if formatArgs is not None: ... else:
        if len(userData) > 3: # probably <10% formatArgss
            event._format    = 'Sequence %s'
            event._args      = [userData]
            event._arg_types = ['_Sequence']
        else:
            event._args      = userData
            event._format    = ' '.join(['%s' for _ in event._args])
            #static _arg_type is unknown

    elif isinstance(userData, dict):
        # TODO if formatArgs is not None: ... else:
        if len(userData) > 9: # TODO adjust threshold or use source
            event._format    = 'Record %s'
            event._args      = [userData]
            event._arg_types = ['_Record']
        else:
            event._args      = list(userData.values())
            event._arg_names = [modmedName(key) for key in userData.keys()]
            event._format    = ' '.join([('' if name is None else name+' ')+'%s' for name in event._arg_names])
            #static _arg_type is unknown
            # so the following is dubious: event._arg_types = [baseType(arg) for arg in event._args]

    else:
        assert False


def readIncasLogJson(incasLogJsonPath, tracepoints=Tracepoints()):
    '''
    yields a modmed.trace.Event for each line of incasLogJsonPath.
    sourceDirPaths are optionally scanned to retrieve from the source code that generated the trace more relevant _arg_names, _arg_types or _id.
    '''
    with open(incasLogJsonPath, 'r') as incasLogJson:
        root = json.load(incasLogJson, object_pairs_hook=OrderedDict)

        headers = root['document'] if isinstance(root, dict) else ['severity','category','name','userData','logData'] # InCAS log 1.3 convention
        lines   = root['data'    ] if isinstance(root, dict) else root
        assert isinstance(lines, list)

        firstTimestamp = None
        scopedMessage  = None
        skippedMessage = None
        previous       = None
        for lineNumber, line in enumerate(lines,1):
            incas = line if isinstance(line, dict) else dict(zip(headers, line)) if isinstance(line, list) else {'userData': line}
            if 'logData' in incas: # following InCAS log JSON 1.3 convention
                incas.update(incas['logData'])

            # Map incas::log fields to MODMED fields according to WP2/D3 v1.2 specification
            event     = Event()

            try:
                if 'thread' in incas:
                    event._thread_id = incas['thread']
                if 'source' in incas:
                    event._path, rest = incas['source'].split('(',1)
                    event._line = int(rest[:rest.find(')')])
            except Exception as e:
                logging.debug('%s at %s(%s): %s' % (e, incasLogJsonPath, lineNumber, line))

            # Handle incas::log::*Message specific classes

            if 't0' in incas and incas.get('userData', None) is None:
                logging.debug('first incas::log::ScopedMessage')

                event._format    = 'incas ScopedMessage t0 %s dt %s'
                event._args      = [timestamp(incas['t0']),[]         ]
                event._arg_names = ['t0'                  ,'dt'       ]
                event._arg_types = ['_Timestamp'          ,'_Sequence']
                scopedMessage = event

                event = None # to detect bad uses

            elif 'dt_msec' in incas and scopedMessage is not None:
                logging.debug('incas::log::ScopedMessages following first ScopedMessage')

                event.dt_msec = float(incas['dt_msec'])
                if 'userData' in incas:
                    event.userData = incas['userData']
                scopedMessage._args[1].append(event)

                event = None # to detect bad uses

            else:
                event._format = ''
                event._args   = []

                if scopedMessage is not None:
                    logging.debug('last incas::log::ScopedMessage')

                    if incas.get('userData', None) is None: # only merge scopedMessage with an empty Message immediately following scopedMessages
                        event.update(scopedMessage) # with _format and _args
                    else:
                        logging.debug(scopedMessage)
                    scopedMessage = None

                event._severity = severity(incas.get('severity'))

                if 'category' in incas: event._category = incas['category']
                if 'name'     in incas: event._function = incas['name'] # (without arguments)

                if 'skipped' in incas and incas.get('userData') is None:
                    logging.debug('incas::log::SkippedMessage just before next Message')

                    event._format    = 'incas SkippedMessage skipped %s'
                    event._args      = [int(incas['skipped'])]
                    event._arg_names = ['skipped'            ]
                    event._arg_types = ['_Integer'           ]
                    skippedMessage = event

                    event = None # to detect bad uses

                else:
                    try:
                        event._elapsed_s = 0 if previous is None else previous._elapsed_s # until we know more
                        if 't' in incas:
                            event._timestamp = timestamp(incas['t'])
                        if firstTimestamp is None:
                            firstTimestamp = event._timestamp
                        if firstTimestamp is not None and '_timestamp' in event:
                            event._elapsed_s = (event._timestamp - firstTimestamp).total_seconds()

                        if incas.get('userData', None) is not None: # treat as incas::log::Message (although it may be an unexpected ScopedMessage)
                            mapUserData(incas, event, tracepoints)

                        if skippedMessage is not None:
                            skippedMessage._elapsed_s = event._elapsed_s
                            if '_timestamp' in event:
                                skippedMessage._timestamp = event._timestamp
                            yield Event(skippedMessage)
                            skippedMessage = None

                        previous = event
                        yield event

                    except Exception as e:
                        logging.warning(traceback.format_exc())

        if skippedMessage is not None and previous is not None:
            skippedMessage._elapsed_s = previous._elapsed_s
            if '_timestamp' in previous:
                skippedMessage._timestamp = previous._timestamp
            yield Event(skippedMessage)
            skippedMessage = None

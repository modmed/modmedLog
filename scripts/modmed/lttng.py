# -*- coding: utf-8 -*-
'''
Requires LTTng babeltrace and cbor2

Reads LTTng+CBOR files as MODMED _Trace
'''

from datetime import datetime

from modmed import trace

import babeltrace
import cbor2


def nanoseconds2datetime(nanotime):
    return datetime.fromtimestamp(nanotime // 1000000000)


def readLTTngCbor(path):
    '''
    Reads a MODMED LTTng+CBOR log file at source_path and yield a dict for each event, restoring redundancy.
    '''
    col = babeltrace.TraceCollection()

    # Collect events from lttng traces metadata (tsdl, ctf)
    if col.add_traces_recursive(path, 'ctf') is None:
        raise RuntimeError('Cannot add trace')

    timestampFormat = '%Y-%m-%dT%H:%M:%S%z'
    isTimestampInit = False
    tempdict = {}
    for lttngEvent in col.events:
        if (lttngEvent.name.startswith("modmedLog:")):
            severity = lttngEvent.name.split(":")[1]
            if severity == "start" or severity == "stop" or severity == "info":
                tempdict["_severity"] = 6
            if severity == "fatal":
                tempdict["_severity"] = 0
            if severity == "critical":
                tempdict["_severity"] = 2
            if severity == "warning":
                tempdict["_severity"] = 4
            if severity == "debug":
                tempdict["_severity"] = 7
            # Get Modmed keys
            for key in lttngEvent.keys():
                if key.startswith("_"):
                    if key == "_args" or key == "_arg_types" or key == "_arg_names":
                        # cbor load
                        valuebytes = bytes(lttngEvent[key])
                        tempdict[key] = cbor2.loads(valuebytes).value
                    else:
                        tempdict[key] = lttngEvent[key]

            # Get Lttng keys
            if lttngEvent.name.split(":")[1] == "start" and isTimestampInit == False:
                initTimestamp = lttngEvent.timestamp
                isTimestampInit = True
            # Change timestamp
            tempdict["_elapsed_s"] = (lttngEvent.timestamp - initTimestamp) *1E-9
            tempdict["_timestamp"] = nanoseconds2datetime(int(lttngEvent.timestamp))
            if "vpid" in lttngEvent:
                tempdict["_thread_id"] = lttngEvent["vpid"]

            yield trace.Event(tempdict)

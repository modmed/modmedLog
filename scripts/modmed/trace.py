# -*- coding: utf-8 -*-
'''
Implements generic MODMED _Trace and _Event specification as defined in:
https://docs.google.com/viewer?a=v&pid=sites&srcid=bWlubWF4bWVkaWNhbC5jb218bW9kbWVkfGd4OjU4YTI2N2ZhMDIzZTBlZTU

Provides a TSV+JSON file reader
'''

import os
import json
import re
import ast
from glob        import glob
from datetime    import datetime, timedelta
from collections import defaultdict, OrderedDict, Mapping, Sequence, Iterable
from numbers     import Integral, Number
from string      import hexdigits
import logging
import traceback


class Message(list):
    '''
    A list of str format of (formatter,name,type) args with autoInsertSpace support compatible with Qt QDebug and MODMED LogMessage
    '''
    def __init__(self, *args):
        list.__init__(self, *args)
        self.autoInsertSpace = True

    def maybeSpace(self):
        if self.autoInsertSpace and len(self):
            if isinstance(self[-1], str):
                self[-1] += ' '
            else:
                super().append(' ')

    def append(self, item):
        if len(self) and isinstance(self[-1], str) and isinstance(item, str):
            self[-1] += item
        else:
            super().append(item)
        self.maybeSpace() # at the end as in qdebug.cpp

    def appendNewline(self):
        if len(self) and isinstance(self[-1], str):
            self[-1] += '\n'
        else:
            super().append('\n')


class Tracepoint(dict):
    '''
    Dict-derived class representing a single point in executable code generating Events with a fixed format that can be partly guessed from the source code
    Warning: a single source line with template code can generate several tracepoints
    '''
    def __init__(self, *args, **kwargs):
        self.__dict__ = self
        _path, self._line, self.formatArgs = args

        # last space is of no interest to match a message and may not be present depending on autoInsertSpace conventions
        if len(self.formatArgs) and isinstance(self.formatArgs[-1], str):
            if self.formatArgs[-1]==' ':
                self.formatArgs = self.formatArgs[:-1]
            elif self.formatArgs[-1][-1:]==' ':
                self.formatArgs[-1] = self.formatArgs[-1][:-1]

        dir, self.file  = os.path.split(_path)
        self.parentDirs = list(reversed(os.path.normpath(dir).split(os.sep)))
        self.statement  = kwargs.get('statement', None)
        self.severity   = kwargs.get('severity' , None)
        self.source     = kwargs.get('source'   , None)

    def __getattr__(self, name):
        return self[name]

    def __setattr__(self, n, value):
        try:
            if   n=='severity' : dict.__setattr__(self, n, int(value) if value else None)
            elif n=='_line'    : dict.__setattr__(self, n, int(value))
            else:
                assert (n!='formatArgs' or isinstance(value, list)) and \
                       (n!='_format'                              ) and \
                       (n!='_arg_names'                           ) and \
                       (n!='_arg_types'                           )
                       # the latter are always computed by __missing__
                dict.__setattr__(self, n, value)
        except Exception as e:
            raise TypeError(n+' cannot be:'+str(value)+' '+str(e))

    def __missing__(self, name):
        try:
            if   name=='dir'       : return os.sep.join(reversed(self.parentDirs))
            elif name=='_format'   : return ''.join([f if isinstance(f, str) else f[0] for f in formatArgs                        ])
            elif name=='_arg_names': return         [                             f[1] for f in formatArgs if isinstance(f, tuple)]
            elif name=='_arg_types': return         [                             f[2] for f in formatArgs if isinstance(f, tuple)]
            else:                    return None
        except Exception as e:       return None


cxxExtensions         = ['cpp','cxx','hpp','hxx','c']

qtTracepointIds       = ['qFatal'] + \
                        ['q'+cat+sev for cat in ['','C'        ] for sev in ['Critical','Warning','Info','Debug']]

# For comparison, the following modmed macros gather tracepoint format args at runtime
modmedTracepointIds   = ['m'+cat+sev for cat in ['','C','U','N'] for sev in ['Critical','Warning','Info','Debug','DebugModulo','DebugNSecs','DebugOut']] + \
                        ['m'+cat+suf for cat in [   'C','U','N'] for suf in [ 'Assumption','Requirement','Trace','TraceModulo','TraceNSecs','TraceOut','TraceQObject']] # TODO +str(expr) for expr in range(0,9)

cxxTemplatedIds       = ['dynamic_cast','static_cast','reinterpret_cast','const_cast']

qtFormatters          = ['endl','reset', 
                         'flush','bom', 
                         'bin','oct','dec','hex', 
                         'left','center','right', 
                         'fixed','scientific', 
                         'forcesign','noforcesign', 
                         'forcepoint','noforcepoint', 
                         'lowercasebase','uppercasebase','lowercasedigits','uppercasedigits', 
                         'showbase','noshowbase', 
                         'qSetFieldWidth', 'qSetPadChar', 'qSetRealNumberPrecision']
                      # see http://doc.qt.io/qt-5/qtextstream.html except those used on input like 'ws'

qtFormatMethods       = ['resetFormat', 'space','nospace','maybeSpace', 'quote','noquote','maybeQuote']
                      # see http://doc.qt.io/qt-5/qdebug.html

cxxFormatters         = ['boolalpha','noboolalpha',
                         'showbase','noshowbase', 
                         'showpoint','noshowpoint', 
                         'showpos','noshowpos', 
                         'uppercase','nouppercase', 
                         'unitbuf','nounitbuf',
                         'left','internal','right', 
                         'oct','dec','hex', 
                         'fixed','scientific','hexfloat','defaultfloat',
                         'ends','endl',
                         'flush','flush_emit','emit_on_flush','no_emit_on_flush',
                         'setiosflags','resetiosflags',
                         'setbase','setfill','setprecision','setw']
                      # see http://en.cppreference.com/w/cpp/io/manip except those used on input like 'skipws'

cxxAlternateSpellings = {'!':'not','&&':'and','||':'or','==':'eq','!=':'ne','<':'lt','<=':'le','>':'gt','>=':'ge'}


class Tracepoints:
    '''
    A collection of Tracepoint indexed by source path and line
    '''
    headers = ['dir','file','_line','formatArgs','severity','statement','source']

    def __init__(self, csvPath=None):
        self._index = defaultdict(list)
        self.csvPath = csvPath
        if csvPath and os.path.isfile(csvPath):
            try:
                self.addTracepoints(csvPath)
            except:
                logging.warning(traceback.format_exc())

    def addTracepoint(self, new):
        tps = self._index[(new._line,new.file)]
        for tp in tps:
            if tp.__dict__ == new.__dict__:
                return
        tps.append(new)

    def addTracepoints(self, path, ids=[], cxxExts=cxxExtensions, clangArgs='', autoInsertSpace=True, formatters=[], formatMethods=[], templatedIds=[], alternateSpellings={}):
        if ids:
            assert isinstance(ids, list) and len(ids)
            assert os.path.isdir(path), path
            for ext in cxxExts:
                import modmed.cxx
                p = modmed.cxx.TracepointParser(ids, autoInsertSpace, formatters, formatMethods, templatedIds, alternateSpellings)
                for file in glob(path+'/**/*.'+ext, recursive=True):
                    try:
                        for tp in p.parseTracepoints(file, clangArgs):
                            self.addTracepoint(tp)
                    except Exception as e:
                        logging.warning(traceback.format_exc())
        else:
            with open(path, 'r') as csvFile:
                import csv
                r = csv.DictReader(csvFile, dialect='excel-tab')
                assert set(r.fieldnames)==set(self.headers), r.fieldnames
                for tp in r:
                    self.addTracepoint(Tracepoint(os.sep.join([tp['dir'],tp['file']]),tp['_line'],ast.literal_eval(tp['formatArgs']),severity=tp['severity'],statement=tp['statement'],source=tp['source']))

    def bestMatch(self, event): # TODO match against optional _message, _format, etc.
        assert isinstance(event, Event)
        dirs, file = os.path.split(event._path)
        bestMatch = None
        for t in self._index[(event._line,file)]:
            if t.severity in [None, event._severity]:
                searchedParentDirs = list(reversed(os.path.normpath(dirs).split(os.sep)))
                if not bestMatch or len(os.path.commonprefix([        t.parentDirs, searchedParentDirs])) \
                                  > len(os.path.commonprefix([bestMatch.parentDirs, searchedParentDirs])):
                    bestMatch = t
        return bestMatch

    def save(self):
        import csv
        with open(self.csvPath, 'w', newline='') as csvFile:
            w = csv.DictWriter(csvFile, self.headers, dialect='excel-tab', extrasaction='ignore')
            w.writeheader()
            for tps in self._index.values():
                for tp in tps:
                    w.writerow({k:tp[k] for k in self.headers}) # for Tracepoint does not exposes all available keys() to DictWriter


def parseCFormat(cFormat):
    '''
    Parses cFormat to return a corresponding list of str literals or (format,None,type) tuple
    '''
    # https://stackoverflow.com/questions/30011379/how-can-i-parse-a-c-format-string-in-python
    # https://msdn.microsoft.com/en-us/library/hf4y5e3w.aspx for 'C' type
    # http://fr.cppreference.com/w/cpp/io/c/fprintf          for 'F' type
    verboseCFormatPattern='''\
    (?P<percent>%%)                    # literal "%%"       available as MatchObject.group('percent')
    |
    (?P<format>%                       # format specifier   available as MatchObject.group('format')
    (?:[-+0 #]{0,5})                   # optional flags
    (?:\d+|\*)?                        # optional width
    (?:\.(?:\d+|\*))?                  # optional precision
    (?:h|hh|l|ll|j|z|t|L|w|I|I32|I64)? # optional size
    (?P<type>[cCdiouxXeEfFgGaAnpsSZ])) # type               available as MatchObject.group('type')
    |
    (?P<literal>[^%]*)                 # other literal      available as MatchObject.group('literal')
    '''
    def modmedType(type):
        return \
            '_Integer' if type in 'diouxX'   else \
            '_Decimal' if type in 'eEfFgGaA' else \
            None # '_Text' would not be more precise than the default

    format  = []
    literal = ''
    for m in re.finditer(verboseCFormatPattern, cFormat, flags=re.X):
        if m.group('format'):
            if literal != '':
                format += [literal]
            format += [(m.group('format'),None,modmedType(m.group('type')))]
            literal = ''
        elif m.group('percent'):
            literal += '%'
        else:
            literal += m.group('literal')
    if literal != '':
        format += [literal]
    return format


def severity(severityId, default=6): # INFO (_severity is mandatory, so unless we know more, let us say INFO to get noticed)
    assert isinstance(severityId, str)
    s = severityId.lower()
    return 0 if s=='fatal' \
      else 2 if s=='critical' \
      else 4 if s=='warning'  \
      else 6 if s=='info'     \
      else 7 if s=='debug'    \
      else defaultValue


class Event(dict): # TODO remove dict to avoid possibilities to bypass __setattr__ ?!
    '''
    Dict-derived class allowing using attributes to read key values as long as:
    - they are valid identifiers
    - they are not hidden by the implementation
    NB: non-existing attributes return None instead of raising AttributeError for convenience. Users can do 'evt.data or ""' to replace None values with "".
    '''
    # TODO class Severity(IntEnum) ? https://docs.python.org/3/library/enum.html
    severityIds     = {0:'EMERGENCY',
                       1:'ALERT',
                       2:'CRITICAL',
                       3:'ERROR',
                       4:'WARNING',
                       5:'NOTICE',
                       6:'INFORMATIONAL',
                       7:'DEBUG'}
    severitySymbols = {0:'X',
                       1:'!',
                       2:'!',
                       3:'!',
                       4:'?',
                       5:'i',
                       6:'i',
                       7:'_'}

    ids = {}

    def __init__(self, values={}):
        dict.__init__(self)
        self.__dict__ = self
        deferred = {'_arg_names':None,'_arg_types':None}
        if isinstance(values, Mapping):
            for k in values.keys():
                if k in deferred.keys():
                    deferred[k] = values[k]
                else:
                    setattr(self, k, values[k])
        elif isinstance(values, Iterable):
            for k, v in values:
                if k in deferred.keys():
                    deferred[k] = v
                else:
                    setattr(self, k, v)
        for k in deferred.keys():
            if deferred[k] is not None:
                setattr(self, k, deferred[k])

    def __getattr__(self, name):
        try:
            if   name=='_arg_names': return self[name][:len(self._args)] + [None]*(len(self._args)-len(self[name]))
            elif name=='_arg_types': return self[name][:len(self._args)] + [None]*(len(self._args)-len(self[name]))
            else:
                return self[name]
        except:
            return None

    def __setattr__(self, n, value):
        if name(n, True)!=n:
            raise KeyError(n+' is not a valid modmed _Name')
        try:
            if value is None:
                dict.pop(self, n, None)
            else:
                if   n=='_elapsed_s': dict.__setattr__(self, n, float(value))
                elif n=='_timestamp': dict.__setattr__(self, n,       value  if isinstance(value, datetime) else timestamp(value))
                elif n=='_severity' : dict.__setattr__(self, n, int  (value))
                elif n=='_line'     : dict.__setattr__(self, n, int  (value))
                elif n=='_count'    : dict.__setattr__(self, n, int  (value))
                else:
                    assert (n!='_id'        or  isinstance(value, str ) and value                       ) and \
                           (n!='_format'    or  isinstance(value, str )                                 ) and \
                           (n!='_args'      or  isinstance(value, list)                                 ) and \
                           (n!='_arg_names' or (isinstance(value, list) and len(value)==len(self._args))) and \
                           (n!='_arg_types' or (isinstance(value, list) and len(value)==len(self._args)))
                    dict.__setattr__(self, n, value)
        except Exception as e:
            raise TypeError(n+' cannot be:'+str(value)+' '+str(e))

    def __missing__(self, n):
        try:
            if n=='_id': 
                try:
                    fmt = (self.severityIds[self._severity] if self._severity < 7 else (self._function or ''))+' '
                    arg = 0
                    for f in parseCFormat(self._format):
                        if isinstance(f, str):
                            fmt += f+' '
                        else:
                            fmt += str(arg)+' '
                            arg += 1
                    return name(fmt.strip())
                except Exception as e:
                    return None
            elif n=='_severity' : return 7 # DEBUG as specified by syslog and used by MODMED
            elif n=='_format'   : return ''
            elif n=='_args'     : return []
            elif n=='_arg_names': return [None]*len(self._args)
            elif n=='_arg_types': return [None]*len(self._args) # as their types are not statically defined
            elif n=='_severity_id':
                try:
                    return self.severityIds[int(self._severity)]
                except:
                    return str(self._severity)
            elif n=='_severity_symbol':
                try:
                    return self.severitySymbols[int(self._severity)]
                except:
                    return str(self._severity)
            elif n=='_message':
                try:
                    return self._format % tuple(self._args)
                except:
                    return self._format
            else:
                return self._args[int(n[1:])] # _0..n == _args[0..n]
        except Exception as e:
            return None

    def __repr__(self):
        return '{'+', '.join(n+':'+repr(v) for n,v in self.items())+'}' # remove quotes around


# Python <-> MODMED Conversions

baseTypes = ['_Trace','_Event','_Record','_Sequence','_Null','_Text','_Boolean','_Integer','_Decimal','_Timestamp','_Bytes','_Tag','_Name','_Base_Type','_Type','_Character']

def baseType(data):
    '''
    Returns the most precise _Base_Type for data, for instance '_Integer' instead of '_Decimal' for 123 or '_Decimal' instead of '_Text' for 'NaN'
    '''
    textType = None         
    if isinstance(data, str):
        textData = baseData(data)
        textType = baseType(textData) if data!=textData                           \
              else '_Tag'             if data[:1]=='#' and data==name(data, True) \
              else '_Base_Type'       if data in baseTypes                        \
              else '_Name'            if data==name(data, True)                   \
              else '_Character'       if len(data)==1                             \
              else '_Text'
              # TRUE is also a _Name
              # 0    is also a _Character

    return '_Record'    if isinstance(data, Mapping )                             \
      else '_Sequence'  if isinstance(data, Sequence) and not textType            \
      else '_Boolean'   if data is True or data is False                          \
      else '_Null'      if data is None                                           \
      else '_Integer'   if isinstance(data, Integral)                             \
      else '_Decimal'   if isinstance(data, Number  )                             \
      else '_Timestamp' if isinstance(data, datetime)                             \
      else '_Bytes'     if isinstance(data, bytes) or isinstance(data, bytearray) \
      else '_Event'     if isinstance(data, Event)                                \
      else textType

def baseData(data, raiseTypeError=False, type=None):
    '''
    Returns the Python value corresponding to type or the most precise _Base_Type for data
    '''
    assert type in baseTypes+[None], type

    textData = None
    if isinstance(data, str): # check for more precise data than _Text
        n = number(data)
        t = timestamp(data)
        textData =        number(data)     if n is not None         \
            else       timestamp(data)     if t is not None         \
            else                 True      if data.upper()=='TRUE'  \
            else                 False     if data.upper()=='FALSE' \
            else   bytes.fromhex(data[2:]) if data[:2].lower()=='0x' ''' and not [c for c in data[2:] if c not in hexdigits] ''' \
            else                 data
        if not type:
            return textData

    baseDataType = baseType(data)
    type = type or baseDataType # when not specified
    baseData = OrderedDict([(name,baseData(item, raiseTypeError)) for name, item in data.items()]) if type==baseDataType=='_Record'   \
        else               [      baseData(item, raiseTypeError)  for       item in data        ]  if type==baseDataType=='_Sequence' \
        else                                                                    textData           if type==baseType(textData)        \
        else                                                                        data           if type==baseDataType              \
        else float(textData)   if type=='_Decimal'   and baseDataType=='_Integer'                                           \
        else           data    if type=='_Text'      and baseDataType in ['_Character','_Tag','_Name','_Base_Type','_Type'] \
        else           data    if type=='_Type'      and baseDataType in ['_Character','_Name','_Base_Type']                \
        else           data[0] if type=='_Character' and baseDataType=='_Text' and len(data)                                \
        else           None

    if baseData is not None:
        return baseData
    elif data is None or not raiseTypeError: 
        return None
    else: 
        raise TypeError(data+' is not a '+type)

def number(text):
    assert isinstance(text, str) # otherwise would return int for a float and lose information
    try:        return   int(text) # more restrictive than float
    except:
        try:    return float(text) # more permissive than modmed
        except: return None

def timestamp(text):
    assert isinstance(text, str) # otherwise would would return None for a datetime and lose information
    if text and re.match(r'.*[+-]\d\d:\d\d$', text): # extended ISO 8601 UTC offset is not supported by strptime, so...
        text = text[:-3]+text[-2:]
    try:            return datetime.strptime(text, '%Y-%m-%dT%H:%M:%S.%f%z')
    except:
        try:        return datetime.strptime(text, '%Y-%m-%dT%H:%M:%S%z')
        except:
            try:    return datetime.strptime(text, '%Y-%m-%dT%H:%M%z')
            except: return None

def name(text, maybeReserved=False):
    assert isinstance(text, str)
    name = ''
    for char in text:
        if char in '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789':
            name      += char
        elif name[-1:] != '_':
            name      += '_'
    if not maybeReserved and name[:1]=='_':
        name = name[1:]
    return \
        None if name=='' else \
        ('_' if name[0] in '0123456789' else '')+name # digits are forbidden in the beginning


class JsonEncoder(json.JSONEncoder):
    def default(self, data):
        return data.isoformat()      if isinstance(data, datetime)                             \
          else '0x'+data.hex()       if isinstance(data, bytes) or isinstance(data, bytearray) \
          else super().default(data)


def readJson(path):
    '''
    Reads a MODMED JSON file at path and return the corresponding _Base_Type Python value
    '''
    return baseData(json.load(path, object_pairs_hook=OrderedDict), raiseTypeError=True)


def unquotedTsvValue(cell):
    '''
    Return the str value of a possibly quoted TSV value
    Remark: This is simpler than https://www.w3.org/TR/tabular-data-model/#dfn-quoted which accepts delimiters in quoted cells (Excel also accepts line terminators in quoted cells and '""' as quoted empty cell value)
    '''
    assert '\t' not in cell
    assert '\n' not in cell
    stripped = cell.strip()
    if not ('"'==stripped[:1]==stripped[-1:] and '""' in stripped[1:-1]):
        return cell
    return stripped[1:-1].replace('""','"')


def readTsvJson(path, fields=None):
    '''
    Reads a MODMED TSV+JSON log file at path and yield Events, restoring redundancy.
    '''
    with open(path, 'r') as tsv:
        if not fields:# get TSV+JSON structure from 1st line
            fields = tsv.readline().rstrip('\n').split('\t')

        names  = fields # TODO [name(n, maybeReserved=True) for n in fields]
        try:    argsIndex = names.index('_args')
        except: argsIndex = len(names)
        elapsedSecsIndex = names.index('_elapsed_s')
        if not(elapsedSecsIndex < argsIndex):
            raise '_elapsed_s not found'
        try:    timestampIndex = names.index('_timestamp')
        except: timestampIndex = -1
        try:    countIndex = names.index('_count')
        except: countIndex = -1
        #
        # yield event restoring eliminated redundancy
        eventCounts = {}
        previousEvent = None
        previousValues = []
        for line in tsv:
            values = [unquotedTsvValue(cell) for cell in line.rstrip('\n').split('\t')]
            for i in range(len(values)):
                # restore generic and mandatory redundant values
                if values[i]=='':
                    if i==timestampIndex:
                        try:    values[i] = previousValues[i] + timedelta(0, float(values[elapsedSecsIndex]) - previousValues[elapsedSecsIndex])
                        except: pass
                    else:
                        values[i] = previousValues[i] if i!=countIndex and i < len(previousValues) else None
                else: 
                    # decode JSON value
                    try:        values[i] = json.loads(    values[i]    )
                    except:
                        try:    values[i] = json.loads('"'+values[i]+'"') # unquoted JSON string # TODO fix spec
                        except: values[i] = None
                    if i==timestampIndex:
                        try:    values[i] = timestamp(values[i])
                        except: pass
            previousValues = values
            #
            event = Event(zip(['_args']+names[:argsIndex],[values[argsIndex:]]+values[:argsIndex])) # _args must precede _arg_*
            try:
                otherData = event._other_data
                for name in otherData.keys():
                    event[name] = otherData[name]
                event.pop('_other_data')
            except:
                pass

            # restore specifically redundant values
            if event._id:
                eventCounts[event._id] = \
                    event._count               if isinstance(event._count, int) else \
                    1 + eventCounts[event._id] if event._id in eventCounts      else \
                    0
                event._count = eventCounts[event._id]

            previousEvent = event
            yield event


def writeTsvJson(path, events, fields=['_timestamp','_elapsed_s','_severity','_category','_function','_path','_line','_thread_id','_id','_count','_format','_arg_names','_arg_types','_args']):
    '''
    Writes a MODMED TSV+JSON log file at path, eliminating redundancy.
    See https://docs.google.com/viewer?a=v&pid=sites&srcid=bWlubWF4bWVkaWNhbC5jb218bW9kbWVkfGd4OjU4YTI2N2ZhMDIzZTBlZTU
    '''
    names = [m for m in ('_timestamp','_elapsed_s','_format') if m not in fields]           # MUST line 310
    for n in fields:                                                                           
        if n!='_args' and n==name(n, maybeReserved=True) and n not in names:                # MUST line 308, 311
            names.append(n)
    
    withoutRedundancy = [i for i,n in enumerate(names) 
                         if n not in ['_timestamp','_severity','_function','_path','_line','_count',
                                      '_computer_id','_process_id','_thread_id',
                                      '_user_id','_group_id','_object_id']] 
    elapsedSecsField = names.index('_elapsed_s')
    j = JsonEncoder()
    j.ensure_ascii = False
    
    with open(path, 'w') as tsv:
        print('\t'.join(names+['_other_data','_args']), file=tsv)                           # MUST line 303, 309
        previous        = None
        previousOthers  = None
        for event in events:
            assert isinstance(event, Event), event                                          # MUST line 315
            try:
                others = {n:getattr(event,n) 
                          for n in event.keys() if n not in names+['_args']}                # MUST line 320
                values = [getattr(event,f) for f in names]+[others]+event._args             # MUST line 311, 319

                # eliminate values redundant with previous ones using '' (not 'null' nor '""')
                try:
                    if others==previousOthers:
                        values[len(names)] = ''
                    if previous:
                        for field,n in enumerate(names):
                            if n=='_timestamp':
                                t0 = previous._timestamp
                                t1 = event   ._timestamp
                                elapsed = timedelta(0,event._elapsed_s-previous._elapsed_s)
                                if     abs((t0        -t1).total_seconds()) < 60  :         # line 337
                                    if abs((t0+elapsed-t1).total_seconds()) <  0.1:         # line 340
                                        values[field] = ''                                  # line 328
                            elif event[n]==previous[n]:
                                if n=='_severity' and event._severity==7:                   # line 332
                                    values[field] = ''
                                # TODO _count with eventCounts
                                elif n in ['_function','_path','_line'
                                          ,'_computer_id','_process_id','_thread_id'
                                          ,'_user_id','_group_id','_object_id'
                                          ,'_arg_names','_arg_types'] \
                                    or event[n] is None:                                    # line 330, 334
                                    values[field] = ''
                except Exception as e: 
                    logging.info(traceback.format_exc())

                previous        = event
                previousOthers  = others

                for i, v in enumerate(values):
                    value = j.encode(v)                                                     # MUST line 323
                    assert '\t' not in value, value
                    assert '\n' not in value, value
                    if isinstance(v, str) and '"' not in value[1:-1]:
                        values[i] = value[1:-1]                                             # WARNING encodes "1" as 1 which will be read back as int
                    elif '"' in value:
                        values[i] = '"'+value.replace('"','""')+'"'                         # TODO Fix spec
                    else:
                        values[i] = value

                print('\t'.join(values), file=tsv)                                          # MUST line 303, 318
            except Exception as e:
                logging.warning(traceback.format_exc())

# -*- coding: utf-8 -*-

import sys
import glob
import argparse
import json
import traceback

import logging
logging.basicConfig(stream=sys.stderr, level=logging.WARNING)

from modmed import trace

def tracepoints(filePath, dirs, **kwargs):
    '''
    Parses sourceDirPath and returns a collection of modmed.trace.Tracepoint optionally saved to tpPath
    '''
    tracepoints = trace.Tracepoints(filePath)
    try:
        for d in dirs:
            tracepoints.addTracepoints(d, **kwargs)
    except Exception as e:
        logging.warning(traceback.format_exc())

    tracepoints.save()


def convert(format, filePath, dstPath, dstFormat='json', tracepointsCsvPath=''):
    '''
    Reads a trace file using format (among 'tsv+modmed', 'lttng+cbor', 'json+incas', ...) at filePath and formats each event to dstPath according to % dstFormat.
    '''
    tracepoints = trace.Tracepoints(tracepointsCsvPath)

    # TODO if format=='json+modmed': ... else:
    if format.lower()=='tsv+modmed':
        src = trace.readTsvJson(filePath)
    elif format.lower()=='json+incas':
        from modmed.incas import readIncasLogJson
        src = readIncasLogJson(filePath, tracepoints)
    elif format.lower()=='lttng+cbor':
        from modmed.lttng import readLTTngCbor
        src = readLTTngCbor(filePath) # TODO tracepoints

    # TODO if dstFormat=='json+modmed': ... else:
    # TODO if dstFormat=='tsv+modmed': ... else:
    if dstFormat.startswith('tsv'):
        trace.writeTsvJson(dstPath, src)
    else:
        with open(dstPath, 'w') as dst:
            print(dstFormat, file=dst)
            for event in src:
                print((dstFormat % event).replace('\n',''), file=dst) #event._message = event._message # required to store _message before calling ** operator if using new-style format()


class AddNameValue(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        super().__init__(option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        n, v = values.split('=')
        if option_string=='-a':
            getattr(namespace, 'alternateSpelling')[n]=v
        else:
            getattr(namespace, option_string.lstrip('-'))[n]=v


if __name__ == '__main__':
    cmd = argparse.ArgumentParser()
    cmd.add_argument('-v','--verbose', type=bool                                            , help='logs informations')
    cmds = cmd.add_subparsers()

    csv = cmds.add_parser('tps', help='tracepoints')
    csv.add_argument('file'                                                                 , help='a tracepoints index TSV file')
    csv.add_argument('dir', nargs='*', default=['.']                                        , help='directory trees containing C++ source files with tracepoints to add')
    csv.add_argument('-c','--config' , default='traceCmd.json', type=argparse.FileType('r') , help='a JSON file containing an object with cxxExts, ids, clangArgs, templatedIds, alternateSpellings fields')

    cvt = cmds.add_parser('cvt', help='convert')
    cvt.add_argument('file'                                                                 , help='a trace file to convert')
    cvt.add_argument('format', choices=['tsv+modmed', 'json+incas', 'lttng+cbor'])
    cvt.add_argument('dstFile')
    cvt.add_argument('dstFormat', choices=['json', 'python']                                , help='a supported trace format or Python % format string')
    cvt.add_argument('tracepointsCsvPath', default=''                                       , help='a tracepoints index CSV file to help convert the trace to structured events')

    try:
        a = cmd.parse_args()
        if ('verbose' in a):
            logging.basicConfig(stream=sys.stderr, level=logging.INFO)
        
        if ('format' in a):
            convert(a.format,a.file,a.dstFile,a.dstFormat,a.tracepointsCsvPath)
        else:
            config = {'cxxExts'           :trace.cxxExtensions
                     ,'ids'               :trace.qtTracepointIds
                     ,'clangArgs'         :[]
                     ,'autoInsertSpace'   :True
                     ,'formatters'        :trace.qtFormatters
                     ,'formatMethods'     :trace.qtFormatMethods
                     ,'templatedIds'      :[]
                     ,'alternateSpellings':{}
                     }
            try:
                config.update(json.load(a.config))
            except:
                logging.warning(traceback.format_exc())
            tracepoints(a.file,a.dir,**config)
    
    except Exception as e:
        traceback.print_exc()
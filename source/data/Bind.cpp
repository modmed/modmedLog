/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <QtCore/qchar.h>
#include <QtCore/qstring.h>
#include <QtCore/qtextcodec.h>

#include <limits>

#include <modmed/data/Bind.h>

namespace modmed {
namespace data {

Hex::Hex(void* p_ptr) : Hex(uintptr_t(p_ptr))
{}

void Hex::bind(Item<Writer> p_atom) const
{
    QString s = "0x";
    QTextStream ss(&s);
    switch (m_valueType)
    {
    case Bool:
        ss << hex << *(bool*)m_valuePtr;
        break;
    case U8:
        ss << hex << *(uint8_t*)m_valuePtr;
        break;
    case U16:
        ss << hex << *(uint16_t*)m_valuePtr;
        break;
    case U32:
        ss << hex << *(uint32_t*)m_valuePtr;
        break;
    case U64:
        ss << hex << *(uint64_t*)m_valuePtr;
        break;
    }
    p_atom.bind(ss.string());
}

void Hex::bind(Item<Reader> p_atom)
{
    QString s;
    p_atom.bind(s);
    if (s[0] != '0' || s[1] != 'x')
    {
        p_atom.state()->addError(FailBindFrom(s, "Hex"));
        return;
    }
    else
    {
        QTextStream ss(&s);
        switch (m_valueType)
        {
        case Bool:
            {
                int b;
                ss >> hex >> b;
                *(bool*)m_valuePtr = b;
            }
            break;
        case U8:
            {
                int i;
                ss >> hex >> i;
                *(uint8_t*)m_valuePtr = i;
            }
            break;
        case U16:
            ss >> hex >> *(uint16_t*)m_valuePtr;
            break;
        case U32:
            ss >> hex >> *(uint32_t*)m_valuePtr;
            break;
        case U64:
            ss >> hex >> *(uint64_t*)m_valuePtr;
            break;
        }
    }
}

//-----------------------------------------------------------------------------

void Bind<Writer, const void*>::bind(Item<Writer> data, const void*& userData)
{
    data.bind(reinterpret_cast<const uintptr_t>(userData));
}

//-----------------------------------------------------------------------------

void Bind<Reader, Item<Writer>>::bind(Item<Reader> src, Item<Writer>& dst)
{
    if (!src.state()->isOk() || !dst.state()->isOk())
    {
        Q_ASSERT(src.state()->isOk() && dst.state()->isOk());
        return;
    }
    if (src.isNull())
    {
        dst.null();
        return;
    }
    Record<Reader> srcRecord(src.record());
    if (srcRecord.state()->isOk())
    {
        // TODO if (isOk()) Clear errors coming from previous src.isNull()
        Record<Writer> dstRecord(dst.record());
        if (dstRecord.state()->isOk())
        {
            QString srcKey;
            for (Item<Reader> srcItem = srcRecord.item(srcKey); srcItem.state()->isOk(); srcItem = srcRecord.item(srcKey))
            {
                Item<Writer> dstItem = dstRecord.item(srcKey);
                srcItem.bind(dstItem);
            }
        }
        return;
    }
    Sequence<Reader> srcSequence(src.sequence());
    if (srcSequence.state()->isOk())
    {
        // TODO if (isOk()) Clear errors coming from previous src.record()
        Sequence<Writer> dstSequence(dst.sequence());
        if (dstSequence.state()->isOk())
            for (Item<Reader> srcItem=srcSequence.item(); srcItem.state()->isOk(); srcItem=srcSequence.item())
            {
                Item<Writer> dstItem = dstSequence.item();
                srcItem.bind(dstItem);
            }
        return;
    }
    // FIXME add check for leaf type text/number/bool/...
    QString srcData;
    src.bind(srcData);
    dst.bind(srcData);
}

void Bind<Reader, std::string>::bind(Item<Reader> d, std::string& data)
{
    QString s;
    BindErrors err;
    if (d.state()->errors() == nullptr)
    { d.state()->trapErrors(&err); }
    if (d.bind(s).noError())
    {
        data.clear();
        data.reserve((size_t)s.size());
        QTextCodec* codec = QTextCodec::codecForLocale();
        Q_FOREACH (QChar c, s)
        {
            QTextCodec::ConverterState state;
            QByteArray conv = codec->fromUnicode(&c, 1, &state);
            if (conv.size() != 1) { data = s.toStdString(); return; }
            if (!state.invalidChars && c == codec->toUnicode(conv))
            { data += conv.data(); }
            else
            {
                data += '\x01A';
                d.state()->addError(FailBind("Converting a QString that contains non Local 8 bits characters into a std::string", true));
            }
        }
    }
}

void Bind<Writer, std::string>::bind(Item<Writer> d, std::string& data)
{
    QString tmp(QString::fromStdString(data));
    d.bind(tmp);
}

void Bind<Reader, std::wstring>::bind(Item<Reader> d, std::wstring& data)
{
    QString s;
    BindErrors err;
    if (d.state()->errors() == nullptr)
    { d.state()->trapErrors(&err); }
    if (d.bind(s).noError())
    { data = s.toStdWString(); }
}

void Bind<Writer, std::wstring>::bind(Item<Writer> d, std::wstring& data)
{
    QString tmp(QString::fromStdWString(data));
    d.bind(tmp);
}

template<>
void Bind<Reader, QtMsgType>::bind(Item<Reader> data, QtMsgType& userData)
{
    // Same as RFC5424 (Syslog) "PRI"
    // \see _severity_id
    QString  canonicalMsgType;
    data.bind(canonicalMsgType);
    canonicalMsgType = canonicalMsgType.toUpper();
    /**/ if (canonicalMsgType == "DEBUG"        ) { userData = QtDebugMsg; }
    else if (canonicalMsgType == "INFORMATIONAL") { userData = QtInfoMsg; }
    else if (canonicalMsgType == "WARNING"      ) { userData = QtWarningMsg; }
    else if (canonicalMsgType == "CRITICAL"     ) { userData = QtCriticalMsg; }
    else if (canonicalMsgType == "EMERGENCY"    ) { userData = QtFatalMsg; }
    else { data.state()->addError(FailBindFrom(canonicalMsgType, "QtMsgType")); }
}

template<>
void Bind<Writer, QtMsgType>::bind(Item<Writer> data, QtMsgType& userData)
{
    /**/ if (userData == QtDebugMsg   ) { data.bind(QStringLiteral("DEBUG"        )); }
    else if (userData == QtInfoMsg    ) { data.bind(QStringLiteral("INFORMATIONAL")); }
    else if (userData == QtWarningMsg ) { data.bind(QStringLiteral("WARNING"      )); }
    else if (userData == QtCriticalMsg) { data.bind(QStringLiteral("CRITICAL"     )); }
    else if (userData == QtFatalMsg   ) { data.bind(QStringLiteral("EMERGENCY"    )); }
    // data addError ?
}

template<>
void Bind<Writer, QDate>::bind(Item<Writer> data, QDate& userData)
{
    data.bind(userData.toString(Qt::ISODate));
}

template<>
void Bind<Reader, QDate>::bind(Item<Reader> data, QDate& userData)
{
    QString s;
    data.bind(s);
    userData = QDate::fromString(s, Qt::ISODate);
    if (userData.isNull())
        data.state()->addError(FailBindFrom(s, "QDate"));
}

template<>
void Bind<Writer, QTime>::bind(Item<Writer> data, QTime& userData)
{
    data.bind(userData.toString(Qt::ISODateWithMs));
}

template<>
void Bind<Reader, QTime>::bind(Item<Reader> data, QTime& userData)
{
    QString s;
    data.bind(s);
    userData = QTime::fromString(s, Qt::ISODateWithMs);
    if (userData.isNull())
        data.state()->addError(FailBindFrom(s, "QTime"));
}

template<>
void Bind<Writer, QUuid>::bind(Item<Writer> data, QUuid& userData)
{
    data.bind(userData.toString());
}

template<>
void Bind<Reader, QUuid>::bind(Item<Reader> data, QUuid& userData)
{
    QString s;
    data.bind(s);
    userData = QUuid(s);
    if (userData.isNull())
        data.state()->addError(FailBindFrom(s, "QUuid"));
}

QT_WARNING_PUSH
QT_WARNING_DISABLE_MSVC(4100)
void Bind<Writer, QCoreApplication>::bind(Item<Writer> data, QCoreApplication& userData)
{
    data.record()
    .bind(QStringLiteral("application_name"     ), userData.applicationName())
    .bind(QStringLiteral("application_version"  ), userData.applicationVersion())
    .bind(QStringLiteral("organization_domain"  ), userData.organizationDomain())
    .bind(QStringLiteral("organization_name"    ), userData.organizationName())
    .bind(QStringLiteral("application_file_path"), userData.applicationFilePath())
    .bind(QStringLiteral("application_pid"      ), userData.applicationPid())
    .bind(QStringLiteral("arguments"            ), userData.arguments())
    .bind(QStringLiteral("library_paths"        ), userData.libraryPaths())
    ;
}
QT_WARNING_POP

template<>
void Bind<Writer, QHostAddress>::bind(Item<Writer> data, QHostAddress& userData)
{
    data.bind(userData.toString());
}

template<>
void Bind<Reader, QHostAddress>::bind(Item<Reader> data, QHostAddress& userData)
{
    QString s;
    data.bind(s);
    userData = QHostAddress(s);
    if (userData.isNull())
        data.state()->addError(FailBindFrom(s, "QHostAddress"));
}

template<>
void Bind<Writer, QVersionNumber>::bind(Item<Writer> data, QVersionNumber& userData)
{
    data.bind(userData.toString());
}

template<>
void Bind<Reader, QVersionNumber>::bind(Item<Reader> data, QVersionNumber& userData)
{
    QString s;
    data.bind(s);
    QStringList list = s.split('.');
    QVector<int> listVersionNumber(list.size());
    for (int i = 0; i < list.size(); i++)
    {
        listVersionNumber[i] = list[i].toInt();
    }

    userData = QVersionNumber(listVersionNumber);
    if (userData.isNull())
        data.state()->addError(FailBindFrom(s, "QVersionNumber"));
    // Check userData.isNull ?
}

void Bind<Reader, QProcessEnvironment>::bind(Item<Reader> data, QProcessEnvironment& userData)
{
    Record<Reader> r(data.record());
    if (!r.state()->isOk()) { return; }
    QString k;
    userData.clear();
    for (Item<Reader> i(r.item(k)); i.state()->isOk(); i = r.item(k))
    {
        QString v;
        i.bind(v);
        userData.insert(k, v);
    }
}

void Bind<Writer, QProcessEnvironment>::bind(Item<Writer> data, QProcessEnvironment& userData)
{
    Record<Writer> r(data.record());
    if (!r.state()->isOk()) { return; }
    QStringList keysList = userData.keys();
    if (keysList.size() == 0) {return;}
    for (int items = 0; items < keysList.size() ; items++)
    {
        r.bind(keysList[items], userData.value(keysList[items]));
    }
}

} // data
} // modmed

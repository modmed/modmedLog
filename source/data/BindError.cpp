/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <modmed/data/BindError.h>

#include <modmed/data/Bind.h>

namespace modmed {
namespace data {

void BindErrors::bind(Item<Writer> data)
{
    bindQtDictAsSequenceOfRecord(data, *this);
}

//==============================================================================
struct BindErrorPrivate
{
    BindErrorPrivate(const QString& p_description, bool p_isReader) :
        m_description(p_description), m_isReader(p_isReader) {}
    QString m_description;
    bool m_isReader;
};

//==============================================================================
QString BindError::toString() const { return m_d->m_description; }

//==============================================================================
bool BindError::isReader() const { return m_d->m_isReader; }

//==============================================================================
BindError::~BindError() {}

//==============================================================================
BindError::BindError(const QString& p_description, bool p_isReader) :
    m_d(new BindErrorPrivate(p_description, p_isReader)) {}

//==============================================================================
void Bind<Writer, BindError>::bind(Item<Writer> d, BindError& t) { d.bind(t.toString()); }

//==============================================================================
Unknown::Unknown(const QString& p_description, bool p_isReader) :
    BindError(p_description, p_isReader), _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> Unknown::clone() const
{
    return QSharedPointer<Unknown>(new Unknown(toString(), isReader()));
}

//==============================================================================
FailBind::FailBind(const QString& p_description, bool p_isReader) :
    BindError(p_description, p_isReader), _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailBind::clone() const
{
    return QSharedPointer<FailBind>(new FailBind(toString(), isReader()));
}

//==============================================================================
FailBindFrom::FailBindFrom(QString p_read, QString p_type):
    BindError("Fail bindFrom(\""+ p_read + "\", " + p_type + ") returned false", true),
    m_d(new QPair<QString,QString>(p_read, p_type)) {}

//==============================================================================
QSharedPointer<BindError> FailBindFrom::clone() const
{
    return QSharedPointer<FailBindFrom>(new FailBindFrom(m_d->first, m_d->second));
}

//==============================================================================
QString FailBindFrom::getRead() const
{
    return m_d->first;
}

//==============================================================================
QString FailBindFrom::getType() const
{
    return m_d->second;
}

//==============================================================================
FailNull::FailNull(bool p_isReader) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " null", p_isReader),
    _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailNull::clone() const
{
    return QSharedPointer<FailNull>(new FailNull(isReader()));
}

//==============================================================================
FailSequence::FailSequence(bool p_isReader) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a sequence", p_isReader),
    _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailSequence::clone() const
{
    return QSharedPointer<FailSequence>(new FailSequence(isReader()));
}

//==============================================================================
FailSequenceItem::FailSequenceItem(bool p_isReader, int p_idx) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " element " + QString::number(p_idx), p_isReader),
    m_idx(new int(p_idx)) {}

//==============================================================================
QSharedPointer<BindError> FailSequenceItem::clone() const
{
    return QSharedPointer<FailSequenceItem>(new FailSequenceItem(isReader(), getIdx()));
}

//==============================================================================
int FailSequenceItem::getIdx() const
{
    return *m_idx;
}

//==============================================================================
FailSequenceOut::FailSequenceOut(bool p_isReader) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a sequence out", p_isReader),
    _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailSequenceOut::clone() const
{
    return QSharedPointer<FailSequenceOut>(new FailSequenceOut(isReader()));
}

//==============================================================================
FailRecord::FailRecord(bool p_isReader) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a record", p_isReader),
    _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailRecord::clone() const
{
    return QSharedPointer<FailRecord>(new FailRecord(isReader()));
}

//==============================================================================
FailRecordItem::FailRecordItem(bool p_isReader, QString p_key) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " field \"" + p_key + "\"", p_isReader),
    m_key(new QString(p_key)) {}

//==============================================================================
QSharedPointer<BindError> FailRecordItem::clone() const
{
    return QSharedPointer<FailRecordItem>(new FailRecordItem(isReader(), getKey()));
}

//==============================================================================
QString FailRecordItem::getKey() const
{
    return *m_key;
}

//==============================================================================
FailRecordItemEmptyKey::FailRecordItemEmptyKey(bool p_isReader) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a record field with an empty key", p_isReader),
    _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailRecordItemEmptyKey::clone() const
{
    return QSharedPointer<FailRecordItemEmptyKey>(new FailRecordItemEmptyKey(isReader()));
}

//==============================================================================
FailRecordItemWrongKey::FailRecordItemWrongKey(QString p_expectedKey, QString p_readKey) :
    BindError("Try to read key \"" + p_expectedKey + "\" but found key \"" + p_readKey + '"', true),
    m_d(new QPair<QString,QString>(p_expectedKey, p_readKey)) {}

//==============================================================================
QSharedPointer<BindError> FailRecordItemWrongKey::clone() const
{
    return QSharedPointer<FailRecordItemWrongKey>(new FailRecordItemWrongKey(m_d->first, m_d->second));
}

//==============================================================================
QString FailRecordItemWrongKey::getExpectedKey() const
{
    return m_d->first;
}

//==============================================================================
QString FailRecordItemWrongKey::getReadKey() const
{
    return m_d->second;
}

//==============================================================================
FailRecordOut::FailRecordOut(bool p_isReader) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a record out", p_isReader),
    _RESERVED_FOR_FUTURE_USE_(nullptr) {}

//==============================================================================
QSharedPointer<BindError> FailRecordOut::clone() const
{
    return QSharedPointer<FailRecordOut>(new FailRecordOut(isReader()));
}

//==============================================================================
FailText::FailText(bool p_isReader, QString p_txt) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a constant text \"" + p_txt + QChar('"'), p_isReader),
    m_txt(new QString(p_txt)) {}

//==============================================================================
QString FailText::getText() const
{
    return *m_txt;
}

//==============================================================================
QSharedPointer<BindError> FailText::clone() const
{
    return QSharedPointer<FailText>(new FailText(isReader(), getText()));
}

//==============================================================================
FailBindText::FailBindText(bool p_isReader, QString p_txt) :
    BindError(QString("Fail to ") + (p_isReader ? "read" : "write") + " a text data \"" + p_txt + QChar('"'), p_isReader),
    m_txt(new QString(p_txt)) {}

//==============================================================================
QSharedPointer<BindError> FailBindText::clone() const
{
    return QSharedPointer<FailBindText>(new FailBindText(isReader(), getText()));
}

//==============================================================================
QString FailBindText::getText() const
{
    return *m_txt;
}

} // data
} // modmed

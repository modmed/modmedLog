/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <modmed/data/BindState.h>
#include <modmed/data/Bind.h>
#include <modmed/data/JsonWriter.h>

#include <QtCore/qvector.h>
#include <QtCore/qstring.h>

#include <modmed/log/LogMessage.h>
#include <modmed/log/LogEvent.h>

namespace modmed {
namespace data {

M_NAMESPACE_LOG_CATEGORY()

//==============================================================================
template<Operator Op>
ItmState<Op>::~ItmState()
{
    out();
}

template ItmState<Reader>::~ItmState();
template ItmState<Writer>::~ItmState();

// Return true if bindNumber succeeded (no need to try bindText)
template<typename T>
static bool bindNumber(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, T& num)
{
    if (std::is_integral<T>::value) // CR && size<T> <= size<qlonglong> ?
    {
        qlonglong n = num;
        if (!i->bindNumber(*state, n))
        {
            return false;
        }
        if (n < std::numeric_limits<T>::min() || n > std::numeric_limits<T>::max())
        {
            state->addError(FailBind("Fail to read a number (overflow with current type)", true));
        }
        num = (T)n;
    }
    else
    {
        double d = num;
        if (!i->bindNumber(*state, d))
        {
            return false;
        }
        num = (T)d;
    }
    return true;
}

// Return true if bindNumber succeeded (no need to try bindText)
template<typename T>
static void bindNumber(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, T& num)
{
    qlonglong n = num;
    if (i->bindNumber(*state, n))
    {
        return;
    }
    QString bindText = QString::number(num);
    if (!i->bindText(*state, bindText))
    {
        state->addError(FailBind("Call to bindText failed", false));
    }
}

void BindDispatch<Reader, QDateTime>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, QDateTime& p_userAtom)
{
    if (!i->bindTimeStamp(*state, p_userAtom))
    {
        QString bindText;
        if (i->bindText(*state, bindText))
        {
            QString offsetFromUtc = bindText.right(5);
            QTime timeFromUtc = QTime::fromString(bindText.right(4), "HHmm");
            if (!timeFromUtc.isValid())
            {
                state->addError(FailBindFrom(bindText, "QDateTime"));
                return;
            }

            QString qdt = bindText;
            qdt.chop(5);
            p_userAtom = QDateTime::fromString(qdt, "yyyy-MM-ddTHH:mm:ss.zzz");
            p_userAtom.setTimeSpec(Qt::OffsetFromUTC);
            if (offsetFromUtc[0] == '-')
            {
                p_userAtom.setOffsetFromUtc(-timeFromUtc.msecsSinceStartOfDay() / 1000);
            }
            else if (offsetFromUtc[0] == '+')
            {
                p_userAtom.setOffsetFromUtc(timeFromUtc.msecsSinceStartOfDay() / 1000);
            }
            else
            {
                state->addError(FailBindFrom(bindText, "QDateTime"));
                return;
            }
            if (p_userAtom.isNull())
                state->addError(FailBindFrom(bindText, "QDateTime"));
        }
        else
        {
            state->addError(FailBind("Call to bindText failed", true));
        }
    }
}

void BindDispatch<Writer, QDateTime>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, QDateTime& p_userAtom)
{
    if (!i->bindTimeStamp(*state, p_userAtom))
    {
        QString bindText = p_userAtom.toString("yyyy-MM-ddTHH:mm:ss.zzz");
        if (bindText.isEmpty())
        {
            state->addError(FailBind("Fail to convert a QDateTime into a QString", false));
            return;
        }
        int offsetFromUtc = p_userAtom.offsetFromUtc();
        if (offsetFromUtc < 0)
        {
            bindText.append("-");
            offsetFromUtc = -offsetFromUtc;
        }
        else
        {
            bindText.append("+");
        }
        QTime timeFromUtc(offsetFromUtc / 3600, offsetFromUtc % 3600 / 60);
        if (!timeFromUtc.isValid())
        {
            state->addError(FailBind("Fail to compute timeFromUtc", false));
            return;
        }
        bindText.append(timeFromUtc.toString("HHmm"));
        if (!i->bindText(*state, bindText))
        {
            state->addError(FailBind("Call to bindText failed", false));
        }
    }
}

void BindDispatch<Reader, bool>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, bool& p_userAtom)
{
    if (!i->bindBoolean(*state, p_userAtom))
    {
        QString bindText;
        if (i->bindText(*state, bindText))
        {
            if (bindText != "true" && bindText != "false")
            {
                state->addError(FailBindFrom(bindText, "bool"));
                return;
            }
            p_userAtom = bindText == "true";
        }
        else
        {
            state->addError(FailBind("Call to bindText failed", true));
        }
    }
}

void BindDispatch<Writer, bool>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, bool& p_userAtom)
{
    if (!i->bindBoolean(*state, p_userAtom))
    {
        QString bindText;
        if (p_userAtom) { bindText = "true"; }
        else { bindText = "false"; }
        if (!i->bindText(*state, bindText))
        {
            state->addError(FailBind("Call to bindText failed", false));
        }
    }
}

void BindDispatch<Reader, char>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, char& p_userAtom)
{
    if (!bindNumber(i, state, p_userAtom))
    {
        QString bindText;
        if (i->bindText(*state, bindText))
        {
            bool result;
            int i = bindText.toInt(&result);
            if (!result || (i >= 128) || (i < -128))
            {
                state->addError(FailBindFrom(bindText, "char"));
                return;
            }
            p_userAtom = (char)i;
        }
        else
        {
            state->addError(FailBind("Call to bindText failed", true));
        }
    }
}
void BindDispatch<Writer, char>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, char& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, unsigned char>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned char& p_userAtom)
{
    if (!bindNumber(i, state, p_userAtom))
    {
        QString bindText;
        if (i->bindText(*state, bindText))
        {
            bool result;
            int i = bindText.toInt(&result);
            if (!result || (i >= 256) || (i < 0))
            {
                state->addError(FailBindFrom(bindText, "unsigned char"));
                return;
            }
            p_userAtom = (unsigned char)i;
        }
        else
        {
            state->addError(FailBind("Call to bindText failed", true));
        }
    }
}
void BindDispatch<Writer, unsigned char>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned char& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

#define BIND_NUMBER_READER(ConvFuncName, StrTypeName) \
    if (!bindNumber(i, state, p_userAtom)) \
    { \
        QString bindText; \
        if (i->bindText(*state, bindText)) \
        { \
            bool result; \
            p_userAtom = bindText.to##ConvFuncName(&result); \
            if (!result) \
            { \
                state->addError(FailBindFrom(bindText, StrTypeName)); \
                return; \
            } \
        } \
        else \
        { \
            state->addError(FailBind("Call to bindText failed", true)); \
        } \
    }

void BindDispatch<Reader, short>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, short& p_userAtom)
{
    BIND_NUMBER_READER(Short, "short")
}
void BindDispatch<Writer, short>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, short& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, unsigned short>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned short& p_userAtom)
{
    BIND_NUMBER_READER(UShort, "unsigned short")
}
void BindDispatch<Writer, unsigned short>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned short& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, int>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, int& p_userAtom)
{
    BIND_NUMBER_READER(Int, "int")
}
void BindDispatch<Writer, int>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, int& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, unsigned int>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned int& p_userAtom)
{
    BIND_NUMBER_READER(UInt, "unsigned int")
}
void BindDispatch<Writer, unsigned int>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned int& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, long>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, long& p_userAtom)
{
    BIND_NUMBER_READER(Long, "long")
}
void BindDispatch<Writer, long>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, long& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, unsigned long>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned long& p_userAtom)
{
    BIND_NUMBER_READER(ULong, "unsigned long")
}
void BindDispatch<Writer, unsigned long>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned long& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, long long>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, long long& p_userAtom)
{
    BIND_NUMBER_READER(LongLong, "long long")
}
void BindDispatch<Writer, long long>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, long long& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, unsigned long long>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, unsigned long long& p_userAtom)
{
    BIND_NUMBER_READER(ULongLong, "unsigned long long")
}
void BindDispatch<Writer, unsigned long long>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, unsigned long long& p_userAtom)
{
    bindNumber(i, state, p_userAtom);
}

void BindDispatch<Reader, float>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, float& p_userAtom)
{
    BIND_NUMBER_READER(Float, "float")
}
void BindDispatch<Writer, float>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, float& p_userAtom)
{
    double d = p_userAtom;
    if (i->bindNumber(*state, d))
    {
        return;
    }
    QString bindText  = QString::number(p_userAtom, 'g', std::numeric_limits<float>::max_digits10);
    if (!i->bindText(*state, bindText))
    {
        state->addError(FailBind("Call to bindText failed", false));
    }
}

void BindDispatch<Reader, double>::bind(IItem<Reader>* i, QSharedPointer<ItmState<Reader>> state, double& p_userAtom)
{
    BIND_NUMBER_READER(Double, "double")
}
void BindDispatch<Writer, double>::bind(IItem<Writer>* i, QSharedPointer<ItmState<Writer>> state, double& p_userAtom)
{
    if (i->bindNumber(*state, p_userAtom))
    {
        return;
    }
    QString bindText  = QString::number(p_userAtom, 'g', std::numeric_limits<double>::max_digits10);
    if (!i->bindText(*state, bindText))
    {
        state->addError(FailBind("Call to bindText failed", false));
    }
}

} // data
} // modmed

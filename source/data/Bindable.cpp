/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/

#include <modmed/data/Bind.h>
#include <modmed/data/Bindable.h>

#include <cstring>

namespace modmed {
namespace data {

//==============================================================================
IBindable::~IBindable() {}

//==============================================================================
BindableData::BindableData(BindableMode p_mode) :
    m_data(new QSharedPointer<IBindable>),
    m_mode(p_mode)
{}

//==============================================================================
BindableData::BindableData(const BindableData& p_src) :
    m_data(new QSharedPointer<IBindable>)
{
    *m_data = *(p_src.m_data);
    m_mode = p_src.m_mode;
}

//==============================================================================
BindableData& BindableData::operator=(const BindableData& p_src)
{
    *m_data = *(p_src.m_data);
    m_mode = p_src.m_mode;
    return *this;
}

//==============================================================================
BindableSequence* BindableData::sequence()
{
    auto p_src = new BindableSequence(m_mode);
    m_data->reset(p_src);
    return p_src;
}

//==============================================================================
BindableRecord* BindableData::record()
{
    auto r = new BindableRecord(m_mode);
    m_data->reset(r);
    return r;
}

//==============================================================================
void BindableData::null()
{
    m_data->reset();
}

//==============================================================================
void BindableData::text(const char* txt)
{
    if (m_mode == BindableModeCopy)
        m_data->reset(new BindableCopy<const char*>(txt));
    else if (m_mode == BindableModeRef)
        m_data->reset(new BindableRef<const char*>(txt));
}

//==============================================================================
void BindableData::out()
{}

//==============================================================================
void BindableData::bind(data::Item<Writer> data)
{
    if (*m_data == nullptr) { data.null(); }
    else { (*m_data)->bind(data); }
}

//==============================================================================
Itm<BindableResult> BindableData::write()
{
    return Itm<BindableResult>(this);
}

//==============================================================================
BindableSequence::BindableSequence(BindableMode p_mode) :
    m_data(new QVector<BindableData>()),
    m_mode(p_mode)
{}

//==============================================================================
BindableData* BindableSequence::item()
{
    m_data->push_back(BindableData(m_mode));
    return &(m_data->back());
}

//==============================================================================
void BindableSequence::out()
{}

//==============================================================================
void BindableSequence::bind(Item<Writer> data)
{
    Sequence<Writer> p_src(data.sequence());
    for (auto& userItem : *m_data)
    {
        if (p_src.state()->isOk())
            p_src.bind(userItem);
        else
            break;
    }
}

//==============================================================================
BindableRecord::BindableRecord(BindableMode p_mode) :
    m_data(new QVector<QPair<QString, BindableData>>()),
    m_mode(p_mode)
{}

//==============================================================================
BindableData* BindableRecord::item(const QString& p_key)
{
    m_data->push_back(qMakePair(p_key, BindableData(m_mode)));
    return &(m_data->back().second);
}

//==============================================================================
void BindableRecord::out()
{}

//==============================================================================
void BindableRecord::bind(Item<Writer> data)
{
    Record<Writer> r(data.record());
    for (auto& userItem : *m_data)
    {
        if (r.state()->isOk())
            r.bind(userItem.first, userItem.second);
        else
            break;
    }
}

BindableRef<const char*>::BindableRef(const char* p_str) :
    m_data(p_str)
{}

void BindableRef<const char*>::bind(Item<Writer> p_data)
{
    p_data.text(m_data);
}

BindableCopy<const char*>::BindableCopy(const char* p_str)
{
    m_data.reset(new char[std::strlen(p_str) + 1]);
    std::strcpy(m_data.data(), p_str);
}

void BindableCopy<const char*>::bind(Item<Writer> p_data)
{
    p_data.text(m_data.data());
}

} // data
} // modmed

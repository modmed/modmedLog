/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <QtCore/qstring.h>
#include <QtCore/qtextcodec.h>
#include <QtCore/qbytearray.h>

//#include <QtCore/qiodevice.h>
#include <QtCore/qfile.h>

#include <modmed/data/CborWriter.h>

#include "cbor.h"

namespace modmed {
namespace data {

struct CborWriterPrivate
{
    bool           m_isDocument;
    QIODevice*     m_io;

    CborWriterPrivate(bool p_isDocument, QIODevice* p_io)
        : m_isDocument(p_isDocument)
        , m_io(p_io)
    {}

    bool text(const QString& p_txt);
    bool integer(char p_type, qlonglong p_integer);
    bool floatingPoint(char p_type, double p_val);
    bool end(const BindState<Writer>& path);
};

bool CborWriterPrivate::text(const QString& p_txt)
{
    QByteArray utf8 = p_txt.toUtf8();
    if (integer(cbor::TextStringType, utf8.length()))
    {
        m_io->write(utf8);
    }
    else
    {
        return false;
    }
    return true;
}

bool CborWriterPrivate::integer(char p_type, qlonglong p_integer)
{
    // See https://tools.ietf.org/html/rfc7049#section-2.2 if more performance is needed

    if (p_type == cbor::NegativeIntegerType)
    {
        p_integer = -(p_integer+1);
    }

    int inc;
    if (p_integer < 24)
    {
        m_io->putChar((p_type << cbor::MajorTypeShift) | p_integer);
        return true;
    }
    else if (p_integer <= 0xff)
    {
        m_io->putChar((p_type << cbor::MajorTypeShift) | cbor::Value8Bit);
        inc = 8;
    }
    else if (p_integer <= 0xffff)
    {
        m_io->putChar((p_type << cbor::MajorTypeShift) | cbor::Value16Bit);
        inc = 16;
    }
    else if (p_integer <= 0xffffffffL)
    {
        m_io->putChar((p_type << cbor::MajorTypeShift) | cbor::Value32Bit);
        inc = 32;
    }
    else
    {
        m_io->putChar((p_type << cbor::MajorTypeShift) | cbor::Value64Bit);
        inc = 64;
    }
    for (int i = inc - 8 ; i >= 0 ; i-=8)
    {
        m_io->putChar(p_integer >> i);
    }
    return true;
}

bool CborWriterPrivate::floatingPoint(char p_type, double p_val)
{
    // double precision
    // sign 1
    // exponent 11
    // precision 53
    // exponent bias 1023

    union {
      double vald;
      uint64_t valu;
    } u64;
    u64.vald = p_val;

    m_io->putChar((p_type << cbor::MajorTypeShift) | cbor::DoublePrecisionFloat);
    int inc = 64;

    for (int i = inc - 8 ; i >= 0 ; i-=8)
    {
        m_io->putChar(u64.valu >> i);
    }

    return true;
}

bool CborWriterPrivate::end(const BindState<Writer>& path)
{
    if (!path.parent() && m_isDocument)
    {
        return m_io->putChar(cbor::BreakByte);
    }
    return true;
}

//==============================================================================
CborWriter::CborWriter(QIODevice* s, const QString& documentInformation /*= QString()*/)
    : m_d(new CborWriterPrivate(false, s))
{
    m_d->m_io->putChar(0xFFu & ((cbor::TagType << cbor::MajorTypeShift) | cbor::Value16Bit));
    m_d->m_io->putChar(0xFFu & ( cbor::CborDocument >> 8                                  ));
    m_d->m_io->putChar(0xFFu & ( cbor::CborDocument                                       ));

    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
CborWriter::~CborWriter() {}

//==============================================================================
CborWriter& CborWriter::document(const QString& information /*= QString("")*/)
{
    Q_ASSERT(!information.isNull());
    Q_ASSERT(!m_d->m_isDocument);
    Q_ASSERT(m_d->m_io->pos() == 0);

    m_d->m_io->putChar(cbor::IndefiniteLengthMapByte);
    if (!information.isEmpty())
    {
        m_d->text("document");
        m_d->text(information);
    }
    m_d->text("_events");
    m_d->m_isDocument = true;
    return *this;
}

//==============================================================================
bool CborWriter::isDocument() { return m_d->m_isDocument; }

//==============================================================================
bool CborWriter::isValid() { return true /*m_d->m_io->good()*/; }

//==============================================================================
bool CborWriter::sequenceOut(const SeqState<Writer>& path)
{
    return m_d->m_io->putChar(cbor::BreakByte) && m_d->end(path);
}

//==============================================================================
bool CborWriter::recordOut  (const RecState<Writer>& path)
{
    return m_d->m_io->putChar(cbor::BreakByte) && m_d->end(path);
}

//==============================================================================
ISequence<Writer>* CborWriter::sequence(const ItmState<Writer>&)
{
    return m_d->m_io->putChar(cbor::IndefiniteLengthArrayByte) ? this : nullptr;
}

//==============================================================================
IRecord<Writer>* CborWriter::record(const ItmState<Writer>&)
{
    return m_d->m_io->putChar(cbor::IndefiniteLengthMapByte) ? this : nullptr;
}

//==============================================================================
bool CborWriter::bindNumber(const ItmState<Writer>&, qlonglong& p_integer)
{
    if (p_integer < 0)
    {
        return m_d->integer(cbor::NegativeIntegerType, p_integer);
    }
    return m_d->integer(cbor::UnsignedIntegerType, p_integer);
}

//==============================================================================
bool CborWriter::bindNumber(const ItmState<Writer>&, double& p_floatingPoint)
{
    return m_d->floatingPoint(cbor::SimpleTypesType, p_floatingPoint);
}

//==============================================================================
bool CborWriter::bindBoolean(const ItmState<Writer>&, bool& p_bool)
{
    return m_d->m_io->putChar(p_bool ? cbor::TrueByte : cbor::FalseByte);
}

//==============================================================================
bool CborWriter::bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime)
{
    m_d->m_io->putChar(0xFFu & (cbor::tag::Tag | cbor::tag::DateTime)); // tag datetime
    QString timestamp = p_datetime.toString(Qt::DateFormat::ISODate);
    if (timestamp.endsWith("Z", Qt::CaseSensitive))
    {
        timestamp.remove(timestamp.length()-1, 1);
        timestamp.append("+00:00");
    }
    return m_d->text(timestamp);
}

//==============================================================================
bool CborWriter::bindBytes(const ItmState<Writer>&, QByteArray& p_bytes)
{
    if (m_d->integer(cbor::ByteStringType, p_bytes.length()))
    {
        m_d->m_io->write(p_bytes);
    }
    else
    {
        return false;
    }
    return true;
}

//==============================================================================
bool CborWriter::null(const ItmState<Writer>&)
{
    return m_d->m_io->putChar(cbor::NullByte);
}

//==============================================================================
IItem<Writer>* CborWriter::sequenceItem(const SeqState<Writer>&)
{
    return this;
}

//==============================================================================
IItem<Writer>* CborWriter::recordItem(const RecState<Writer>&, QString& key)
{
    Q_ASSERT(!key.isEmpty());
    return m_d->text(key) ? this : nullptr;
}

//==============================================================================
bool CborWriter::text(const ItmState<Writer>& tp, const QString& txt)
{
    return m_d->text(txt.toUtf8()) && m_d->end(tp);
}

//==============================================================================
bool CborWriter::bindText(const ItmState<Writer>& tp, QString& txt)
{
    return m_d->text(txt.toUtf8()) && m_d->end(tp);
}

} // data
} // modmed

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <QtCore/qtextcodec.h>
#include <QtCore/qtextstream.h>
#include <iostream>
#include <modmed/data/JsonReader.h>

namespace modmed {
namespace data {

struct JsonReaderPrivate
{
    JsonReader*    q_ptr;
    bool           m_isDocument;
    QTextStream*   m_stream;
    QChar          m_nextChar;

    JsonReaderPrivate(JsonReader* q, bool p_isDocument, QTextStream* p_s) :
        q_ptr(q),
        m_isDocument(p_isDocument),
        m_stream(p_s)
    {}

    void       init();
    ISequence<Reader>* sequence    ();
    IRecord<Reader>*   record      ();
    bool       bindNumber(qlonglong& p_longlong, double& p_double);
    bool       bindBoolean(bool& p_bool);
    bool       bindTimeStamp(QDateTime& p_bool);
    bool       null        ();
    bool       sequenceOut ();
    bool       recordOut   ();

    QChar      next(QString ends);
    bool       get(QChar expected);
    bool       get(QChar expected, QString others);
    QChar      getChar();
    QChar      getCharInString();
    int        getDigit();
    bool       skipItem();
    bool       skipKey();
    bool       readKey(QString&);
};

//! \see http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf

//==============================================================================
JsonReader::JsonReader(QTextStream* s, const QString& documentInformation /*= QString()*/):
    m_d(new JsonReaderPrivate(this, false, s))
{
    m_d->init();
    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
JsonReader::~JsonReader() {}

//==============================================================================
void JsonReaderPrivate::init()
{
    getChar(); // to peek m_nextChar
}

//==============================================================================
JsonReader& JsonReader::document(const QString& information /*= QString("\"\"")*/)
{
    Q_UNUSED(information);
    Q_ASSERT(!information.isNull());
    Q_ASSERT(!m_d->m_isDocument);
    Q_ASSERT(m_d->m_stream->pos() == 0);
    Q_ASSERT(false); //! \todo Read up to document root
    m_d->m_isDocument = true;
    return *this;
}

//==============================================================================
bool JsonReader::isDocument() { return m_d->m_isDocument; }

//==============================================================================
void JsonReader::flush() { m_d->m_stream->flush(); }

//==============================================================================
bool JsonReader::isValid() { return m_d->m_stream->status() == QTextStream::Ok; }

//==============================================================================
ISequence<Reader>* JsonReader::sequence(const ItmState<Reader>&) { return m_d->sequence(); }

//==============================================================================
IRecord<Reader>* JsonReader::record(const ItmState<Reader>&) { return m_d->record(); }

//==============================================================================
bool JsonReader::bindNumber(const ItmState<Reader>&, qlonglong& ll) { double d; return m_d->bindNumber(ll, d); }

//==============================================================================
bool JsonReader::bindNumber(const ItmState<Reader>&, double& d) { qlonglong ll; return m_d->bindNumber(ll, d); }

//==============================================================================
bool JsonReader::bindBoolean(const ItmState<Reader>&, bool& b) { return m_d->bindBoolean(b); }

//==============================================================================
bool JsonReader::bindTimeStamp(const ItmState<Reader>&, QDateTime& b) { return m_d->bindTimeStamp(b); }

//==============================================================================
bool JsonReader::bindBytes(const ItmState<Reader>&, QByteArray& p_bytes)
{
  // FIXME not implemented yet
  Q_UNUSED(p_bytes);
  return false;
}

//==============================================================================
bool JsonReader::null(const ItmState<Reader>&) { return m_d->null(); }

//==============================================================================
bool JsonReader::sequenceOut(const SeqState<Reader>&)
{
    bool out = m_d->sequenceOut();
    return out;
}

//==============================================================================
bool JsonReader::recordOut  (const RecState<Reader>&)
{
    bool out = m_d->recordOut();
    return out;
}

//==============================================================================
ISequence<Reader>* JsonReaderPrivate::sequence()
{
    if (!get('[', "ntf-.0123456789[{\""))
    { return nullptr; }
    return q_ptr;
}

//==============================================================================
IRecord<Reader>* JsonReaderPrivate::record()
{
    if (!get('{', "ntf-.0123456789[{\""))
    { return nullptr; }
    return q_ptr;
}

//==============================================================================
bool JsonReaderPrivate::bindNumber(qlonglong& p_longlong, double& p_double)
{
    qlonglong ll=0; double d=0;

    // roughly:
    // m_stream >> ll;
    bool isNegative = false;
    if (m_nextChar == '-')
    {
        getChar();
        isNegative = true;
    }
    int digit = getDigit();
    if (digit < 0 || 9 < digit)
    {
        return false; // do not accept no digit otherwise we may accept an empty mantissa or string!
    }
    do
    {
        // TODO detect overflow
        ll=ll*10+digit; d=d*10+digit;
        digit = getDigit();
    }
    while (0 <= digit && digit <= 9); // accept many leading '0' which is more permissive than JSON

    if (m_nextChar == '.')
    {
        // roughly
        // d = ll;
        // m_stream >> decimal // except it would eat exponent
        // d += decimal

        getChar();
        double decimal=.1;
        for (int digit = getDigit(); 0 <= digit && digit <= 9; digit = getDigit(), decimal/=10)
        {
            d+=decimal*digit;
        }
    }

    if (m_nextChar == 'e' || m_nextChar == 'E')
    {
        getChar();
        qlonglong exponent = 0;
        bool isNegativeExponent = false;
        if (m_nextChar == '-')
        {
            getChar();
            isNegativeExponent = true;
        }
        if (m_nextChar == '+')
        {
            getChar();
        }
        for (int digit = getDigit(); 0 <= digit && digit <= 9; digit = getDigit())
        {
            exponent=exponent*10+digit; // TODO detect overflow
        }
        // TODO detect overflow
        ll*=10^(isNegativeExponent ? -exponent : exponent);
        d *=10^(isNegativeExponent ? -exponent : exponent);
    }
    // TODO if (!next(",]}") {}

    p_longlong = isNegative ? -ll : ll;
    p_double   = isNegative ? -d  :  d;
    return true;
}

//==============================================================================
int JsonReaderPrivate::getDigit()
{
    return '0' <= m_nextChar && m_nextChar <= '9' ? (getChar().toLatin1())-'0' : -1;
}

//==============================================================================
bool JsonReaderPrivate::bindBoolean(bool& p_boolean)
{
    if (get('t', "ntf-.0123456789[{\""))
    {
        if (m_nextChar == 'r') { getChar(); } else { next("[{\""); return false; }
        if (m_nextChar == 'u') { getChar(); } else { next("[{\""); return false; }
        if (m_nextChar == 'e') { getChar(); } else { next("[{\""); return false; }
        p_boolean = true;
        return true;
    }
    else if (get('f', "ntf-.0123456789[{\""))
    {
        if (m_nextChar == 'a') { getChar(); } else { next("[{\""); return false; }
        if (m_nextChar == 'l') { getChar(); } else { next("[{\""); return false; }
        if (m_nextChar == 's') { getChar(); } else { next("[{\""); return false; }
        if (m_nextChar == 'e') { getChar(); } else { next("[{\""); return false; }
        p_boolean = false;
        return true;
    }
    return false;
}

//==============================================================================
bool JsonReaderPrivate::bindTimeStamp(QDateTime& p_datetime)
{
  Q_UNUSED(p_datetime);
    return false; // <= TODO
}

//==============================================================================
bool JsonReaderPrivate::null()
{
    if (!get('n', "[{\"ntf-.0123456789")) { return false; }
    if (m_nextChar == 'u') { getChar(); } else { next("[{\""); return false; }
    if (m_nextChar == 'l') { getChar(); } else { next("[{\""); return false; }
    if (m_nextChar == 'l') { getChar(); } else { next("[{\""); return false; }
    return true;
}

//==============================================================================
bool JsonReaderPrivate::sequenceOut()
{
    while (get(',', "]"))
    {
        skipItem();
    }
    return get(']');
}

//==============================================================================
bool JsonReaderPrivate::recordOut()
{
    while (get(',', "}"))
    {
        skipKey() && skipItem();
    }
    return get('}');
}

//==============================================================================
IItem<Reader>* JsonReader::sequenceItem(const SeqState<Reader>& path)
{
    if (!path.isFirst() && !m_d->get(',',"]"))
    { return nullptr; }
    return this;
}

//==============================================================================
IItem<Reader>* JsonReader::recordItem(const RecState<Reader>& path, QString& key)
{
    if (!((path.isFirst() || (m_d->get(',',"}"))) &&
          m_d->readKey(key) && m_d->get(':')
          ))
    { return nullptr; }
    return this;
}

//==============================================================================
bool JsonReader::text(const ItmState<Reader>&, const QString& txt)
{
    if (!m_d->get('"')) { return false; }
    QString::const_iterator text=txt.cbegin();
    QChar read = m_d->getCharInString();
    while (read != QChar('\0') && read != QChar(' ') && text != txt.cend() && *text == read)
    {
        read = m_d->getCharInString();
        ++text;
    }
    // reach both ends
    return (read == QChar('\0') || read == QChar(' ')) && text == txt.cend() && m_d->get('"');
}

//==============================================================================
bool JsonReaderPrivate::readKey(QString& var)
{
    if (!get('"')) { return false; }
    var.clear();
    for (QChar read = getCharInString(); !(read == QChar('\0') || read.isSpace()); read = getCharInString())
    {
        var.push_back(read);
    }
    return get('"');
}

//==============================================================================
bool JsonReader::bindText(const ItmState<Reader>&, QString& txt)
{
    if (!m_d->get('"')) { return false; }
    txt.clear();
    for (QChar read = m_d->getCharInString();
         !(read == QChar('\0'));
         read = m_d->getCharInString())
    {
        txt.push_back(read);
    }
    return m_d->get('"');
}

//-----------------------------------------------------------------------------
// private:

//==============================================================================
QChar JsonReaderPrivate::next(QString ends)
{
    while (m_nextChar != QChar(0) && (m_nextChar < QChar(32) || m_nextChar.isSpace() || !ends.contains(m_nextChar)))
    {
        getChar();
    }
    return m_nextChar;
}

//==============================================================================
bool JsonReaderPrivate::get(QChar expected)
{
    return getChar() == expected;
}

//==============================================================================
bool JsonReaderPrivate::get(QChar expected, QString others)
{
    auto valid = others;
    valid.push_back(expected);
    if (next(valid) != expected)
    {
        return false;
    }
    return get(expected);
}

//==============================================================================
QChar JsonReaderPrivate::getChar()
{
    QChar read = m_nextChar;
    if (m_stream->atEnd())
    {
        m_nextChar = '\0';
    }
    else
    {
        *m_stream >> m_nextChar;
    }
    return read;
}

//==============================================================================
bool JsonReaderPrivate::skipItem()
{
    bool b;
    qlonglong ll;
    double d;
    QDateTime dt;
    return
        (   sequence() && sequenceOut())
        || (  record() &&   recordOut())
        || null()
        || bindBoolean(b)
        || bindNumber(ll, d)
        || bindTimeStamp(dt)
        // CR || bindText ?
        ;
}

//==============================================================================
bool JsonReaderPrivate::skipKey()
{
    QString skippedKey;
    return readKey(skippedKey) && get(':');
}

//==============================================================================
QChar JsonReaderPrivate::getCharInString()
{
    if (m_nextChar == QChar('\0') || m_nextChar == QChar('"'))
    {
        return '\0';
    }
    if (m_nextChar == '\\')
    {
        getChar();
        bool isHex; unsigned hex;
        switch (getChar().toLatin1())
        {
        case'\\': return '\\';
        case '"': return  '"';
        case 'b': return '\b';
        case 'f': return '\f';
        case 'n': return '\n';
        case 'r': return '\r';
        case 't': return '\t';
        case '/': return  '/';
        case 'u':
        {
            QString read;
            read.push_back(getChar());
            read.push_back(getChar());
            read.push_back(getChar());
            read.push_back(getChar());
            hex = read.toUInt(&isHex, 16);
            if (isHex) {
                return hex;
            }
            return  '?';
        }
        default : return  '?';
        }
    }
    else
    {
        return getChar();
    }
}

} // data
} // modmed

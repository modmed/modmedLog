/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <QtCore/qbytearray.h>
#include <QtCore/qiodevice.h>
#include <QtCore/qstring.h>
#include <QtCore/qtextcodec.h>
#include <QtCore/qtextstream.h>

#include <modmed/data/JsonWriter.h>

namespace modmed {
namespace data {

struct JsonWriterPrivate
{
    bool           m_isDocument;
    QTextStream*   m_stream;
    unsigned short m_maxIndentLevel;
    char           m_maxIndentLevelSeparator;
    bool           m_ioIsGood = true;
    QTextStream    internalStream;

    JsonWriterPrivate(bool p_isDocument, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator, QTextStream* p_s) :
        m_isDocument(p_isDocument),
        m_maxIndentLevel(p_maxIndentLevel),
        m_maxIndentLevelSeparator(p_maxIndentLevelSeparator)
    {
        m_stream = p_s;
    }

    JsonWriterPrivate(bool p_isDocument, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator, QIODevice* p_io) :
        m_isDocument(p_isDocument),
        m_maxIndentLevel(p_maxIndentLevel),
        m_maxIndentLevelSeparator(p_maxIndentLevelSeparator),
        internalStream(p_io)
    {
        m_stream = &internalStream;
        m_stream->setCodec("UTF-8");
    }

    JsonWriterPrivate(bool p_isDocument, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator, FILE* p_f) :
        m_isDocument(p_isDocument),
        m_maxIndentLevel(p_maxIndentLevel),
        m_maxIndentLevelSeparator(p_maxIndentLevelSeparator),
        internalStream(p_f)

    {
        m_stream = &internalStream;
        m_stream->setCodec("UTF-8");
    }
    bool message();
    bool messageOut();

    JsonWriterPrivate(bool p_isDocument, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator, QString* p_s) :
        m_isDocument(p_isDocument),
        m_maxIndentLevel(p_maxIndentLevel),
        m_maxIndentLevelSeparator(p_maxIndentLevelSeparator),
        internalStream(p_s)

    {
        m_stream = &internalStream;
        m_stream->setCodec("UTF-8");
    }

    void escape(const QString& s);
    void indent(const BindState<Writer>& path, bool isItem = false );
    void end   (const BindState<Writer>& path);
};

//! \see http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf

void JsonWriterPrivate::escape(const QString& s)
{
    Q_ASSERT(m_stream);
    JsonWriter::escape(*m_stream, s);
}

void JsonWriterPrivate::indent(const BindState<Writer>& path, bool isItem)
{
    if (0 < m_maxIndentLevel)
    {
        auto level = path.level();
        if (level < m_maxIndentLevel) {
            *m_stream << endl;
            for (unsigned i = 0; i < level ; ++i)
                *m_stream << ' ';
        }
    }
    if (isItem && m_maxIndentLevelSeparator != '\0' && path.level() == m_maxIndentLevel)
    {
        *m_stream << m_maxIndentLevelSeparator;
    }
}

void JsonWriterPrivate::end(const BindState<Writer>& path)
{
    if (!path.parent() && m_isDocument)
    {
         *m_stream << endl;
         *m_stream << '}';
    }
}

//==============================================================================
JsonWriter::JsonWriter(QTextStream* s, const QString& documentInformation /*= QString()*/, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator):
    m_d(new JsonWriterPrivate(false, p_maxIndentLevel, p_maxIndentLevelSeparator, s))
{
    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
JsonWriter::JsonWriter(QIODevice* io, const QString& documentInformation /*= QString()*/, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator):
    m_d(new JsonWriterPrivate(false, p_maxIndentLevel, p_maxIndentLevelSeparator, io))
{
    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
JsonWriter::JsonWriter(FILE* f, const QString& documentInformation /*= QString()*/, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator):
    m_d(new JsonWriterPrivate(false, p_maxIndentLevel, p_maxIndentLevelSeparator, f))
{
    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
JsonWriter::JsonWriter(QString* s, const QString& documentInformation /*= QString()*/, unsigned short p_maxIndentLevel, char p_maxIndentLevelSeparator):
    m_d(new JsonWriterPrivate(false, p_maxIndentLevel, p_maxIndentLevelSeparator, s))
{
    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
JsonWriter::~JsonWriter() {}

//==============================================================================
JsonWriter& JsonWriter::document(const QString& information /*= QString("\"\"")*/)
{
    Q_ASSERT(!information.isNull());
    Q_ASSERT(!m_d->m_isDocument);
    Q_ASSERT(m_d->m_stream->pos() == 0);

    *m_d->m_stream << "{";
    if (!information.isEmpty())
    {
        *m_d->m_stream << "\"document\":" << information << ",";
    }
    *m_d->m_stream << "\"data\":" << endl;
    m_d->m_isDocument = true;
    return *this;
}

//==============================================================================
bool JsonWriter::isDocument() { return m_d->m_isDocument; }

//==============================================================================
void JsonWriter::flush() { m_d->m_stream->flush(); }

//==============================================================================
bool JsonWriter::isValid() { return m_d->m_stream->status() == QTextStream::Ok; }

//==============================================================================
ISequence<Writer>* JsonWriter::sequence(const ItmState<Writer>&)
{
    *m_d->m_stream << "[";
    return isValid() ? this : nullptr;
}

//==============================================================================
IRecord<Writer>* JsonWriter::record(const ItmState<Writer>&)
{
    *m_d->m_stream << "{";
    return isValid() ? this : nullptr;
}

//==============================================================================
bool JsonWriterPrivate::message()
{
    *m_stream << '"';
    return m_stream->status() == QTextStream::Ok;;
}

//==============================================================================
bool JsonWriter::null(const ItmState<Writer>&)
{
    *m_d->m_stream << "null" ;
    return isValid();
}

//==============================================================================
bool JsonWriter::sequenceOut(const SeqState<Writer>& path)
{
    m_d->indent(path);
    *m_d->m_stream << ']';
    m_d->end(path);
    return isValid();
}

//==============================================================================
bool JsonWriter::recordOut  (const RecState<Writer>& path)
{
    m_d->indent(path);
    *m_d->m_stream << '}';
    m_d->end(path);
    return isValid();
}

//==============================================================================
bool JsonWriterPrivate::messageOut()
{
    *m_stream << '"';
    return m_stream->status() == QTextStream::Ok;;
}

//==============================================================================
bool JsonWriter::bindNumber(const ItmState<Writer>&, qlonglong& p_number)
{
    *m_d->m_stream << QByteArray::number(p_number);
    return isValid();
}

//==============================================================================
bool JsonWriter::bindNumber(const ItmState<Writer>&, double& p_number)
{
    *m_d->m_stream << QByteArray::number(p_number);
    return isValid();
}

//==============================================================================
bool JsonWriter::bindBoolean(const ItmState<Writer>&, bool& p_bool)
{
    *m_d->m_stream << QByteArray(p_bool ? "true" : "false");
    return isValid();
}

//==============================================================================
bool JsonWriter::bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime)
{
    if (!m_d->message()) { return false; }
    QString timestamp = p_datetime.toString(Qt::DateFormat::ISODate);
    if (timestamp.endsWith("Z", Qt::CaseSensitive))
    {
        timestamp.remove(timestamp.length()-1, 1);
        timestamp.append("+00:00");
    }

    *m_d->m_stream << timestamp;
    return m_d->messageOut();
}

//==============================================================================
bool JsonWriter::bindBytes(const ItmState<Writer>&, QByteArray& p_bytes)
{
    QByteArray res("0x");
    for (auto byte : p_bytes)
    {
        res += QByteArray::number(byte, 16);
    }
    *m_d->m_stream << res;
    return isValid();
}

//==============================================================================
IItem<Writer>* JsonWriter::sequenceItem(const SeqState<Writer>& path)
{
    if (!path.isFirst())
    {
        m_d->indent(path, true);
        *m_d->m_stream << ',';
    }
    return isValid() ? this : nullptr;
}

//==============================================================================
IItem<Writer>* JsonWriter::recordItem(const RecState<Writer>& path, QString& key)
{
    Q_ASSERT(!key.isEmpty());
    if (!path.isFirst())
    {
        m_d->indent(path, true);
        *m_d->m_stream << ',';
    }
    *m_d->m_stream << '"';
    m_d->escape     (key);
    *m_d->m_stream << '"';
    *m_d->m_stream << ':';
    return isValid() ? this : nullptr;
}

//==============================================================================
bool JsonWriter::text(const ItmState<Writer>&, const QString& txt)
{
    if (!m_d->message()) { return false; }
    m_d->escape(txt);
    return m_d->messageOut();
}

//==============================================================================
bool JsonWriter::bindText(const ItmState<Writer>&, QString& txt)
{
    if (!m_d->message()) { return false; }
    m_d->escape(txt);
    return m_d->messageOut();
}

void JsonWriter::escape(QTextStream &jsonStream, const QString &t)
{
    for (int i=0; i<t.size(); ++i)
    {
        JsonWriter::escape(jsonStream, t[i].unicode());
    }
}

// \see qdebug.cpp putUcs4
// \see http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf
void JsonWriter::escape(QTextStream &jsonStream, unsigned ucs4)
{
    /**/ if (ucs4 == '\\') { jsonStream << '\\' << '\\'; }
    else if (ucs4 == '\t') { jsonStream << '\\' <<  't'; }
    else if (ucs4 ==  '"') { jsonStream << '\\' <<  '"'; }
    else if (ucs4 == '\b') { jsonStream << '\\' <<  'b'; }
    else if (ucs4 == '\f') { jsonStream << '\\' <<  'f'; }
    else if (ucs4 == '\n') { jsonStream << '\\' <<  'n'; }
    else if (ucs4 == '\r') { jsonStream << '\\' <<  'r'; }
    else if (ucs4 <   ' ' || 
			 (127 <  ucs4 && // avoid huge performance penalty of canEncode for 80% of ucs4
			 !jsonStream.codec()->canEncode(ucs4)))
    {
        jsonStream << '\\' << 'u';
        jsonStream << hex;
        jsonStream.setPadChar('0');
        jsonStream.setFieldWidth(4);
        if (QChar::requiresSurrogates(ucs4))
        {
            jsonStream << QChar::highSurrogate(ucs4);
            jsonStream.setFieldWidth(0); // \see QTextStream::setFieldWidth()
            jsonStream << '\\' << 'u';
            jsonStream.setFieldWidth(4);
            jsonStream << QChar:: lowSurrogate(ucs4);
            jsonStream.setFieldWidth(0);
        }
        else
        {
            jsonStream <<                      ucs4 ;
        }
        jsonStream.setFieldWidth(0); // \see QTextStream::setFieldWidth()
    }
    else
    {
        jsonStream <<                    QChar(ucs4);
    }
}

void JsonWriter::escapeUtf8(QIODevice &jsonUtf8, QByteArray utf8)
{
    for (int i=0; i<utf8.size(); ++i)
    {
        /**/ if (utf8[i] == '\\') { jsonUtf8.putChar('\\'); jsonUtf8.putChar('\\'); }
        else if (utf8[i] ==  '"') { jsonUtf8.putChar('\\'); jsonUtf8.putChar( '"'); }
        else if (utf8[i] < ' ')
        {
            jsonUtf8.putChar('\\');
            switch (utf8[i]) {
            case '\t': jsonUtf8.putChar( 't'); break;
            case '\b': jsonUtf8.putChar( 'b'); break;
            case '\f': jsonUtf8.putChar( 'f'); break;
            case '\n': jsonUtf8.putChar( 'n'); break;
            case '\r': jsonUtf8.putChar( 'r'); break;
            default  : jsonUtf8.putChar( 'u'); jsonUtf8.write(QByteArray::number(utf8[i]).rightJustified(4,'0')); break;
            }
        }
        else // including UTF-8 sequence bytes
        {
            jsonUtf8.putChar(utf8[i]);
        }
    }
}

} // data
} // modmed

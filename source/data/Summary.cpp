/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/

#include <modmed/data/Summary.h>

namespace modmed {
namespace data {

struct SummaryPrivate
{
    QTextStream* m_stream;
    unsigned m_currentLevel;
    unsigned m_currentItem;
    QString m_lastItem;
    unsigned m_itemsToWrite;   // if > 2 => n-1 first items + last
    unsigned m_completeLevels; // number of non truncated levels
    QTextStream    internalStream;

    SummaryPrivate(QTextStream* s)
        : m_stream(s)
        , m_currentLevel(0)
        , m_currentItem(0)
        , m_itemsToWrite(3)
        , m_completeLevels(1)
    {}

    SummaryPrivate(QIODevice* p_io)
        : m_currentLevel(0)
        , m_currentItem(0)
        , m_itemsToWrite(3)
        , m_completeLevels(1)
        , internalStream(p_io)
    {
        m_stream = &internalStream;
    }

    SummaryPrivate(FILE* f)
        : m_currentLevel(0)
        , m_currentItem(0)
        , m_itemsToWrite(3)
        , m_completeLevels(1)
        , internalStream(f)
    {
        m_stream = &internalStream;
    }

    SummaryPrivate(QString* p_s)
        : m_currentLevel(0)
        , m_currentItem(0)
        , m_itemsToWrite(3)
        , m_completeLevels(1)
        , internalStream(p_s)
    {
        m_stream = &internalStream;
    }

    bool append(QString t);

    //! Write the number of items skipped and the last item if necessary
    void closeCollection();

    //! return true if one of the following is true:
    //! * currentLevel &lt; completeLevels
    //! * currentLevel == completeLevels and currentItem &lt; itemsToWrite
    //! * currentLevel == completeLevels and currentItem &lt; itemsToWrite and itemsToWrite &lt; 2
    bool itemToWrite();
};

//==============================================================================
Summary::Summary(QTextStream* s) :
    m_d(new SummaryPrivate(s))
{}

//==============================================================================
Summary::Summary(QIODevice* p_io) :
    m_d(new SummaryPrivate(p_io))
{}

//==============================================================================
Summary::Summary(FILE* f) :
    m_d(new SummaryPrivate(f))
{}

//==============================================================================
Summary::Summary(QString* p_s) :
    m_d(new SummaryPrivate(p_s))
{}

//==============================================================================
Summary::~Summary() {}

//==============================================================================
void Summary::setLevelOfDetails(unsigned p_numberOfCompleteLevels, unsigned p_numberOfItemsInLastLevel)
{
    m_d->m_completeLevels = p_numberOfCompleteLevels;
    m_d->m_itemsToWrite = p_numberOfItemsInLastLevel;
}

//==============================================================================
void Summary::flush()
{
    m_d->m_stream->flush();
}

//==============================================================================
bool Summary::isValid() { return m_d->m_stream->status() == QTextStream::Ok; }

//==============================================================================
inline bool SummaryPrivate::itemToWrite()
{
    return  m_currentLevel <  m_completeLevels ||
           (m_currentLevel == m_completeLevels && m_currentItem < m_itemsToWrite) ||
           (m_itemsToWrite < 2 && m_currentItem <= m_itemsToWrite);
}

//==============================================================================
ISequence<Writer>* Summary::sequence(const ItmState<Writer>& path)
{
    Q_UNUSED(path);
    if (m_d->m_currentLevel <= m_d->m_completeLevels)
    {
        m_d->append("[");
    }
    m_d->m_currentLevel++;
    if (m_d->m_currentLevel == m_d->m_completeLevels)
    {
        m_d->m_currentItem = 0;
    }
    return this;
}

//==============================================================================
IRecord<Writer>* Summary::record(const ItmState<Writer>& path)
{
    Q_UNUSED(path);
    if (m_d->m_currentLevel <= m_d->m_completeLevels)
    {
        m_d->append("{");
    }
    m_d->m_currentLevel++;
    if (m_d->m_currentLevel == m_d->m_completeLevels)
    {
        m_d->m_currentItem = 0;
    }
    return this;
}

//==============================================================================
bool Summary::bindNumber(const ItmState<Writer>&, qlonglong& p_number)
{
    return m_d->append(QString::number(p_number));
}

//==============================================================================
bool Summary::bindNumber(const ItmState<Writer>&, double& p_number)
{
    return m_d->append(QString::number(p_number));
}

//==============================================================================
bool Summary::bindBoolean(const ItmState<Writer>&, bool& p_bool)
{
    return m_d->append(p_bool ? "true" : "false");
}

//==============================================================================
bool Summary::bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime)
{
    return m_d->append(p_datetime.toString());
}

//==============================================================================
bool Summary::bindBytes(const ItmState<Writer>&, QByteArray& p_bytes)
{
  // FIXME not implemented yet
  Q_UNUSED(p_bytes)
  return false;
}

//==============================================================================
bool Summary::null(const ItmState<Writer>&)
{
    return m_d->append("null");
}

//==============================================================================
inline void SummaryPrivate::closeCollection()
{
    if (m_currentLevel == m_completeLevels)
    {
        if (m_currentItem > m_itemsToWrite)
        {
            if (m_itemsToWrite < 2)
            {
                if (m_itemsToWrite == 1)
                {
                    *m_stream << " | ";
                }
                *m_stream << QString::number(m_currentItem - m_itemsToWrite) << " more";
            }
            else
            {
                if (m_currentItem != m_itemsToWrite)
                {
                    *m_stream << " | " << QString::number(m_currentItem - m_itemsToWrite) << " more";
                }
                *m_stream << " | " << m_lastItem;
            }
        }
        else if (m_currentItem == m_itemsToWrite && m_itemsToWrite >= 2)
        {
            *m_stream << " | " << m_lastItem;
        }
    }
}

//==============================================================================
bool Summary::sequenceOut(const SeqState<Writer>& path)
{
    Q_UNUSED(path);
    m_d->closeCollection();
    m_d->m_currentLevel--;
    if (m_d->m_currentLevel <= m_d->m_completeLevels)
    {
        m_d->append("]");
    }
    return true;
}

//==============================================================================
bool Summary::recordOut(const RecState<Writer>& path)
{
    Q_UNUSED(path);
    m_d->closeCollection();
    m_d->m_currentLevel--;
    if (m_d->m_currentLevel <= m_d->m_completeLevels)
    {
        m_d->append("}");
    }
    return true;
}

//==============================================================================
IItem<Writer>* Summary::sequenceItem(const SeqState<Writer>& path )
{
    if (m_d->m_currentLevel == m_d->m_completeLevels)
    {
        m_d->m_currentItem++;
    }
    if (m_d->itemToWrite() && path.currentIndex() > 0 )
    {
        *m_d->m_stream << " | ";
    }
    m_d->m_lastItem = "";
    return this;
}

//==============================================================================
IItem<Writer>* Summary::recordItem(const RecState<Writer>& path, QString& key)
{
    Q_ASSERT(!key.isEmpty());
    if (m_d->m_currentLevel == m_d->m_completeLevels)
    {
        m_d->m_currentItem++;
    }
    if (m_d->itemToWrite())
    {
        if (path.currentIndex() > 0)
        {
            *m_d->m_stream << " | ";
        }
        *m_d->m_stream << key << ':';
    }
    m_d->m_lastItem = key + QChar(':');
    return this;
}

//==============================================================================
bool Summary::text(const ItmState<Writer>& tp, const QString& t)
{
    bool res = true;
    if (tp.level() <= m_d->m_completeLevels)
    {
        res &= m_d->append(t);
    }
    return res;
}


//==============================================================================
bool Summary::bindText(const ItmState<Writer>& tp, QString& t)
{
    bool res = true;
    if (tp.level() <= m_d->m_completeLevels)
    {
        res &= m_d->append(t);
    }
    return res;
}

//==============================================================================
bool SummaryPrivate::append(QString t)
{
    if (itemToWrite())
    {
        *m_stream << t;
        if (m_stream->status() != QTextStream::Ok)
        {
            return false;
        }
    }
    else
    {
        m_lastItem.append(t);
    }
    return true;
}

} // data
} // modmed

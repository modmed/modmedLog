/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <QtCore/qtextstream.h>
#include <QtCore/qtextcodec.h>

#include <modmed/data/XmlWriter.h>

namespace modmed {
namespace data {

struct XmlWriterPrivate
{
    bool           m_isDocument;
    QTextStream*   m_stream;
    QChar          m_nextChar;
    bool m_ioIsGood = true;
    QTextStream    internalStream;

    XmlWriterPrivate(bool p_isDocument, QTextStream* p_s) :
        m_isDocument(p_isDocument)
    {
        m_stream = p_s;
        init();
    }

    XmlWriterPrivate(bool p_isDocument, QIODevice* p_io) :
        m_isDocument(p_isDocument),
        internalStream(p_io)
    {
        m_stream = &internalStream;
        m_stream->setCodec("UTF-8");
    }

    XmlWriterPrivate(bool p_isDocument, FILE* p_f) :
        m_isDocument(p_isDocument),
        internalStream(p_f, QIODevice::WriteOnly)
    {
        m_stream = &internalStream;
        m_stream->setCodec("UTF-8");
    }

    XmlWriterPrivate(bool p_isDocument, QString* p_s) :
        m_isDocument(p_isDocument),
        internalStream(p_s)
    {
        m_stream = &internalStream;
        m_stream->setCodec("UTF-8");
    }


    QString    encode(const BindState<Writer>& p, QString t);

    bool message(const ItmState<Writer>& path, const char *p_type = nullptr);
    bool messageOut(const ItmState<Writer>& path);

private:
    void init()
    {
        // NB: QIconvCodec which may be the codecForLocale() on some Qt builds does not report a valid IANA MIB or name, so...
        // Use IANA registered codecs only
        // \see http://www.iana.org/assignments/character-sets/character-sets.xml
        if (m_stream->codec()->mibEnum() < 3)
        {
          m_stream->setCodec("UTF-8"); // this is highly interoperable and should select the high performance built-in QUtf8Codec
        }
    }
};

//==============================================================================
XmlWriter::XmlWriter(QTextStream* p_stream,  const QString& documentInformation /*= QString()*/) :
    m_d(new XmlWriterPrivate(false, p_stream))
{
    if (!documentInformation.isNull()) { document(documentInformation); }
}

//==============================================================================
XmlWriter::XmlWriter(QIODevice* p_io, const QString& p_documentInformation /*= QString()*/) :
    m_d(new XmlWriterPrivate(false, p_io))
{
    if (!p_documentInformation.isNull()) { document(p_documentInformation); }
}

//==============================================================================
XmlWriter::XmlWriter(FILE* p_f, const QString& p_documentInformation /*= QString()*/) :
    m_d(new XmlWriterPrivate(false, p_f))
{
    if (!p_documentInformation.isNull()) { document(p_documentInformation); }
}

//==============================================================================
XmlWriter::XmlWriter(QString* p_s, const QString& p_documentInformation /*= QString()*/) :
    m_d(new XmlWriterPrivate(false, p_s))
{
    if (!p_documentInformation.isNull()) { document(p_documentInformation); }
}

//==============================================================================
XmlWriter::~XmlWriter() {}

//==============================================================================
XmlWriter& XmlWriter::document(const QString& information /*= QString("")*/)
{
    Q_ASSERT(!information.isNull());
    Q_ASSERT(!m_d->m_isDocument);
    Q_ASSERT(m_d->m_stream->pos() == 0);

    *m_d->m_stream << "<?xml version=\"1.0\" encoding=\"" << codecName() << "\"?>\n";
    if (!information.isEmpty())
    {
        *m_d->m_stream << information << "\n";
    }
    *m_d->m_stream << "<data>\n";
    m_d->m_isDocument = true;
    return *this;
}

//==============================================================================
bool XmlWriter::isDocument() { return m_d->m_isDocument; }

//==============================================================================
void XmlWriter::flush() { m_d->m_stream->flush(); }

//==============================================================================
bool XmlWriter::isValid() { return m_d->m_stream->status() == QTextStream::Ok; }

//==============================================================================
QString XmlWriter::codecName() { return m_d->m_stream->codec()->name(); }

//==============================================================================
ISequence<Writer>* XmlWriter::sequence(const ItmState<Writer>& path)
{
    *m_d->m_stream << "<s";
    auto p = path.parent().dynamicCast<const RecState<Writer>>();
    if (p)
    {
        *m_d->m_stream << " key=\"" << m_d->encode(path, p->currentKey()) << "\"";
    }
	*m_d->m_stream << '>';
    return isValid() ? this : nullptr;
}
//==============================================================================
IRecord<Writer>*  XmlWriter::record (const ItmState<Writer>& path)
{
    *m_d->m_stream << "<r";
    auto p = path.parent().dynamicCast<const RecState<Writer>>();
    if (p)
    {
        *m_d->m_stream << " key=\"" << m_d->encode(path, p->currentKey()) << "\"";
    }
    *m_d->m_stream << '>';
    return isValid() ? this : nullptr;
}

//==============================================================================
bool XmlWriterPrivate::message  (const ItmState<Writer>& path, const char* p_type)
{
    *m_stream << "<t";
    if (p_type)
        *m_stream << " type=\"" << p_type << "\"";
    auto p = path.parent().dynamicCast<const RecState<Writer>>();
    if (p)
    {
        *m_stream << " key=\"" << encode(path, p->currentKey()) << "\"";
    }
    *m_stream << '>';
    return m_stream->status() == QTextStream::Ok;
}

//==============================================================================
bool XmlWriter::bindNumber(const ItmState<Writer>& tp, qlonglong& p_number)
{
    m_d->message(tp, "integer");
    *m_d->m_stream << QByteArray::number(p_number);
    return m_d->messageOut(tp);
}

//==============================================================================
bool XmlWriter::bindNumber(const ItmState<Writer>& tp, double& p_number)
{
    m_d->message(tp);
    *m_d->m_stream << QByteArray::number(p_number);
    return m_d->messageOut(tp);
}

//==============================================================================
bool XmlWriter::bindBoolean(const ItmState<Writer>& tp, bool& p_boolean)
{
    m_d->message(tp, "boolean");
    *m_d->m_stream << (p_boolean ? "true" : "false");
    return m_d->messageOut(tp);
}

//==============================================================================
bool XmlWriter::bindTimeStamp(const ItmState<Writer>& tp, QDateTime& p_datetime)
{
    m_d->message(tp);

    QString timestamp = p_datetime.toString(Qt::DateFormat::ISODate);
    if (timestamp.endsWith("Z", Qt::CaseSensitive))
    {
        timestamp.remove(timestamp.length()-1, 1);
        timestamp.append("+00:00");
    }
    *m_d->m_stream << timestamp;

    return m_d->messageOut(tp);
}

//==============================================================================
bool XmlWriter::bindBytes(const ItmState<Writer>& tp, QByteArray& p_bytes)
{
    m_d->message(tp, "hexBinary");

    QByteArray res("0x");
    for (auto byte : p_bytes)
    {
        res += QByteArray::number(byte, 16);
    }
    *m_d->m_stream << res;

    return m_d->messageOut(tp);
}

//==============================================================================
bool    XmlWriter::null  (const ItmState<Writer>& path)
{
    *m_d->m_stream << "<n";
    auto p = path.parent().dynamicCast<const RecState<Writer>>();
    if (p)
    {
        *m_d->m_stream << " key=\"" << m_d->encode(path, p->currentKey()) << "\"";
    }
    *m_d->m_stream << " />";
    return isValid();
}

//==============================================================================
bool XmlWriter::sequenceOut(const SeqState<Writer>& path)
{
    if (path.level() == 0)
    {
        *m_d->m_stream << "\n";
    }
    *m_d->m_stream << "</s>";
    if (!path.parent() && m_d->m_isDocument)
    {
        *m_d->m_stream << "\n</data>";
    }
    return isValid();
}

//==============================================================================
bool XmlWriter::recordOut (const RecState<Writer>& path)
{
    if (path.level() == 0)
    {
        *m_d->m_stream << "\n";
    }
    *m_d->m_stream << "</r>";
    if (!path.parent() && m_d->m_isDocument)
    {
        *m_d->m_stream << "\n</data>";
    }
    return isValid();
}

//==============================================================================
bool    XmlWriterPrivate::messageOut  (const ItmState<Writer>& path)
{
    *m_stream << "</t>";
    if (!path.parent() && m_isDocument)
    {
        *m_stream << "\n</data>";
    }
    return m_stream->status() == QTextStream::Ok;
}

//==============================================================================
IItem<Writer>* XmlWriter::sequenceItem(const SeqState<Writer>& path)
{
    if (!path.isFirst())
    {
      if (path.level() == 0)
      {
        *m_d->m_stream << "\n   ";
      }
    }
    return isValid() ? this : nullptr;
}

//==============================================================================
IItem<Writer>* XmlWriter::recordItem(const RecState<Writer>& path, QString& key)
{
    Q_UNUSED(key);
    Q_ASSERT(!key.isEmpty());
    if (!path.isFirst())
    {
      if (path.level() == 0)
      {
        *m_d->m_stream << "\n   ";
      }
    }
    return isValid() ? this : nullptr;
}

//==============================================================================
bool XmlWriter::text(const ItmState<Writer>& tp, const QString& txt)
{
    if (!m_d->message(tp)) { return false; }
    *m_d->m_stream << m_d->encode(tp, txt);
    return m_d->messageOut(tp);
}

//==============================================================================
bool XmlWriter::bindText(const ItmState<Writer>& tp, QString& txt)
{
    if (!m_d->message(tp)) { return false; }
    *m_d->m_stream << m_d->encode(tp, txt);
    return m_d->messageOut(tp);
}

InvalidXmlChar::InvalidXmlChar(QChar c) : BindError(QString("Cannot write the character \"") + c + "\" in an XmlWriter Document", false),
    m_d(new QChar(c))
{}

QSharedPointer<BindError> InvalidXmlChar::clone() const
{
    return QSharedPointer<BindError>(new InvalidXmlChar(*m_d));
}

QChar InvalidXmlChar::getInvalidChar() const
{
    return *m_d;
}

//-----------------------------------------------------------------------------
// private:

//! \see http://stackoverflow.com/questions/5665231/most-efficient-way-to-escape-xml-html-in-c-string

//==============================================================================
QT_WARNING_DISABLE_GCC("-Wtype-limits")
QString XmlWriterPrivate::encode(const BindState<Writer>& path, QString t)
{
    QString encoded;
    for (int i=0; i<t.size(); ++i)
    {
        QChar tmp = t[i];
        char ascii(t[i].toLatin1());
        switch (ascii)
        {
        case '&': encoded.append("&amp;"); break;
        case '"': encoded.append("&quot;"); break;
        case'\'': encoded.append("&apos;"); break;
        case '<': encoded.append("&lt;")  ; break;
        case '>': encoded.append("&gt;")  ; break;
        default:
            if (' ' <= ascii && m_stream->codec()->canEncode(t[i]))
            {
                encoded.append(t[i]);
            }
            else if (ascii == 0x9 || ascii == 0xA || ascii == 0xD ||
                 (0x20    <= t[i] && t[i] <= 0xD7FF  ) ||
                 (0xE000  <= t[i] && t[i] <= 0xFFFD  )) // || See http://www.w3.org/TR/2004/REC-xml-20040204/#NT-Char
            //(0x10000 <= t[i] && t[i] <= 0x10FFFF))    // Not supported by QChar
            {
                encoded.append("&#x").append(QString::number(t[i].unicode(), 16)).append(';'); // See http://www.w3.org/TR/2004/REC-xml-20040204/#sec-references
            }
            else
            {
                path.addError(InvalidXmlChar(tmp));
                encoded.append(' ');
            }
        }
    }
    return encoded;
}


} // data
} // modmed

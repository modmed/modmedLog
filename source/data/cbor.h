/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

namespace cbor {

//! CBOR Major types
//! Encoded in the high 3 bits of the descriptor byte
//! \see http://tools.ietf.org/html/rfc7049#section-2.1
enum MajorTypes {
    UnsignedIntegerType = 0,
    NegativeIntegerType = 1,
    ByteStringType      = 2,
    TextStringType      = 3,
    ArrayType           = 4,
    MapType             = 5,
    TagType             = 6,
    SimpleTypesType     = 7
};

//! CBOR simple and floating point types
//! Encoded in the low 5 bits of the descriptor byte when the Major Type is 7.
enum SimpleTypes {
    FalseValue              = 20,
    TrueValue               = 21,
    NullValue               = 22,
    UndefinedValue          = 23,
    SimpleTypeInNextByte    = 24,
    HalfPrecisionFloat      = 25,
    SinglePrecisionFloat    = 26,
    DoublePrecisionFloat    = 27,
    Break                   = 31
};

enum {
    SmallValueBitLength         = 5U,
    SmallValueMask              = (1U << SmallValueBitLength) - 1, // 31
    Value8Bit                   = 24U,
    Value16Bit                  = 25U,
    Value32Bit                  = 26U,
    Value64Bit                  = 27U,
    IndefiniteLength            = 31U,

    MajorTypeShift              = SmallValueBitLength,
    MajorTypeMask               = (int) (~0U << MajorTypeShift),
};

enum : unsigned char {
    BreakByte                   = (unsigned)Break | (SimpleTypesType << MajorTypeShift),

    IndefiniteLengthMapByte     = IndefiniteLength | (MapType         << MajorTypeShift),
    IndefiniteLengthArrayByte   = IndefiniteLength | (ArrayType       << MajorTypeShift),
    NullByte                    = NullValue        | (SimpleTypesType << MajorTypeShift),
    TrueByte                    = TrueValue        | (SimpleTypesType << MajorTypeShift),
    FalseByte                   = FalseValue       | (SimpleTypesType << MajorTypeShift)
};

enum tag{
    Tag              = 192,
    DateTime         = 0,
    CborDocument     = 55799,
};

}

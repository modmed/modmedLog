#include <QtCore/qt_windows.h> // Defines macros used by TraceLoggingProvider.h
#include <TraceLoggingProvider.h>  // The native TraceLogging API
#include <winmeta.h> // for WINEVENT_LEVEL_* definitions

#include <QtCore/qdatetime.h>
#include <QtCore/qbuffer.h>
#include <modmed/data/CborWriter.h>
#include <modmed/data/Bind.h>
#include <modmed/log/EtlLogWriter.h>

// 2cece823-c96b-4ad7-b13c-0f6feab6322d
TRACELOGGING_DEFINE_PROVIDER(
    s_modmedLogComponentProvider,
    "SimpleTraceLoggingProvider",
    (0x2cece823, 0xc96b, 0x4ad7, 0xb1, 0x3c, 0x0f, 0x6f, 0xea, 0xb6, 0x32, 0x2d));

namespace modmed {
namespace log {

EtlLogWriter::EtlLogWriter()
{
    TraceLoggingRegister(s_modmedLogComponentProvider);
    TraceLoggingWrite(s_modmedLogComponentProvider, // handle to my provider
                      "_start",        // Event Name that should uniquely identify your event.
                      TraceLoggingValue(QDateTime::currentDateTime().toString(Qt::ISODateWithMs).toStdWString().c_str(), "_local_datetime")); // Field for your event in the form of (value, field name).
}

EtlLogWriter::~EtlLogWriter()
{
    TraceLoggingWrite(s_modmedLogComponentProvider, // handle to my provider
                      "_stop",        // Event Name that should uniquely identify your event.
                      TraceLoggingValue(QDateTime::currentDateTime().toString(Qt::ISODateWithMs).toStdWString().c_str(), "_local_datetime")); // Field for your event in the form of (value, field name).
    TraceLoggingUnregister(s_modmedLogComponentProvider);
}

#define _INTERNAL_ETL_LOG(level) \
        TraceLoggingWrite(s_modmedLogComponentProvider, \
                          "modmedLog", \
                          TraceLoggingLevel(level), \
                          TraceLoggingString(p_eventData.file(), "_source_path"), \
                          TraceLoggingValue(p_eventData.line(), "_source_line"), \
                          TraceLoggingString(p_eventData.function(), "_function"), \
                          TraceLoggingString(p_eventData.category(), "_category"), \
                          TraceLoggingValue(p_eventData.format().constData(), "_format"), \
                          TraceLoggingValue(p_eventData.id().constData(), "_id"), \
                          TraceLoggingValue(p_eventData.count(), "_count"), \
                          TraceLoggingHexInt8Array(buf.buffer().data(), (UINT16)buf.buffer().size(), "_args"), \
                          TraceLoggingHexInt8Array(bufT.buffer().data(), (UINT16)bufT.buffer().size(), "_args_types"), \
                          TraceLoggingHexInt8Array(bufN.buffer().data(), (UINT16)bufN.buffer().size(), "_args_names") \
                          );

bool EtlLogWriter::output(const LogEventData& p_eventData)
{

    QBuffer bufT;
    if (!bufT.open(QIODevice::WriteOnly))
        return false;
    data::CborWriter writerT(&bufT);
    QVector<QLatin1String> readableTypes;
    for (auto it = p_eventData.argTypes().cbegin(); it != p_eventData.argTypes().cend(); ++it)
    {
        readableTypes.append(QLatin1String(*it));
    }
    writerT.write().bind(readableTypes);

    QBuffer bufN;
    if (!bufN.open(QIODevice::WriteOnly))
        return false;
    data::CborWriter writerN(&bufN);
    QVector<QLatin1String> readableNames;
    for (auto it = p_eventData.argNames().cbegin(); it != p_eventData.argNames().cend(); ++it)
    {
        readableNames.append(QLatin1String(*it));
    }
    writerN.write().bind(readableNames);

    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
        return false;

    data::CborWriter writer(&buf);
    writer.write().bind(p_eventData.args());

    switch (p_eventData.type())
    {
    case QtFatalMsg:
        _INTERNAL_ETL_LOG(WINEVENT_LEVEL_LOG_ALWAYS);
        break;
    case QtCriticalMsg:
        _INTERNAL_ETL_LOG(WINEVENT_LEVEL_CRITICAL);
        break;
    case QtWarningMsg:
        _INTERNAL_ETL_LOG(WINEVENT_LEVEL_WARNING);
        break;
    case QtInfoMsg:
        _INTERNAL_ETL_LOG(WINEVENT_LEVEL_INFO);
        break;
    default:
    case QtDebugMsg:
        _INTERNAL_ETL_LOG(WINEVENT_LEVEL_VERBOSE);
        break;
    }

    return true;
}

void EtlLogWriter::flush() { }

} // log
} // modmed

#include <modmed/log/JournaldWriter.h>

#define SD_JOURNAL_SUPPRESS_LOCATION 1
#include <systemd/sd-journal.h> // int sd_journal_send(const char *format, ...);

#include <modmed/log/LogEvent.h>
#include <modmed/log/TsvJsonLogWriter.h>
#include <modmed/data/JsonWriter.h>

namespace modmed {
namespace log {

    M_NAMESPACE_LOG_CATEGORY(QtDebugMsg)

    JournaldWriter::JournaldWriter() = default;

    bool JournaldWriter::output(const LogEventData& p_eventData)
    {
        return 0 == sd_journal_send(
            "PRIORITY=%i"       , TsvJsonLogWriter::severity(p_eventData.type()),
            "DATETIME=%s"       , qUtf8Printable(p_eventData.dateTime().toString(Qt::ISODate)), // CR Taking p_eventData.dateTime() may be too slow for some uses
            "ELAPSED=%f"        ,        (double)p_eventData.nsecsElapsed()/1000000000,
            // Keep formatted "MESSAGE=%s"?
            "MESSAGE_FORMAT=%s" , qUtf8Printable(p_eventData.format()),
            "MESSAGE_ARGS=%s"   , qUtf8Printable(data::JsonString().write().bind(p_eventData.args()).toString()),
            "CODE_FILE=%s"      ,                p_eventData.file(),
            "CODE_LINE=%i"      ,                p_eventData.line(),
            "CODE_FUNC=%s"      ,                p_eventData.function(),
            "CATEGORY=%s"       ,                p_eventData.category(),
            "THREAD_ID=%s"      , qUtf8Printable(p_eventData.threadId()),
            "ID=%s"             , qUtf8Printable(p_eventData.id()),
            "COUNT=%s"          , qUtf8Printable(QByteArray::number(p_eventData.count())),
            NULL);
    }
    void JournaldWriter::flush() {}

} // log
} // modmed

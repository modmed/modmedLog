/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#define QT_BUILD_INTERNAL // for qCleanupFuncinfo() which cleans up template params (and gcc template param values)

#include <QtCore/qbytearraylist.h>
#include <QtCore/qthread.h>

#include <modmed/log/LogEvent.h>
#include <modmed/log/LogMessage.h>
#include <modmed/log/LogManager.h>

namespace modmed {
namespace log {

QAtomicInt LogEvent::s_newId(0);

LogMessage LogEvent::message(LogMessage::Options options, const char *emptyVariadicMacroExpr)
{
    if (!m_category.isEnabled(m_type))
        return LogMessage();

    if (!m_format.isNull())
        options |= LogMessage::NoLiterals;

    return LogMessage(this, options, emptyVariadicMacroExpr);
}

void LogEvent::log(QByteArray format, QVector<QSharedPointer<data::IBindable>> args, QVector<QByteArray> argTypes, QVector<QByteArray> argNames)
{
    if (m_id == 0)
        m_id = ++s_newId;

    if (!format.isNull()) {
        m_format = format;
        m_argTypes = argTypes;
        m_argNames = argNames;
    }

    LogEventData data(m_type, m_file, m_line, m_function, m_categoryName);
    data.setId(QByteArray::number(m_id, 36).toUpper());
    data.setCount(m_count);
    data.setFormat(m_format.constData());
    data.setArgs(args);
    data.setArgTypes(m_argTypes);
    data.setArgNames(m_argNames);

    LogManager::getInstance().process(data);
    ++m_count;
}

int LogEvent::id() const
{
    return m_id;
}

int LogEvent::count() const
{
    return m_count;
}

const QLoggingCategory& LogEvent::category() const
{
    return m_category;
}

bool LogSample::isSampled() const
{
    return m_count == 0 ||
           ((m_count-m_lastCount >= m_countInterval ) &&
            (m_lastTime.nsecsElapsed() >= m_nsecsInterval));
}

void LogSample::log(QByteArray format, QVector<QSharedPointer<data::IBindable>> args, QVector<QByteArray> argTypes, QVector<QByteArray> argNames)
{
    if (isSampled())
    {
        m_lastCount = m_count;
        LogEvent::log(format, args, argTypes, argNames);
        m_lastTime.restart();
    }
    else
        ++m_count;
}

QByteArray categoryFromFunctionNamespace(const char* functionSignature, const char* functionName)
{
    QByteArray functionNamespace(functionName);
    if (functionNamespace.contains(':'))
    {
        functionNamespace.truncate(functionNamespace.lastIndexOf(':'));
    }
    else
    {
        functionNamespace = functionSignature;
        functionNamespace.truncate(functionNamespace.indexOf(functionName));
        int lastSpace = functionNamespace.lastIndexOf(' ');
        if (0 < lastSpace)
            functionNamespace = functionNamespace.right(functionNamespace.length()-lastSpace);
    }
    QByteArray category;
    Q_FOREACH(char c, functionNamespace)
    {
        if (  ('0' <= c && c <= '9')
            ||('A' <= c && c <= 'Z')
            ||('a' <= c && c <= 'z')
            || c == '_'             )
            category.push_back(c);
        else if (31 < c && c < 127 && category.size()>0 && category[category.size()-1]!='.')
            category.push_back('.');
    }
    if (category.size()>0 && category[category.size()-1]=='.')
        category[category.size()-1]='\0';
    else
        category.push_back('\0');

    return category;
}

// tokens as recognized in QT_MESSAGE_PATTERN
//static const char categoryTokenC[] = "%{category}";
//static const char typeTokenC[] = "%{type}";
//static const char messageTokenC[] = "%{message}";
//static const char fileTokenC[] = "%{file}";
//static const char lineTokenC[] = "%{line}";
//static const char functionTokenC[] = "%{function}";
//static const char pidTokenC[] = "%{pid}";
//static const char appnameTokenC[] = "%{appname}";
//static const char threadidTokenC[] = "%{threadid}";
//static const char qthreadptrTokenC[] = "%{qthreadptr}";
//static const char timeTokenC[] = "%{time"; //not a typo: this command has arguments
//static const char backtraceTokenC[] = "%{backtrace"; //ditto

} // log
} // modmed

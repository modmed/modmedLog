/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <modmed/log/LogEventData.h>

#include <QtCore/qthread.h>

namespace modmed {
namespace log {

LogEventData::LogEventData(const QtMsgType& type,
                               const char *file,
                               int line,
                               const char *function,
                               const char *category):
    m_type(type),
    m_file(file),
    m_line(line),
    m_function(function),
    m_category(category)
{}

int LogEventData::line() const
{
    return m_line;
}

const char* LogEventData::file() const
{
    return m_file;
}

const char* LogEventData::function() const
{
    return m_function;
}

const char* LogEventData::category() const
{
    return m_category;
}

const QByteArray& LogEventData::format() const
{
    return m_format;
}

const QByteArray& LogEventData::id() const
{
    return m_id;
}

const QByteArray& LogEventData::threadId() const
{
    if (m_threadId.isNull())
        m_threadId = QByteArray::number(qint64(QThread::currentThreadId()));
    return m_threadId;
}

quint64 LogEventData::count() const
{
    return m_count;
}

const QtMsgType& LogEventData::type() const
{
    return m_type;
}

const QVector<QSharedPointer<data::IBindable>>& LogEventData::args() const
{
    return m_args;
}

const QVector<QByteArray>& LogEventData::argTypes() const
{
    return m_argTypes;
}

const QVector<QByteArray>& LogEventData::argNames() const
{
    return m_argNames;
}

const QDateTime& LogEventData::dateTime() const
{
    if (m_dateTime.isNull()) {
        auto now = QDateTime::currentDateTime();
        m_dateTime = now.toOffsetFromUtc(now.offsetFromUtc()); // save offset into the QDateTime to print it
    }
    return m_dateTime;
}

qint64 LogEventData::nsecsElapsed() const
{
    return m_nsecsElapsed;
}

void LogEventData::setFormat(const QByteArray& format)
{
    m_format = format;
}

void LogEventData::setId(const QByteArray& id)
{
    m_id = id;
}

void LogEventData::setCount(quint64 count)
{
    m_count = count;
}

void LogEventData::setArgs(const QVector<QSharedPointer<data::IBindable>>& data)
{
    m_args = data;
}

void LogEventData::setArgTypes(const QVector<QByteArray>& data)
{
    m_argTypes = data;
}

void LogEventData::setArgNames(const QVector<QByteArray>& data)
{
    m_argNames = data;
}

void LogEventData::setNSecsElapsed(qint64 t)
{
    m_nsecsElapsed = t;
}



} // log
} // modmed

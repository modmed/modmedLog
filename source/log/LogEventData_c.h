#pragma once

#include <stdint.h>

typedef struct {
    const char* _source_path;
    int _source_line;
    const char* _function;
    const char* _category;
    const char* _format;
    const char* _id;
    uint64_t _count;
    const char* _args;
    size_t _args_size;
    const char* _arg_types;
    size_t _arg_types_size;
    const char* _arg_names;
    size_t _arg_names_size;
} LogEventData_c;


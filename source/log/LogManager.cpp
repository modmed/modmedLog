#include <modmed/log/LogEvent.h>
#include <modmed/log/LogManager.h>
namespace ml = ::modmed::log;

#include <iostream>
#include <mutex> // call_once

#include <QtCore/qdebug.h>

namespace modmed {
namespace log {

M_NAMESPACE_LOG_CATEGORY()

QScopedPointer<LogManager> LogManager::m_instance(nullptr);
static std::once_flag s_instanceFlag;

void LogManager::qtMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    ml::LogEventData e(type, context.file, context.line, context.function, context.category);
    e.setFormat(msg.toUtf8());       // data is already merged with the constant part of the message
    LogManager::getInstance().process(e);
    // TODO Abort on QMsgFatal to provide default Qt behaviour
}

LogManager::LogManager() : m_output(nullptr),
    m_defaultOutput()
{
    Q_ASSERT(m_startWriteTime.isMonotonic());
    m_startWriteTime.start();
    // TODO Record QDateTime::currentDateTime() and loop if m_startWriteTime.secsElapsed() < 1 to sync both clocks
    // TODO store m_startWriteTime::msecsSinceReference() to facilitate later merging of log files (an immediately following restart()<1000 should be given too so that both are within 1sec?)
}

LogManager::~LogManager()
{}

void LogManager::setOutput(ILogOutput* p_output)
{
    QWriteLocker lock(&m_outputMutex);
    m_output = p_output ? p_output : &m_defaultOutput; // in case an allocation returned a nullptr?!
}

const QElapsedTimer& LogManager::startWriteTime()
{
    return m_startWriteTime;
}

LogManager& LogManager::getInstance()
{
    std::call_once(s_instanceFlag, [] { m_instance.reset(new LogManager); });
    return *m_instance.data();
}

void LogManager::process(LogEventData& eventData)
{
    eventData.setNSecsElapsed(m_startWriteTime.nsecsElapsed());
    output(eventData);
}

void LogManager::output(const LogEventData& eventData)
{
    // Passing eventData to Qt message handler encoding constant message and corresponding data separated with '\t' would allow
    // forwarding to all Qt supported outputs like journald. But anyway, it is interesting to map LogEventData differently to each sink:
    // journald, ETW, sqlite, tsv+json files, etc.

    // So, we prefer defining our own ILogOutput::output(LogEventData) interface that can be implemented by thin wrapper classes

    QReadLocker lock(&m_outputMutex);
    if (!m_output
        || !m_output->output(eventData))
    {
        m_defaultOutput.output(eventData);
    }
}

} // log
} // modmed

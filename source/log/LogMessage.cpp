/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <QtCore/qdebug.h>

#include <modmed/log/LogMessage.h>
#include <modmed/log/LogEvent.h>

namespace modmed {
namespace log {

MODMED_API const char* ACTION_CREATE        = "#Create";
MODMED_API const char* ACTION_COPY          = "#Copy";
MODMED_API const char* ACTION_MOVE          = "#Move";
MODMED_API const char* ACTION_REMOVE        = "#Remove";

MODMED_API const char* ACTION_OPEN          = "#Open";
MODMED_API const char* ACTION_READ          = "#Read";
MODMED_API const char* ACTION_WRITE         = "#Write";
MODMED_API const char* ACTION_EXECUTE       = "#Execute";
MODMED_API const char* ACTION_ACCESS        = "#Access";
MODMED_API const char* ACTION_CLOSE         = "#Close";

MODMED_API const char* ACTION_LOGIN         = "#Login";
MODMED_API const char* ACTION_ALLOW         = "#Allow";
MODMED_API const char* ACTION_BLOCK         = "#Block";
MODMED_API const char* ACTION_LOGOUT        = "#Logout";

MODMED_API const char* ACTION_ALLOCATE      = "#Allocate";
MODMED_API const char* ACTION_INITIALIZE    = "#Initialize";
MODMED_API const char* ACTION_LOCK          = "#Lock";
MODMED_API const char* ACTION_UNLOCK        = "#Unlock";
MODMED_API const char* ACTION_FREE          = "#Free";

MODMED_API const char* ACTION_BIND          = "#Bind";
MODMED_API const char* ACTION_CONNECT       = "#Connect";
MODMED_API const char* ACTION_DOWNLOAD      = "#Download";
MODMED_API const char* ACTION_UPLOAD        = "#Upload";
MODMED_API const char* ACTION_DISCONNECT    = "#Disconnect";

MODMED_API const char* ACTION_START         = "#Start";
MODMED_API const char* ACTION_SUSPEND       = "#Suspend";
MODMED_API const char* ACTION_RESUME        = "#Resume";
MODMED_API const char* ACTION_STOP          = "#Stop";

MODMED_API const char* ACTION_INSTALL       = "#Install";
MODMED_API const char* ACTION_UPDATE        = "#Update";
MODMED_API const char* ACTION_UPGRADE       = "#Upgrade";
MODMED_API const char* ACTION_UNINSTALL     = "#Uninstall";

MODMED_API const char* ACTION_TRACE         = "#Trace";

// "object" tags for use in LogMessage
MODMED_API const char* OBJECT_ACCOUNT       = "#Account";
MODMED_API const char* OBJECT_APP           = "#App";
MODMED_API const char* OBJECT_CONNECTION    = "#Connection";
MODMED_API const char* OBJECT_DRIVER        = "#Driver";
MODMED_API const char* OBJECT_EMAIL         = "#Email";
MODMED_API const char* OBJECT_FILE          = "#File";
MODMED_API const char* OBJECT_HOST          = "#Host";
MODMED_API const char* OBJECT_IPV4          = "#Ipv4";
MODMED_API const char* OBJECT_IPV6          = "#Ipv6";
MODMED_API const char* OBJECT_LIBRARY       = "#Library";
MODMED_API const char* OBJECT_MEMORY        = "#Memory";
MODMED_API const char* OBJECT_PROCESS       = "#Process";
MODMED_API const char* OBJECT_SYSTEM        = "#System";
MODMED_API const char* OBJECT_THREAD        = "#Thread";

MODMED_API const char* OBJECT_ASSUMPTION    = "#Assumption";
MODMED_API const char* OBJECT_REQUIREMENT   = "#Requirement";

// "status" tags for use in LogMessage
MODMED_API const char* STATUS_ONGOING       = "#Ongoing";
MODMED_API const char* STATUS_CANCEL        = "#Cancel";
MODMED_API const char* STATUS_FINISHED      = "#Finished";
MODMED_API const char* STATUS_FAILURE       = "#Failure";
MODMED_API const char* STATUS_SUCCESS       = "#Success";
MODMED_API const char* STATUS_UNKNOWN       = "#Unknown";

LogMessage::~LogMessage()
{
    if (!--stream->ref) {
        if (stream->ts) {
            *stream->ts << QString(stream->utf8Format);
            stream->ts->flush(); // not flushed in QDebug?!
        }
        if (stream->pEvent) {
            if (stream->options.testFlag(NoLiterals)) {
                stream->pEvent->log(QByteArray(), args());
            }
            else {
                stream->pEvent->log(format(), args(), argTypes(), argNames());
            }
        }
        delete stream;
    }
}

QByteArray LogMessage::format() const
{
    if (stream->space && stream->utf8Format.endsWith(' '))
    {
        QByteArray trimmed(stream->utf8Format);
        trimmed.chop(1);
        return trimmed;
    }
    return stream->utf8Format;
}

QByteArray LogMessage::toName(const char* text)
{
    QByteArray name;
    for (; *text != '\0'; text++)
    {
        char c = *text;
        if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z') || ('0' <= c && c <= '9') || (c == '_' && !name.isEmpty()))
        {
            name += c;
        }
        else if (!name.isEmpty() && name[name.size()-1] != '_')
        {
            name += '_';
        }
    }
    if (!name.isEmpty() && '0' <= name[0] && name[0] <= '9') // Digits are forbidden at the beginning
    {
        name.prepend('_');
    }
    return name;
}

/*!
    \internal
    Duplicated from QtTest::toPrettyUnicode().
*/
void LogMessage::putUcs4(unsigned ucs4)
{
    if (stream->testFlag(Stream::NoQuotes) && !stream->options.testFlag(NoLiterals))
    {
        stream->utf8Format.append(QString(ucs4).toUtf8());
    }
    else
    {
        putItem(ucs4);
    }
}

/*!
    \internal
    Duplicated from QtTest::toPrettyUnicode().
*/
void LogMessage::putString(const QString& t)
{
    if (stream->testFlag(Stream::NoQuotes) && !stream->options.testFlag(NoLiterals))
    {
        stream->utf8Format.append(t);
    }
    else
    {
        putItem(t);
    }
}

void LogMessage::putLatin1String(QLatin1String s)
{
    if (stream->testFlag(Stream::NoQuotes) && !stream->options.testFlag(NoLiterals))
    {
        stream->utf8Format.append(QString(s).toUtf8());
    }
    else
    {
        putItem(s);
    }
}

void LogMessage::putByteArray(const QByteArray& ba)
{
    if (stream->testFlag(Stream::NoQuotes) && !stream->options.testFlag(NoLiterals))
    {
        stream->utf8Format.append(ba); // best choice for QByteArray constructed using compiler generated const char* ? (MSVC 2013 for instance uses utf-8 execution_charset)
    }
    else
    {
        putItem(ba);
    }
}

LogMessage &LogMessage::resetFormat()
{
    if (stream->ts) stream->ts->reset();
    stream->space = true;
    stream->flags = 0;
    stream->setVerbosity(Stream::DefaultVerbosity);
    return *this;
}

class LogMessageStateSaverPrivate
{
public:
    LogMessageStateSaverPrivate(LogMessage &dbg)
        : m_dbg(dbg),
          m_spaces(dbg.autoInsertSpaces()),
          m_flags(0)//,
          // TODO m_streamOutExprs(dbg.stream->ts.m_d->params)
    {
        m_flags = m_dbg.stream->flags;
    }

    void restoreState()
    {
        const bool currentSpaces = m_dbg.autoInsertSpaces();
        if (currentSpaces && !m_spaces)
            if (m_dbg.stream->ts &&
                m_dbg.stream->ts->string() &&
                m_dbg.stream->ts->string()->endsWith(QChar(' ')))
                m_dbg.stream->ts->string()->chop(1);

        m_dbg.setAutoInsertSpaces(m_spaces);
        // TODO m_dbg.stream->ts.m_d->params = m_streamOutExprs;
        m_dbg.stream->flags = m_flags;

        if (!currentSpaces && m_spaces && m_dbg.stream->ts)
            *m_dbg.stream->ts << ' ';
    }

    LogMessage &m_dbg;

    // LogMessage state
    const bool m_spaces;
    int m_flags;

    // QTextStream state
    // TODO const QTextStreamPrivate::Params m_streamOutExprs;
};


LogMessageStateSaver::LogMessageStateSaver(LogMessage &dbg)
    : d(new LogMessageStateSaverPrivate(dbg))
{
}

LogMessageStateSaver::~LogMessageStateSaver()
{
    d->restoreState();
}

#if MODMED_USERS_REALLY_NEED_IT
// TODO in QtBind.cpp
#include <QtCore/qmetaobject.h>

/*!
    \internal

    Specialization of the primary template in qdebug.h to out-of-line
    the common case of QFlags<T>::Int being int.

    Just call the generic version so the two don't get out of sync.
*/
void qt_QMetaEnum_flagDebugOperator(LogMessage &debug, size_t sizeofT, int value)
{
    qt_QMetaEnum_flagDebugOperator<int>(debug, sizeofT, value);
}

#ifndef QT_NO_QOBJECT
/*!
    \internal
 */
LogMessage qt_QMetaEnum_debugOperator(LogMessage &dbg, int value, const QMetaObject *meta, const char *name)
{
    LogMessageStateSaver saver(dbg);
    QMetaEnum me = meta->enumerator(meta->indexOfEnumerator(name));
    const char *key = me.valueToKey(value);
    dbg.nospace() << meta->className() << "::" << name << '(';
    if (key)
        dbg << key;
    else
        dbg << value;
    dbg << ')';
    return dbg;
}

LogMessage qt_QMetaEnum_flagDebugOperator(LogMessage &debug, quint64 value, const QMetaObject *meta, const char *name)
{
    LogMessageStateSaver saver(debug);
    debug.resetFormat();
    debug.noquote();
    debug.nospace();
    debug << "QFlags<";
    const QMetaEnum me = meta->enumerator(meta->indexOfEnumerator(name));
    if (const char *scope = me.scope())
        debug << scope << "::";
    debug << me.name() << ">(" << me.valueToKeys(value) << ')';
    return debug;
}
#endif // !QT_NO_QOBJECT

#endif

} // log
} // modmed

QDebug operator<<(QDebug debug, const modmed::log::LogMessage& message)
{
    QDebugStateSaver saver(debug);
    return debug.noquote().nospace()
        << message.format()  <<   '\t'
        << message.args();     // '\t' separated
}

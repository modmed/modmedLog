#include <QtCore/qdatetime.h>
#include <QtCore/qbuffer.h>
#include <modmed/data/CborWriter.h>
#include <modmed/data/Bind.h>
#include <modmed/log/LttngLogWriter.h>
#include "LttngTracepointProvider.h"
#include "LogEventData_c.h"
#include <cstdlib>
#include <cstring>

namespace modmed {
namespace log {

LttngLogWriter::LttngLogWriter()
{
    tracepoint(TRACEPOINT_PROVIDER, start, QDateTime::currentDateTime().toString(Qt::ISODateWithMs).toUtf8().constData());
}

LttngLogWriter::~LttngLogWriter()
{
    tracepoint(TRACEPOINT_PROVIDER, stop, QDateTime::currentDateTime().toString(Qt::ISODateWithMs).toUtf8().constData());
}

bool LttngLogWriter::output(const LogEventData& p_eventData)
{
    LogEventData_c led;

    led._source_path = p_eventData.file();
    led._source_line = p_eventData.line();
    led._function = p_eventData.function();
    led._category = p_eventData.category();
    led._format = p_eventData.format().constData();
    led._id = p_eventData.id().constData();
    led._count = p_eventData.count();

    QBuffer bufT;
    if (!bufT.open(QIODevice::WriteOnly))
        return false;
    data::CborWriter writerT(&bufT);
    QVector<QLatin1String> readableTypes;
    for (auto it = p_eventData.argTypes().cbegin(); it != p_eventData.argTypes().cend(); ++it)
    {
        readableTypes.append(QLatin1String(*it));
    }
    writerT.write().bind(readableTypes);
    led._arg_types = bufT.buffer().data();
    led._arg_types_size = bufT.buffer().size();

    QBuffer bufN;
    if (!bufN.open(QIODevice::WriteOnly))
        return false;
    data::CborWriter writerN(&bufN);
    QVector<QLatin1String> readableNames;
    for (auto it = p_eventData.argNames().cbegin(); it != p_eventData.argNames().cend(); ++it)
    {
        readableNames.append(QLatin1String(*it));
    }
    writerN.write().bind(readableNames);
    led._arg_names = bufN.buffer().data();
    led._arg_names_size = bufN.buffer().size();

    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
        return false;

    data::CborWriter writer(&buf);
    writer.write().bind(p_eventData.args());
    led._args = buf.buffer().data();
    led._args_size = buf.buffer().size();

    switch (p_eventData.type())
    {
        case QtFatalMsg:    tracepoint(TRACEPOINT_PROVIDER, fatal, &led);
            break;
        case QtCriticalMsg: tracepoint(TRACEPOINT_PROVIDER, critical, &led);
            break;
        case QtWarningMsg:  tracepoint(TRACEPOINT_PROVIDER, warning, &led);
            break;
        case QtInfoMsg:     tracepoint(TRACEPOINT_PROVIDER, info, &led);
            break;
        default:
            case QtDebugMsg:    tracepoint(TRACEPOINT_PROVIDER, debug, &led);
                break;
    }
    return true;

}

void LttngLogWriter::flush() { }

} // log
} // modmed

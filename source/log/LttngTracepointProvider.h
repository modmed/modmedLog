#undef TRACEPOINT_PROVIDER
#define TRACEPOINT_PROVIDER modmedLog

#undef TRACEPOINT_INCLUDE
#define TRACEPOINT_INCLUDE "./LttngTracepointProvider.h"

#if !defined(_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define _TP_H

#include <lttng/tracepoint.h>
#include "LogEventData_c.h"

/*
 * Use TRACEPOINT_EVENT(), TRACEPOINT_EVENT_CLASS(),
 * TRACEPOINT_EVENT_INSTANCE(), and TRACEPOINT_LOGLEVEL() here.
 */

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    start,

    /* Input arguments */
    TP_ARGS(
	const char*, local_datetime
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_local_datetime, local_datetime)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, start, TRACE_INFO)

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    stop,

    /* Input arguments */
    TP_ARGS(
	const char*, local_datetime
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_local_datetime, local_datetime)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, stop, TRACE_INFO)

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    fatal,

    /* Input arguments */
    TP_ARGS(
        const LogEventData_c *, logData
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_source_path, logData->_source_path)
        ctf_integer(int, _source_line, logData->_source_line)
        ctf_string(_function, logData->_function)
        ctf_string(_category, logData->_category)
        ctf_string(_format, logData->_format)
        ctf_string(_id, logData->_id)
        ctf_integer(uint64_t, _count, logData->_count)
        ctf_sequence_hex(unsigned char, _args, logData->_args, size_t, logData->_args_size)
        ctf_sequence_hex(unsigned char, _arg_types, logData->_arg_types, size_t, logData->_arg_types_size)
        ctf_sequence_hex(unsigned char, _arg_names, logData->_arg_names, size_t, logData->_arg_names_size)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, fatal, TRACE_EMERG)

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    critical,

    /* Input arguments */
    TP_ARGS(
        const LogEventData_c*, logData
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_source_path, logData->_source_path)
        ctf_integer(int, _source_line, logData->_source_line)
        ctf_string(_function, logData->_function)
        ctf_string(_category, logData->_category)
        ctf_string(_format, logData->_format)
        ctf_string(_id, logData->_id)
        ctf_integer(uint64_t, _count, logData->_count)
	ctf_sequence_hex(unsigned char, _args, logData->_args, size_t, logData->_args_size)
    ctf_sequence_hex(unsigned char, _arg_types, logData->_arg_types, size_t, logData->_arg_types_size)
    ctf_sequence_hex(unsigned char, _arg_names, logData->_arg_names, size_t, logData->_arg_names_size)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, critical, TRACE_CRIT)

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    warning,

    /* Input arguments */
    TP_ARGS(
        const LogEventData_c*, logData
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_source_path, logData->_source_path)
        ctf_integer(int, _source_line, logData->_source_line)
        ctf_string(_function, logData->_function)
        ctf_string(_category, logData->_category)
        ctf_string(_format, logData->_format)
        ctf_string(_id, logData->_id)
        ctf_integer(uint64_t, _count, logData->_count)
	ctf_sequence_hex(unsigned char, _args, logData->_args, size_t, logData->_args_size)
    ctf_sequence_hex(unsigned char, _arg_types, logData->_arg_types, size_t, logData->_arg_types_size)
    ctf_sequence_hex(unsigned char, _arg_names, logData->_arg_names, size_t, logData->_arg_names_size)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, warning, TRACE_WARNING)

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    info,

    /* Input arguments */
    TP_ARGS(
        const LogEventData_c*, logData
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_source_path, logData->_source_path)
        ctf_integer(int, _source_line, logData->_source_line)
        ctf_string(_function, logData->_function)
        ctf_string(_category, logData->_category)
        ctf_string(_format, logData->_format)
        ctf_string(_id, logData->_id)
        ctf_integer(uint64_t, _count, logData->_count)
	ctf_sequence_hex(unsigned char, _args, logData->_args, size_t, logData->_args_size)
    ctf_sequence_hex(unsigned char, _arg_types, logData->_arg_types, size_t, logData->_arg_types_size)
    ctf_sequence_hex(unsigned char, _arg_names, logData->_arg_names, size_t, logData->_arg_names_size)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, info, TRACE_INFO)

TRACEPOINT_EVENT(
    /* Tracepoint provider name */
    modmedLog,

    /* Tracepoint name */
    debug,

    /* Input arguments */
    TP_ARGS(
        const LogEventData_c*, logData
        ),

    /* Output event fields */
    TP_FIELDS(
        ctf_string(_source_path, logData->_source_path)
        ctf_integer(int, _source_line, logData->_source_line)
        ctf_string(_function, logData->_function)
        ctf_string(_category, logData->_category)
        ctf_string(_format, logData->_format)
        ctf_string(_id, logData->_id)
        ctf_integer(uint64_t, _count, logData->_count)
	ctf_sequence_hex(unsigned char, _args, logData->_args, size_t, logData->_args_size)
    ctf_sequence_hex(unsigned char, _arg_types, logData->_arg_types, size_t, logData->_arg_types_size)
    ctf_sequence_hex(unsigned char, _arg_names, logData->_arg_names, size_t, logData->_arg_names_size)
        )
    )

TRACEPOINT_LOGLEVEL(modmedLog, debug, TRACE_DEBUG)

#endif /* _TP_H */

#include <lttng/tracepoint-event.h>

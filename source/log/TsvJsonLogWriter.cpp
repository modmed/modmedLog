#include <QtCore/qstringlist.h>

#include <modmed/log/TsvJsonLogWriter.h>
#include <modmed/log/LogManager.h>

namespace modmed {
namespace log {

const QByteArray TsvJsonLogWriter::s_nullString("-");

TsvJsonLogWriter::TsvJsonLogWriter(QTextStream* p_stream /*= nullptr*/, LogOptions p_options /*= LogOptions()*/)
{
    m_options = p_options;
    m_streamPointer = p_stream;
    init();
}

TsvJsonLogWriter::TsvJsonLogWriter(QIODevice* p_io /*= nullptr*/, LogOptions p_options /*= LogOptions()*/)
{
    m_options = p_options;
    m_streamPointer = new QTextStream(p_io);
    init();
}

TsvJsonLogWriter::TsvJsonLogWriter(FILE* p_f /*= nullptr*/, LogOptions p_options /*= LogOptions()*/)
{
    m_options = p_options;
    m_streamPointer = new QTextStream(p_f, QIODevice::WriteOnly);
    init();
}

TsvJsonLogWriter::TsvJsonLogWriter(QString* p_s /*= nullptr*/, LogOptions p_options /*= LogOptions()*/)
{
    m_options = p_options;
    m_streamPointer = new QTextStream(p_s);
    init();
}

TsvJsonLogWriter::~TsvJsonLogWriter()
{
}

void TsvJsonLogWriter::init()
{
    Q_ASSERT(m_streamPointer);

//    m_streamPointer->setLocale(p_locale); // TODO for elapsedSecs only, "C" locale for JSON fields
    *m_streamPointer
        << "_elapsed_s\t"
        << (m_options.testFlag(log::TsvJsonLogWriter::HideDateTime) ? "" :   "_timestamp\t")
        << "_severity\t"
        << (m_options.testFlag(log::TsvJsonLogWriter::HideCategory) ? "" :   "_category\t")
        << (m_options.testFlag(log::TsvJsonLogWriter::HideFunction) ? "" :   "_function\t")
        << "_id\t"
        << "_count\t"
        << (m_options.testFlag(log::TsvJsonLogWriter::HideFileName) ? "" : "_path\t")
        << (m_options.testFlag(log::TsvJsonLogWriter::HideLine    ) ? "" : "_line\t")
        << "_thread_id\t"
        << "_arg_types\t"
        << "_arg_names\t"
        << "_format\t"
        << "_args"
        << endl;
}

int TsvJsonLogWriter::severity(QtMsgType p_type)
{
    switch (p_type) {
    case QtFatalMsg:    return 0;
    case QtCriticalMsg: return 2;
    case QtWarningMsg:  return 4;
    default:
    case QtInfoMsg:     return 6;
    case QtDebugMsg:    return 7;
    }
}

bool TsvJsonLogWriter::output(const LogEventData& p_eventData)
{
    if (!m_streamPointer)
        return false;

    // Save event date time and elapsedTime to later detect redundant _date_time
    qint64 writeNSecs = p_eventData.nsecsElapsed();

    // _elapsed_s
    *m_streamPointer << (double)writeNSecs/1000000000 << '\t';

    // _timestamp YYYY-MM-DDTHH:mm:ss[+/-]HH:mm
    // Write all if HideOptions is not set
    if (!m_options.testFlag(log::TsvJsonLogWriter::HideDateTime))
    {
        if (m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) ||
            m_previousDateTimeNSecs + DateTimeIntervalNSecs <= writeNSecs)
        {
            *m_streamPointer << p_eventData.dateTime().toString(Qt::DateFormat::ISODate);
            m_previousDateTimeNSecs = writeNSecs;
        }
        *m_streamPointer << '\t';
    }

    // _severity
    // only redundant 7 are eliminated
    {
        int s = severity(p_eventData.type());
        *m_streamPointer << (s == 7 && m_previousSeverity == 7 ? "" : QString::number(s)) << '\t';
        m_previousSeverity = s;
    }

    // _category
    if (!m_options.testFlag(log::TsvJsonLogWriter::HideCategory))
    {
        *m_streamPointer << p_eventData.category() << '\t';
    }

    // _function
    if (!m_options.testFlag(log::TsvJsonLogWriter::HideFunction))
    {
        if (m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy))
            *m_streamPointer << p_eventData.function() << '\t';
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousFunction == p_eventData.function()))
            *m_streamPointer << '\t';
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousFunction != p_eventData.function()))
        {
            *m_streamPointer << p_eventData.function() << '\t';
            m_previousFunction = p_eventData.function();
        }
    }

    // _id
    if (p_eventData.id().isEmpty())
        *m_streamPointer << s_nullString << '\t';
    else
        *m_streamPointer << p_eventData.id() << '\t';

    // _count
    *m_streamPointer << p_eventData.count() << '\t';

    // _path
    if (!m_options.testFlag(log::TsvJsonLogWriter::HideFileName))
    {
        if (m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy))
            *m_streamPointer << p_eventData.file() << '\t';
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousPath == p_eventData.file()))
            *m_streamPointer << '\t';
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousPath != p_eventData.file()))
        {
            *m_streamPointer << p_eventData.file() << '\t';
            m_previousPath = p_eventData.file();
            m_changedFile = true;
        }
    }

    // _line
    if (!m_options.testFlag(log::TsvJsonLogWriter::HideLine))
    {
        if (m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy))
            *m_streamPointer << p_eventData.line() << '\t';
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousLine == p_eventData.line()) && !m_changedFile)
            *m_streamPointer << '\t';
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousLine == p_eventData.line()) &&  m_changedFile)
        {
            *m_streamPointer << p_eventData.line() << '\t';
            m_changedFile = false;
        }
        if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousLine != p_eventData.line()))
        {
            *m_streamPointer << p_eventData.line() << '\t';
            m_previousLine = p_eventData.line();
            m_changedFile = false;
        }
    }

    // _thread_id
    if (m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy))
        *m_streamPointer << p_eventData.threadId() << '\t';
    if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousThreadId == p_eventData.threadId()))
        *m_streamPointer << '\t';
    if (!m_options.testFlag(log::TsvJsonLogWriter::KeepRedundancy) && (m_previousThreadId != p_eventData.threadId()))
    {
        m_previousThreadId = p_eventData.threadId().isEmpty() ? s_nullString : p_eventData.threadId();

        *m_streamPointer << m_previousThreadId << '\t';
    }

    // _arg_types
    QVector<QLatin1String> readableTypes;
    for (auto it = p_eventData.argTypes().cbegin(); it != p_eventData.argTypes().cend(); ++it)
    {
        readableTypes.append(QLatin1String(*it));
    }
    QString argT(data::JsonString().write().bind(readableTypes));
    argT.replace(QString("\t"), QString("")); // CR Use JsonWriter(&arg, indent=0, sep="")
    argT.replace(QString("\n"), QString(""));
    *m_streamPointer << argT << '\t';

    // _arg_names
    QVector<QLatin1String> readableNames;
    for (auto it = p_eventData.argNames().cbegin(); it != p_eventData.argNames().cend(); ++it)
    {
        readableNames.append(QLatin1String(*it));
    }
    QString argN(data::JsonString().write().bind(readableNames));
    argN.replace(QString("\t"), QString(""));
    argN.replace(QString("\n"), QString(""));
    *m_streamPointer << argN << '\t';

    // _format
    // _args
    QString wholeMsg(p_eventData.format());
    wholeMsg.replace(QString("\t"), QString(""));
    for (auto it = p_eventData.args().cbegin(); it != p_eventData.args().cend(); ++it) {
        QString arg(data::JsonString().write().bind(*it));
        arg.replace(QString("\t"), QString(""));
        wholeMsg += '\t' + arg;
    }
    wholeMsg.replace(QString("\n"), QString(""));

    *m_streamPointer << wholeMsg << endl;

    return isValid();
}

} // log
} // modmed

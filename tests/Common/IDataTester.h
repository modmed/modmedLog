/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qvector.h>
#include <QtCore/qlinkedlist.h>

#include <modmed/data/IData.h>

#ifdef Q_CC_MSVC
#pragma warning(push)
#pragma warning(disable:4584)
#endif
using namespace modmed;
using namespace data;

template<Operator Op = Writer>
struct IDataTester :
    public virtual data::IItem<Op>,
    public virtual data::ISequence<Op>,
    public virtual data::IRecord<Op>
{
    static int m_callOrder;

    IDataTester() :
        m_sequenceRetNullAfter(1),
        m_recordRetNullAfter(1),
        m_null(true),
        m_text(true),
        m_bindText(true),
        m_sequenceItemRetNullAfter(1),
        m_sequenceOut(true),
        m_recordItemRetNull(false),
        m_recordOut(true)
    {}

    // IItem<Op>
    ISequence<Op>* sequence(const ItmState<Op>&);
    IRecord<Op>* record (const ItmState<Op>&);
    bool bindNumber(const ItmState<Op>&, qlonglong& p_numb);
    bool bindNumber(const ItmState<Op>&, double& p_numb);
    bool bindBoolean(const ItmState<Op>&, bool& p_bool);
    bool bindTimeStamp(const ItmState<Op>&, QDateTime& p_datetime);
    bool bindBytes(const ItmState<Op>&, QByteArray& p_bytes);
    bool null(const ItmState<Op>&);

    int m_sequenceRetNullAfter;
    QVector<int> m_sequenceCall;
    QVector<const ItmState<Op>*> m_sequencePath;
    int m_recordRetNullAfter;
    QVector<int> m_recordCall;
    QVector<const ItmState<Op>*> m_recordPath;
    bool m_null;
    QVector<int> m_nullCall;
    QVector<const ItmState<Op>*> m_nullPath;

    // IMessage<Op>
    bool text    (const ItmState<Op>&, const QString&);
    bool bindText(const ItmState<Op>&,       QString&);

    bool m_text;
    QVector<int> m_textCall;
    QVector<const ItmState<Op>*> m_textPath;
    QLinkedList<QString>  m_textStrings;
    bool m_bindText;
    QVector<int> m_bindTextCall;
    QVector<const ItmState<Op>*> m_bindTextPath;
    QLinkedList<QString>  m_bindTextStrings;

    // ISequence<Op>
    IItem<Op>* sequenceItem(const SeqState<Op>&);
    bool   sequenceOut (const SeqState<Op>&);

    // number of not null before null
    int m_sequenceItemRetNullAfter;
    QVector<int> m_sequenceItemCall;
    QVector<const SeqState<Op>*> m_sequenceItemPath;
    bool m_sequenceOut;
    QVector<int> m_sequenceOutCall;
    QVector<const SeqState<Op>*> m_sequenceOutPath;

    // IRecord<Op>
    IItem<Op>* recordItem(const RecState<Op>&, QString& key);
    bool   recordOut (const RecState<Op>&);

    bool m_recordItemRetNull;
    QVector<int> m_recordItemCall;
    QLinkedList<QString> m_recordItemKey;
    QVector<const RecState<Op>*> m_recordItemPath;
    bool m_recordOut;
    QVector<int> m_recordOutCall;
    QVector<const RecState<Op>*> m_recordOutPath;

    QVector<int> m_numberCall;
    QLinkedList<qlonglong>  m_longlongValues;
    QLinkedList<double>  m_doubleValues;

    QVector<int> m_booleanCall;
    QLinkedList<bool>  m_boolValues;

    QVector<int> m_timestampCall;
    QLinkedList<QDateTime>  m_timestampValues;

    QVector<int> m_bytesCall;
    QLinkedList<QByteArray>  m_bytesValues;
};

#ifdef Q_CC_MSVC
#pragma warning(pop)
#endif

#include "TestingOutput.h"

namespace modmed {
namespace log {

TestingOutput::TestingOutput()
{}

bool TestingOutput::output(const LogEventData& p_eventData)
{
    m_eventDataList.push_back(p_eventData);

    return true;
}

void TestingOutput::flush()
{}

} // log
} // modmed

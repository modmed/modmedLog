#pragma once

#include <modmed/log/ILogOutput.h>
#include <modmed/log/LogEventData.h>

namespace modmed {
namespace log {

class TestingOutput : public ILogOutput
{
public:
    TestingOutput();
    virtual bool output(const LogEventData& p_eventData);
    virtual void flush();
    void clearAll() {m_eventDataList.clear();}

    QList<LogEventData> m_eventDataList;
};

} // log
} // modmed

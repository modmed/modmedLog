# Design

The core QTransmogrifier implementation (excluding QAbstractValue implementations) is a few hundreds line of C++11 using templates defined
in the headers, an abstract QAbstractValue class, and QAbstractValueWriter/QAbstractValueReader base classes.

## The key idea

QTransmogrifier is more general than (de)serialization and should be understood as a generic way to traverse[^1] a C++ dataset and
another generic dataset, binding the related parts together. In effect:
* the traversal may be partial, leaving out unrelated dataset parts (satisfying R2)
* the same traversal may be used to:
  - read/write (resp. deserialize/serialize) files or buffers
  - visit/build pointer-based data structures
  - compute statistics on C++ data
  - bind ordinary C++ data to succinct data structures (like [SDSL](https://github.com/simongog/sdsl-lite))

Hence, from now on, we will use the term *bind* instead of the more restricted *(de)serialization* term.

This traversal is driven by QTransmogrifier<T> methods which may use a QValueMode (Read,Write,...) to determine whether to read the generic dataset or write it according to the C++ one.

[^1]: *traverse* meaning to go through without returning back

QDebug and QDataStream translate all data to a "flat" sequence of characters/bytes which structure becomes implicit and can only
be determined for sure by reading the code that produced it. But R1, R2, R3 require that we describe our data in a little bit more
detail. Moreover, W1 and RW2 require that we choose a careful compromise between data format features.

QTransmogrifier allows binding C++ `data` to a choice of:
* `sequence` of adjacent `data` `item`s
* `record` of named `data` `item`s
* `null` value (meaning information is irrelevant on `data`)
* Atomic values with a textual representation and optional binary ones:
  - text (utf16/utf8 encodings)
  - boolean (true/false)
  - numbers (integral/floating-point, unsigned/signed, 8/16/32/64 bits)
  - *date/time (TBD)*
  - *uuid (TBD)*
* Generically supported T values for which a specialized QTransmogrifier<T>::bind() is defined

We argue that QTransmogrifier allows lossless conversions between all supported formats, and that the addition of optional metadata (RW3)
can address most peculiarities of the supported formats. However, it may not always conveniently address formats that do not have standard
ways of representing data structures such as XML (e.g. binding the Person type with enough metadata to conform the result to
[xCard schema](https://tools.ietf.org/html/rfc6351) would be cumbersome).

## QTransmogrifier grammar

The QTransmogrifier traversal is formally described by the following recursive automaton:
```mermaid
graph LR
subgraph QVal
i((start))--"any()"          --> x((end))
i((start))--"null()"         --> x((end))
i((start))--"bind#lt;T#gt;()"--> x((end))
i((start))--"sequence()"     --> QSeq
i((start))--"record()"       --> QRec
QSeq       --"out()"         --> x((end))
QRec       --"out()"         --> x((end))
QSeq       --"item()"        --> vs["QVal#lt;QSeq#gt;"]
QRec       --"item(name)"    --> vr["QVal#lt;QRec#gt;"]
end
```
- Boxes (nested) represent possible states when traversing the data, the automaton is always in a single valid state
- Edges represent possible state transitions that translate to specific data format read/write actions

The automaton is implemented as follows:
- `QVal<_>`, `QRec<_>` and `QSeq<_>` types implement possible states where _ denotes the type of outer state
- State types only expose public methods corresponding to possible transitions, that return the destination state type
- The initial state type is `QValue` and the final state is `QCur` (for instance: `QValue::null()` returns a `QCur`)
- Returning a `QRec<_>` or `QSeq<_>` type automatically convert to the final `QCur` type invoking as much `out()` transitions as required
- `QCur` is a non-owning pointer to an `QAbstractValue` interface which implementations translate data traversal into specific
  data format read/write actions
- `QCur` instance is moved from the start state type to the end state type only for successful transitions, allowing to test
  alternatives before proceeding with the traversal
- Transitions may fail for various reasons specific to `QAbstractValue` implementations:
  - builders may not be able to allocate new items
  - readers may read data not matching the expected transition
  - ...
- In case of unsuccessfull transition the returned state type receives a null `QCur` that transparently bypasses calls to `QAbstractValue`
- `bind<T>()` calls are forwarded to the actual `QAbstractValue` or generic `QTransmogrifier` depending on `BindSupport<T>`:
  - BindNative  : **QAbstractValue** interface method
  - BindGeneric : **QTransmogrifier** template specialization for T
- Every `bind<T>()` starts from a QValue which is an un *unsafe* QCur copy wrt well-formedness (these `unsafeItem()` copies are protected from incorrect use)

## C++ types extensibility

QTransmogrifier is a functor templated on T type receiving a Value and T reference (either lvalue or rvalue reference) and returning the QCur.
Template specializations can be defined for any T and optionally refined for specific Cur<TImpl> with different sets of BindNative types.

A default QTransmogrifier specialization attempts to call `T::bind(...)` to conveniently bind `T* this` without having to understand template syntax,
rvalue or forwarding references. Such custom implementations are facilitated by the fluent interface below.

## Convenient fluent interface

A convenient side-effect of encoding the QTransmogrifier traversal in the type system is that smart C++ editors offer QTransmogrifier-aware code completion
to the fluent interface making it similar to using an advanced XML editor for typing XML tags. Say, after typing `Value(myImpl).` the
editor will propose to either `bind(myData.item)`, or to construct a `sequence()`, `record()` or `null()` value.

## Well-formedness guarantees

Thanks to this design, the compiler will make sure that the only possibility to return a QCur from a `QValue` is to traverse
the data without backtracking, calling only and all necessary QAbstractValue virtual methods.

The addition of default and optional values take into account most data schema evolutions in a purely declarative fluent interface without
having to test schema versions and the like. The benefit is that it is not possible to introduce bugs using just the fluent interface.

The downside is that writing loops with the fluent interface is unnatural as one must never forget to follow the valid QCur.
For instance:
```cpp
auto seq(v.sequence());
for (auto&& t : ts) {
    seq = seq.bind(t); // do not forget to reassign seq, or subsequent items will be `bind` to the moved-from QCur and automatically ignored
}
```

## Write performance

Since `QCur`, `QVal`, `QRec` and `QSeq` have no data member other than outer types and `QAbstractValue*`, calling their methods can be
optimized and replaced with just the following operations:
1. test the QAbstractValue pointer validity [^1]
2. call the QAbstractValue virtual method corresponding to the possible transitions
3. return the resulting QCur, QVal, QRec or QSeq with a valid or invalid QCur depending on QAbstractValue method success or failure

[^1]: Experiments to use constexpr to bypass this step for writers that always return true did not seem to improve performance.

`QTransmogrifier<T>` can define up to 3 bind() overloads to efficiently and conveniently handle lvalue references, const lvalue references, and
rvalue references depending on T characteristics (which of copy/move is 1/possible and 2/efficient).

Compared to manually calling non-virtual, format-specific implementations, the overhead of always testing the validity of QAbstractValue*
and calling virtual methods is around 20% in our benchmark, with a maximum of 100% for trivial format-specific implementations
like copying a single char to a pre-allocated buffer.

This performance cost is usually dwarfed by unoptimized code and the ability to select a data format that performs well for the data
at hand. For instance, choosing Cbor with a mixture of boolean, character literals and numbers makes this overhead negligible.

Other than that, write performance depends on several factors:
- An explicit QUtf8Data type allows handling item names much more efficiently than using utf16 QString, while still being
  distinguishable from QByteArray binary data
- Using QData<TContent> classes to tag string encodings allowed to pinpoint unnecessary encoding conversions, notably in QVariant handling
- In the end, directly using QByteArray buffers instead of using QIODevice can amount to ~ 2x better write performance
- QAbstractValue implementations need to use efficient data structures from storing state. For instance, using an optimized std::vector<bool>
  to memorize opened JSON object/array can usually be stored in a single byte and avoid memory allocations, resulting in ~ 10x better
  write performance compared to QVector<bool>

## Read robustness

Performance is not so important for Read. But compared to manually calling non-virtual, format-specific implementations, QTransmogrifier
enforces well-formedness checks necessary to reliably read data coming from unknown sources (QAbstractValue implementations being responsible
for low-level checks).

All errors are reported as `QIdentifierLiteral` to the `QAbstractValue` implementations that will decide what to do with them:
- ignore the details and set a global error status enumeration is usually appropriate to QAbstractValueWriter implementations
- storing all read mismatches is usually more appropriate to world-facing QAbstractValueReader implementations
Standardizing error literals allows efficient reporting and analysis while ensuring that various libraries can define new ones independantly.

## Data types extensibility

QAbstractValue is an abstract base class for translating fluent interface calls to format-specific read or write actions.
The fluent interface guarantees that QAbstractValue virtual methods are always called at appropriate times, so QAbstractValue implementations do not
have to check well-formedness. It defines a set of BindNative types and default textual representations and encodings for non-textual
native types simplifying again the implementations (see TextWriter example).

QAbstractValueWriter and QAbstractValueReader provide partial QAbstractValue implementations simplifying the work of implementors and offering default textual representations.

*NB:* BindNative types could be extended by specifying BindSupport<T> trait but then, (de)serialization code must be specialized
for each TImpl. For instance, a QTransmogrifier<QColor,QDataStream> may be implemented differently from QTransmogrifier<QColor,QAbstractValue> but QTransmogrifier<QColor>
example shows that meta() can also be used to avoid specialized serialization code that breaks RW2 requirement. If meta() is deemed
enough, the BindSupport trait and TImpl template parameters can be removed.

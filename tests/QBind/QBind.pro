#-------------------------------------------------
#
# Project created by QtCreator 2018-06-12T22:10:15
#
#-------------------------------------------------

QT += widgets

TARGET = QBind

CONFIG += c++11 console
CONFIG -= app_bundle

TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# C++17 allows replacing 'if /*constexpr*/' with 'if constexpr' with little performance improvement
#gcc:QMAKE_CXXFLAGS += -std=c++1z
#msvc:QMAKE_CXXFLAGS += /std=c++17

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

gcc:QMAKE_CXXFLAGS += -ftemplate-backtrace-limit=0 -fdata-sections -ffunction-sections
gcc:QMAKE_LFLAGS_RELEASE += -Wl,--gc-sections -Xlinker -Map=release/QBind.map

#PROTO_FILE = persons.proto
#protoc.output = $${OUT_PWD}/${QMAKE_FILE_IN_BASE}.pb.cc
#protoc.variable_out = GENERATED_SOURCES
#protoc.input = PROTO_FILE
#win32 {
#    PROTO_PATH = /path/to/protobuf-3.7.1-msvc2019
#    DEFINES += PROTOBUF_USE_DLLS
#    protoc.commands = $${PROTO_PATH}/bin/protoc -I=$$relative_path($${PWD}, $${OUT_PWD}) --cpp_out=. ${QMAKE_FILE_NAME}
#    INCLUDEPATH += $${PROTO_PATH}/include
#    DEPENDPATH += $${PROTO_PATH}/include
#    LIBS += -L$${PROTO_PATH}/lib -llibprotobuf
#}
#QMAKE_EXTRA_COMPILERS += protoc

SOURCES += \
    QData.cpp \
    QValue.cpp \
    data.cpp \
    main.cpp

HEADERS += \
    QCbor_impl.h \
    QData.h \
    QJson_impl.h \
    QSettings_impl.h \
    QValue.h \
    QVariant_impl.h \
    QModel_impl.h \
    QData_impl.h \
    QXml_impl.h \
    data.h

DISTFILES += \
    DESIGN.md \
    README.md \
    persons.proto \
    sample.ini \
    samples.txt

#qt_o.target = main.qt.o
#qt_o.commands = ${QMAKE_CXX} ${QMAKE_CFLAGS_RELEASE} -DNO_QBIND_CODE -DNO_OTHER_CODE -DNO_JSON_RW_CODE -DNO_CBOR_RW_CODE -DNO_GUI_CODE $$mytarget.target -o $$qt_o.target main.cpp
#qt_o.depends = main.cpp
#
#QMAKE_EXTRA_TARGETS += qt_o

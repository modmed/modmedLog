/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <QtCore/qvector.h>
#include <QtCore/qmap.h>
//#include <QtCore/qcbormap.h>

#include "QBind_impl.h"
#include "QJson_impl.h"

// //////////////////////////////////////////////////////////////////////////
// QBind<QTableWriter,T> support

/// Binds generically any T that would bind as sequence() of record().bind("k",vk)... as:
/// record()
/// .bind("cols",names,QVector<const char*>())
/// .bind("rows",sequence() of sequence(names.size()).bind(v1)...)
/// 
/// \warning This kind of mapping is really specific to selected TResult (TSV, CBOR data-tables...)
///  whereas other TResult would bind T to native 
template<class TResult>
class QTableWriter : public QMovedWriter<QTableWriter<TResult>>
{
    TResult* r=nullptr;
    Names names;    //!< of bound columns
    quint32 max=0;  //!<    bound columns (we need to take its address)
    enum : int {
        T=0, //!< nothing called
//        H=1, //!< TODO ? outer record() called
        S=1, //!< sequence() called, expecting out() or item()... calls
        R=2, //!< record  () called, expecting out() or item(key)... calls
    };
    int d=T;                            // bind depth wrt successive fluent interface calls
    int col=-1;                         // bound column following names
    int bound=-1;                       // bound record item
    QMap<int,QWritable<TResult>> cache; // for record() items bound too soon w.r.t. names
protected:
    QTableWriter(const QTableWriter &o) : QMovedWriter<QTableWriter<TResult>>(), r(o.r), names(o.names), max(o.max), d(o.d), col(o.col), bound(o.bound), cache(o.cache) {}
    QTableWriter &operator=(const QTableWriter &) = delete;
public:
    Q_ENABLE_MOVE(QTableWriter, std::swap(r, o.r); std::swap(names,o.names); max=o.max; d=o.d; col=o.col; bound=o.bound; std::swap(cache,o.cache); ) // (d,col,bound) copied to facilitate debugging
    QTableWriter(TResult* r, const Names& c) : r(r), names(c), max(quint32(names.size())) {
//        if (r->_record()) {
//            ++d;
//            if (r->_item("cols") && r->_bind(names) && r->_item("rows"))
//                bound=0;
//        }
    }
   ~QTableWriter() {
//        if (!(bound<int(max)))
//            Q_ASSERT(false);
//        if (r) { while (T<d || (R<d && 0<bound)) _out(); }
    }

    operator bool() { return r && r->operator bool(); } // for QMovedWriter
protected:
    friend class QMovedWriter<QTableWriter>;

    template<class T_> friend class Val; // calls methods below
    operator TResult() { return r ? *r : TResult(); }

    /**/                 bool _null(     ) { if (d>=R && (col!=bound || ___null())) { return true; } else { return false; } }
    template<typename T> bool _bind(T&& t) { if (d>=R && (col!=bound || __bind(t))) { return true; } else { return false; } }

    bool _sequence (quint32* rows=nullptr) {
        /**/ if (d>=R && (col!=bound || r->_sequence(rows))) { ++d; return true      ; }
        else if (d==T &&                r->_sequence(rows) ) { ++d; return true      ; }
        else                                                 {      return false     ; }
    }
    bool _record   (quint32* size=nullptr) {
        /**/ if (d>=R && (col!=bound || r->_record  (size))) { ++d; return true      ; }
        else if (d==S &&                r->_sequence(&max) ) { ++d; return __record(); }
        else                                                 {      return false     ; }
    }

    template<class T_> friend class Seq; // calls methods below
    template<class T_> friend class Rec; // calls methods below

    bool _out      (                     ) {
        /**/ if (       d> R  && (col!=bound || r->_out())) { --d; return true ; }
        else if ((d==S||d==R) &&                r->_out() ) { --d; return true ; }
        else                                                {      return false; }
    }
    bool _item     (                     ) {
        /**/ if (d> R && (col!=bound || r->_item())) { return true ; }
        else if (d==S &&                r->_item() ) { return true ; }
        else                                         { return false; }
    }
    bool _item     (        QByteArray& k) {
        bound = names.indexOf(k.constData()); // std::enable_if<IsWriter<TResult>>
        /**/ if (d> R && (col!=bound || r->_item(k))) { return true    ; }
        else if (d==R                               ) { return __item(); }
        else                                          { return false   ; }
    }

    // Handling of record items in or out of order using a cache
    bool __record() {
        col = -1;
        cache.clear();
        return true;
    }
    bool __item() {
        while (col+1<bound // previously cached items are ignored
               && cache.contains(col+1)) {
            col++;
            if (!(r->_item() && cache[col].bind(Val<TResult>(r->_unsafeCopy()))))
                return false;
        }
        if (col+1==bound) {
            col++;
            return r->_item();
        }
        else
            return true; /* ignored */
    }
    bool ___null() {
        if (col<bound) {
            cache.insert(bound,QWritable<TResult>(nullptr));
            return true;
        }
        else
            return col!=bound || r->_null();
    }
    template<typename T>
    bool __bind(T&& t) {
        if (col<bound) {
            cache.insert(bound,QWritable<TResult>(t));
            return true;
        }
        else
            return col!=bound || r->_bind(t);
    }
};
template<class TResult,typename T> struct BindSupport<QTableWriter<TResult>,T> : BindSupport<TResult,T> {};

template<class TResult_>
template<typename Ts>
TResult_ Val<TResult_>::table(Ts&& ts, const Names& cols) {
    Q_ASSERT(cols.size()>0);
    auto unsafeCopy(m_out._unsafeCopy());
    return QTableWriter<TResult_>(&unsafeCopy,cols).bind(ts);
}
template<class TResult_>
template<typename Ts>
TResult_ Val<TResult_>::table(Ts&& ts) {
    using T = typename std::remove_reference<Ts>::type::value_type;
    T t; QJsonValue record;
    QJsonBuilder(&record).bind(t); // TODO QCborBuilder to avoid char* > QString > toUtf8 conversions
    Names cols;
    Q_FOREACH(auto&& k, record.toObject().keys()) {
        cols.append(k.toUtf8());
    }
    return table(ts,cols);
}

// TODO template<class TImpl> class QTableReader

// //////////////////////////////////////////////////////////////////////////
// QBind<_,QTable<TSequence>> support allowing BindNative support for, say, TSV, databases, etc.

/// Binds generically any iterable TSequence ts with default-constructible TSequence::Item as:
/// sequence().bind(*ts++).bind(*ts++)...
///
/// \warning Requires binding to a dynamic map to associate record fields to table columns without reflection
/// Achieving good performance would require to define
///
template<typename Ts>
class QTable {
    // TODO use QBind<_,T> to map T fields to names?
public:
    QTable(Ts&& ts, const Names& cols={}) : m_ts(ts), m_cols(cols) {}

    template<class TResult, typename TEnabled = void> TResult bind(Val<TResult> value);
//private:
    Ts& m_ts;
    Names m_cols;
};
//template<typename Ts>
//template<class TResult, IsWriter<TResult>>
//TResult QTable<Ts>::bind(Val<TResult> value) {
//    auto s = value.sequence();
//    for (auto&& t : m_ts)
//         s = s.bind(t);
//    return s;
//}

// PRB
//template<class TResult, class Ts> // where Ts is a Container https://en.cppreference.com/w/cpp/named_req/Container
//struct QBind<TResult, QTable<Ts>&, IsWriter<TResult>> { static TResult bind(Val<TResult> dst, QTable<Ts>& src) {
//    using T = typename Ts::item_value;
//    T defaultT;
//    QCborMap defaultItem;
//    // TODO remove unused cols
//    auto s(dst.record().bind("cols",defaultItem).sequence("rows"));
//    for (auto&& t : src.m_ts) {
//        s = s.bind(t); // no added value compared to QVector !
//    }
//    return s;
//}};
template<class TResult, typename T> // where T is a Container and T::value_type is DefaultConstructible https://en.cppreference.com/w/cpp/types/is_default_constructible
struct QBind<TResult, QTable<T>&, IsReader<TResult>> { static TResult bind(Val<TResult> src, QTable<T>& dst) {
    auto s(src.sequence());
    Val<Seq<TResult>> i;
    while ((i = s.item())) {
        T t;
        if((s = i.bind(t))) // real value would be to translate from an optimized generic format (sequence() of values without keys) to the record() in QBind<_,T>
            dst.insert(t); // PRB: How to map to T
    }
    return s;
}};

/// Returns QTable<T>(t, cols) deducing T from t
template<typename T> QTable<T> table(T&& t, const Names& cols={})
{
    return QTable<T>(t, cols);
};

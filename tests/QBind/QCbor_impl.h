/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <QtCore/qiodevice.h>
#include <QtCore/qendian.h>

#include "QValue.h"

namespace cbor {

//! CBOR Major types
//! Encoded in the high 3 bits of the descriptor byte
//! \see http://tools.ietf.org/html/rfc7049#section-2.1
enum MajorType : char {
    UnsignedIntType     =  0,
    NegativeIntType     =  1,
    ByteStringType      =  2,
    TextStringType      =  3,
    ArrayType           =  4,
    MapType             =  5,
    TagType             =  6,
    SimpleValueType     =  7
};

//! CBOR simple and floating point types
//! Encoded in the low 5 bits of the descriptor byte when the Major Type is 7.
enum SimpleValue : char {
//  Unassigned          =  0-19
    False               = 20,
    True                = 21,
    Null                = 22,
    Undefined           = 23,
//  NextByteValue       = 24,
//  Next16BitFloat      = 25,
    Next32BitFloat      = 26,
    Next64BitFloat      = 27,
//  Unassigned          = 28-30
    Break               = 31
};

enum MajorValue : char {
//     SimpleUnsignedInt=  0-23
     NextByteUnsignedInt= 24,
    Next16BitUnsignedInt= 25,
    Next32BitUnsignedInt= 26,
    Next64BitUnsignedInt= 27,

    IndefiniteLength    = 31, // along with ArrayType or MapType
};

enum {
    Tag          = 192,
    DateTime     = 0,
    CborDocument = 55799,
    MimeMessage  = 36,
};

} // cbor

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier<T,QCbor*> support

class QCborWriter : public QAbstractValueWriter // TODO Support CBOR tags for QAbstractValue BindNative types and multi-dimensional meta() (e.g. support qmColumns with http://cbor.schmorp.de/stringref, qmSizes with https://datatracker.ietf.org/doc/draft-ietf-cbor-array-tags/?include_text=1 )
{
    Q_DISABLE_COPY(QCborWriter)
public:
   ~QCborWriter() { while (!levels.empty()) tryOut(); }
    QCborWriter(QByteArray* io) : io(io) { Q_ASSERT(io); }

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* rows=nullptr) { levels.push_back(rows);
                                              if (Q_LIKELY(rows)) { putInteger   (*rows                 , cbor::ArrayType); }
                                              else                { putMajorValue(cbor::IndefiniteLength, cbor::ArrayType); } return true; }
    bool tryRecord  (quint32* cols=nullptr) { levels.push_back(cols);
                                              if (Q_LIKELY(cols)) { putInteger   (*cols                 , cbor::MapType  ); }
                                              else                { putMajorValue(cbor::IndefiniteLength, cbor::MapType  ); } return true; }
    bool tryAny     (                     ) { putSimpleValue(cbor::Undefined); return true; }
    bool tryNull    (                     ) { putSimpleValue(cbor::Null     ); return true; }
    bool tryBind    (      QUtf8DataView u) { putInteger    (quint64(u.size()), cbor::TextStringType); io->append(u.data(), u.size()); return true; }
    bool tryBind    (          QString&& s) { QUtf8Data u(s.toUtf8());
                                              putInteger    (quint64(u.size()), cbor::TextStringType); io->append(u.utf8()); return true; }
                                              //static const char mimeHeader[] = "Content-Type:text/plain;charset=utf-16\xD\xA\xD\xA";
                                              //QByteArray utf16(QByteArray::fromRawData(reinterpret_cast<const char*>(s.utf16()), s.size()*int(sizeof(QChar))));
                                              //return          putMajorValue (cbor::NextByteUnsignedInt,cbor::TagType       ) && io->putChar(cbor::MimeMessage) &&
                                              //                putInteger    (sizeof(mimeHeader)+
                                              //                               utf16.size()          ,cbor::ByteStringType) && io->write(mimeHeader, sizeof(mimeHeader))
                                              //                                                                            && (Q_LIKELY(io->write(utf16)) || s[0]==QChar('\0')); }
    bool tryBind    (       QByteArray&& s) { putInteger    (quint64(s.size()), cbor::ByteStringType); io->append(s); return true; }
    bool tryBind    (             bool&& b) { putSimpleValue(b ? cbor::True : cbor::False);                           return true; }
    bool tryBind    (            float&& n) { putSimpleValue(cbor::Next32BitFloat); putBigEndian(n);                  return true; }
    bool tryBind    (           double&& n) { putSimpleValue(cbor::Next64BitFloat); putBigEndian(n);                  return true; }

    bool tryBind    (       quint64&& t) {            putInteger(quint64(   t), cbor::UnsignedIntType);               return true; }
    bool tryBind    (        qint64&& t) { if (t<0) { putInteger(quint64(-1-t), cbor::NegativeIntType); } // see https://tools.ietf.org/html/rfc7049#section-2.1
                                         else     { putInteger(quint64(   t), cbor::UnsignedIntType); }             return true; }

    bool tryItem(QIdentifier&       n) { putInteger(quint64(n.size()), cbor::TextStringType); io->append(n.utf8()          ); return true; }
    bool tryItem(QIdentifierLiteral n) { putInteger(quint64(n.size()), cbor::TextStringType); io->append(n.utf8(), n.size()); return true; }
    bool tryItem(                    ) { return true; }
    bool tryOut (                    ) { bool definite = levels.back(); levels.pop_back(); if (!definite) putSimpleValue(cbor::Break); return true; }
private:
    inline void putMajorValue (cbor:: MajorValue v, cbor::MajorType majorType) { io->append(v|char (            majorType << 5)); }
    inline void putSimpleValue(cbor::SimpleValue v                           ) { io->append(v|char (cbor::SimpleValueType << 5)); }
    inline void putInteger    (          quint64 n, cbor::MajorType majorType)
    {
        // See https://tools.ietf.org/html/rfc7049#section-2.2 if more performance is needed
        if (n <    24       ) { putMajorValue(cbor::MajorValue(n)       , majorType)                          ; } else
        if (n <= 0xff       ) { putMajorValue(cbor::NextByteUnsignedInt , majorType); putBigEndian(quint8 (n)); } else
        if (n <= 0xffff     ) { putMajorValue(cbor::Next16BitUnsignedInt, majorType); putBigEndian(quint16(n)); } else
        if (n <= 0xffffffffL) { putMajorValue(cbor::Next32BitUnsignedInt, majorType); putBigEndian(quint32(n)); } else
                              { putMajorValue(cbor::Next64BitUnsignedInt, majorType); putBigEndian(quint64(n)); }
    }
    template<typename T> inline void putBigEndian(T&& t) {
#if Q_BYTE_ORDER == Q_BIG_ENDIAN
        io->append(reinterpret_cast<char *>(&t), sizeof(t));
#else
        using TValue = typename std::remove_reference<T>::type;
        if (sizeof(TValue)==1) {                                                                          io->append(                               char(t     )); } else
        if (sizeof(TValue)==2) { union { TValue t; quint16 bytes; } u = { t }; u.bytes = qbswap(u.bytes); io->append(reinterpret_cast<char *>(&u),sizeof(TValue)); } else
        if (sizeof(TValue)==4) { union { TValue t; quint32 bytes; } u = { t }; u.bytes = qbswap(u.bytes); io->append(reinterpret_cast<char *>(&u),sizeof(TValue)); } else
        if (sizeof(TValue)==8) { union { TValue t; quint64 bytes; } u = { t }; u.bytes = qbswap(u.bytes); io->append(reinterpret_cast<char *>(&u),sizeof(TValue)); }
#endif
    }

    QByteArray* io;
    std::vector<bool> levels; //!< minimal dynamic context to ensure well-formedness in case QCur is abandoned: true indicates Definite levels with prefixed size
};

// --------------------------------------------------------------------------

#include <QtCore/qcborvalue.h>
#include <QtCore/qcborarray.h>
#include <QtCore/qcbormap.h>
#include <QtCore/qstack.h>

class QCborBuilder : public QAbstractValueWriter
{
    Q_DISABLE_COPY(QCborBuilder)
public:
    QCborBuilder(QCborValue* v) : cbor(v) { Q_ASSERT(v); }

    // Shortcuts
    /**/                 QValue       value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence    sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QCur bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* =nullptr) { levels.push(Step(                   )); return true; }
    bool tryRecord  (quint32* =nullptr) { levels.push(Step(qBindExpectedItem  )); return true; }
    bool tryAny     (                 ) { set(QCborValue(QCborValue::Undefined)); return true; }
    bool tryNull    (                 ) { set(QCborValue(nullptr              )); return true; }

    bool tryBind    (  QUtf8DataView u) { set(QCborValue(QString::fromUtf8(u.data()))); return true; }

    bool tryBind    (         bool&& b) { set(QCborValue(       b )); return true; }
    bool tryBind    (        float&& d) { set(QCborValue(double(d))); return true; }
    bool tryBind    (       double&& d) { set(QCborValue(       d )); return true; }
    bool tryBind    (      quint64&& n) { if (quint64(std::numeric_limits<qint64>::max())<n) { if (isErrorFiltered(qBindExpectedSmallerInteger)) { set(QCborValue(std::numeric_limits<qint64>::max())); return true; } return false; }
                                          set(QCborValue(qint64(n))); return true; }
    bool tryBind    (       qint64&& n) { set(QCborValue(       n )); return true; }
    bool tryBind    (   QByteArray&& s) { set(QCborValue(       s )); return true; }

    bool tryItem(QIdentifier& n) { levels.last().key=n            ; return true; }
    bool tryItem(              ) { levels.last().key=QIdentifier(); return true; }
    bool tryOut (              ) { auto level = levels.pop(); set(!level.key.isNull() ? QCborValue(level.object) : QCborValue(level.array)); return true; }
private:
    void set(const QCborValue& v) {
        if (levels.isEmpty()) {
            *cbor = v;
        }
        else {
            if (!levels.last().key.isNull())
                levels.last().object[levels.last().key.latin1()]=v;
            else
                levels.last().array.append(v);
        }
    }

    QCborValue* cbor;
    struct Step { QIdentifier key; /* TODO union */ QCborMap object; QCborArray array; Step(QIdentifier k=QIdentifier()) : key(k) {} };
    QStack<Step> levels = QStack<Step>(); //!< minimal dynamic context to implement out() and ensure actual building in case QCborBuilder is abandoned
};

// --------------------------------------------------------------------------

class QCborVisitor : public QAbstractValueReader
{
    Q_DISABLE_COPY(QCborVisitor)
public:
    QCborVisitor(QCborValue* v) : cbor(v) { Q_ASSERT(v); }
    void reset(QCborValue* v) { cbor=v; Q_ASSERT(v); steps.resize(0); }

    QVariant valuePath() const {
        QByteArray path;
        Q_FOREACH(Step s, steps) {
            if (!s.key.isNull()) { path.append('{').append(                   s.key.utf8() ); }
            else                 { path.append('[').append(QByteArray::number(s.idx       )); }
        }
        return path;
    }

    // Shortcuts
    /**/                 QValue       value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence    sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QCur bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* =nullptr) {            if (current().isArray    ()) {          steps.push(Step()); return true; } return false; }
    bool tryRecord  (quint32* =nullptr) {            if (current().isMap      ()) {          steps.push(Step()); return true; } return false; }
    bool tryNull    (                 ) {            if (current().isNull     ()) {                              return true; } return false; }
    bool tryBind    (     QUtf8Data& u) { QString s; if (tryBind(s)             ) { u = s        .toUtf8     (); return true; } return false; }
    bool tryBind    (       QString& v) {            if (current().isString   ()) { v = current().toString   (); return true; } return false; }
    bool tryBind    (          bool& v) {            if (current().isBool     ()) { v = current().toBool     (); return true; } return false; }
    bool tryBind    (        qint64& t) {            if (current().isInteger  ()) { t = current().toInteger  (); return true; } return false; }
    bool tryBind    (       quint64& t) {  qint64 i; if (tryBind(i)             ) { t = quint64(i)             ; return true; } return false; }
    bool tryBind    (         float& v) {  double d; if (tryBind(d)             ) { v = float(d)               ; return true; } return false; }
    bool tryBind    (        double& v) {            if (current().isDouble   ()) { v = current().toDouble   (); return true; } return false; }
    bool tryBind    (    QByteArray& v) { QString s; if (current().isByteArray()) { v = current().toByteArray(); return true; } return false; }

    bool tryItem(QIdentifierLiteral u) { steps.last().key=u; return !(steps.last().item = current(1).toMap().value(steps.last().key.latin1())).isUndefined(); }
    bool tryItem(QIdentifier&       k) { steps.last().key=k; return !(steps.last().item = current(1).toMap().value(steps.last().key.latin1())).isUndefined(); }
    bool tryItem(                    ) { steps.last().idx++; return !(steps.last().item = current(1).toArray(). at(steps.last().idx         )).isUndefined(); }
    bool tryOut (                    ) { steps.pop()       ; return true; }

    bool tryAny() { return true; }
private:
    const QCborValue& current(int outer=0) const { Q_ASSERT(0<=outer); return steps.size()-outer <= 0 ? *cbor : steps[steps.size()-outer-1].item; }

    QCborValue* cbor;
    struct Step { QIdentifier key; int idx=-1; QCborValue item; Step() = default; };
    QStack<Step> steps = QStack<Step>();
};

// --------------------------------------------------------------------------

#include <QtCore/qcborstream.h>
#include <QtCore/qpair.h>

class QCborReader : public QAbstractValueReader, public QCborStreamReader
{
    Q_DISABLE_COPY(QCborReader)
public:
    QCborReader(QIODevice* io) : QCborStreamReader(io), cacheVisitor(&cachedValue) { Q_ASSERT(io); }

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* s=nullptr) { if (caching) { cacheLevel++; return caching->trySequence(s); }
                                           skipTag(); if (isArray () && enterContainer()) { levels.push(Level()); return true; } return false; }
    bool tryRecord  (quint32* s=nullptr) { if (caching) { cacheLevel++; return caching->tryRecord(s); }
                                           skipTag(); if (isMap   () && enterContainer()) { levels.push(Level()); return true; } return false; }
    bool tryAny     (                  ) { if (caching) { return caching->tryAny() && cacheOut(); }
                                           skipTag(); return next(); }
    bool tryNull    (                  ) { if (caching) { return caching->tryNull() && cacheOut(); }
                                           skipTag(); return isNull() && next(); }
    bool tryBind    (     QUtf8Data& u) { QString s; if (tryBind(s)) { u = s.toUtf8(); return true; } return false; }
    bool tryBind    (       QString& s) { if (caching) { return caching->tryBind(s) && cacheOut(); }
                                          skipTag(); if (isString()) {
                                                         s.resize(0);
                                                         auto r = readString();
                                                         while (r.status == QCborStreamReader::Ok) {
                                                             s.append(r.data);
                                                             r = readString();
                                                         }
                                                         return r.status == QCborStreamReader::EndOfString || isErrorFiltered(qBindUnexpectedData);
                                                     } else { return false; } }
    bool tryBind    (    QByteArray& s) { if (caching) { return caching->tryBind(s) && cacheOut(); }
                                          skipTag(); if (isByteArray()) {
                                                         s.resize(0);
                                                         auto r = readByteArray();
                                                         while (r.status == QCborStreamReader::Ok) {
                                                             s.append(r.data);
                                                             r = readByteArray();
                                                         }
                                                         return r.status == QCborStreamReader::EndOfString || isErrorFiltered(qBindUnexpectedData);
                                                     } else { return false; } }
    bool tryBind    (          bool& b) { if (caching) { return caching->tryBind(b) && cacheOut(); }
                                          skipTag(); if (isBool()) {
                                                         b=toBool();
                                                         return next();
                                                     } else { return false; } }
    bool tryBind    (        double& n) { if (caching) { return caching->tryBind(n) && cacheOut(); }
                                          skipTag();
                                          if (isFloat16()) { n = double(toFloat16()); return next(); }
                                          if (isFloat  ()) { n = double(toFloat  ()); return next(); }
                                          if (isDouble ()) { n =        toDouble () ; return next(); }
                                          return false; }
    bool tryBind    (         float& n) { if (caching) { return caching->tryBind(n) && cacheOut(); }
                                          skipTag();
                                          if (isFloat16()) { n = float(toFloat16()); return next(); }
                                          if (isFloat  ()) { n =       toFloat  () ; return next(); }
                                          if (isDouble ()) {
                                              double d = toDouble();
                                              float rounded = d; // rounds to nearest value (inf if qAbs(d)>std::numeric_limits<float>::max()
                                              if (rounded != d && !isErrorFiltered(qBindExpectedLessPreciseDecimal)) {
                                                  return false;
                                              }
                                              n = rounded;
                                              return next(); }
                                          return false; }
    bool tryBind    (       quint64& t) { if (caching) { return caching->tryBind(t) && cacheOut(); }
                                          skipTag();
                                          if (!isUnsignedInteger()) { return false; }
                                          t=toUnsignedInteger(); return next(); }
    bool tryBind    (        qint64& t) { if (caching) { return caching->tryBind(t) && cacheOut(); }
                                          skipTag();
                                          if (isNegativeInteger()) { quint64 i = quint64(toNegativeInteger()); if (i<=quint64(std::numeric_limits<qint64>::max())+1) { t = -qint64(i); return next(); } else if (isErrorFiltered(qBindExpectedSmallerInteger)) { t=std::numeric_limits<qint64>::min(); return next(); } }
                                          if (isUnsignedInteger()) { quint64 i =         toUnsignedInteger() ; if (i<=quint64(std::numeric_limits<qint64>::max())  ) { t =  qint64(i); return next(); } else if (isErrorFiltered(qBindExpectedSmallerInteger)) { t=std::numeric_limits<qint64>::max(); return next(); } }
                                          return false; }

    bool tryItem(       QIdentifierLiteral u) { if (caching) { return caching->tryItem(u); }
                                 QIdentifier s;
                                 while (true) {
                                     if (levels.last().cachedItems.          contains(QIdentifier(u))) { // must be checked before we consume tryItem(s)
                                         cachedValue = levels.last().cachedItems.take(QIdentifier(u));
                                         Q_ASSERT(!cacheLevel);
                                         if (!device()->isSequential()) {
                                             cachingAfter = device()->pos();
                                             device()->seek(cachedValue.toInteger());
                                             if (!cacheReader) {
                                                 cacheReader.reset(new QCborReader(device()));
                                             }
                                             else {
                                                cacheReader->setDevice(device());
                                             }
                                             caching = cacheReader.get(); // let outer QCur drive QCborReader depending on bound T
                                         }
                                         else {
                                             cacheVisitor.reset(&cachedValue);
                                             caching = &cacheVisitor; // let outer QCur drive QCborReader depending on bound T
                                         }
                                         return true;
                                     }
                                     else if (!tryItem(s)) { // record() end reached
                                         return false;
                                     }
                                     else if (u  ==  s ) {
                                         return true ;
                                     }
                                     else {
                                         if (!device()->isSequential()) {
                                             levels.last().cachedItems.insert(s, QCborValue(device()->pos()));
                                             next();
                                         }
                                         else {
                                             levels.last().cachedItems.insert(s, QCborValue::fromCbor(*this));
                                         }
                                         continue; // until !tryItem(s) or u==s
                                     }
                                 }}
    bool tryItem(QIdentifier& k) { if (caching) { return caching->tryItem(k); }
                                 if (!hasNext()) {
                                    return false;
                                 }
                                 QString s;
                                 if (!tryBind(s) || s.isEmpty()) {
                                     return false;
                                 }
                                 k = QIdentifier(s.toLatin1());
                                 return true; }
    bool tryItem(              ) { if (caching) { return caching->tryItem(); }
                                 if (!hasNext()) {
                                     return false;
                                 }
                                 return true; }

    bool trySequenceOut() { if (caching) { bool out = caching->trySequenceOut(); if (out) { --cacheLevel; cacheOut(); } return out; }
                            levels.pop();
                            while (hasNext()) { tryAny(); }
                            return leaveContainer(); }
    bool tryOut        () { if (caching) { bool out = caching->tryRecordOut  (); if (out) { --cacheLevel; cacheOut(); } return out; }
                            levels.pop();
                            while (hasNext()) { tryAny(); }
                            return leaveContainer(); }

    QVariant valuePath() const { return currentOffset(); }
private:
    void skipTag() { if (isTag()) next(); }
    bool cacheOut() { if (!cacheLevel) {
                          caching = nullptr;
                          if (!device()->isSequential()) {
                              device()->seek(cachingAfter);
                          }
                      }
                      return true; }

    // Read/Write caching of out-of-order item keys
    struct Level { QMap<QIdentifier,QCborValue> cachedItems; };
    QStack<Level> levels = QStack<Level>(); //!< dynamic context required to cache out-of-order item(QIdentifierLiteral)
    quint8        cacheLevel  = 0;
    QCborValue    cachedValue    ; //!< Only used AFTER --cacheLevel==1
    qint64        cachingAfter= 0; //!< Only used AFTER --cacheLevel==1
    QAbstractValue*        caching = nullptr; //!< Only used when cacheLevel>=1 && key!=expectedKey
    QCborVisitor  cacheVisitor; //!< Only used as caching
    QScopedPointer<QCborReader> cacheReader; //!< Only used as caching
};

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier<QCbor*,_> support

#include <QtCore/qjsonvalue.h>
#include <QtCore/qjsonarray.h>
#include <QtCore/qjsonobject.h>

template<>
struct QTransmogrifier<QCborValue> {
    static QValueEnd zap(QValue&& v, QCborValue&& j) {
        if (v->mode()==Write) {
            if (j.isMap      ()) return v.bind(j.toMap      ());
            if (j.isArray    ()) return v.bind(j.toArray    ());
            if (j.isBool     ()) return v.bind(j.toBool     ());
            if (j.isInteger  ()) return v.bind(j.toInteger  ());
            if (j.isDouble   ()) return v.bind(j.toDouble   ());
            if (j.isString   ()) return v.bind(j.toString   ());
            if (j.isByteArray()) return v.bind(j.toByteArray());
            if (j.isNull     ()) return v.null();
            return QValueEnd();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QCborValue& j) {
        if (v->mode()==Write) {
            return zap(std::move(v),std::move(j));
        }
        else if (v->mode()==Read) {
            QValueEnd r;
            v.setIsVariant();
            QCborArray  a; if ((r = v.bind(a))) { j =                   a  ; return r; }
            QCborMap    o; if ((r = v.bind(o))) { j =                   o  ; return r; }
            QString     t; if ((r = v.bind(t))) { j = QCborValue(       t ); return r; }
            bool        b; if ((r = v.bind(b))) { j = QCborValue(       b ); return r; }
            qint64      i; if ((r = v.bind(i))) { j = QCborValue(       i ); return r; }
            quint64     u; if ((r = v.bind(u))) { Q_ASSERT(quint64(std::numeric_limits<qint64>::max())<u); // or tryBind<qint64>() would have succeeded
                                                  j = QCborValue(double(u)); return r; }
            double      d; if ((r = v.bind(d))) { j = QCborValue(       d ); return r; }
            QByteArray  y; if ((r = v.bind(y))) { j = QCborValue(       y ); return r; }
            /**/           if ((r = v.null( ))) { j = QCborValue( nullptr ); return r; }
            return r;
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

template<>
struct QTransmogrifier<QCborArray> {
    static QValueEnd zap(QValue&& v, QCborArray&& j) {
        if (v->mode()==Write) {
            quint32 size=quint32(j.size());
            auto s(v.sequence(&size));
            for (QCborValue item : j) {
                s = s.bind(item);
            }
            return s.out();
        }
        else { Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QCborArray& j) {
        if (v->mode()==Write) {
            return zap(std::move(v),std::move(j));
        }
        else if (v->mode()==Read) {
            auto s(v.sequence());
            QVal<QSequence> i;
            while ((i = s.item())) {
                QCborValue json;
                if ((s = i.bind(json)))
                    j.append(json);
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

template<>
struct QTransmogrifier<QCborMap> {
    static QValueEnd zap(QValue&& v, QCborMap&& j) {
        if (v->mode()==Write) {
            quint32 size=quint32(j.size());
            auto r(v.record(&size));
            for (QCborValue key : j.keys()) {
                auto k = key.toString();
                if (!k.isEmpty()) {
                    auto id = QIdentifier(k.toLatin1());
                    r = r.item(id).bind(QCborValue(j[key]));
                }
            }
            return r.out();
        }
        else { Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QCborMap& j) {
        if (v->mode()==Write) {
            return zap(std::move(v),std::move(j));
        }
        else if (v->mode()==Read) {
            auto s(v.record());
            QIdentifier k; QVal<QRecord> i;
            while ((i = s.item(k))) {
                QCborValue json;
                if ((s = i.bind(json)))
                    j.insert(k.latin1(),json);
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

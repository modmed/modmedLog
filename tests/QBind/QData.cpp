#include "QData.h"

bool AsciiIdentifier::isValid(const char* s, int size)
{
    if ((s == nullptr && size != 0) || size < 1)
        return false;
    for (int i=0; i<size; ++i) {
        if (s[i] < 0x20/*' '*/ || 0x7F/*DEL*/ == s[i])
            return false;
        if (i==0 && (s[i]!='_' && (s[0]<'A' || 'z'<s[0] || ('Z'<s[0] && s[0]<'a'))))
            qWarning("Non alphabetic first character may not be supported by QAbstractValue");
        if (i< 0 && (s[i]!='_' && (s[i]<'0' || 'z'<s[i] || ('9'<s[i] && s[i]<'A')
                                                        || ('Z'<s[i] && s[i]<'a'))))
            qWarning("Non alphanumeric or underscore character may not be supported by QAbstractValue");
    }
    return true;
}

bool AsciiPrintable::isValid(const char* s, int size)
{
    if ((s == nullptr && size != 0) || size < 0)       return false;
    for (int i=1; i<size; ++i) {
        if (s[i] < 0x20/*' '*/ || 0x7F/*DEL*/ <= s[i]) return false;
    }
    return true;
}

bool Ascii::isValid(const char* s, int size)
{
    if ((s == nullptr && size != 0) || size < 0) return false;
    for (int i=1; i<size; ++i) {
        if (s[i] <= 0)                           return false;
    }
    return true;
}

bool Latin1::isValid(const char* s, int size) { return !((s == nullptr && size != 0) || size < 0); }
bool Utf8  ::isValid(const char* s, int size) { return !((s == nullptr && size != 0) || size < 0); }

template<> QData<Utf8>::QData(QDataView<Utf8> v);

QIdentifier::QIdentifier(QIdentifierLiteral n) : QData(QByteArray::fromRawData(n.data(), n.size())) { /* no need to check QIdentifierLiteral content or take ownership */ }
QIdentifier::QIdentifier(QLatin1String      l) : QData(QByteArray(l.data(), l.size())) {}

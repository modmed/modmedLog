#pragma once

#include <QtCore/qbytearray.h>
#include <QtCore/qdatastream.h>

// Content types

//! Non-empty string of [0x20..0x7E] ASCII subset with a preference for [A..Za..z_][0..9A..Za..z_]* identifiers
struct AsciiIdentifier { static bool isValid(const char* s, int size); };
struct AsciiPrintable  { static bool isValid(const char* s, int size); };
struct Ascii           { static bool isValid(const char* s, int size); };
struct Latin1          { static bool isValid(const char* s, int size); };
struct Utf8            { static bool isValid(const char* s, int size); };

template<class TContent> class QDataView;
// ...
//! Associates a QByteArray with TContent type to allow static checks and optimizations when data is transferred
template<class TContent> class QData
{
public:
    using Content = TContent;

    explicit QData(      QByteArray&& b) : m_bytes(b) { Q_ASSERT(Content::isValid(m_bytes.data(), m_bytes.size())); }
    explicit QData(const QByteArray&  b) : m_bytes(b) { Q_ASSERT(Content::isValid(m_bytes.data(), m_bytes.size())); }

    QData(QDataView<TContent> v);

    QData() : m_bytes() {}

    constexpr bool      isNull() const noexcept { return m_bytes.isNull (); }
    constexpr bool     isEmpty() const noexcept { return m_bytes.isEmpty(); }
    constexpr int         size() const noexcept { return m_bytes.size   (); }
    constexpr const char* data() const noexcept { return m_bytes.data   (); }

    QData& operator=(      QByteArray&& b) noexcept { m_bytes=b; Q_ASSERT(Content::isValid(m_bytes.data(), m_bytes.size())); return *this; }
    QData& operator=(const QByteArray&  b) noexcept { m_bytes=b; Q_ASSERT(Content::isValid(m_bytes.data(), m_bytes.size())); return *this; }
protected:
    template<class T> friend bool operator==(const QData    <T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator!=(const QData    <T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator< (const QData    <T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator<=(const QData    <T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator> (const QData    <T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator>=(const QData    <T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator==(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator!=(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator< (const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator<=(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator> (const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator>=(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator==(const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator!=(const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator< (const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator<=(const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator> (const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator>=(const QDataView<T> &a, const QData    <T> &b) noexcept;

    friend QDataStream &operator<<(QDataStream &out, const QData<TContent> &p) { return out << p.m_bytes; }
    friend QDataStream &operator>>(QDataStream &out,       QData<TContent> &p) { return out >> p.m_bytes; }

    QByteArray m_bytes;
};

#include <QtCore/qstring.h>

#ifdef NO_DEBUG
#define CONSTEXPR_IF_NO_DEBUG constexpr
#else
#define CONSTEXPR_IF_NO_DEBUG
#endif

#ifdef NO_DEBUG
#define DEBUG_CHECK(cond)
#else
#define DEBUG_CHECK(cond) Q_ASSERT(cond)
#endif

template<class TContent> class QDataView
{
public:
    using Content = TContent;

    CONSTEXPR_IF_NO_DEBUG explicit QDataView(const char* s                 ) : m_bytes(s, s ? int(qstrlen(s)) : 0) { DEBUG_CHECK(Content::isValid(m_bytes.data(), m_bytes.size())); }
    template<int Size>
    CONSTEXPR_IF_NO_DEBUG explicit QDataView(const char(&s)[Size]          ) : m_bytes(s, Size-int(sizeof('\0')) ) { DEBUG_CHECK(Content::isValid(m_bytes.data(), m_bytes.size())); }
    CONSTEXPR_IF_NO_DEBUG explicit QDataView(const char* s, int size       ) : m_bytes(s, s+size                 ) { DEBUG_CHECK(Content::isValid(m_bytes.data(), m_bytes.size())); }
    CONSTEXPR_IF_NO_DEBUG explicit QDataView(const char* s, const char* end) : m_bytes(s, end                    ) { DEBUG_CHECK(Content::isValid(m_bytes.data(), m_bytes.size())); }

    explicit QDataView(      QByteArray&& s) : m_bytes(s.constData(), s.size()) { DEBUG_CHECK(Content::isValid(m_bytes.data(), m_bytes.size())); }
    explicit QDataView(const QByteArray&  s) : m_bytes(s.constData(), s.size()) { DEBUG_CHECK(Content::isValid(m_bytes.data(), m_bytes.size())); }

    QDataView(QData<Content>&& d) : m_bytes(d.data(), d.size()) {}
    QDataView(QData<Content>&  d) : m_bytes(d.data(), d.size()) {}

    constexpr QDataView() : m_bytes() {}

    constexpr bool   isNull() const noexcept { return m_bytes.isNull (); }
    constexpr bool  isEmpty() const noexcept { return m_bytes.isEmpty(); }
    constexpr int      size() const noexcept { return m_bytes.size   (); }

    constexpr const char*    data() const noexcept { return m_bytes.data(); }
    inline QData<Content> content() const noexcept { return *this; }
protected:
    template<class T> friend bool operator==(const QDataView<T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator!=(const QDataView<T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator< (const QDataView<T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator<=(const QDataView<T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator> (const QDataView<T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator>=(const QDataView<T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator==(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator!=(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator< (const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator<=(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator> (const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator>=(const QData    <T> &a, const QDataView<T> &b) noexcept;
    template<class T> friend bool operator==(const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator!=(const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator< (const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator<=(const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator> (const QDataView<T> &a, const QData    <T> &b) noexcept;
    template<class T> friend bool operator>=(const QDataView<T> &a, const QData    <T> &b) noexcept;

    QLatin1String m_bytes;
};

template<class T> QData<T>::QData(QDataView<T> v) : m_bytes(v.data(), v.size()) {}

template<class T> inline bool operator==(const QDataView<T> &a, const QDataView<T> &b) noexcept { return (a.m_bytes.size() == b.m_bytes.size()) && (memcmp(a.m_bytes.data(), b.m_bytes.data(), a.m_bytes.size())==0); }
template<class T> inline bool operator!=(const QDataView<T> &a, const QDataView<T> &b) noexcept { return !(a==b); }
template<class T> inline bool operator< (const QDataView<T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <  0; }
template<class T> inline bool operator<=(const QDataView<T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <= 0; }
template<class T> inline bool operator> (const QDataView<T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >  0; }
template<class T> inline bool operator>=(const QDataView<T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >= 0; }
template<class T> inline bool operator==(const QData    <T> &a, const QDataView<T> &b) noexcept { return (a.size() == b.m_bytes.size()) && (memcmp(a.data(), b.m_bytes.data(), a.size())==0); }
template<class T> inline bool operator!=(const QData    <T> &a, const QDataView<T> &b) noexcept { return !(a==b); }
template<class T> inline bool operator< (const QData    <T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <  0; }
template<class T> inline bool operator<=(const QData    <T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <= 0; }
template<class T> inline bool operator> (const QData    <T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >  0; }
template<class T> inline bool operator>=(const QData    <T> &a, const QDataView<T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >= 0; }
template<class T> inline bool operator==(const QDataView<T> &a, const QData    <T> &b) noexcept { return (a.m_bytes.size() == b.size()) && (memcmp(a.m_bytes.data(), b.data(), a.m_bytes.size())==0); }
template<class T> inline bool operator!=(const QDataView<T> &a, const QData    <T> &b) noexcept { return !(a==b); }
template<class T> inline bool operator< (const QDataView<T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <  0; }
template<class T> inline bool operator<=(const QDataView<T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <= 0; }
template<class T> inline bool operator> (const QDataView<T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >  0; }
template<class T> inline bool operator>=(const QDataView<T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >= 0; }
template<class T> inline bool operator==(const QData    <T> &a, const QData    <T> &b) noexcept { return (a.m_bytes.size() == b.m_bytes.size()) && (memcmp(a.m_bytes.data(), b.m_bytes.data(), a.m_bytes.size())==0); }
template<class T> inline bool operator!=(const QData    <T> &a, const QData    <T> &b) noexcept { return !(a==b); }
template<class T> inline bool operator< (const QData    <T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <  0; }
template<class T> inline bool operator<=(const QData    <T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) <= 0; }
template<class T> inline bool operator> (const QData    <T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >  0; }
template<class T> inline bool operator>=(const QData    <T> &a, const QData    <T> &b) noexcept { return qstrcmp(a.m_bytes, b.m_bytes) >= 0; }

// //////////////////////////////////////////////////////////////////////////
// Specialized QData

#include <QtCore/qmetatype.h>

//! Explicitely utf8 encoded string of char
class QUtf8Data : public QData<Utf8>
{
public:
    using QData::QData;
    using QData::operator=;

    const QByteArray utf8() const noexcept { return m_bytes; }
protected:
    friend QDataStream &operator<<(QDataStream &out, const QUtf8Data &p) { return out << QString(p.m_bytes); }
    friend QDataStream &operator>>(QDataStream &out,       QUtf8Data &p) { QString s; out >> s; p.m_bytes = s.toUtf8(); return out; }
};
Q_DECLARE_METATYPE(QUtf8Data)

using QUtf8DataView = QDataView<Utf8>;

class QAsciiData : public QData<Ascii>
{
public:
    using QData::QData;

    const QByteArray      utf8() const noexcept { return m_bytes; }
    const QLatin1String latin1() const noexcept { return QLatin1String(m_bytes.data(), m_bytes.size()); }
};
Q_DECLARE_METATYPE(QAsciiData)

using QAsciiDataView = QDataView<Ascii>;

class QIdentifierLiteral;

//! A string using a highly-interoperable, printable, subset of ASCII that is
//! compatible with utf8, latin1 and other ANSI codepages without transformation
//! \warning This implementation only enforces input ASCII subset if DEBUG is defined
class QIdentifier : public QData<AsciiIdentifier>
{
public:
    using QData::QData;

    QIdentifier() : QData() {}
    explicit QIdentifier(QIdentifierLiteral);
    explicit QIdentifier(QLatin1String     );
    explicit QIdentifier(      QByteArray&& b) : QData(b) {}
    explicit QIdentifier(const QByteArray&  b) : QData(b) {}

    const QByteArray&     utf8() const noexcept { return m_bytes; }
    const QLatin1String latin1() const noexcept { return QLatin1String(m_bytes.data(), m_bytes.size()); }
};
Q_DECLARE_METATYPE(QIdentifier)

//! Read-only view of a null or non-empty string literal using a highly-interoperable, printable, subset of ASCII that is
//! compatible with utf8, latin1 and other ANSI codepages without transformation
//! \warning This implementation only enforces input ASCII subset if DEBUG is defined
class QIdentifierLiteral : public QDataView<AsciiIdentifier>
{
public:
    using QDataView::QDataView;

    constexpr inline const char*     utf8() const noexcept { return m_bytes.data(); }
    constexpr inline QLatin1String latin1() const noexcept { return m_bytes; }
};

inline QIdentifierLiteral operator""_idv(const char* s, size_t sz) { return QIdentifierLiteral(s, int(sz)); }

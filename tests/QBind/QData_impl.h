/****************************************************************************
 * **
 * ** Copyright (C) 2018 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <QtCore/qdatastream.h>
#include <QtCore/qvariant.h>

#include "QValue.h"

// //////////////////////////////////////////////////////////////////////////
// QDataStream* support

// TODO Use QByteArray directly

//! \warning As with QDataStream, QDataWriter and QDataReader must bind compatible C++ data types, and QDataStream ByteOrder, FloatingPointPrecision and Version
//! \remark When not statically known, such information can be transmitted using meta("type",...) although some QAbstractValue implementations may not support it
class QDataWriter : public QAbstractValueWriter // allows runtime flexibility but requires QTransmogrifier<T> in addition to T::operator<<(QDataStream&) and does not warrant QDataStream operators compatibility if QTransmogrifier<T> ignores qmDataStreamVersion in meta()
{
    Q_DISABLE_COPY(QDataWriter)
public:
    QDataWriter(QDataStream* io) : io(io) { Q_ASSERT(io); version = QAsciiData(QByteArray::number(io->version())); }

    QValueMode mode() const noexcept { return QValueMode::Write; }

    // Shortcuts
    QValue    value   (                  ) { return QCur(this).value(); }
    QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }

    template<typename T> QValueEnd zap(T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    friend class QCur;
    bool trySequence(quint32* s=nullptr) { if (s) *io <<       *s; return io->status()==QDataStream::Ok; }
    bool tryRecord  (quint32* s=nullptr) { if (s) *io <<       *s; return io->status()==QDataStream::Ok; }
    bool tryNull    (                  ) {        *io <<  nullptr; return io->status()==QDataStream::Ok; }
    bool tryBind    (   QUtf8DataView u) { QByteArray ba = QByteArray::fromRawData(u.data(), u.size()+int(sizeof('\0'))); return static_cast<QAbstractValue*>(this)->tryBind(ba); }

    bool tryBind    (     QUtf8Data&& t) {        *io << t.utf8(); return io->status()==QDataStream::Ok; }
    bool tryBind    (       QString&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (          bool&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (         qint8&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (        quint8&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (        qint16&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (       quint16&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (        qint32&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (       quint32&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (        qint64&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (       quint64&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (         float&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (        double&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (    QByteArray&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }
    bool tryBind    (      QVariant&& t) {        *io <<        t; return io->status()==QDataStream::Ok; }

    bool tryOut (                  ) { return true; }
    bool tryItem(                  ) { return true; }
    bool tryItem(QIdentifier&      ) { return true; } // record keys are implicit, maps should be serialized as a sequence of records with key and value items
    bool tryItem(QIdentifierLiteral) { return true; } // for QBaseWriter

    void _meta(QIdentifierLiteral& n, QAsciiData& m) { if (n.isNull()) n=qmDataStreamVersion; if (n == qmDataStreamVersion) m=version; }
private:
    QDataStream* io;
    QAsciiData version;
};

// --------------------------------------------------------------------------

// TODO QDataReader
// \remark Providing a general QDataReader may be deceiving because the QDataStream structure is implicit, thus
// a reader is usually assumed to statically know the data type to read based on the stream origin and version.
// Though QVariant can be used for parts which type are not statically known though, such as the dynamic type of a message coming from a socket.
// Finally, QDataStream version can be retrieved from meta() to adapt to data schema changes over the time \see QTransmogrifier<QColor> for an example

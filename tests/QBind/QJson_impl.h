/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <QtCore/qjsonvalue.h>
#include <QtCore/qjsonobject.h>
#include <QtCore/qjsonarray.h>
#include <QtCore/qiodevice.h>
#include <QtCore/qstack.h>

#include "QValue.h"

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier<T,QJson*> support

class QJsonBuilder : public QAbstractValueWriter
{
    Q_DISABLE_COPY(QJsonBuilder)
public:
    QJsonBuilder(QJsonValue* v) : json(v) { Q_ASSERT(v); }
    void reset(QJsonValue* v) { json=v; Q_ASSERT(v); steps.resize(0); }

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    friend class QJsonReader; //!< Calls methods below for out-of-order cachedItems
    bool trySequence(quint32* s=nullptr) { Q_UNUSED(s); steps.push(Step(                 )); return true; }
    bool tryRecord  (quint32* s=nullptr) { Q_UNUSED(s); steps.push(Step(qBindExpectedItem)); return true; }
    bool tryAny     (                  ) { set(QJsonValue(QJsonValue::Undefined)); return true; }
    bool tryNull    (                  ) { set(QJsonValue(QJsonValue::Null     )); return true; }
    bool tryBind    (   QUtf8DataView u) { set(QJsonValue(u.data())); return true; }
    bool tryBind    (          bool&& b) { set(QJsonValue(b       )); return true; }
    bool tryBind    (        double&& d) { set(QJsonValue(d       )); return true; }
    bool tryBind    (       quint64&& n) { return tryBind(double(n)); }
    bool tryBind    (        qint64&& n) { return tryBind(double(n)); }

    bool tryItem(QIdentifier& n) { steps.last().key=n            ; return true; }
    bool tryItem(              ) { steps.last().key=QIdentifier(); return true; }
    bool tryOut (              ) { auto level = steps.pop(); set(!level.key.isNull() ? QJsonValue(level.object) : QJsonValue(level.array)); return true; }
private:
    void set(const QJsonValue& v) {
        if (steps.isEmpty()) {
            *json = v;
        }
        else {
            if (!steps.last().key.isNull())
                steps.last().object[steps.last().key.latin1()]=v;
            else
                steps.last().array.append(v);
        }
    }

    QJsonValue* json;
    struct Step { QIdentifier key; /* TODO union */ QJsonObject object; QJsonArray array; Step(QIdentifier k=QIdentifier()) : key(k) {} };
    QStack<Step> steps = QStack<Step>(); //!< minimal dynamic context to implement out() and ensure actual building in case QJsonBuilderImpl is abandoned
};

// --------------------------------------------------------------------------

class QJsonVisitor : public QAbstractValueReader
{
    Q_DISABLE_COPY(QJsonVisitor)
public:
    QJsonVisitor(const QJsonValue* v) : json(v) { Q_ASSERT(v); }
    void reset(QJsonValue* v) { json=v; Q_ASSERT(v); steps.resize(0); }

    QVariant valuePath() const {
        QByteArray path;
        Q_FOREACH(Step s, steps) {
            if (!s.key.isNull()) { path.append('{').append(                   s.key.utf8() ); }
            else                 { path.append('[').append(QByteArray::number(s.idx       )); }
        }
        return path;
    }

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* =nullptr) {            if (current().isArray ()) {       steps.push(Step()); return true; } return false; }
    bool tryRecord  (quint32* =nullptr) {            if (current().isObject()) {       steps.push(Step()); return true; } return false; }
    bool tryNull    (                 ) {            if (current().isNull()  ) {                           return true; } return false; }
    bool tryBind    (     QUtf8Data& u) { QString s; if (tryBind(s)          ) { u = s        .toUtf8  (); return true; } return false; }
    bool tryBind    (       QString& v) {            if (current().isString()) { v = current().toString(); return true; } return false; }
    bool tryBind    (          bool& v) {            if (current().isBool  ()) { v = current().toBool  (); return true; } return false; }
    bool tryBind    (        qint64& t) {  double d; if (tryBind(d)          ) { t =  qint64(d)          ; return true; } return false; }
    bool tryBind    (       quint64& t) {  double d; if (tryBind(d)          ) { t = quint64(d)          ; return true; } return false; }
    bool tryBind    (         float& v) {  double d; if (tryBind(d)          ) { v = float(d)            ; return true; } return false; }
    bool tryBind    (        double& v) {            if (current().isDouble()) { v = current().toDouble(); return true; } return false; }

    bool tryItem(QIdentifierLiteral u) { steps.last().key=u; return !(steps.last().item = current(1).toObject().value(steps.last().key.latin1())).isUndefined(); }
    bool tryItem(QIdentifier&       k) { steps.last().key=k; return !(steps.last().item = current(1).toObject().value(steps.last().key.latin1())).isUndefined(); }
    bool tryItem(                    ) { steps.last().idx++; return !(steps.last().item = current(1).toArray ().   at(steps.last().idx         )).isUndefined(); }
    bool tryOut (                    ) { steps.removeLast(); return true; }

    bool tryAny() { return true; }
private:
    const QJsonValue& current(int outer=0) const { Q_ASSERT(0<=outer && json); return steps.size()-outer <= 0 ? *json : steps[steps.size()-outer-1].item; }

    const QJsonValue* json;
    struct Step { QIdentifier key; int idx=-1; QJsonValue item; Step() = default; };
    QStack<Step> steps = QStack<Step>();
};

// --------------------------------------------------------------------------

class QJsonWriter : public QAbstractValueWriter
{
    Q_DISABLE_COPY(QJsonWriter)
public:
    QJsonWriter( QIODevice* io) : io(io) { Q_ASSERT(io || ba); }
    QJsonWriter(QByteArray* ba) : ba(ba) { Q_ASSERT(io || ba); }
   ~QJsonWriter() { while (!levels.isEmpty()) write(levels.takeLast().end); }
    void reset(QByteArray* other) {
        levels.clear();
        io=nullptr;
        ba=other;
        Q_ASSERT(ba);
    }

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    template<class T> friend class QModelWriter;

    bool trySequence(quint32* s=nullptr) { Q_UNUSED(s); levels.push(Step{"","]"}); return putChar('['); }
    bool tryRecord  (quint32* s=nullptr) { Q_UNUSED(s); levels.push(Step{"","}"}); return putChar('{'); }
    bool tryAny     (                  ) { return write("undefined"); } // TODO || (isStrictJson && isErrorFiltered(qBindUnexpectedValue) && write("null");
    bool tryNull    (                  ) { return write("null"); }
    bool tryBind    (   QUtf8DataView u) { return putString(u.data(), u.size()); }
    // JSON literals
    bool tryBind    (          bool&& n) { return write(                   n?"true":"false"       ); }
    bool tryBind    (         float&& n) { return write(QByteArray::number(n,'g',std::numeric_limits< float>::max_digits10)); }
    bool tryBind    (        double&& n) { return write(QByteArray::number(n,'g',std::numeric_limits<double>::max_digits10)); }
    bool tryBind    (       quint64&& t) { return write(QByteArray::number(t)); }
    bool tryBind    (        qint64&& t) { return write(QByteArray::number(t)); }

    bool tryItem(QIdentifier&       n) { auto r=(write(levels.last().sep) || *levels.last().sep=='\0') && putString(n.data(), n.size()) && putChar(':'); levels.last().sep = ","; return r; }
    bool tryItem(QIdentifierLiteral n) { auto r=(write(levels.last().sep) || *levels.last().sep=='\0') && putString(n.data(), n.size()) && putChar(':'); levels.last().sep = ","; return r; }
    bool tryItem(                    ) { auto r=(write(levels.last().sep) || *levels.last().sep=='\0')                                                 ; levels.last().sep = ","; return r; }
    bool tryOut (                    ) { return  write(levels.pop() .end); }
private:
    struct Step { const char* sep; const char* end; };

    bool write    (const char* utf8) { if (ba) { ba->append( utf8); return true; } else return io->write  ( utf8); }
    bool putChar  (      char ascii) { if (ba) { ba->append(ascii); return true; } else return io->putChar(ascii); }
    bool putString(const char* utf8, int size)
    {
        putChar('"');
        for (int i=0 ; i<size ; ++i)
        {
            /**/ if (utf8[i] == '\\') { putChar('\\'); putChar('\\'); }
            else if (utf8[i] ==  '"') { putChar('\\'); putChar( '"'); }
            else if (utf8[i] <    0|| // UTF-8 sequence bytes
                     utf8[i] >=  ' ') { putChar(utf8[i]); }
            else                      {
                                        putChar('\\'); switch (utf8[i]) {
                                           case '\t' : putChar( 't'); break;
                                           case '\b' : putChar( 'b'); break;
                                           case '\f' : putChar( 'f'); break;
                                           case '\n' : putChar( 'n'); break;
                                           case '\r' : putChar( 'r'); break;
                                           default   : putChar( 'u'); {
                                              char high=((utf8[i]) / 16), low=((utf8[i]) % 16);
                                              putChar(              '0'        );
                                              putChar(              '0'        );
                                              putChar(high+(high<10?'0':'A'-10));
                                              putChar( low+( low<10?'0':'A'-10)); }    break;
                                           }
            }
        }
        return putChar('"');
    }

    QIODevice*   io     = nullptr;
    QByteArray*  ba     = nullptr;
    QStack<Step> levels = QStack<Step>(); // std::vector<bool> does not increase performance here
};

// --------------------------------------------------------------------------

#include "QVariant_impl.h" // as caching value reader and writer for out-of-order item keys

class QJsonReader : public QAbstractValueReader
{
    Q_DISABLE_COPY(QJsonReader)
public:
    QJsonReader(QIODevice* io) : io(io), cacheWriter(&cachedValue), cacheReader(&cachedValue) { Q_ASSERT(io); }
    void reset(QIODevice* other) {
        levels.clear();
        line=0; column=0; index=-1;
        cachedNumber = None; d=.0; i=0; neg=bool();
        cacheLevel = 0; cachedValue = QJsonValue();
        cacheWriter.reset(&cachedValue);
        caching = nullptr; //!< Only used when cacheLevel>=1 && key!=expectedKey
        cacheReader.reset(&cachedValue);
        io=other;
        Q_ASSERT(io);
    }

    struct Step  { int index; const char* end; QMap<QIdentifier,QJsonValue/*TODO QVariant for meta() support*/> cachedItems; Step(int i=-1, const char* e=nullptr) : index(i), end(e) {} };

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    enum CachedNumber : quint8 { None=0, Integer, FloatingPoint };

    bool trySequence(quint32* s=nullptr) { if (caching) { cacheLevel++; return caching->trySequence(s); }
                                           if (get('[', "[{\"ntf-0123456789.")) { levels.push(Step(-1,"]")); return true; } return false; }
    bool tryRecord  (quint32* s=nullptr) { if (caching) { cacheLevel++; return caching->tryRecord(s); }
                                           if (get('{', "[{\"ntf-0123456789.")) { levels.push(Step(-1,"}")); return true; } return false; }
    bool tryAny     (                  ) { if (caching) { return caching->tryAny() && cacheOut(); }
                                           return next(",}]"); }
    bool tryNull    (                  ) { if (caching) { return caching->tryNull() && cacheOut(); }
                                           if (get('n', "[{\"ntf-0123456789.") &&
                                               get('u') &&
                                               get('l') &&
                                               get('l')) {
                                               return true;
                                           } else { return false; } }
    bool tryBind    (      QUtf8Data& s) { if (caching) { return caching->tryBind(s) && cacheOut(); }
                                           QByteArray u;
                                           if (          get('"', "[{\"ntf-0123456789.")) { u.resize(0); char c;
                                               while ((c=getCharInString()) != '\0'     ) { u.append(c); }
                                               s=QUtf8Data(u);
                                               return    get('"');
                                           } else { return false; } }
    bool tryBind    (        QString& s) { QUtf8Data u; if (tryBind(u)) { s = QString(u.utf8()); return true; } return false; }
    bool tryBind    (           bool& b) { if (caching) { return caching->tryBind(b) && cacheOut(); }
                                           if (get('t', "[{\"ntf-0123456789.") &&
                                               get('r') &&
                                               get('u') &&
                                               get('e')) { b=true;
                                               return true;
                                           } else
                                           if (get('f', "[{\"ntf-0123456789.") &&
                                               get('a') &&
                                               get('l') &&
                                               get('s') &&
                                               get('e')) { b=false;
                                               return true;
                                           } else { return false; } }
    bool tryBind    (          float& n) { if (caching) { return caching->tryBind(n) && cacheOut(); }
                                           if (getNumber()==None) return false;
                                           float rounded = d; // rounds to nearest value (inf if qAbs(d)>std::numeric_limits<float>::max()
                                           if (rounded != d && !isErrorFiltered(qBindExpectedLessPreciseDecimal)) {
                                               return false;
                                           }
                                           n = rounded; cachedNumber=None; return true; }
    bool tryBind    (         double& n) { if (caching) { return caching->tryBind(n) && cacheOut(); }
                                           if (getNumber()==None) return false;
                                           n = d ; cachedNumber=None; return true; }
    bool tryBind    (        quint64& t) { if (caching) { return caching->tryBind(t) && cacheOut(); }
                                           if (getNumber()==None) return false;
                                           if (cachedNumber==FloatingPoint) { if (isErrorFiltered(qBindExpectedInteger)) { t=i; cachedNumber=None; return true; } return false; }
                                           if (neg) { return false; }
                                           t = i ; cachedNumber=None; return true; }
    bool tryBind    (         qint64& t) { if (caching) { return caching->tryBind(t) && cacheOut(); }
                                           if (getNumber()==None) return false;
                                           if (cachedNumber==FloatingPoint) { if (isErrorFiltered(qBindExpectedInteger)) { t=i; cachedNumber=None; return true; } return false; }
                                           if (!neg && quint64(std::numeric_limits<qint64>::max())  <i) { if (isErrorFiltered(qBindExpectedSmallerInteger)) { t=std::numeric_limits<qint64>::max(); cachedNumber=None; return true; } return false; }
                                           if ( neg && quint64(std::numeric_limits<qint64>::max())+1<i) { if (isErrorFiltered(qBindExpectedSmallerInteger)) { t=std::numeric_limits<qint64>::min(); cachedNumber=None; return true; } return false; }
                                           t = neg ? -qint64(i) : qint64(i); cachedNumber=None; return true; }
    bool tryBind    (     QVariant& dst) { if (caching) { return caching->tryBind(dst) && cacheOut(); }
        {
            QSuspendedValueErrorHandler(this);
            quint32 size=0; QIdentifier key; QVariant item;
            if (       trySequence(&size)) { QVariantList l; while (tryItem(   )) { l.append(              tryBind(item) ? item : QVariant()); } dst = l; return tryOut(); }
            if (       tryRecord  (&size)) { QVariantMap  l; while (tryItem(key)) { l.insert(key.latin1(), tryBind(item) ? item : QVariant()); } dst = l; return tryOut(); }
            bool        b; if (tryBind(b)) { dst = QVariant(b                                       ); return true; }
            quint64     u; if (tryBind(u)) { dst = QVariant(u                                       ); return true; } // may fail after consuming integer part
            qint64      l; if (tryBind(l)) { dst = QVariant(l                                       ); return true; } // may fail after consuming integer part
            double      d; if (tryBind(d)) { dst = QVariant(d+/*integer part consumed by one of*/u+l); return true; }
            QUtf8Data   s; if (tryBind(s)) {
                QByteArray b;
                if (toByteArray(b, s)) {
                    toVariant(dst, b);
                    return true;
                }
                dst = QVariant(QString::fromUtf8(s.utf8()));
                return true;
            }
            if (tryNull()) { dst = QVariant::fromValue(nullptr); return true; }
        }
        return false;
    }

    bool trySequenceOut() { if (caching) { bool out = caching->trySequenceOut(); if (out) { cacheLevel--; cacheOut(); } return out; }
                            auto level = levels.pop();
                            while (get(',', "}]")) {
                                tryAny();
                            }
                            return get(*level.end, "}]"); }
    bool tryOut        () { if (caching) { bool out = caching->tryRecordOut  (); if (out) { cacheLevel--; cacheOut(); } return out; }
                            auto level = levels.pop();
                            while (get(',', "}]")) {
                                tryAny();
                            }
                            return get(*level.end, "}]"); }

    bool tryItem(QIdentifierLiteral u) { if (caching) { return caching->tryItem(u); }
                                   QIdentifier s;
                                   while (true) {
                                       if (levels.last().cachedItems.          contains(QIdentifier(u))) { // must be checked before we consume tryItem(s)
                                           cachedValue = levels.last().cachedItems.take(QIdentifier(u));
                                           cacheReader.reset(&cachedValue);
                                           cacheReader.setErrorFilter(errorFilter);
                                           Q_ASSERT(!cacheLevel);
                                           caching = &cacheReader; // let outer QCur drive QJsonReader depending on bound T
                                           return true;
                                       }
                                       else if (!tryItem(s)) { // record() end reached
                                           return false;
                                       }
                                       else if (u  ==  s ) {
                                           return true ;
                                       }
                                       else { // read cachedValue using a dedicated QCur and QTransmogrifier<QValue> accepting value of any 'shape' (outer QTransmogrifier is specialized on outer T which is not usually generic enough)
                                           cachedValue = QJsonValue();
                                           cacheWriter.reset(&cachedValue);
                                           cacheWriter.setErrorFilter(errorFilter);
                                           if (!QCur(this).value().bind(QCur(&cacheWriter).value())) {
                                               return false;
                                           }
                                           levels.last().cachedItems.insert(s, cachedValue);
                                           continue; // until !tryItem(s) or u==s
                                       }
                                   }}
    bool tryItem(QIdentifier& k) { if (caching) { return caching->tryItem(k); }
                                   Step& level = levels.last();
                                   if ((-1 < level.index && !get(',',level.end)) || next("[{\"ntf-0123456789.",'}')=='}') {
                                       return false;
                                   }
                                   level.index++;
                                   QUtf8Data s;
                                   if (!tryBind(s)) {
                                       return false;
                                   }
                                   k = QIdentifier(s.utf8()); // TODO check errors
                                   return get(':',level.end); }
    bool tryItem(              ) { if (caching) { return caching->tryItem(); }
                                   Step& level = levels.last();
                                   if ((-1 < level.index && !get(',',level.end)) || next("[{\"ntf-0123456789.",']')==']') {
                                       return false;
                                   }
                                   level.index++;
                                   return true; }

    QVariant valuePath() const { return QVariantList{line,column}; }
private:
    CachedNumber getNumber() {
        if (cachedNumber!=None) return cachedNumber;

        neg                         = get('-', "[{\"ntf-0123456789.");
        qint8 digit;
        if ((digit                  = getDigit()) < 0) {
            return None; // do not accept no digit otherwise we may accept an empty mantissa or string!
        }
        cachedNumber=Integer;
        i=0; d=0;
        do {// TODO return FloatingPoint on overflow (precision being limited by the type)
            i=i*10+quint8(digit); d=d*10+digit;
        }
        while (0 <= (digit          = getDigit())); // accept many leading '0' which is more permissive than JSON

        if (                          get('.')) { // TODO handle as negative exponent to support scientific notation of integers
            cachedNumber=FloatingPoint;
            // roughly
            // d = i;
            // m_stream >> decimal // except it would eat exponent
            // d += decimal
            double decimal=.1;
            while (0 <= (digit      = getDigit())) {
                d+=decimal*digit; decimal/=10;
            }
        }

        if (                          get('e') || get('E')) {
            qint64 exponent = 0;
            bool isNegativeExponent = get('-');
                                      get('+');
            while (0 <= (digit      = getDigit())) {
                exponent=exponent*10+digit; // TODO return None on overflow
            }
            // TODO return FloatingPoint on overflow (precision being limited by the type)
            d *=10^(isNegativeExponent ? -exponent : exponent);
        }
        // TODO if (!next(",]}") {}

        if (neg)
            d=-d;

        return cachedNumber;
    }
    qint8 getDigit(quint8 base = 10) { Q_ASSERT(0<base);
        qint8 digit;
        if ( 0 <= (digit=nextChar()-'0'   ) && digit < base && base <= 10) { getChar(); return digit; }
        if (10 <= (digit=nextChar()-'a'+10) && digit < base && base <= 36) { getChar(); return digit; }
        if (10 <= (digit=nextChar()-'A'+10) && digit < base && base <= 36) { getChar(); return digit; }
        return -1;
    }
    char getCharInString() // TODO Support user-defined QTextCodec
    {
        if (nextChar() == '\0' || nextChar() == '"') {
            return '\0';
        }
        if (get('\\')) {
            unsigned hex = 0;
            switch (getChar())
            {
            case'\\': return '\\';
            case '"': return  '"';
            case 'b': return '\b';
            case 'f': return '\f';
            case 'n': return '\n';
            case 'r': return '\r';
            case 't': return '\t';
            case '/': return  '/';
            case 'u': {
                int digits = 0, digit;
                while ((digits++ < 4) && (0 <= (digit=getDigit(16)))) {
                    hex = 16*hex + unsigned(digit);
                }
                if (digits==4) {
                    return char(hex);
                }
                return  '?';
            }
            default : return  '?';
            }
        }
        else {
            return getChar();
        }
    }
    bool nextIs(char expected, const char* validChars = nullptr) { return next(validChars, expected) == expected; }
    bool get(char expected, const char* validChars = nullptr) {
        if (nextIs(expected, validChars)) {
            return getChar() == expected;
        }
        return false;
    }
    char getChar () { char c; if (io->getChar(&c)     ) { if (c=='\n') { line++; column=0; } else { column++; } index++; return c; } return '\0'; }
    char nextChar() { char c; if (io->peek   (&c,1)==1) {
            return c; } return '\0'; }
    char next(const char* validChars, char expected='\0') {
        if (validChars) {
            while (!(nextChar() == expected || strchr(validChars, nextChar()) || nextChar() == '\0')) {
                char ignored = getChar();
                if (!isspace(ignored)) {
                    if (!isErrorFiltered(qBindUnexpectedData)) {
                        return false;
                    }
                }
            }
        }
        return nextChar();
    }

    QIODevice* io;
    int line = 0, column = 0, index = -1;
    QStack<Step> levels = QStack<Step>(); //!< dynamic context required to implement item() and report meaningful errors

    // Read caching of numbers
    CachedNumber cachedNumber = None;
    double d; quint64 i; bool neg;

    bool cacheOut() { if (!cacheLevel) {
                          caching = nullptr;
                      }
                      return true; }

    // Read/Write caching of out-of-order item keys
    quint8       cacheLevel    = 0;
    QJsonValue   cachedValue      ; //!< Only used AFTER --cacheLevel==1
    QJsonBuilder cacheWriter      ;
    QAbstractValue* caching = nullptr; //!< Only used when cacheLevel>=1 && key!=expectedKey
    QJsonVisitor cacheReader      ; //!< Only used as caching
};

// //////////////////////////////////////////////////////////////////////////
// QJson* support

#include <QtCore/qjsonvalue.h>
#include <QtCore/qjsonarray.h>
#include <QtCore/qjsonobject.h>

template<>
struct QTransmogrifier<QJsonValue> {
    static QValueEnd zap(QValue&& v, QJsonValue&& j) {
        if (v->mode()==Write) {
            if (j.isObject   ()) return v.bind(j.toObject());
            if (j.isArray    ()) return v.bind(j.toArray ());
            if (j.isBool     ()) return v.bind(j.toBool  ());
            if (j.isDouble   ()) return v.bind(j.toDouble());
            if (j.isString   ()) return v.bind(j.toString());
            if (j.isNull     ()) return v.null();
            return v.unexpected();
        }
        else { Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QJsonValue& j) {
        if (v->mode()==Write) {
            return zap(std::move(v),std::move(j));
        }
        else if (v->mode()==Read) {
            QValueEnd r;
            v.setIsVariant();
            QJsonArray  a; if ((r = v.bind(a))) { j =                   a  ; return r; }
            QJsonObject o; if ((r = v.bind(o))) { j =                   o  ; return r; }
            QString     t; if ((r = v.bind(t))) { j = QJsonValue(       t) ; return r; }
            bool        b; if ((r = v.bind(b))) { j = QJsonValue(       b) ; return r; }
            qint64      i; if ((r = v.bind(i))) { j = QJsonValue(double(i)); return r; }
            quint64     u; if ((r = v.bind(u))) { j = QJsonValue(double(u)); return r; }
            double      d; if ((r = v.bind(d))) { j = QJsonValue(       d ); return r; }
            /**/           if ((r = v.null( ))) { j = QJsonValue(         ); return r; }
            return v.unexpected();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

template<>
struct QTransmogrifier<QJsonArray> {
    static QValueEnd zap(QValue&& v, QJsonArray&& j) {
        if (v->mode()==Write) {
            quint32 size=quint32(j.size());
            auto s(v.sequence(&size));
            for (QJsonValue item : j) {
                s = s.bind(item);
            }
            return s.out();
        }
        else { Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QJsonArray& j) {
        if (v->mode()==Write) {
            return zap(std::move(v),std::move(j));
        }
        else if (v->mode()==Read) {
            auto s(v.sequence());
            QVal<QSequence> i;
            while ((i = s.item())) {
                QJsonValue json;
                if ((s = i.bind(json)))
                    j.append(json);
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

template<>
struct QTransmogrifier<QJsonObject> {
    static QValueEnd zap(QValue&& v, QJsonObject&& j) {
        if (v->mode()==Write) {
            quint32 size=quint32(j.size());
            auto s(v.record(&size));
            for (QString key : j.keys()) {
                QIdentifier n(key.toLatin1());
                s = s.item(n).bind(QJsonValue(j[key]));
            }
            return s.out();
        }
        else { Q_ASSERT_X(false, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QJsonObject& j) {
        if (v->mode()==Write) {
            return zap(std::move(v),std::move(j));
        }
        else if (v->mode()==Read) {
            auto s(v.record());
            QIdentifier k; QVal<QRecord> i;
            while ((i = s.item(k))) {
                QJsonValue json;
                if ((s = i.bind(json)))
                    j.insert(k.latin1(),json);
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

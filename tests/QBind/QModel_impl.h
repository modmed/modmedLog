/****************************************************************************
 * **
 * ** Copyright (C) 2018 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <QtCore/qabstractitemmodel.h>
#include <QtCore/qvariant.h>
#include <QtCore/qstring.h>
#include <QtCore/qbuffer.h>
#include <QtGui/qcolor.h>

#include "QValue.h"
#include "QJson_impl.h"

// //////////////////////////////////////////////////////////////////////////
// Q*Model support

class QModelBind
{
    Q_DISABLE_COPY(QModelBind)
public:
    QModelBind(QAbstractItemModel* m, bool rowFirst=true) : m(m), rowFirst(rowFirst) { Q_ASSERT(m); }
protected:
    enum : int {
        T=0, //!< initial state
        R=1, //!< 1st model dimension (and all odd  ones in a tree) where items bind to row (views being row-wise by default)
        C=2, //!< 2nd model dimension (and all even ones in a tree) where items bind to col or index(row,col,parent).model().index(0,0) where row>sizes[?] or QIdentifierLiteral n==childrenName
        I=3, //!< final model dimension handled by TItemWriter to set leaf items depending on their type
    };

    virtual void _meta(QIdentifierLiteral& n, QAsciiData& meta) {
        if (T==d) {
            if (n == qmChildren) {
                childrenName = QIdentifier(meta.latin1());
            }
            else if (n == qmColumns) {
                metaColumnNames=true;
                int i=0;
                for (auto&& k : meta.utf8().split(',')) {
                    m->insertColumn(i);
                    m->setHeaderData(i,Qt::Horizontal,k);
                    columnNames.insert(i,k);
                    i++;
                }
            }
            else if (n == qmSizes) {
                for (auto&& k : meta.utf8().split(',')) {
                    int i = k.toInt();
                    if (0<i) sizes.append(i);
                }
            }
            else { qDebug("Ignored meta %s=%s", n.utf8(), meta.data()); }
        }
        else if (C==d) {
            if (n == qmColor) {
                m->setData(current(),QColor(meta.latin1()),Qt::ForegroundRole);
            }
            else { qDebug("Ignored meta %s=%s", n.utf8(), meta.data()); }
        }
        else {
            itemBind()->_meta(n, meta);
        }
    }

    static QVariantList path(QModelIndex current) {
        if (current.isValid()) {
            auto p = path(current.parent());
            p.append(QVariantList{current.row(),current.column()});
            return p;
        }
        return QVariantList();
    }

    QVariant valuePath() const { return path(parent); }

    virtual QAbstractValue* itemBind() = 0;

    //! Checks whether current dimension \c d is R or C, inverting R and C dimensions if !rowFirst
    bool   is(int rc) { Q_ASSERT(rc==R || rc==C); return rowFirst ? d==rc : d==(rc==R ? C : R); }
    int  next(      ) { Q_ASSERT( d==R ||  d==C); return                    d==(    R ? C : R); }
    //! Returns a valid QModelIndex inserting rows/columns as needed
    QModelIndex current() {
        int rows = m->   rowCount(parent); if (rows<=row) m->insertRows   (rows,1+row-rows,parent);
        int cols = m->columnCount(parent); if (cols<=col) m->insertColumns(cols,1+col-cols,parent);
        auto idx = m->index(row,col,parent);
        Q_ASSERT(hidden() || m->checkIndex(idx));
        return idx;
    }
    //! Returns whether current data must be hidden because a current or parent column or row was filtered out
    bool hidden() noexcept {
        for(auto i=parent;;i=i.parent()) {
            if (col<0 || row<0)
                return true;
            if (!i.isValid()) // after previous test
                return false; // without trying i=i.parent()
        }
        Q_ASSERT(false);
    }
    //! Returns an unlimited size if sizes.isEmpty() or the size corresponding to the current dimension or 0 if the dimension exceeds defined sizes
    int dimension() {
        int dimension = d-R;
        for (auto index = parent; index.isValid() ; index = index.parent()) {
            dimension += 2;
        }
        return dimension;
    }
    //! Returns int max value if no dimension sizes or 0 if current dimension exceeds number of sizes
    int max(int dimension) {
        return sizes.isEmpty()        ? std::numeric_limits<int>::max() :
               dimension<sizes.size() ? sizes.at(dimension)
                                      : 0;
    }
    inline bool write(QVariant v) { m->setData(current(), v); return true; }
    inline QVariant read() { return m->   data(current()); }

    QAbstractItemModel* m;
    bool rowFirst;

    // Current bind state
    int d=T;
    QModelIndex parent;
    int row=0, col=0; //!< Allows handling {null} equally to {} (but not equally to null) without requiring QModelIndex to reference rows/columns before they actually exist

    // Supported meta()
    QIdentifier childrenName;
    QList<QByteArray> columnNames;
    bool metaColumnNames=false;
    QVector<int> sizes;
};

// --------------------------------------------------------------------------

template <class TItemWriter=QJsonWriter>
class QModelWriter : public QModelBind, public QAbstractValueWriter
{
    Q_DISABLE_COPY(QModelWriter)
public:
    QModelWriter(QAbstractItemModel* m, bool rowFirst=true) : QModelBind(m, rowFirst), w(&ba) {}

    virtual QValueMode mode() const noexcept { return QValueMode::Write; }
    virtual QValue value() { return QCur(this).value(); }
protected:
    virtual void _meta(QIdentifierLiteral& n, QAsciiData& meta) { return QModelBind::_meta(n, meta); }

    virtual bool tryOut() {
        d--;
        if (I<=(d+1)) {
            if (hidden()) {
                return true;
            }
            if  (!itemBind()->tryOut()) {
                return false;
            }
            if (I==(d+1)) {
                return tryBind(QString::fromUtf8(ba.data(), ba.size()));
            }
            return true;
        }
        else {
            if (T==d && parent.isValid()) { // case of a tree, even if hidden()
                row=parent.row(); parent=parent.parent(); d=C;
            }
            return true;
        }
    }

    virtual bool trySequence(quint32* s=nullptr) {
        ++d;
        if (hidden()) {
            return true;
        }
        else if (I<=d) {
            if  (I==d) {
                resetItemData();
            }
            return itemBind()->trySequence(s);
        }
        else if (is(C)) {
            col=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else if (is(R)) {
            row=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else { Q_ASSERT(T==d);
            return true;
        }
    }

    virtual bool tryRecord  (quint32* s=nullptr) {
        ++d;
        if (hidden()) {
            return true;
        }
        else if (I<=d) {
            if  (I==d) {
                resetItemData();
            }
            return itemBind()->tryRecord(s);
        }
        else if (is(C)) {
            col=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else if (is(R)) {
            row=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else { Q_ASSERT(T==d);
            return true;
        }
    }

    virtual bool tryItem() {
        Q_ASSERT(d!=T);
        if (is(C)) {
            col++; // mandatory after trySequence()
            if (hidden()) {
                return true;
            }
            else if (max(dimension())<=col) {
                if (d==R && row+1<max(dimension()+1)) { // new row
                    row++; col=0;
                    return true;
                }
                Q_UNUSED(isErrorFiltered(qBindUnexpectedItem))
                return false;
            }
            return true;
        }
        else if (is(R)) {
            row++; // mandatory after trySequence()
            if (hidden()) {
                return true;
            }
            else if (max(dimension())<=row) {
              if (d==R && col+1<max(dimension()+1)) { // new row
                  row=0; col++;
                  return true;
              }
              Q_UNUSED(isErrorFiltered(qBindUnexpectedItem))
              return false;
            }
            return true;
        }
        else { Q_ASSERT(I<=d);
            return hidden() || itemBind()->tryItem();
        }
    }

    virtual bool tryItem(QIdentifierLiteral n) { QIdentifier id(n); return tryItem(id); }
    virtual bool tryItem(QIdentifier& n) {
        Q_ASSERT(d!=T);
        if (is(C)) {
            col++; // mandatory after tryRecord()
            if (hidden()) {
                return true;
            }
            else if (n!=childrenName) {
                if (!metaColumnNames) {
                    if (!parent.isValid() && row==0) {
                        int i = m->columnCount();
                        m->insertColumn(i);
                        m->setHeaderData(i,Qt::Horizontal,n.latin1());
                        columnNames.insert(i,n.utf8());
                    }
                }
                else {
                     col=columnNames.indexOf(n.utf8());
                     // TODO if (max(dimension())<=col) col=-1;
                     if (col<0 && !isErrorFiltered(qBindUnexpectedItem, n.latin1())) {
                         return false;
                     }
                }
                return true;
            }
            else {
                d=T; parent=m->index(row,0,parent); row=col=0;
                return true;
            }
        }
        else if (is(R)) {
            row++; // mandatory after tryRecord()
            if (hidden()) {
                return true;
            }
            else {
                // TODO if (rowNames...
                return isErrorFiltered(qBindUnexpectedItem, n.latin1());
            }
        }
        else { Q_ASSERT(I<=d);
            return hidden() || itemBind()->tryItem(n);
        }
    }

    using QAbstractValue::tryBind; // full overload set so that following overloads calls work (instead of calling themselves and overflowing the call stack)

    virtual bool tryBind(QUtf8DataView u) { return tryBind(QString::fromUtf8  (u.data(), u.size())); }

    virtual bool tryAny (             ) { return hidden() ? true : I<=d ? itemBind()->tryAny()              : write(QVariant()); }
    virtual bool tryNull(             ) { return hidden() ? true : I<=d ? itemBind()->tryNull()             : write(QVariant::fromValue(nullptr)); }
    virtual bool tryBind(  QString&& u) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(u)) : write(u         ); }
    virtual bool tryBind(     bool&& b) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(b)) : write(b         ); }
    virtual bool tryBind(    float&& f) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(f)) : write(f         ); }
    virtual bool tryBind(   double&& f) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(f)) : write(f         ); }
    virtual bool tryBind(  quint64&& i) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(i)) : i<=quint64(std::numeric_limits<unsigned int>::max()) ? write(static_cast<unsigned int>(i)) :
                                                                                                              write(i         ); } // with QSpinBox delegate
    virtual bool tryBind(   qint64&& i) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(i)) : i<= qint64(std::numeric_limits<         int>::max()) ? write(static_cast<         int>(i)) :
                                                                                                              write(i         ); }
    virtual bool tryBind( QVariant&& t) { return hidden() ? true : I<=d ? itemBind()->tryBind( QVariant(t)) : write(t         ); }
    // TODO QDate*, QTime
    // TODO QPixmap if metadata suggests a QMimeData image ?

    void resetItemData() { ba.clear(); w.reset(&ba); }
    QAbstractValue* itemBind() { return static_cast<QAbstractValue*>(&w); }

    QByteArray ba;
    TItemWriter w;
};

// --------------------------------------------------------------------------

template <class TItemReader=QJsonReader>
class QModelReader : public QModelBind, public QAbstractValueReader
{
    Q_DISABLE_COPY(QModelReader)
public:
    QModelReader(QAbstractItemModel* m, bool rowFirst=true) : QModelBind(m, rowFirst), r(&io) { io.open(QIODevice::ReadOnly); }

    virtual QValueMode mode() const noexcept { return QValueMode::Read; }
    virtual QValue value() { return QCur(this).value(); }
protected:
    virtual void _meta(QIdentifierLiteral& n, QAsciiData& meta) { return QModelBind::_meta(n, meta); }

    virtual bool tryOut() {
        if (I<=d) {
            d--;
            return hidden() || itemBind()->tryOut();
        }
        else { // FIXME Q_ASSERT(C==d || R==d);
            d--;
            if (R==d && parent.isValid()) { // case of a tree, even if hidden()
                row=parent.row(); parent=parent.parent(); d=C;
            }
            return true;
        }
    }

    virtual bool trySequence(quint32* s=nullptr) {
        ++d;
        if (hidden()) {
            return true;
        }
        else if (I<=d) {
            if  (I==d) {
                QVariant v = read();
                if (v.type()!=QVariant::String) {
                    return false;
                }
                setItemData(QUtf8Data(v.toString().toUtf8()));
            }
            return itemBind()->trySequence(s);
        }
        else if (is(C)) {
            col=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else if (is(R)) {
            row=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else { Q_ASSERT(T==d);
            return true;
        }
    }

    virtual bool tryRecord  (quint32* s=nullptr) {
        ++d;
        if (hidden()) {
            return true;
        }
        else if (I<=d) {
            if  (I==d) {
                QVariant v = read();
                if (v.type()!=QVariant::String) {
                    return false;
                }
                setItemData(QUtf8Data(v.toString().toUtf8()));
            }
            return itemBind()->tryRecord(s);
        }
        else if (is(C)) {
            col=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else if (is(R)) {
            row=-1; // set to 0 by mandatory tryItem() following
            return true;
        }
        else { Q_ASSERT(T==d);
            return true;
        }
    }

    virtual bool tryItem() {
        Q_ASSERT(d!=T);
        if (is(C)) {
            col++; // mandatory after trySequence()
            if (hidden()) {
                return true;
            }
            else {
                if (max(dimension())<=col) {
                    if (d==R && row+1 < max(dimension()+1)) { // new row
                        row++; col=0;
                        if (row < m->rowCount()) { Q_ASSERT(col < m->columnCount());
                            return true;
                        }
                    }
                }
                if (col < m->columnCount()) {
                    return true;
                }
                Q_UNUSED(isErrorFiltered(qBindUnexpectedItem))
                return false;
            }
            return true;
        }
        else if (is(R)) {
            if (m->rowCount() <= row+1) {
                return false;
            }
            row++; // mandatory after trySequence()
            if (hidden()) {
                return true;
            }
            else if (max(dimension())<=row) {
              if (d==R && col+1<max(dimension()+1)) { // new row
                  row=0; col++;
                  return true;
              }
              Q_UNUSED(isErrorFiltered(qBindUnexpectedItem))
              return false;
            }
            return true;
        }
        else { Q_ASSERT(I<=d);
            return hidden() || itemBind()->tryItem();
        }
    }

    virtual bool tryItem(QIdentifierLiteral n) { QIdentifier id(n); return tryItem(id); }
    virtual bool tryItem(QIdentifier& n) {
        Q_ASSERT(d!=T);
        if (is(C)) {
            col++; // mandatory after tryRecord()
            if (hidden()) {
                return true;
            }
            else if (n!=childrenName) {
                if (!metaColumnNames) {
                    col=-1;
                    for (int i=0; i < m->columnCount(); ++i) {
                        if (m->headerData(i, Qt::Horizontal).toString()==n.latin1()) {
                            col=i;
                            break;
                        }
                    }
                }
                else {
                     col=columnNames.indexOf(n.utf8());
                }
                // TODO if (max(dimension())<=col) col=-1;
                if (col<0 && !isErrorFiltered(qBindUnexpectedItem)) {
                    return false;
                }
                return true;
            }
            else { return true; }
        }
        else if (is(R)) {
            row++; // mandatory after tryRecord()
            if (hidden()) {
                return true;
            }
            else if (n!=childrenName) {
                // TODO if (rowNames...
                return isErrorFiltered(qBindUnexpectedItem);
            }
            else { return true; }
        }
        else { Q_ASSERT(I<=d);
            if (hidden()) {
                return true;
            }
            else if (I==d && n==childrenName) {
                d=T; parent=m->index(row,0,parent); row=col=0;
                return true;
            }
            else {
                return itemBind()->tryItem(n);
            }
        }
    }

    using QAbstractValue::tryBind; // full overload set so that following overloads calls work (instead of calling themselves and overflowing the call stack)

    virtual bool tryAny (             ) { return hidden() ? true : I<=d ? itemBind()->tryAny()              :                !read().isValid () ; }
    virtual bool tryNull(             ) { return hidden() ? true : I<=d ? itemBind()->tryNull()             :                 read().isNull  () ; }
    virtual bool tryBind(  QString&& u) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(u)) :              u==read().toString() ; }
    virtual bool tryBind(     bool&& b) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(b)) :              b==read().toBool  () ; }
    virtual bool tryBind(    float&& f) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(f)) : qFuzzyCompare(f,read().toFloat ()); }
    virtual bool tryBind(   double&& f) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(f)) : qFuzzyCompare(f,read().toDouble()); }
    virtual bool tryBind(  quint64&& i) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(i)) :              i==read().toUInt  () ; }
    virtual bool tryBind(   qint64&& i) { return hidden() ? true : I<=d ? itemBind()->tryBind(std::move(i)) :              i==read(). toInt  () ; }
    virtual bool tryBind( QVariant&& t) { return hidden() ? true : I<=d ? itemBind()->tryBind( QVariant(t)) :              t==read()            ; }
    // TODO QDate*, QTime
    // TODO QPixmap if metadata suggests a QMimeData image ?

    virtual bool tryBind( QUtf8Data& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); if (v.type()!=QVariant ::String   ) return false; r=QUtf8Data(v.toString().toUtf8()); return true; }
    virtual bool tryBind(   QString& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); if (v.type()!=QVariant ::String   ) return false; r=          v.toString         () ; return true; }
    virtual bool tryBind(      bool& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); if (v.type()!=QVariant ::Bool     ) return false; r=          v.toBool           () ; return true; }
    virtual bool tryBind(    qint64& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); bool isConverted=false;  qint64 i=v. toLongLong(&isConverted); if (!isConverted || v.type()==QVariant::Bool || v.type()==QVariant::ByteArray) { return false; } r=i; return true; }
    virtual bool tryBind(   quint64& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); bool isConverted=false; quint64 i=v.toULongLong(&isConverted); if (!isConverted || v.type()==QVariant::Bool || v.type()==QVariant::ByteArray) { return false; } r=i; return true; }
    virtual bool tryBind(     float& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); bool isConverted=false; float   i=v.toFloat    (&isConverted); if (!isConverted || v.type()==QVariant::Bool || v.type()==QVariant::ByteArray) { return false; } r=i; return true; }
    virtual bool tryBind(    double& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); bool isConverted=false; double  i=v.toDouble   (&isConverted); if (!isConverted || v.type()==QVariant::Bool || v.type()==QVariant::ByteArray) { return false; } r=i; return true; }
    virtual bool tryBind(QByteArray& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r); QVariant v=read(); if (v.type()!=QVariant ::ByteArray) return false; r=          v.toByteArray      () ; return true; }
    virtual bool tryBind(  QVariant& r) { if (hidden()) return true; if (I<=d) return itemBind()->tryBind(r);          r=read()                                                                                      ; return true; }

    QAbstractValue*  itemBind() { return static_cast<QAbstractValue*>(&r); }
    void setItemData(QUtf8Data u) { io.buffer() = u.utf8(); io.seek(0); r.reset(&io); }

    QBuffer io;
    TItemReader r;
};

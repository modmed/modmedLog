/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qsettings.h>
#include <QtCore/qvariant.h>
#include <QtCore/qstack.h>

#include <algorithm>

#include "QValue.h"

// //////////////////////////////////////////////////////////////////////////
// QSettings* support for the fixed set of QSettingsWriter's BindNative types

// Compatibility notes:
// TODO Add QSize, QRect, QPoint for QSettings::IniFormat special string representations. See https://code.woboq.org/qt5/qtbase/src/corelib/io/qsettings.cpp.html#430
// QPolygon special mac handling should be ok by default since it is a list of points. See https://code.woboq.org/qt5/qtbase/src/corelib/io/qsettings_mac.cpp.html#121
// QMap multiple entries should be ok by default on Mac when QMap is actually handled as a sequence of {key,value}. See https://code.woboq.org/qt5/qtbase/src/corelib/io/qsettings_mac.cpp.html#126
// Windows MULTI_SZ support is easy to read but trickier to optimize at write time since generic a sequence() can only use MULTI_SZ iff all values do not contain '\0'. See https://code.woboq.org/qt5/qtbase/src/corelib/io/qsettings_win.cpp.html#673

class QSettingsWriter : public QAbstractValueWriter
{
    Q_DISABLE_COPY(QSettingsWriter)
public:
    QSettingsWriter(QSettings* s) : settings(s) { Q_ASSERT(s); }

    QVariant valuePath() const { return key; }

    // Shortcuts
    /**/                 QValue    value   (                  ) { return QCur(this).value(); }
    /**/                 QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }
    template<typename T> QValueEnd bind    (             T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool tryBind    (     quint8&& t) { settings->setValue(key, int(t)                               ); return true; }
    bool tryBind    (    quint16&& t) { settings->setValue(key, int(t)                               ); return true; }
    bool tryBind    (    quint32&& t) { settings->setValue(key, int(t)                               ); return true; }
    bool tryBind    (QUtf8DataView u) { settings->setValue(key, QString::fromUtf8(u.data(), u.size())); return true; }
    bool tryBind    (  QStringView s) { settings->setValue(key, s.toString()                         ); return true; }
    bool tryBind    (    QString&& s) { settings->setValue(key, s                                    ); return true; }

    bool tryAny () {                                                        return true; }
    bool tryNull() { settings->setValue(key, QVariant::fromValue(nullptr)); return true; }

    bool trySequence(quint32* =nullptr) { if (!key.isEmpty()) key+='/'; return true; }
    bool tryRecord  (quint32* =nullptr) { if (!key.isEmpty()) key+='/'; return true; }

    bool tryItem(QIdentifier& n) {               Q_UNUSED(pop()  )  key+=n.latin1(); return true; }
    bool tryItem(              ) { auto n=QString::number(pop()+1); key+=n         ; return true; }

    bool trySequenceOut() { settings->setValue(key+"size", pop()); key.chop(1); return true; }
    bool tryRecordOut  () {           Q_UNUSED(            pop())  key.chop(1); return true; }
private:
    int pop() {
        auto last=key.lastIndexOf('/')+1;
        auto popped=key.mid(last).toInt();
        key.truncate(last);
        return popped;
    }

    QSettings* settings;
    QString key;
};

// --------------------------------------------------------------------------

class QSettingsReader : public QAbstractValueReader
{
    Q_DISABLE_COPY(QSettingsReader)
public:
    QSettingsReader(QSettings* s) : settings(s) { Q_ASSERT(s); allKeys=settings->allKeys(); }

    QVariant valuePath() const { return key; }

    // Shortcuts
    template<typename T> QValueEnd bind(T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    template<typename T>
    bool tryBind(T& t) { return set(t, "Expected declared metatype T"); }

    bool tryBind(  QUtf8Data& t) { return set(t); }
    bool tryBind(    QString& t) { return set(t); }
    bool tryBind(       bool& t) { return set(t); }
    bool tryBind( QByteArray& t) { return set(t); }
    // Convert numerical types to strictly larger ones // TODO convert all compatible values
    bool tryBind(      qint8& t) { return set(t); }
    bool tryBind(     qint16& t) { return set(t); }
    bool tryBind(     qint32& t) { return set(t); }
    bool tryBind(     qint64& t) { return set(t); }
    bool tryBind(     quint8& t) { return set(t); }
    bool tryBind(    quint16& t) { return set(t); }
    bool tryBind(    quint32& t) { return set(t); }
    bool tryBind(    quint64& t) { return set(t); }
    bool tryBind(      float& t) { return set(t); }
    bool tryBind(     double& t) { return set(t); }

    bool trySequence(quint32* =nullptr) { if (key.isEmpty()) return true; if (allKeys.contains(key+"/size")) { key+='/'; return true; } return false; }
    bool tryRecord  (quint32* =nullptr) { if (key.isEmpty()) return true; if (   keyStartsWith(key+'/'    )) { key+='/'; return true; } return false; } // QSettings is ambiguous as arrays are also groups, so one should always test sequence before record which is unfortunate

    bool tryNull() { return settings->value(key).isNull(); }

    bool tryItem(QIdentifier&       k) {               Q_UNUSED(pop()  )         key+=k.latin1(); return keyStartsWith(key); }
    bool tryItem(QIdentifierLiteral k) {               Q_UNUSED(pop()  )         key+=k.latin1(); return keyStartsWith(key); }
    bool tryItem(                    ) { auto n=QString::number(pop()+1); auto group=key; key+=n; return settings->value(group+"size").toInt() >= n.toInt(); }

    bool tryOut() { Q_UNUSED(pop()) key.chop(1); return true; }

    bool tryAny() { return true; }
private:
    template<typename T>
    bool set(T& t) { QVariant v = settings->value(key); if (v.convert(qMetaTypeId<T>())) { t = v.value<T>(); return true; } return false; }

    bool keyStartsWith(const QString& key) { return std::find_if(allKeys.cbegin(), allKeys.cend(), [key](const QString& found) { return found.startsWith(key); }) != allKeys.cend(); }
    int pop() {
        auto last=key.lastIndexOf('/')+1;
        auto popped=key.mid(last).toInt();
        key.truncate(last);
        return popped;
    }

    QSettings* settings;
    QStringList allKeys;
    QString key;
};

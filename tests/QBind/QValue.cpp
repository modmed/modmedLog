#include "QValue.h"

QIdentifierLiteral qBindUnexpectedValue             ("UnexpectedValue"              );
QIdentifierLiteral qBindUnexpectedEnd               ("UnexpectedEnd"                );
QIdentifierLiteral qBindIllFormedValue              ("IllFormedValue"               );

QIdentifierLiteral qBindExpectedItem                ("ExpectedItem"                 );
QIdentifierLiteral qBindExpectedNull                ("ExpectedNull"                 );
QIdentifierLiteral qBindExpectedSequence            ("ExpectedSequence"             );
QIdentifierLiteral qBindExpectedRecord              ("ExpectedRecord"               );
QIdentifierLiteral qBindExpectedText                ("ExpectedText"                 );
QIdentifierLiteral qBindExpectedBytes               ("ExpectedBytes"                );
QIdentifierLiteral qBindExpectedInteger             ("ExpectedInteger"              );
QIdentifierLiteral qBindExpectedDecimal             ("ExpectedDecimal"              );
QIdentifierLiteral qBindExpectedLessPreciseDecimal  ("ExpectedLessPreciseDecimal"   );
QIdentifierLiteral qBindExpectedSmallerInteger      ("ExpectedSmallerInteger"       );
QIdentifierLiteral qBindExpectedPositiveInteger     ("ExpectedPositiveInteger"      );
QIdentifierLiteral qBindExpectedBoolean             ("ExpectedBoolean"              );
QIdentifierLiteral qBindExpectedConstant            ("ExpectedConstant"             );

QIdentifierLiteral qBindUnexpectedItem              ("UnexpectedItem"               );
QIdentifierLiteral qBindUnexpectedData              ("UnexpectedData"               );

QIdentifierLiteral qBindKeepItemsNotRead            ("KeepItemsNotRead"             );

QIdentifierLiteral qmDataStreamVersion              ("qmDataStreamVersion"          );
QIdentifierLiteral qmColumns                        ("columns"                      );
QIdentifierLiteral qmSizes                          ("sizes"                        );
QIdentifierLiteral qmChildren                       ("children"                     );
QIdentifierLiteral qmName                           ("name"                         );
QIdentifierLiteral qmColor                          ("color"                        );

QValue QCur::value() noexcept { return QValue(std::move(*this)); }

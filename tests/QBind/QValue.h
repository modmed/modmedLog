/****************************************************************************
 * **
 * ** Copyright (C) 2017 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qglobal.h>
#define Q_ENABLE_MOVE_DEFAULT(Class) \
    Class           (         ) noexcept = default; \
    Class           (Class&& o) noexcept = default; \
    Class& operator=(Class&& o) noexcept = default;
#define Q_VERIFY(Predicate) if (!(Predicate)) { Q_ASSERT(false); }
#include <QtCore/qvariant.h>
#include <QtCore/qsequentialiterable.h>
#include <QtCore/qassociativeiterable.h>
#include <QtCore/qiodevice.h>

#include "QData.h"

// //////////////////////////////////////////////////////////////////////////
// Standard error names

// First-hand "errors"

extern QIdentifierLiteral qBindUnexpectedValue; //!< Values that cannot be bound (invalid or not supported by the bound data type or our data model)
extern QIdentifierLiteral qBindUnexpectedEnd;
extern QIdentifierLiteral qBindIllFormedValue;

extern QIdentifierLiteral qBindExpectedItem;
extern QIdentifierLiteral qBindExpectedNull;
extern QIdentifierLiteral qBindExpectedSequence;
extern QIdentifierLiteral qBindExpectedRecord;
extern QIdentifierLiteral qBindExpectedText;
extern QIdentifierLiteral qBindExpectedBytes;
extern QIdentifierLiteral qBindExpectedInteger;
extern QIdentifierLiteral qBindExpectedDecimal;
extern QIdentifierLiteral qBindExpectedLessPreciseDecimal;
extern QIdentifierLiteral qBindExpectedSmallerInteger;
extern QIdentifierLiteral qBindExpectedPositiveInteger;
extern QIdentifierLiteral qBindExpectedBoolean;
extern QIdentifierLiteral qBindExpectedConstant;

extern QIdentifierLiteral qBindUnexpectedItem;
extern QIdentifierLiteral qBindUnexpectedData;

// Second-hand "errors" that can trigger when first-hand errors are filtered (ignored)

extern QIdentifierLiteral qBindKeepItemsNotRead; //!< Otherwise, erase them when possible (only for sequences)

// //////////////////////////////////////////////////////////////////////////
// Standard meta data names

#include <vector>
#include <utility>
#include <algorithm>
#include <string>

#include <QtCore/qstring.h>

// meta(QIdentifierLiteral name, QAsciiData value) is optional meta-data about current data to enable optimized processing (e.g. tag, , style, etc.) and/or transmission (QAbstractItemModel headers, CBOR tags, XML attribute, CSS, numpy.ndarray shapes, etc.)

extern QIdentifierLiteral qmDataStreamVersion; //!< Allows QAbstractValue support of QDataStream

// N-dimensional data structures for which specific QAbstractValueWriter may have optimized implementations
// BEWARE though that nested calls to QAbstractValueWriter are not guaranteed to follow the declared structure (this would prevent reusing general QTransmogrifier functors for nested data structures)

//! Sequence of records can be implemented as table with columns below (record item names are always the same in a fixed order)
//! Contains comma-separated names
extern QIdentifierLiteral qmColumns;

//! Sequence of nested non-empty sequences of definite sizes can be implemented as multi-dimensional array
//! Contains comma-separated natural numbers
extern QIdentifierLiteral qmSizes;

//! Sequence of records where item(qmNodes) contains children can be implemented as trees
//! Contains name of a sequence of records with recursively the same name
extern QIdentifierLiteral qmChildren;

//! Name of current data
extern QIdentifierLiteral qmName;

extern QIdentifierLiteral qmColor;

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier<T>

enum QValueMode { Invalid=0, Read=1, Write=2 }; //!< Specifies QTransmogrifier::zap traversal and processing (the design would support other QValueMode like Append or Diff)

using QValueErrorFilter = std::function<bool(QIdentifierLiteral,QVariant)>;

#include <QtCore/qvariant.h>
//#include <QtCore/qfloat16.h>
//#include <QtCore/qdatetime.h>
//#include <QtCore/quuid.h>

//! Interface for QValue implementations with a fixed subset of Bind native types and just a few optional methods
//!
//! Every tryXxx method must either:
//! \li return true if anything was done with the data that cannot be reversed (such as Read or Write Xxx construct, even only partially)
//! \li else return false, allowing other tryXxx to be called from the same place
//!
//! \warning tryXxxx methods MUST call setIllFormed() whenever they started doing things with the data that can be neither completed nor reversed
//!
struct QAbstractValue {
    virtual ~QAbstractValue() = default;

    virtual QValueMode mode() const noexcept = 0; //!< \remark a static constexpr QValueMode Mode did not exhibit noticeable performance improvements and may trigger twice more code generation for Read/Write independant QTransmogrifier like Person::bind

    virtual bool trySequence(quint32* size=nullptr) = 0;
    virtual bool tryRecord  (quint32* size=nullptr) = 0;

    virtual bool tryItem(                    ) = 0;
    virtual bool tryItem(QIdentifier&       n) = 0;
    virtual bool tryItem(QIdentifierLiteral n) = 0;
    virtual bool tryOut (                    ) = 0; //!< End of sequence or record

    virtual bool trySequenceOut() { return tryOut(); }
    virtual bool tryRecordOut  () { return tryOut(); }

    virtual bool tryBind(     QUtf8DataView u) = 0;
    virtual bool tryBind(       const char* u) = 0;
    virtual bool tryBind(    QAsciiDataView a) = 0;
    virtual bool tryBind(     QLatin1String l) = 0;
    virtual bool tryBind(       QStringView s) = 0;

    virtual bool tryBind(       QUtf8Data&  r) = 0;
    virtual bool tryBind(         QString&  r) = 0;
    virtual bool tryBind(            bool&  r) = 0;
    virtual bool tryBind(           qint8&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(          quint8&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(          qint16&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(         quint16&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(          qint32&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(         quint32&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(          qint64&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(         quint64&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(           float&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(          double&  r) = 0; //!< \warning Must return false instead of losing sign or digit
    virtual bool tryBind(      QByteArray&  r) = 0;
    virtual bool tryBind(        QVariant&  r) = 0;

    // TODO QChar, QDateTime, QDate, QTime, QUuid (text or numerical)

    // Overloads for const& and && T

    virtual bool tryBind(const  QUtf8Data&  r) = 0;
    virtual bool tryBind(const    QString&  r) = 0;
    virtual bool tryBind(const       bool&  r) = 0;
    virtual bool tryBind(const      qint8&  r) = 0;
    virtual bool tryBind(const     quint8&  r) = 0;
    virtual bool tryBind(const     qint16&  r) = 0;
    virtual bool tryBind(const    quint16&  r) = 0;
    virtual bool tryBind(const     qint32&  r) = 0;
    virtual bool tryBind(const    quint32&  r) = 0;
    virtual bool tryBind(const     qint64&  r) = 0;
    virtual bool tryBind(const    quint64&  r) = 0;
    virtual bool tryBind(const      float&  r) = 0;
    virtual bool tryBind(const     double&  r) = 0;
    virtual bool tryBind(const QByteArray&  r) = 0;
    virtual bool tryBind(const   QVariant&  r) = 0;

    virtual bool tryBind(       QUtf8Data&& r) = 0;
    virtual bool tryBind(         QString&& r) = 0;
    virtual bool tryBind(            bool&& r) = 0;
    virtual bool tryBind(           qint8&& r) = 0;
    virtual bool tryBind(          quint8&& r) = 0;
    virtual bool tryBind(          qint16&& r) = 0;
    virtual bool tryBind(         quint16&& r) = 0;
    virtual bool tryBind(          qint32&& r) = 0;
    virtual bool tryBind(         quint32&& r) = 0;
    virtual bool tryBind(          qint64&& r) = 0;
    virtual bool tryBind(         quint64&& r) = 0;
    virtual bool tryBind(           float&& r) = 0;
    virtual bool tryBind(          double&& r) = 0;
    virtual bool tryBind(      QByteArray&& r) = 0;
    virtual bool tryBind(        QVariant&& r) = 0;

    virtual bool tryNull() = 0; //!< Null values are used where the information is irrelevant like in optional values or nullptr
    virtual bool tryAny () = 0; //!< Any value is used where the information is unknown or undefined

    //! \warning meta() is ignored by default and subject to various interpretation, so users should define or adopt existing meta data standards like XSD for sake of interoperability
    virtual void _meta(QIdentifierLiteral&, QAsciiData&) {}

    virtual void setIllFormed() { illFormed = true; Q_VERIFY(isErrorFiltered(qBindIllFormedValue)) }
    virtual bool isIllFormed() const noexcept { return illFormed; }; //!< Current operation status

    virtual QValueErrorFilter setErrorFilter(QValueErrorFilter newFilter) {
        auto previousHandler = errorFilter;
        errorFilter = newFilter;
        return previousHandler;
    }
    virtual bool isErrorFiltered(QIdentifierLiteral e, QVariant context = QVariant()) const { auto c = this->valuePath(); return errorFilter && errorFilter(e, c.isNull() ? context : context.isNull() ? c : QVariantList({context, this->valuePath()})); }
    virtual QVariant valuePath() const { return QVariant(); }
protected:
    bool illFormed = false;
    QValueErrorFilter errorFilter = nullptr;
};

// //////////////////////////////////////////////////////////////////////////
// Default TResult implementations

template<class T_> class QVal;

#include <type_traits>
template<class T> using RemoveCvRef = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

//! A QCur is a cursor inside a structured value implemented as a move-only, non-owning pointer to QAbstractValue
//!
//! Its public interface only allows to check the status of QAbstractValue calls using explicit operator bool(), and to isErrorFiltered().
//! Its protected interface is used by QVal, QSeq, QRec to only allow QAbstractValue calls from the previous valid QCur.
//!
//! It allows to easily add QTransmogrifier support to existing Reader/Writers by adding at least one value() method returning a QCur(this).value()
//! \see TextWriter in main.cpp
//!
//! \remark QCur allows using a fluent interface without checking intermediate status by setting impl=nullptr on error
//! and subsequently bypassing structured value impl calls. Errors can result from:
//! - using the same intermediate QVal/QSeq/QRec twice (a programmer error), or
//! - runtime impl errors like trying to bind a trySequence() whereas the data read matches a tryRecord()
//!
class QCur
{
    Q_DISABLE_COPY(QCur)
public:
    QCur           (        ) noexcept : impl(nullptr) {}
    QCur           (QCur&& o) noexcept : impl(o.impl) { o.impl = nullptr; }
    QCur& operator=(QCur&& o) noexcept { auto prev = impl; impl = o.impl; o.impl = prev; return *this; }

    explicit QCur(QAbstractValue* i) : impl(i) { Q_ASSERT(impl); }

    QValueMode        mode() const noexcept { return impl ? impl->mode() : QValueMode::Invalid; }
    explicit operator bool() const noexcept { return isValid(); }
    bool           isValid() const noexcept { return impl && !impl->isIllFormed(); } //!< Drives QTransmogrifier<T>::bind() traversal
    QCur*       operator->()       noexcept { return this; }
    QVal<QCur>       value()       noexcept ;

    QValueErrorFilter setErrorFilter(QValueErrorFilter newFilter) { return Q_LIKELY(impl) ? impl->setErrorFilter(newFilter) : newFilter; }
    bool isErrorFiltered(QIdentifierLiteral name, QString context = QString()) const { return Q_LIKELY(impl) && impl->isErrorFiltered(name, context); }
    bool isErrorFiltered(const char*   asciiName, QString context = QString()) const { return isErrorFiltered(QIdentifierLiteral(asciiName), context); }
protected:
    template<class T_> friend class QVal; // enables calling methods below
    template<class T_> friend class QSeq;
    template<class T_> friend class QRec;

    void _meta      (QIdentifierLiteral& n,
                     QAsciiData&         m) { if (Q_LIKELY(impl) )   impl->_meta(n,m); } //!< idempotent (can be called by more than once)
    bool tryAny     (                  ) { return Q_LIKELY(impl) &&  impl->tryAny(); }
    bool trySequence(quint32* s=nullptr) { return Q_LIKELY(impl) && (impl->trySequence(s) || (impl->isErrorFiltered(qBindExpectedSequence  ) && impl->tryAny())); }
    bool tryRecord  (quint32* s=nullptr) { return Q_LIKELY(impl) && (impl->tryRecord  (s) || (impl->isErrorFiltered(qBindExpectedRecord    ) && impl->tryAny())); }
    bool tryNull    (                  ) { return Q_LIKELY(impl) && (impl->tryNull    ( ) || (impl->isErrorFiltered(qBindExpectedNull      ) && impl->tryAny())); }

    bool tryBind(        QUtf8Data&  t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(          QString&  t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }

    bool tryBind(const   QUtf8Data&  t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(const     QString&  t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(        QUtf8Data&& t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(          QString&& t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }

    bool tryBind(        const char* t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(      QUtf8DataView t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(     QAsciiDataView t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(      QLatin1String t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }
    bool tryBind(        QStringView t) { return Q_LIKELY(impl) && (impl->tryBind(t) || (impl->isErrorFiltered(qBindExpectedText           ) && impl->tryAny())); }

    bool tryBind(             bool&  b) { return Q_LIKELY(impl) && (impl->tryBind(b) || (impl->isErrorFiltered(qBindExpectedBoolean        ) && impl->tryAny())); }
    bool tryBind(const        bool&  b) { return Q_LIKELY(impl) && (impl->tryBind(b) || (impl->isErrorFiltered(qBindExpectedBoolean        ) && impl->tryAny())); }
    bool tryBind(             bool&& b) { return Q_LIKELY(impl) && (impl->tryBind(b) || (impl->isErrorFiltered(qBindExpectedBoolean        ) && impl->tryAny())); }

    bool tryBind(           quint8&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(          quint16&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(          quint32&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(          quint64&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(const      quint8&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(const     quint16&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(const     quint32&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(const     quint64&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(           quint8&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(          quint16&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(          quint32&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }
    bool tryBind(          quint64&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedPositiveInteger) && impl->tryAny())); }

    bool tryBind(            qint8&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(           qint16&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(           qint32&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(           qint64&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(const       qint8&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(const      qint16&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(const      qint32&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(const      qint64&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(            qint8&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(           qint16&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(           qint32&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }
    bool tryBind(           qint64&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedInteger        ) && impl->tryAny())); }

    bool tryBind(            float&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedDecimal        ) && impl->tryAny())); }
    bool tryBind(           double&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedDecimal        ) && impl->tryAny())); }
    bool tryBind(const       float&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedDecimal        ) && impl->tryAny())); }
    bool tryBind(const      double&  n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedDecimal        ) && impl->tryAny())); }
    bool tryBind(            float&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedDecimal        ) && impl->tryAny())); }
    bool tryBind(           double&& n) { return Q_LIKELY(impl) && (impl->tryBind(n) || (impl->isErrorFiltered(qBindExpectedDecimal        ) && impl->tryAny())); }

    bool tryBind(       QByteArray&  v) { return Q_LIKELY(impl) && (impl->tryBind(v) || (impl->isErrorFiltered(qBindExpectedBytes          ) && impl->tryAny())); }
    bool tryBind(const  QByteArray&  v) { return Q_LIKELY(impl) && (impl->tryBind(v) || (impl->isErrorFiltered(qBindExpectedBytes          ) && impl->tryAny())); }
    bool tryBind(       QByteArray&& v) { return Q_LIKELY(impl) && (impl->tryBind(v) || (impl->isErrorFiltered(qBindExpectedBytes          ) && impl->tryAny())); }

    bool tryBind(         QVariant&  v) { return Q_LIKELY(impl) && (impl->tryBind(v) || (impl->isErrorFiltered(qBindUnexpectedValue        ) && impl->tryAny())); }
    bool tryBind(const    QVariant&  v) { return Q_LIKELY(impl) && (impl->tryBind(v) || (impl->isErrorFiltered(qBindUnexpectedValue        ) && impl->tryAny())); }
    bool tryBind(         QVariant&& v) { return Q_LIKELY(impl) && (impl->tryBind(v) || (impl->isErrorFiltered(qBindUnexpectedValue        ) && impl->tryAny())); }

    bool tryItem(QIdentifierLiteral  n) { return Q_LIKELY(impl) && impl->tryItem(n); }
    bool tryItem(QIdentifier&        n) { return Q_LIKELY(impl) && impl->tryItem(n); }
    bool tryItem(                     ) { return Q_LIKELY(impl) && impl->tryItem( ); }

    bool trySequenceOut() { return Q_LIKELY(impl) && impl->trySequenceOut(); }
    bool tryRecordOut  () { return Q_LIKELY(impl) && impl->tryRecordOut  (); }

    template<typename T> bool tryBind(T&& t);
private:
    template<class T> friend class QVal;
    template<class T> friend class QSeq;
    template<class T> friend class QRec;
    void setIllFormed() { if (Q_LIKELY(impl)) { impl->setIllFormed(); impl = nullptr; } }

    QCur _unsafeCopy() noexcept { return impl ? QCur(impl) : QCur(); } // FIXME replace with std::move(outer) to avoid resuming read/write when QCur was actually stopped!

    QAbstractValue* impl = nullptr;
};

//! A QValueEnd represents the end of a QValue traversal and processing
//!
//! \warning isValid does not take into account intermediate errors that are handled by QValueErrorFilter
//!
class QValueEnd {
public:
    QValueEnd() = default;
    QValueEnd(QCur&& result) : cursor(std::move(result)) {}
    bool           isValid() { return cursor.isValid(); }
    explicit operator bool() { return cursor.isValid(); }
private:
    QCur cursor;
};

//!< A pair of T value reference and defaultValue to use when reading any() other value
template<typename T> struct QDefaultValue { T& value; const T& defaultValue; };

// //////////////////////////////////////////////////////////////////////////
// Generic fluent interface for traversing structured values and processing them along the way:
// - serializing, deserializing
// - constructing generic in-memory data structures
// - etc...

template<class T_> class QRec; //!< a Record   data structure defined below
template<class T_> class QSeq; //!< a Sequence data structure defined below
template<class T_> class QVal; //!< a choice of sequence(), record(), null(), or values with at least a textual representation and possibly binary ones

using QValue    = QVal<QCur>;
using QSequence = QSeq<QCur>;
using QRecord   = QRec<QCur>;

// Custom bind support

#include <functional>

#ifndef NO_COMPILER_RTTI_OR_EXCEPTIONS
template<typename T> using QValFunction = QValueEnd(*)(T &,QValue   &&) ;
template<class   Ts> using QSeqFunction = QSequence(*)(Ts&,QSequence&&) ;
#endif
using QValLambda = std::function<QValueEnd(QValue   &&)>;
using QSeqLambda = std::function<QSequence(QSequence&&)>;


struct QSuspendedValueErrorHandler
{
    QSuspendedValueErrorHandler(QAbstractValue* v) : suspended(v->setErrorFilter(nullptr)), resume([v,this](){ v->setErrorFilter(this->suspended); }) {}
   ~QSuspendedValueErrorHandler() { resume(); }
private:
    QValueErrorFilter suspended;
    std::function<void(void)> resume;
};

template<class T_> class QVal
{
    Q_DISABLE_COPY(QVal)
public:
    QVal           (      ) = default;
    QVal           (QVal&&) = default;
    QVal& operator=(QVal&&) = default;
    ~QVal() {
        if (isValid() &&
            resumeErrorHandler() &&
            outer->isErrorFiltered(qBindUnexpectedValue) &&
            !outer->tryAny()) {
            outer->setIllFormed(); // do not attempt outer.out()
        }
    }
    explicit QVal(T_&& out) noexcept { outer = std::move(out); }

    explicit operator bool() const noexcept { return isValid(); }
    bool           isValid() const noexcept { return outer.isValid(); } //!< Drives QTransmogrifier<T>::bind() traversal
    QCur*       operator->()       noexcept { return outer.operator->(); }

    void setIsVariant() { suspendErrorHandler(); }

    /**/ T_ unexpected() { return resumeErrorHandler() && outer->isErrorFiltered(qBindUnexpectedValue) ? any() : T_(); }
    /**/ T_        any() { return outer->tryAny() && resumeErrorHandler() ? std::move(outer) : T_(); }

    QVal<T_> meta    (QIdentifierLiteral& n, QAsciiData& m) { outer->_meta(n,m); return std::move(*this); }

    QSeq<T_> sequence(quint32* s=nullptr) { return outer->trySequence          (s)  && resumeErrorHandler() ? QSeq<T_>(std::move(outer)) : QSeq<T_>(); }
    QRec<T_> record  (quint32* s=nullptr) { return outer->tryRecord            (s)  && resumeErrorHandler() ? QRec<T_>(std::move(outer)) : QRec<T_>(); }
    /**/ T_  null    (                  ) { return outer->tryNull              ( )  && resumeErrorHandler() ?          std::move(outer)  :      T_ (); }

    /**/ T_  bind    (     const char* u) { return outer->tryBind(QUtf8DataView(u)) && resumeErrorHandler() ?          std::move(outer)  :      T_ (); }
    /**/ T_  bind    (   QUtf8DataView u) { return outer->tryBind              (u)  && resumeErrorHandler() ?          std::move(outer)  :      T_ (); }
    /**/ T_  bind    (  QAsciiDataView a) { return outer->tryBind              (a)  && resumeErrorHandler() ?          std::move(outer)  :      T_ (); }
    /**/ T_  bind    (   QLatin1String l) { return outer->tryBind              (l)  && resumeErrorHandler() ?          std::move(outer)  :      T_ (); }
    /**/ T_  bind    (     QStringView u) { return outer->tryBind              (u)  && resumeErrorHandler() ?          std::move(outer)  :      T_ (); }

    template<typename T> T_  bind(             T&& t) { return outer->tryBind(std::forward <T>(t         )) && resumeErrorHandler() ? std::move(outer) : T_(); }
    template<typename T> T_  bind(T& t, T&& defaultT) { return bind(QDefaultValue<T>{t,std::forward<T>(defaultT)}); }

    // Custom bind support
    /**/                 T_  with(      QValLambda      customBind) { return customBind(   std::move(unsafeThis())).isValid() && resumeErrorHandler() ? std::move(outer) : T_(); }
    template<typename T> T_  with(T& t, QValFunction<T> customBind) { return customBind(t, std::move(unsafeThis())).isValid() && resumeErrorHandler() ? std::move(outer) : T_(); }

    // Literal metadata support
    QVal<T_> meta(const char*                n, const char*        m) { return meta(QIdentifierLiteral(n),QAsciiData(m)); }
    QVal<T_> meta(      QIdentifierLiteral&& n,       QAsciiData&& m) { QIdentifierLiteral nref=n; QAsciiData mref=           m ; return meta(nref, mref); }
    QVal<T_> meta(const QIdentifierLiteral&  n,       QAsciiData&& m) { QIdentifierLiteral nref=n; QAsciiData mref=           m ; return meta(nref, mref); }
    QVal<T_> meta(const QIdentifierLiteral&  n, const char*        m) { QIdentifierLiteral nref=n; QAsciiData mref=QAsciiData(m); return meta(nref, mref); }
    QVal<T_> meta(      QIdentifierLiteral&& n, const QAsciiData&  m) { QIdentifierLiteral nref=n; QAsciiData mref=           m ; return meta(nref, mref); }

    QSeq<T_> sequence(quint32     s) { return sequence(&s); }
    QRec<T_> record  (quint32     s) { return record  (&s); }
    QRec<T_> record  (const char* n) { return meta(qmName,QAsciiData(n)).record(); }

    // Shortcuts
    template<typename T> QSeq<T_> operator<<(T&& t) { return sequence().bind(std::forward<T>(t)); } // stream compatible
private:
    QValue unsafeThis() noexcept { return QValue(outer->_unsafeCopy()); }

    void suspendErrorHandler() {
        if (isValid()) {
            if (suspended) {
                qWarning("Already suspended");
            }
            suspended = outer->setErrorFilter(nullptr);
        }
    }
    bool resumeErrorHandler() {
        Q_ASSERT_X(isValid(), Q_FUNC_INFO, "Failed to call resumeErrorHandler before std::move(outer)");
        if (suspended) {
            if (outer->setErrorFilter(suspended) != nullptr) {
                qWarning("QValueErrorFilter set while suspended is lost");
            }
            suspended = nullptr;
        }
        return true; // just to call from boolean expressions
    }

    QValueErrorFilter suspended = nullptr;
    T_ outer = T_(); //!< moved context of current traversal up to QCur that will point to the value itself (be it a QIODevice or QCborValue, ...)

    static bool ignoreError(QIdentifierLiteral,QString){ return true; }
};

template<class T_> class QSeq
{
    Q_DISABLE_COPY(QSeq)
public:
    Q_ENABLE_MOVE_DEFAULT(QSeq)
    ~QSeq() {
        if (isValid() &&
            outer->isErrorFiltered(qBindUnexpectedEnd) &&
            !outer->trySequenceOut()) {
            outer->setIllFormed();
        }
    }

    explicit operator bool() const noexcept { return isValid(); }
    bool           isValid() const noexcept { return outer.isValid(); } //!< Drives QTransmogrifier<T>::bind() traversal
    QCur*       operator->()       noexcept { return outer.operator->(); }

    T_              out() { return outer->trySequenceOut() ? std::move(outer) : T_(); }

    QVal<QSeq<T_>> item() { return outer->tryItem() ? QVal<QSeq<T_>>(std::move(*this)) : QVal<QSeq<T_>>(); }

    // Shortcuts
    QSeq<QSeq<T_>> sequence(quint32* s=nullptr) { return item().sequence          (s) ; }
    QRec<QSeq<T_>> record  (quint32* s=nullptr) { return item().record            (s) ; }
    /**/ QSeq<T_>  null    (                  ) { return item().null              ( ) ; }
    /**/ QSeq<T_>  any     (                  ) { return item().any               ( ) ; }
    /**/ QSeq<T_>  bind    (     const char* u) { return item().bind(QUtf8DataView(u)); }
    /**/ QSeq<T_>  bind    (   QUtf8DataView u) { return item().bind              (u) ; }
    /**/ QSeq<T_>  bind    (  QAsciiDataView a) { return item().bind              (a) ; }
    /**/ QSeq<T_>  bind    (   QLatin1String l) { return item().bind              (l) ; }
    /**/ QSeq<T_>  bind    (     QStringView u) { return item().bind              (u) ; }

    template<typename T> QSeq<T_>  bind    (      T&&        t) { return item().bind(  std::forward<T>(       t)); }
    template<typename T> QRec<T_>  bind    (T& t, T&& defaultT) { return item().bind(t,std::forward<T>(defaultT)); }

    // Custom bind support
    /**/                 QSeq<T_>  with    (        QSeqLambda       customBind) { return bool(customBind(    std::move(unsafeThis()))) ? std::move(*this) : QSeq<T_>(); }
    template<class   Ts> QSeq<T_>  with    (Ts& ts, QSeqFunction<Ts> customBind) { return bool(customBind(ts, std::move(unsafeThis()))) ? std::move(*this) : QSeq<T_>(); }
    template<class   Ts> QSeq<T_>  forEach (Ts& ts, QValueEnd(*)(typename Ts::value_type&, QValue&&)
                                                  , bool(*)(const typename Ts::value_type&) = [](const typename Ts::value_type&) { return true; });

    // Shortcut
    template<typename T> QSeq<T_> operator<<(T&& t) { return item().bind(std::forward<T>(t)); } // stream compatible
private:
    template<class T> friend class QVal;
    explicit QSeq(T_&& out) noexcept { outer = std::move(out); }

    template<typename T, typename TEnabledIf> friend struct QTransmogrifier;
    QValue    unsafeItem() noexcept { return outer->tryItem() ? QValue(outer->_unsafeCopy()) : QValue(); }
    QSequence unsafeThis() noexcept { return                 QSequence(outer->_unsafeCopy())           ; }

    T_ outer = T_();
};

template<class T_> class QRec
{
    Q_DISABLE_COPY(QRec)
public:
    Q_ENABLE_MOVE_DEFAULT(QRec)
    ~QRec() {
        if (isValid() &&
            outer->isErrorFiltered(qBindUnexpectedEnd) &&
            !outer->tryRecordOut()) {
            outer->setIllFormed();
        }
    }

    explicit operator bool() const noexcept { return isValid(); }
    bool           isValid() const noexcept { return outer.isValid(); } //!< Drives QTransmogrifier<T>::bind() traversal
    QCur*       operator->()       noexcept { return outer.operator->(); }

    T_             out() { return outer->tryRecordOut() ? std::move(outer) : T_(); }

    // TODO ignore qBindUnexpectedItem using tryAny() in case QCur::impl is not caching out-of-order items ?!
    QVal<QRec<T_>> item(QIdentifier&       n) { return outer->tryItem(n) || (outer->isErrorFiltered(qBindUnexpectedItem, n.latin1()) && false) ? QVal<QRec<T_>>(std::move(*this)) : QVal<QRec<T_>>(); }
    QVal<QRec<T_>> item(QIdentifierLiteral n) { return outer->tryItem(n) || (outer->isErrorFiltered(qBindUnexpectedItem, n.latin1()) && false) ? QVal<QRec<T_>>(std::move(*this)) : QVal<QRec<T_>>(); }
    QVal<QRec<T_>> item(const char*        n) { return item(QIdentifierLiteral(n)); }

    // Shortcuts
    QSeq<QRec<T_>> sequence(const char* n, quint32* s=nullptr) { return item(n).sequence          (s) ; }
    QRec<QRec<T_>> record  (const char* n, quint32* s=nullptr) { return item(n).record            (s) ; }
    /**/ QRec<T_>  null    (const char* n                    ) { return item(n).null              ( ) ; }
    /**/ QRec<T_>  bind    (const char* n,      const char* u) { return item(n).bind(QUtf8DataView(u)); }
    /**/ QRec<T_>  bind    (const char* n,    QUtf8DataView u) { return item(n).bind              (u) ; }
    /**/ QRec<T_>  bind    (const char* n,   QAsciiDataView a) { return item(n).bind              (a) ; }
    /**/ QRec<T_>  bind    (const char* n,    QLatin1String l) { return item(n).bind              (l) ; }
    /**/ QRec<T_>  bind    (const char* n,      QStringView u) { return item(n).bind              (u) ; }

    template<typename T> QRec<T_> bind(const char* n, T&& t              ) { return item(n).bind(   std::forward<T>(       t)); }
    template<typename T> QRec<T_> bind(const char* n, T&  t, T&& defaultT) { return item(n).bind(t, std::forward<T>(defaultT)); }

    // Custom bind support
    /**/                 QRec<T_> with(const char* n,       QValLambda      customBind) { return item(n).with(   customBind); }
    template<typename T> QRec<T_> with(const char* n, T& t, QValFunction<T> customBind) { return item(n).bind(t, customBind); }
private:
    template<class T> friend class QVal;
    explicit QRec(T_&& out) noexcept { outer = std::move(out); }

    template<typename T, typename TEnabledIf> friend struct QTransmogrifier;
    QValue unsafeItem(QIdentifier& n) noexcept { return outer->tryItem(n) ? QValue(outer._unsafeCopy()) : QValue(); }

    T_ outer = T_();
};

//! QTransmogrifier is a class of template methods binding T parts with QValue obeying a simple generic data model, and returning a QValueEnd
//! Each QTransmogrifier<_,T> should provide bind(T&&) and bind(T&) overloads for convenience and may provide bind(const T&) for performance depending on T
//!
//! Its least specialized definition calls a T::bind(QValue&&) method that is more convenient to define than a QTransmogrifier specialization
//! \see Person::bind() definition in main.cpp for an example
//!
//! \remark bind is not a template function to avoid conflicts with ADL and allow more precise class specialization
//! \remark QValue is a move-only type passed by rvalue reference following http://scottmeyers.blogspot.com/2014/07/should-move-only-types-ever-be-passed.html
//!
template<typename T, typename TEnabledIf=void>
struct QTransmogrifier {
    static QValueEnd zap(QValue&& value,      T&  t) { return                t .zap(std::move(value)); } // In case of error, define a T::bind(QValue) method or external QTransmogrifier<T>::bind(QValue,T&)
    static QValueEnd zap(QValue&& value,const T&  t) { return const_cast<T&>(t).zap(std::move(value)); } // In case of error, define a T::bind(QValue) method or external QTransmogrifier<T>::bind(QValue,const T&)
    static QValueEnd zap(QValue&& value,      T&& t) { return                t .zap(std::move(value)); } // In case of error, define a T::bind(QValue) method or external QTransmogrifier<T>::bind(QValue,T&&)
};

template<class T>
bool QCur::tryBind(T&& t) {
    return QTransmogrifier<RemoveCvRef<T>>::zap(QValue(_unsafeCopy()),std::forward<T>(t)).isValid();
}

// //////////////////////////////////////////////////////////////////////////
// Base QAbstractValue implementations for Read and Write QValueMode
static char *qulltoa2(char *p, qulonglong n, int base=10) //!< Reproduced here for benchmark purposes only
{
    *--p = '\0';
    const char b = 'a' - 10;
    do {
        const int c = n % base;
        n /= base;
        *--p = c + (c < 10 ? '0' : b);
    } while (n);
    return p;
}
static char * qlltoa2(char *p,  qlonglong n, int base=10) //!< Reproduced here for benchmark purposes only
{
    if (n < 0) {
        p = qulltoa2(p, qulonglong(-(1 + n)) + 1, base);
        *--p = '-';
    } else {
        p = qulltoa2(p, qulonglong(n), base);
    }
    return p;
}

#include <QtCore/qdatastream.h>

//! Base QAbstractValue implementation with QValueMode::Write
struct QAbstractValueWriter : public QAbstractValue
{
    virtual ~QAbstractValueWriter() noexcept = default;

    virtual QValueMode mode() const noexcept { return QValueMode::Write; }

    virtual bool tryItem(                    ) = 0;
    virtual bool tryItem(QIdentifier&       n) = 0;
    virtual bool tryItem(QIdentifierLiteral n) { QIdentifier id(n); return tryItem(id); }

    //! End of sequence or record
    //! Few QAbstractValueWriter need to process this (contiguous QAbstractValueWriter need to mark the end of indefinite sequences and records for instance)
    virtual bool tryOut () { return true; }

    virtual bool tryBind( QUtf8DataView u) = 0;
    virtual bool tryBind(   const char* u) { return tryBind(QUtf8DataView(u)); } // required to match better than QString overloads
    virtual bool tryBind(QAsciiDataView a) { return tryBind(QUtf8DataView(a.data(), a.size())); }
    virtual bool tryBind( QLatin1String l) { return tryBind(QString(l)); }
    virtual bool tryBind(   QStringView s) { return tryBind(QUtf8DataView(s.toUtf8())); }

    virtual bool tryBind(   QUtf8Data&& s) { return tryBind(QUtf8DataView(s)); }
    virtual bool tryBind(     QString&& s) { return tryBind(QStringView(s.data(), s.size())); }
    virtual bool tryBind(        bool&& s) { return tryBind(s ? QAsciiDataView("true") : QAsciiDataView("false")); }
    virtual bool tryBind(       qint8&& n) { return tryBind( qint64(n)); }
    virtual bool tryBind(      quint8&& n) { return tryBind(quint64(n)); }
    virtual bool tryBind(      qint16&& n) { return tryBind( qint64(n)); }
    virtual bool tryBind(     quint16&& n) { return tryBind(quint64(n)); }
    virtual bool tryBind(      qint32&& n) { return tryBind( qint64(n)); }
    virtual bool tryBind(     quint32&& n) { return tryBind(quint64(n)); }
    virtual bool tryBind(      qint64&& n) { const int s=66; char c[s]; char* start= qlltoa2(c+s, n); return tryBind(QAsciiDataView(start,(c+s)-start-1)); }
    virtual bool tryBind(     quint64&& n) { const int s=66; char c[s]; char* start=qulltoa2(c+s, n); return tryBind(QAsciiDataView(start,(c+s)-start-1)); }
    virtual bool tryBind(       float&& n) { static QByteArray s; s.setNum(double(n),'g',std::numeric_limits<   float>::max_digits10); return tryBind(QAsciiDataView(s.constData(),s.size())); } // with specific precision
    virtual bool tryBind(      double&& n) { static QByteArray s; s.setNum(       n ,'g',std::numeric_limits<  double>::max_digits10); return tryBind(QAsciiDataView(s.constData(),s.size())); } // with specific precision
    virtual bool tryBind(  QByteArray&& s) { QByteArray h; h.reserve(s.size()*2+2+1); h.append("0x").append(s.toHex())               ; return tryBind(QAsciiDataView(h.constData(),h.size())); }
    virtual bool tryBind(  QVariant&& src) {
        if (src.canConvert<QVariantList>()) {
            QSequentialIterable ts = src.value<QSequentialIterable>();
            quint32 size=quint32(ts.size());
            if (!trySequence(&size)) {
                return false;
            }
            for (QVariant t : ts) {
                if (!tryItem() || !tryBind(QVariant(t))) {
                    return false;
                }
            }
            return trySequenceOut();
        }
        if (src.canConvert<QVariantMap >()) { // TODO _meta(qmColumns,"key,value") trySequence() ...
            QAssociativeIterable ts = src.value<QAssociativeIterable>();
            quint32 size=quint32(ts.size());
            if (!tryRecord(&size)) {
                return false;
            }
            QAssociativeIterable::const_iterator i = ts.begin();
            const QAssociativeIterable::const_iterator end = ts.end();
            for ( ; i != end; ++i) {
                QIdentifier id(i.key().toString().toLatin1());
                if (!tryItem(id) || !tryBind(QVariant(*i))) {
                    return false;
                }
            }
            return tryRecordOut();
        }
        if (src.userType()
                      ==qMetaTypeId<QUtf8Data>()) return tryBind(src.value<  QUtf8Data>());
        if (src.type()==QVariant::String        ) return tryBind(src.value<    QString>());
        if (src.type()==QVariant::Char          ) return tryBind(src.          toString());
        if (src.type()==QVariant::Bool          ) return tryBind(src.value<       bool>());
        if (src.type()==QVariant::LongLong      ) return tryBind(src.value<  qlonglong>()); // or  qint64 to be sure to call the good tryBind()?
        if (src.type()==QVariant::ULongLong     ) return tryBind(src.value< qulonglong>()); // or quint64 to be sure to call the good tryBind()?
        if (src.type()==int(QMetaType::ULong   )) return tryBind(src.value<    quint32>());
        if (src.type()==int(QMetaType::Long    )) return tryBind(src.value<     qint32>());
        if (src.type()==int(QMetaType::Short   )) return tryBind(src.value<      short>());
        if (src.type()==int(QMetaType::Char    )) return tryBind(src.value<       char>());
        if (src.type()==int(QMetaType::UShort  )) return tryBind(src.value<     ushort>());
        if (src.type()==int(QMetaType::UChar   )) return tryBind(src.value<      uchar>());
        if (src.type()==int(QMetaType::SChar   )) return tryBind(src.value<signed char>());
        if (src.type()==QVariant::UInt          ) return tryBind(src.value<       uint>());
        if (src.type()==QVariant::Int           ) return tryBind(src.value<        int>());
        if (src.type()==QVariant::Double        ) return tryBind(src.value<     double>());
        if (src.type()==int(QMetaType::Float   )) return tryBind(src.value<      float>());
        // See QT_FOR_EACH_STATIC_PRIMITIVE_TYPE in qmetatype.h

        // QDataStream format is the only Qt data format that is read-write and easily extensible by users
        // It is binary and includes src.typeName() for QMetaType::User types
        // QByteArray will be encoded into a QVariant to avoid ambiguities when decoding bytes

        QByteArray binaryVariant; QDataStream s(&binaryVariant, QIODevice::WriteOnly);
        s << src;
        if (s.status()==QDataStream::Ok) {
            return tryBind(binaryVariant);
        }

        if (src.isNull()) return tryNull();
        return false;
    }

    virtual bool tryBind(       QUtf8Data& r) {  QUtf8Data copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(         QString& r) {    QString copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(            bool& r) {       bool copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(           qint8& r) {      qint8 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(          quint8& r) {     quint8 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(          qint16& r) {     qint16 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(         quint16& r) {    quint16 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(          qint32& r) {     qint32 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(         quint32& r) {    quint32 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(          qint64& r) {     qint64 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(         quint64& r) {    quint64 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(           float& r) {      float copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(          double& r) {     double copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(      QByteArray& r) { QByteArray copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(        QVariant& r) {   QVariant copy(r); return tryBind(std::move(copy)); }

    virtual bool tryBind(const  QUtf8Data& r) {  QUtf8Data copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const    QString& r) {    QString copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const       bool& r) {       bool copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const      qint8& r) {      qint8 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const     quint8& r) {     quint8 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const     qint16& r) {     qint16 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const    quint16& r) {    quint16 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const     qint32& r) {     qint32 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const    quint32& r) {    quint32 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const     qint64& r) {     qint64 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const    quint64& r) {    quint64 copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const      float& r) {      float copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const     double& r) {     double copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const QByteArray& r) { QByteArray copy(r); return tryBind(std::move(copy)); }
    virtual bool tryBind(const   QVariant& r) {   QVariant copy(r); return tryBind(std::move(copy)); }

    virtual bool tryAny() { return false; }
};

//! Base QAbstractValue implementations with QValueMode::Read
struct QAbstractValueReader : public QAbstractValue
{
    virtual ~QAbstractValueReader() noexcept = default;

    virtual QValueMode mode() const noexcept { return QValueMode::Read; }

    virtual bool tryItem(                    ) = 0;
    virtual bool tryItem(QIdentifier&       n) = 0;
    virtual bool tryItem(QIdentifierLiteral n) {
        QIdentifier id;
        for(;;) {
            if (!tryItem(id))
                return false;
            if (id.utf8()==n.utf8())
                return true;
            if (isErrorFiltered(qBindUnexpectedItem, n.latin1()) && tryAny())
                return true;
            setIllFormed();
            return true;
        }
    }

    virtual bool tryBind(   const char* u) { return tryBind(QUtf8DataView(u)); }
    virtual bool tryBind( QUtf8DataView u) { QUtf8Data r; if (!tryBind(r)) return false; Q_VERIFY(r.utf8()==u.data() || isErrorFiltered(qBindExpectedConstant)) return true; }
    virtual bool tryBind(QAsciiDataView a) { QUtf8Data r; if (!tryBind(r)) return false; Q_VERIFY(r.utf8()==a.data() || isErrorFiltered(qBindExpectedConstant)) return true; }
    virtual bool tryBind( QLatin1String l) { QString   r; if (!tryBind(r)) return false; Q_VERIFY(r       ==l        || isErrorFiltered(qBindExpectedConstant)) return true; }
    virtual bool tryBind(   QStringView s) { QString   r; if (!tryBind(r)) return false; Q_VERIFY(r       ==s        || isErrorFiltered(qBindExpectedConstant)) return true; }

    virtual bool tryBind(  QUtf8Data& s) = 0;
    virtual bool tryBind(    QString& s) { QUtf8Data u; if (!tryBind(u)) return false; s=QString::fromUtf8(u.utf8()); return true; }
    virtual bool tryBind(       bool& b) { QUtf8Data u; if (!tryBind(u)) return false;
                                           if      (u.utf8().compare("true" , Qt::CaseInsensitive)==0) { b=true ; }
                                           else if (u.utf8().compare("false", Qt::CaseInsensitive)==0) { b=false; }
                                           else { Q_VERIFY(isErrorFiltered(qBindExpectedBoolean)) } return true; }

    virtual bool tryBind(      qint8& n) {    qint64 l; if (!tryBind(l)) return false; if (l<std::numeric_limits<  qint8>::min()  ) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits<  qint8>::min(); return true; }
                                                                                       if (  std::numeric_limits<  qint8>::max()<l) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits<  qint8>::max(); return true; } n=  qint8(l); return true; }
    virtual bool tryBind(     quint8& n) {   quint64 l; if (!tryBind(l)) return false; if (  std::numeric_limits< quint8>::max()<l) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits< quint8>::max(); return true; } n= quint8(l); return true; }
    virtual bool tryBind(     qint16& n) {    qint64 l; if (!tryBind(l)) return false; if (l<std::numeric_limits< qint16>::min()  ) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits< qint16>::min(); return true; }
                                                                                       if (  std::numeric_limits< qint16>::max()<l) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits< qint16>::max(); return true; } n= qint16(l); return true; }
    virtual bool tryBind(    quint16& n) {   quint64 l; if (!tryBind(l)) return false; if (  std::numeric_limits<quint16>::max()<l) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits<quint16>::max(); return true; } n=quint16(l); return true; }
    virtual bool tryBind(     qint32& n) {    qint64 l; if (!tryBind(l)) return false; if (l<std::numeric_limits< qint32>::min()  ) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits< qint32>::min(); return true; }
                                                                                       if (  std::numeric_limits< qint32>::max()<l) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits< qint32>::max(); return true; } n= qint32(l); return true; }
    virtual bool tryBind(    quint32& n) {   quint64 l; if (!tryBind(l)) return false; if (  std::numeric_limits<quint32>::max()<l) { Q_VERIFY(isErrorFiltered(qBindExpectedSmallerInteger)) n=std::numeric_limits<quint32>::max(); return true; } n=quint32(l); return true; }
    virtual bool tryBind(      float& n) {    double d; if (!tryBind(d)) return false;
            float rounded = d; // rounds to nearest value (inf if qAbs(d)>std::numeric_limits<float>::max()
            if (rounded != d && !isErrorFiltered(qBindExpectedLessPreciseDecimal)) {
                return false;
            }
            n = rounded;
            return true;
        }
    virtual bool tryBind(     qint64& n) { QUtf8Data s; if (!tryBind(s)) return false; bool isOk=false;  qint64 v; v=s.utf8().toLongLong (&isOk); if (isOk) { n=v; return true; } Q_VERIFY(isErrorFiltered(qBindExpectedInteger        )) return true; }
    virtual bool tryBind(    quint64& n) { QUtf8Data s; if (!tryBind(s)) return false; bool isOk=false; quint64 v; v=s.utf8().toULongLong(&isOk); if (isOk) { n=v; return true; } Q_VERIFY(isErrorFiltered(qBindExpectedPositiveInteger)) return true; }
    virtual bool tryBind(     double& n) { QUtf8Data s; if (!tryBind(s)) return false; bool isOk=false;  double v; v=s.utf8().toDouble   (&isOk); if (isOk) { n=v; return true; } Q_VERIFY(isErrorFiltered(qBindExpectedDecimal        )) return true; }
    virtual bool tryBind( QByteArray& b) { QUtf8Data s; if (!tryBind(s)) return false; return toByteArray(b, s); }
    virtual bool tryBind( QVariant& dst) {
        {
            QSuspendedValueErrorHandler(this);
            quint32 size=0; QIdentifier key; QVariant item;
            if (trySequence(&size)) { QVariantList l; while (tryItem(   )) { if (!tryBind(item)) { item = QVariant(); if (!(isErrorFiltered(qBindUnexpectedValue) && tryAny())) { setIllFormed(); return true; } } l.append(              item); } dst = l; return trySequenceOut(); }
            if (tryRecord  (&size)) { QVariantMap  l; while (tryItem(key)) { if (!tryBind(item)) { item = QVariant(); if (!(isErrorFiltered(qBindUnexpectedValue) && tryAny())) { setIllFormed(); return true; } } l.insert(key.latin1(), item); } dst = l; return tryRecordOut  (); }
            bool        b; if (tryBind( b)) { dst = QVariant(b);                  return true; }
            quint64     u; if (tryBind( u)) { dst = QVariant(u);                  return true; }
            qint64      l; if (tryBind( l)) { dst = QVariant(l);                  return true; }
            double      d; if (tryBind( d)) { dst = QVariant(d);                  return true; }
            QByteArray ba; if (tryBind(ba)) { toVariant(dst,ba);                  return true; }
            QUtf8Data  u8; if (tryBind(u8)) { dst = QVariant::fromValue(u8);      return true; }
            QString     t; if (tryBind( t)) { dst = QVariant(t);                  return true; }
            /**/           if (tryNull(  )) { dst = QVariant::fromValue(nullptr); return true; }
        }
        return false;
    }

    virtual bool tryBind(const  QUtf8Data&  k) {  QUtf8Data r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const    QString&  k) {    QString r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const       bool&  k) {       bool r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const      qint8&  k) {      qint8 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const     quint8&  k) {     quint8 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const     qint16&  k) {     qint16 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const    quint16&  k) {    quint16 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const     qint32&  k) {     qint32 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const    quint32&  k) {    quint32 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const     qint64&  k) {     qint64 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const    quint64&  k) {    quint64 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const      float&  k) {      float r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; } // TODO silent warning
    virtual bool tryBind(const     double&  k) {     double r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; } // TODO silent warning
    virtual bool tryBind(const QByteArray&  k) { QByteArray r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(const   QVariant&  k) {   QVariant r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }

    virtual bool tryBind(       QUtf8Data&& k) {  QUtf8Data r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(         QString&& k) {    QString r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(            bool&& k) {       bool r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(           qint8&& k) {      qint8 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(          quint8&& k) {     quint8 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(          qint16&& k) {     qint16 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(         quint16&& k) {    quint16 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(          qint32&& k) {     qint32 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(         quint32&& k) {    quint32 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(          qint64&& k) {     qint64 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(         quint64&& k) {    quint64 r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(           float&& k) {      float r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; } // TODO silent warning
    virtual bool tryBind(          double&& k) {     double r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; } // TODO silent warning
    virtual bool tryBind(      QByteArray&& k) { QByteArray r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }
    virtual bool tryBind(        QVariant&& k) {   QVariant r; if (!tryBind(r)) return false; Q_VERIFY(k==r || isErrorFiltered(qBindExpectedConstant)); return true; }

    virtual bool tryAny() {
        QSuspendedValueErrorHandler(this);
        if (  (trySequence() && trySequenceOut())
            ||(tryRecord  () && tryRecordOut  ())) {
            return true;
        }
        bool b; // usually easier to check than types below
        double d; // matches most used numbers more easily than types below
        qint64 i; // matches most used integrals more easily than types below
        quint64 u;
        QVariant v;
        QByteArray bytes;
        QString s; // matches any type with usually less encoding work than QUtf8Data (which only captures efficiently utf-8 whereas QChar easily captures Latin1 in addition to ASCII)
        return tryBind(b) || tryBind(d) || tryBind(i) || tryBind(u) || tryBind(v) || tryBind(bytes) || tryNull();
    }
protected:
    bool toByteArray(QByteArray& b, QUtf8Data s) {
        const QByteArray& bytes = s.utf8();
        if (!bytes.startsWith("0x") || s.size()%2==1) return false;
        b.reserve((s.size()-2)/2);
        auto fromHex = [](char a) -> char { return '0'<=a && a<='9' ? a-'0' : 'A'<=a && a<='F' ? a-'A'+10 : 'a'<=a && a<='f' ? a-'a'+10 : -1; };
        for (int i=0; i < b.size(); i++) {
            char highDigit=fromHex(bytes[2+2*i]), lowDigit=fromHex(bytes[2+2*i+1]);
            if (highDigit<0 || lowDigit<0)
                return false;
            b.append(char(highDigit<<4)+lowDigit);
        }
        return true;
    }
    //! QByteArray may either contain a QVariant serialized by QDataStream or raw bytes
    void toVariant(QVariant& v, const QByteArray& b) {
        QDataStream s(b); s >> v;
        if (s.status()==QDataStream::Ok) {
            return;
        }
        v = QVariant(b);
    }
};

// //////////////////////////////////////////////////////////////////////////
// Dynamic and deferred QAbstractValueWriter support

#include <QtCore/qatomic.h>

class QLogger : public QCur
{
    static QAtomicPointer<QAbstractValueWriter> s_impl;
public:
    static QAbstractValueWriter* setWriter(QAbstractValueWriter* i) { return s_impl.fetchAndStoreOrdered(i); }

    QLogger() : QCur(s_impl) {}
};

// TODO in .cpp QAtomicPointer<QAbstractValueWriter> QLogger::s_impl = nullptr;

#include <functional>

//! A QTransmogrifier<T> polymorphic in T (erased at compile-time) which is captured by reference or copy (for rvalue references) to be bound later
//! \warning a T&& may be allocated dynamically if its size exceeds compiler's Small Function Optimization
//! \warning copying should be forced using QBindLater(T(t)) if QBindLater::bind() must be called after t is destructed like when queued to another thread for writing
class QBindLater
{
    QValLambda f;
public:
    QBindLater() {}
    template<typename T> QBindLater(T&  t) : f(/*captured T ref */[&t] (QValue&& v) { return v.bind(t); } ) {}
    template<typename T> QBindLater(T&& t) : f(/*captured T copy*/[ t] (QValue&& v) { return v.bind(t); } ) {}

    QValueEnd zap(QValue&& v) { return f(std::move(v)); }
};

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier partial specializations (usually not generic on v->mode() for dynamically-sized or builtin types)

template<class T_>
template<class Ts>
QSeq<T_> QSeq<T_>::forEach(Ts& ts,
                         QValueEnd(*itemBind)(typename Ts::value_type&, QValue&&),
                         bool(*itemPredicate)(const typename Ts::value_type&)) {
    if ((*this)->mode()==Write) {
        for (auto&& t : ts) {
            if (itemPredicate(t)) {
                itemBind(t, unsafeItem());
            }
        }
        return std::move(*this);
    }
    else if ((*this)->mode()==Read) {
        auto&& i = ts.begin();
        QValue item;
        while ((item = unsafeItem())) {
            typename Ts::value_type newItem;
            if (itemBind(newItem, std::move(item)) && itemPredicate(newItem)) {
                if (i != ts.end()) {
                    *i = newItem;
                }
                else {
                    ts.insert(i, newItem);
                }
            }
        }
        return std::move(*this);
    }
    else { Q_ASSERT_X(!this, Q_FUNC_INFO, "Unsupported implementation mode()"); return null(); }
}

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier partial specializations (usually not generic on v->mode() for dynamically-sized or builtin types)

#include <QtCore/qmetatype.h>
#include <QtCore/qmetaobject.h>

// For Q_DEFINE_ZAP_WITH_METAOBJECT below
template<class T>
QValueEnd qMetaZap(QValue&& v, T* t) {
    auto rw = v->mode();
    auto mo = T::staticMetaObject;
    auto r  = v.meta(qmName,QAsciiData(mo.className())).record();
    for (int i = 0; r && i<mo.propertyCount(); i++) {
        auto p = mo.property(i);
        if (p.isStored()) {
            if (rw==Read) {
                QVariant pv;
                QIdentifier id(p.name());
                r = r.item(id).bind(pv);
                if (!r || !pv.isValid() || pv.isNull()) {
                    if (p.isResettable() && !p.resetOnGadget(t) && !v->isErrorFiltered(qBindUnexpectedItem)) {
                        return QValueEnd();
                    }
                }
                else {
                    if (p.isEnumType()) {
                        pv.convert(QVariant::Int);
                    }
                    if (!p.writeOnGadget(t, pv) && !v->isErrorFiltered(qBindUnexpectedItem)) {
                        return QValueEnd();
                    }
                }
            }
            else if (rw==Write) {
                QVariant pv = p.readOnGadget(t);
                if (!pv.isValid() && !v->isErrorFiltered(qBindUnexpectedItem)) {
                    return QValueEnd();
                }
                else {
                    QIdentifier id(p.name());
                    auto i = r.item(id);
                    if (pv.isValid() && p.isEnumType()) {
                        pv.convert(QVariant::Int);
                        i = i.meta(qmName, p.isFlagType() ?
                            QAsciiData(p.enumerator().valueToKeys(pv.value<int>())) :
                            QAsciiData(p.enumerator().valueToKey (pv.value<int>())));
                    }
                    if  (pv.userType()==qMetaTypeId<QUtf8Data>() ) r = i.bind(pv.value<   QUtf8Data>());
                    else if (pv.type()==QVariant::String         ) r = i.bind(pv.value<     QString>());
                    else if (pv.type()==QVariant::Char           ) r = i.bind(pv.           toString());
                    else if (pv.type()==QVariant::Bool           ) r = i.bind(pv.value<        bool>());
                    else if (pv.type()==QVariant::LongLong       ) r = i.bind(pv.value<   qlonglong>()); // or  qint64 to be sure to call the good tryBind()?
                    else if (pv.type()==QVariant::ULongLong      ) r = i.bind(pv.value<  qulonglong>()); // or quint64 to be sure to call the good tryBind()?
                    else if (pv.type()==int(QMetaType::ULong    )) r = i.bind(pv.value<     quint32>());
                    else if (pv.type()==int(QMetaType::Long     )) r = i.bind(pv.value<      qint32>());
                    else if (pv.type()==int(QMetaType::Short    )) r = i.bind(pv.value<       short>());
                    else if (pv.type()==int(QMetaType::Char     )) r = i.bind(pv.value<        char>());
                    else if (pv.type()==int(QMetaType::UShort   )) r = i.bind(pv.value<      ushort>());
                    else if (pv.type()==int(QMetaType::UChar    )) r = i.bind(pv.value<       uchar>());
                    else if (pv.type()==int(QMetaType::SChar    )) r = i.bind(pv.value< signed char>());
                    else if (pv.type()==QVariant::UInt           ) r = i.bind(pv.value<        uint>());
                    else if (pv.type()==QVariant::Int            ) r = i.bind(pv.value<         int>());
                    else if (pv.type()==QVariant::Double         ) r = i.bind(pv.value<      double>());
                    else if (pv.type()==int(QMetaType::Float    )) r = i.bind(pv.value<       float>());
                    // See QT_FOR_EACH_STATIC_PRIMITIVE_TYPE in qmetatype.h
                    else if (pv.canConvert<QVariantList>()       ) r = i.bind(pv.value<QVariantList>()); // TODO avoid interim data structure
                    else if (pv.canConvert<QVariantMap >()       ) r = i.bind(pv.value<QVariantMap >()); // TODO avoid interim data structure
                    else if (pv.isNull()                         ) r = i.null();
                    else if (v->isErrorFiltered(qBindUnexpectedValue)) r = i.any();
                }
            }
            else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
        }
    }
    return r.out();
}

//! Default bind(QValue&&) based on static QMetaObject reflection
#define Q_DEFINE_ZAP_WITH_METAOBJECT(Class) QValueEnd zap(QValue&& v) { return qMetaZap<Class>(std::move(v), this); }

template<typename T>
struct QTransmogrifier<QDefaultValue<T>> {
    static QValueEnd zap(QValue&& v, QDefaultValue<T>&& t) {
        if (v->mode()!=Read) {
            return v.bind(t.value);
        } else {
            v.setIsVariant();
            auto r = v.bind(t.value);
            if (!r) {
                t.value = t.defaultValue;
                return v.any();
            }
            return r;
        }
    }
};

template<typename T>
struct QTransmogrifier<T, typename std::enable_if<std::is_unsigned<T>::value && std::is_integral<T>::value && !std::is_same<T,bool>::value>::type> {
    static QValueEnd zap(QValue&& v, T&& t) {
        return v.bind(quint64(t));
    }
    static QValueEnd zap(QValue&& v, T&  t) {
        if (v->mode()==Write) {
            return bind(std::move(v),T(t));
        }
        else if (v->mode()==Read) {
            quint64 u; auto r=v.bind(u); if (r) { t=u; } return r;
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};
template<typename T>
struct QTransmogrifier<T, typename std::enable_if<std::is_signed<T>::value && std::is_integral<T>::value && !std::is_same<T,bool>::value>::type> {
    static QValueEnd zap(QValue&& v, T&& t) {
        return v.bind(qint64(t));
    }
    static QValueEnd zap(QValue&& v, T&  t) {
        if (v->mode()==Write) {
            return bind(std::move(v),T(t));
        }
        else if (v->mode()==Read) {
            qint64 u; auto r=v.bind(u); if (r) { t=u; } return r;
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

// TODO QUrl, QRegularExpression, QMimeData, etc.

template<typename T, unsigned Size>
struct QTransmogrifier<T[Size]> {
    static QValueEnd zap(QValue&& v, T(& ts)[Size]) {
        if (v->mode()==Write) {
            quint32 size=Size;
            auto s(v.sequence(&size));
            for (auto&& t : ts) {
                s = s.bind(t);
            }
            return s.out();
        }
        else if (v->mode()==Read) {
            auto s(v.sequence());
            QVal<QSequence> i; unsigned read=0;
            while ((i = s.item())) {
                Q_ASSERT(read<=Size);
                if (read==Size) {
                    if (!i->isErrorFiltered(qBindUnexpectedItem)) {
                        return QValueEnd();
                    }
                    s = i.any();
                }
                else {
                    s = i.bind(ts[read]); // gives back control to s, enabling the next s.item() call
                }
                ++read;
            }
            if (read!=Size && !i->isErrorFiltered(qBindExpectedItem)) {
                return QValueEnd();
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

#include <QtCore/qvector.h>

template<typename T>
struct QTransmogrifier<QVector<T>> {
    static QValueEnd zap(QValue&& v, QVector<T>&& ts) {
        if (v->mode()==Write) {
            quint32 size=ts.size();
            auto s(v.sequence(&size));
            for (auto&& t : ts) {
                s = s.bind(t);
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QVector<T>& ts) {
        if (v->mode()==Write) {
            quint32 size=ts.size();
            auto s(v.sequence(&size));
            for (auto&& t : ts) {
                s = s.bind(t);
            }
            return s.out();
        }
        else if (v->mode()==Read) {
            auto s(v.sequence());
            QVal<QSequence> i; int read = 0;
            while ((i = s.item())) {
                Q_ASSERT(read<=ts.size());
                if (read==ts.size()) {
                    T t{};
                    if ((s = i.bind(t))) // gives back control to s, enabling the next s.item() call
                        ts.insert(read, t);
                }
                else {
                    s = i.bind(ts[read]);
                }
                ++read; // regardless of errors handled internally by bind
            }
            if (read!=ts.size()) {
                if (!s->isErrorFiltered(qBindExpectedItem)) { // default behaviour is to ignore (filter) no errors
                    return QValueEnd(); // fail-fast
                }
                else { // second-hand behaviour is to ignore (filter) all errors allowing partial Read (views)
                    int notRead = ts.size()-read;
                    if (s->isErrorFiltered(qBindKeepItemsNotRead, QString::number(notRead))) {
                        Q_UNUSED(notRead)
                    }
                    else { // third-hand behaviour is to filter qBindExpectedItem but not qBindKeepItemsNotRead
                        ts.remove(read, notRead);
                    }
                }
            }
            return s.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

#include <QtCore/qmap.h>

// TODO QTransmogrifier<QMap<TKey,T>,_> with meta(qmColumns,"key,value").sequence().record().bind("key",i.key()).bind("value",i.value())

template<typename T>
struct QTransmogrifier<QMap<QString,T>> {
    static QValueEnd zap(QValue&& v, QMap<QString,T>&& ts) {
        if (v->mode()==Write) {
            quint32 size=quint32(ts.size());
            auto r(v.record(&size));
            for (QString key : ts.keys()) {
                r = r.bind(key.toLatin1().constData(),ts[key]);
            }
            return r.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
    static QValueEnd zap(QValue&& v, QMap<QString,T>& ts) {
        if (v->mode()==Write) {
            quint32 size=quint32(ts.size());
            auto r(v.record(&size));
            for (QString key : ts.keys()) {
                r = r.bind(key.toLatin1().constData(),ts[key]);
            }
            return r.out();
        }
        else if (v->mode()==Read) {
            auto r(v.record());
            QIdentifier name; QVal<QRecord> i; int read = 0;
            while ((i = r.item(name))) {
                auto key = ts.find(name.latin1());
                if (key==ts.end()) {
                    T value{};
                    if ((r = i.bind(value)))
                        ts.insert(key, value);
                }
                else {
                    r = i.bind(ts[key]);
                }
                ++read;
            }
            if (read!=ts.size() && !r->isErrorFiltered(qBindExpectedItem)) { // default behaviour is to ignore (filter) no errors
                return QValueEnd(); // fail-fast
            }
            return r.out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
};

// --------------------------------------------------------------------------
// QTransmogrifier<QValue> for the set of QAbstractValue's BindNative types

template<> //!< \remark Enumerating types where BindSupport<_> = BindNative with C++11 seems hard, so we just handle the set of default QAbstractValue BindNative types
struct QTransmogrifier<QValue> {
    static QValueEnd zap(QValue&& src, QValue&& dst) {
        if (src->mode()==Read && dst->mode()==Write) {
            src.setIsVariant();
            QSequence srcSeq; QSequence dstSeq;
            if (     (srcSeq = src.sequence())) {
                /**/  dstSeq = dst.sequence();

                QValue srcVal; QValue dstVal;
                while((srcVal = srcSeq.unsafeItem())) { // using item()'s QVal<QSequence would cause an infinite compiler loop to generate corresponding QTransmogrifier<QVal<QSeq<QSeq<...>>,_> functions
                       dstVal = dstSeq.unsafeItem();
                       srcVal.bind(std::move(dstVal));
                }

                Q_UNUSED(dstSeq.out())
                return   srcSeq.out();
            }
            QRecord srcRec; QRecord dstRec;
            if (   (srcRec = src.record())) {
                /**/dstRec = dst.record();

                QIdentifier srcKey; QValue srcVal; QValue dstVal;
                while((srcVal = srcRec.unsafeItem(srcKey))) {
                       dstVal = dstRec.unsafeItem(srcKey);
                       srcVal.bind(std::move(dstVal));
                }

                Q_UNUSED(dstRec.out())
                return   srcRec.out();
            }
            QValueEnd srcEnd;
            QUtf8Data  u; if ((srcEnd = src.bind(  u))) { dst.bind(  u); return srcEnd; } //! \remark QUtf8Data has common valus with QString but is more precise and efficient
            QString    t; if ((srcEnd = src.bind(  t))) { dst.bind(  t); return srcEnd; }
            // Explicitely handled types (not value text representation convertible to those types
            QByteArray a; if ((srcEnd = src.bind(  a))) { dst.bind(  a); return srcEnd; }
            bool       b; if ((srcEnd = src.bind(  b))) { dst.bind(  b); return srcEnd; }

            quint8    u8; if ((srcEnd = src.bind( u8))) { dst.bind( u8); return srcEnd; } //! \remark quint8 has common values with qint8 but is preferred for convenience and static guarantees (no negative values)
            qint8     l8; if ((srcEnd = src.bind( l8))) { dst.bind( l8); return srcEnd; }
            quint16  u16; if ((srcEnd = src.bind(u16))) { dst.bind(u16); return srcEnd; } //! \remark Bigger numeric types can only be handled if QAbstractValue refuses to lose sign or leading digits
            qint16   l16; if ((srcEnd = src.bind(l16))) { dst.bind(l16); return srcEnd; }
            quint32  u32; if ((srcEnd = src.bind(u32))) { dst.bind(u32); return srcEnd; }
            qint32   l32; if ((srcEnd = src.bind(l32))) { dst.bind(l32); return srcEnd; }

            quint64  u64; if ((srcEnd = src.bind(u64))) { dst.bind(u64); return srcEnd; }
            qint64   l64; if ((srcEnd = src.bind(l64))) { dst.bind(l64); return srcEnd; }
            float      f; if ((srcEnd = src.bind(  f))) { dst.bind(  f); return srcEnd; } //! \remark Floating point numeric types can only be handled if QAbstractValue refuses to lose non zero decimals
            double     d; if ((srcEnd = src.bind(  d))) { dst.bind(  d); return srcEnd; } //! \remark Double-precision floating point numeric types can only be handled if QAbstractValue refuses to lose leading digits or precision
            // TODO Other BindNative types we do not want to treat as text: QDateTime, QUuid
            /**/          if ((srcEnd = src.null(   ))) { dst.null(   ); return srcEnd; }
            Q_UNUSED(dst.unexpected())
            return   src.unexpected();
        }
        else if (src->mode()==Write && dst->mode()==Read) { return zap(std::move(dst),std::move(src)); } // To return reader's QCur instead of writer's QCur since it should contain more information (mismatches, etc.)
        else { Q_ASSERT_X(!src || !dst, Q_FUNC_INFO, "Unsupported src->mode() and dst->mode()"); return src.null(); }
    }
    static QValueEnd zap(QValue&& v1, QValue& v2) {
        return zap(std::move(v1),std::move(v2));
    }
};

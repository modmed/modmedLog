/****************************************************************************
 * **
 * ** Copyright (C) 2019 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <type_traits>

#include <QtCore/qxmlstream.h>

#include "QValue.h"

// //////////////////////////////////////////////////////////////////////////
// QXml* support

//! \warning Since XML has no standard way of encoding data structures, we choose a rather implicit one
//! based on the fact that XML elements are implicit data structures unless they contain text without elements,
//! in which case it is customary to let the reader interpret XML text representation as specific data types.
//! \li sequence  :       element named after outer item or metaData[qmName] or "sequence"
//! \li record    :       element named after outer item or metaData[qmName] or "record"
//! \li null      : empty element named after outer item or metaData[qmName] or "null"
//! \li BindNative: text  element named after outer item or metaData[qmName] or XSD typename, containing the default text representation
//! \see https://www.w3.org/TR/xmlschema-2/#built-in-datatypes
//! \note Selecting heterogeneous sequence items reliably can still be made using XPath "*[i]"
//! \note A more explicit encoding variant could add implicit type information as an attribute to enable roundtrips to/from other generic data structures at the expense of space
class QXmlWriter : public QAbstractValueWriter
{
    Q_DISABLE_COPY(QXmlWriter)
public:
    QXmlWriter(QXmlStreamWriter* io) : io(io) { Q_ASSERT(io); }

    QValueMode mode() const noexcept { return QValueMode::Write; }

    // Shortcuts
    QValue    value   (                  ) { return QCur(this).value(); }
    QSequence sequence(quint32* s=nullptr) { return QCur(this).value().sequence(s); }

    template<typename T> QValueEnd bind(T&& t) { return QCur(this).value().bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* =nullptr) { io->writeStartElement(tag("sequence" ).latin1()); att(); return !io->hasError(); }
    bool tryRecord  (quint32* =nullptr) { io->writeStartElement(tag("record"   ).latin1()); att(); return !io->hasError(); }
    bool tryAny     (                 ) { io->writeEmptyElement(tag("undefined").latin1()); att(); return !io->hasError(); }
    bool tryNull    (                 ) { io->writeEmptyElement(tag("null"     ).latin1()); att(); return !io->hasError(); }
    bool tryBind    (  QUtf8DataView u) { return writeText("string" ,QString::fromUtf8(u.data(), u.size())                ); }
    bool tryBind    (    QStringView s) { return writeText("string" ,                  s.toString()                       ); }
    bool tryBind    (      QString&& s) { return writeText("string" ,                  s                                  ); }
    bool tryBind    (         bool&& b) { return writeText("boolean", b ? QStringLiteral("true") : QStringLiteral("false")); }
    bool tryBind    (       qint64&& n) { static QString s; s.setNum(       n                                               ); return writeText("integer"  , s); }
    bool tryBind    (      quint64&& n) { static QString s; s.setNum(       n                                               ); return writeText("integer"  , s); }
    bool tryBind    (        float&& n) { static QString s; s.setNum(double(n),'g',std::numeric_limits< float>::max_digits10); return writeText("decimal"  , s); } // with specific precision
    bool tryBind    (       double&& n) { static QString s; s.setNum(       n ,'g',std::numeric_limits<double>::max_digits10); return writeText("decimal"  , s); } // with specific precision
    bool tryBind    (   QByteArray&& s) { QString h; h.reserve(s.size()*2+2+1); h.append("0x").append(s.toHex().toUpper())   ; return writeText("hexBinary", h); }

    bool tryOut () { io->writeEndElement(); return !io->hasError(); }
    bool tryItem() { return true; }
    bool tryItem(QIdentifier& n) { name=n; return true; }
    void _meta(QIdentifierLiteral &n, QAsciiData &m);
private:
    class Atts : public std::vector<std::pair<QIdentifierLiteral,QAsciiData>>
    {
    public:
        using vector::vector;
        iterator          find(QIdentifierLiteral n) { return std::find_if(begin(), end(), [n](std::pair<QIdentifierLiteral,QAsciiData>& item){ return item.first==n; }); }
        QAsciiData& operator[](QIdentifierLiteral n) { return find(n)->second; }
    };

    bool writeText(const char* def, const QString& text) {
        io->writeStartElement(tag(def).latin1());
        att();
        QString valid;
        valid.reserve(text.size());
        for (QChar c : text) {
            valid.append(c<'\x20' && c!='\x9' && c!='\xA' && c!='\xD' ? '?' : c);
        }
        io->writeCharacters(valid);
        io->writeEndElement();
        return !io->hasError();
    }
    QIdentifier tag(const char* def) { if (name.isNull()) { return QIdentifier(def); } auto n=name; name=QIdentifier(); return n; }
    void att() { for (auto&& a : atts) { if (!a.second.isNull()) io->writeAttribute(a.first.latin1(), a.second.latin1()); } }

    QXmlStreamWriter* io;
    Atts atts;
    QIdentifier name;
};

void QXmlWriter::_meta(QIdentifierLiteral& n, QAsciiData& m) {
    if (n == qmName) {
        name = QIdentifier(m.utf8());
    }
    else {
        auto found = atts.find(n);
        if (found != atts.end()) {
            found->second = m;
        }
        else {
            atts.push_back(std::make_pair(n,m));
        }
    }
}


#include "data.h"

#include<QtCore/qdebug.h>
QDebug &operator<<(QDebug &out, const Phone &p) { out.nospace() << "Phone(" << p._t << ", " << p._n << ")"; return out; }

#include<QtCore/qdatastream.h>
QDataStream &operator<<(QDataStream &out, const Phone &p) { return out << quint8(p._t) << p._n ; }
QDataStream &operator>>(QDataStream &out,       Phone &p) { quint8 t; out >> t >> p._n ; p._t=Phone::Type(t); return out; }

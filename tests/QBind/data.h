#pragma once

#include <QtCore/qmetaobject.h>

#include "QValue.h"

class Phone {
    Q_GADGET
    Q_PROPERTY(Type    type   MEMBER _t)
    Q_PROPERTY(QString number MEMBER _n)
public:
    Q_DEFINE_ZAP_WITH_METAOBJECT(Phone) // making it (de)serializable and printable using reflection

    enum Type : quint8 { Unknown=0, Mobile, Home, Office };
    Q_ENUM(Type)

    Phone(Type t=Unknown, QString n=QString()) : _t(t), _n(n) {}

//private:
    Type _t; QString _n;
};

Q_DECLARE_METATYPE(Phone)

// For comparison purposes:

#include<QtCore/qdebug.h>
QDebug      &operator<<(QDebug      &out, const Phone &p);
#include<QtCore/qdatastream.h>
QDataStream &operator<<(QDataStream &out, const Phone &p);
QDataStream &operator>>(QDataStream &out,       Phone &p);

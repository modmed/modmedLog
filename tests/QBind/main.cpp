/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/

// The core of QTransmogrifier proof-of-concept including:
// - a QValue fluent interface for describing logical data structures, providing auto completion and well-formedness guarantees
// - a set of recursive QTransmogrifier<T> functors binding QAbstractValue and T according to their logical data structure

#include "QValue.h"

// Extensible set of QAbstractValue implementations providing concrete syntax to the logical data structure in a specific Mode among Write, Read, ...

#include "QJson_impl.h"     // QJsonWriter, QJsonReader, QJsonBuilder, QJsonVisitor and QTransmogrifier<QJsonValue> support
#include "QCbor_impl.h"     // QCborWriter demonstrating the performance potential of the approach
#include "QData_impl.h"     // QDataReader, QDataWriter demonstrating a QDataStream with implicit types for benchmarking
#include "QVariant_impl.h"  // QVariantBuilder, QVariantVisitor and QTransmogrifier<QVariant,_> support
#include "QModel_impl.h"    // Q*ModelWriter support
#include "QXml_impl.h"      // QXmlWriter support
#include "QSettings_impl.h" // QSettings* support

#ifdef PROTOBUF
#include "persons.pb.h"
#endif

#include <QtCore/qvariant.h>

#include <QtGui/qstandarditemmodel.h>
#include <QtWidgets/qtreeview.h>
#include <QtWidgets/qtableview.h>
#include <QtWidgets/qdialog.h>
#include <QtWidgets/qgridlayout.h>
#include <QtWidgets/qtabwidget.h>
#include <QtWidgets/qlabel.h>
#include <QtWidgets/qapplication.h>

#include <tuple>

// //////////////////////////////////////////////////////////////////////////
// QValue-enabled types examples

// Using Qt's meta-object system

#include "data.h"

// Using an internal bind method

struct Person
{
    int id; QString firstName, lastName; double height; int age; QVector<Phone> phones; QString comments; QList<Person> children;

//#define NO_QBIND_CODE
#ifndef NO_QBIND_CODE
    QValueEnd zap(QValue&& value) { // works with value->mode()==Read as well as Write
        return value
            .record("Person")
                .sequence("names")
                    .bind(firstName)
                    .bind( lastName)
                    .out()
                .bind("height"  ,height  )
                .bind("age"     ,age  ,-1) // reads unexpected values as -1
                .bind("phones"  ,phones  ) // recursive calls to QTransmogrifier will take care of that part
                .bind("comments",comments)
                .bind("children",children)
                .out(); // not calling this would still automagically close opened record() but always trigger a qBindUnexpectedEnd to help write explicitly correct code
    }
#endif
};

//#define NO_OTHER_CODE
#ifndef NO_OTHER_CODE
// For comparison purposes:

#include<QtCore/qdebug.h>
QDebug &operator<<(QDebug &out, const Person &p)
{
    return out.nospace() << "Person(" // items count and names are implicit
            << p.firstName << ", " << p.lastName << ", " // sequence is implicit
            << p.height    << ", "
            << p.age       << ", "
            << p.phones    << ", "
            << p.comments  << ", "
            << p.children  << ")";
}
#include<QtCore/qdatastream.h>
QDataStream &operator<<(QDataStream &out, const Person &p)
{
    return out // record, items count and names are implicit
            << p.firstName << p.lastName // sequence is implicit
            << p.height
            << p.age
            << p.phones
            << p.comments
            << p.children
            ; // no record delimiter
}
//! Same as operator<<(QDataStream&, Person&) with no possibility to detect errors other than premature end of QDataStream
QDataStream &operator>>(QDataStream &in, Person &p)
{
    return in >> p.firstName >> p.lastName >> p.height >> p.age >> p.phones >> p.comments >> p.children;
}

// Using a view type to customize an existing bind using metadata

struct PersonsView
{
    QList<Person>& persons;

    QValueEnd zap(QValue&& value) {
        return value
            .meta(qmChildren,"children")
            .meta(qmColumns ,"names,height,age,phones,comments") // to optimize multi-dimensional data formats
            .bind(persons);
    }
};

// View types can even bind nested structures into flattened tables using a foreign key to express parent 1-n children relationships

struct PersonsTable {
    QList<Person>& persons;
    PersonsTable(QList<Person>& ps) : persons(ps) {}

    QValueEnd zap(QValue&& v) {
        if (v->mode()==Read) {
            ids.clear();
            orphans.clear();
            persons.clear();
            Person p;
            auto s(v.sequence());
            while ((s = bind(p, std::move(s)))) {
                // merge orphans with persons at the end only
            }
            for (int parentId: orphans.keys()) {
                if (parentId && ids.contains(parentId)) {
                    ids[parentId]->children = orphans[parentId]; //! \bug read access violation due to Qt6 behaviour changes?
                }
                else {
                    persons.append(orphans[parentId]);
                }
            }
            return s.out();
        }
        else if (v->mode()==Write) {
            parentIds.clear();
            ids.clear();
            return flatten(persons, v.sequence()).out();
        }
        else { Q_ASSERT_X(!v, Q_FUNC_INFO, "Unsupported v->mode()"); return v.any(); }
    }
private:
    QStack<int> parentIds;
    QMap<int,Person*> ids;
    QMap<int,QList<Person>> orphans;

    QSequence bind(Person& p, QSequence&& s) {
        if (s->mode()==Read) {
            int parentId=0;
            double height_ft=0.;
            s = s.record()
                    .bind("id"        , p.id       )
                    .bind("parent_id" , parentId   )
                    .bind("first_name", p.firstName)
                    .bind("last_name" , p.lastName )
                    .bind("height_ft" , height_ft  )
                    .bind("age"       , p.age      )
                    .bind("comments"  , p.comments ) // TODO Remove while keeping p.comments untouched
                    .bind("phones"    , p.phones   )
                    .out();
            if (s) {
                p.height = height_ft/3.28;
                orphans[parentId].append(p);
                if (p.id) {
                    Q_ASSERT(!ids.contains(p.id)); // children may be attributed to any of the duplicates
                    ids.insert(p.id, &orphans[parentId].last());
                }
            }
            return std::move(s);
        }
        else if (s->mode()==Write) {
            if (!p.id) {
                int newId=(2019-p.age)*10;
                while (ids.contains(newId)) {
                    newId++;
                    Q_ASSERT(newId<(2020-p.age)*10);
                }
                p.id = newId;
                ids.insert(p.id,&p);
            }

            s = s.record()
                    .bind("id"        , p.id)
                    .bind("parent_id" , parentIds.isEmpty() ? 0 : parentIds.back())
                    .bind("first_name", p.firstName  )
                    .bind("last_name" , p.lastName )
                    .bind("height_ft" , p.height*3.28)
                    .bind("age"       , p.age      )
                    .bind("comments"  , p.comments )
                    .bind("phones"    , p.phones   )
                    .out();

            parentIds.push_back(p.id);
            s = flatten(p.children, std::move(s));
            parentIds.pop_back();

            return std::move(s);
        }
        else { Q_ASSERT_X(!s, Q_FUNC_INFO, "Unsupported v->mode()"); return std::move(s); }
    }

    QSequence flatten(QList<Person>& ps, QSequence&& s) {
        for (auto&& p : ps) {
            s = bind(p, std::move(s));
        }
        return std::move(s);
    }
};

// //////////////////////////////////////////////////////////////////////////
// QTransmogrifier<T> examples using external bind methods

#include <QtGui/qcolor.h>

//! QTransmogrifier<QColor> with special support for QDataStream compatibility using meta()
template<>
struct QTransmogrifier<QColor> {
    static QValueEnd zap(QValue&& v, QColor&& c) {
        QColor copy(c);
        return zap(std::move(v),copy);
    }
    static QValueEnd zap(QValue&& v, QColor& c) {
        if (!c.isValid()) {
            return v.null();
        }
        QIdentifierLiteral n;
        QAsciiData m;
        do {
            v=v.meta(n, m);
            if (n==qmDataStreamVersion) {
                if (m.utf8().toInt()<7) {
                    Q_UNUSED(v->isErrorFiltered("UnsupportedQDataStreamVersion"))
                    return v.null();
                }
                QRgba64 c64 = c.rgba64();
                return v.sequence().bind(quint8(c.spec())).bind(quint16(c64.alpha())).bind(quint16(c64.red())).bind(quint16(c64.green())).bind(quint16(c64.blue())).bind(quint16(0)).out(); // TODO pad
            }
        } while (!n.isNull());
        QRecord r = v.record();
        switch(c.spec()) {
        case QColor::Spec::Rgb : r = r.sequence("RGB" ).bind(quint8(c.red   ())).bind(quint8(c.green        ())).bind(quint8(c.blue     ()))                        .out(); break;
        case QColor::Spec::Hsl : r = r.sequence("HSL" ).bind(qint16(c.hslHue())).bind(quint8(c.hslSaturation())).bind(quint8(c.lightness()))                        .out(); break;
        case QColor::Spec::Hsv : r = r.sequence("HSV" ).bind(qint16(c.hsvHue())).bind(quint8(c.hsvSaturation())).bind(quint8(c.value    ()))                        .out(); break;
        case QColor::Spec::Cmyk: r = r.sequence("CMYK").bind(quint8(c.cyan  ())).bind(quint8(c.magenta      ())).bind(quint8(c.yellow   ())).bind(quint8(c.black())).out(); break;
        default: Q_ASSERT(false);
        }
        if (c.alpha()<255) { r = r.bind("alpha",quint8(c.alpha())); }
        return r.bind("base",quint8(255)).out(); // Explicits ARGBCMYKSLV components' base (H base is 360 corresponding to color wheel degrees, with -1 used for gray in addition to S being 0)
                                                 // QDebug use of float values removes need for base but may lose precision
    }
};

// //////////////////////////////////////////////////////////////////////////
// QAbstractValue basic implementation example

#include <type_traits>
#include <QtCore/qbytearray.h>

class TextWriter : public QAbstractValueWriter
{
    Q_DISABLE_COPY(TextWriter)
public:
    TextWriter(QByteArray* utf8) : utf8(utf8) { Q_ASSERT(utf8); }

    // Shortcuts
    template<typename T> QSequence operator<<(T&& t) { return QCur(this).value().sequence().item().bind(std::forward<T>(t)); }
    template<typename T> QValueEnd bind      (T&& t) { return QCur(this).value()                  .bind(std::forward<T>(t)); }
protected:
    bool trySequence(quint32* =nullptr) { if (0<level++) utf8->append("["); return true; }
    bool tryRecord  (quint32* =nullptr) { if (0<level++) utf8->append("{"); return true; }
    bool tryAny     (                 ) {                utf8->append("*"); return true; }
    bool tryNull    (                 ) {                                   return true; }
    bool tryBind    (  QUtf8DataView u) { utf8->append(u.data(), u.size()); return true; }
    bool tryBind    (        float&& n) { static QByteArray s; s.setNum(double(n),'g',6); return tryBind(QUtf8DataView(s.constData(), s.size())); } // with QDebug precision
    bool tryBind    (       double&& n) { static QByteArray s; s.setNum(       n ,'g',6); return tryBind(QUtf8DataView(s.constData(), s.size())); } // with QDebug precision

    bool tryItem(QIdentifier& n) { utf8->append(" ").append(n.utf8()).append(":"); return true; }
    bool tryItem(              ) { utf8->append(" ")                             ; return true; }

    bool trySequenceOut() { if (0<--level) utf8->append("]"); return true; }
    bool tryRecordOut  () { if (0<--level) utf8->append("}"); return true; }

    void _meta(QIdentifierLiteral& n, QAsciiData& m) { if (n==qmName) utf8->append('(').append(m.utf8()).append(")"); }
private:
    template<class T> friend class QModelWriter;
    QByteArray* utf8;
    int level = 0;
};

class Text
{
    QByteArray result;
public:
    template<typename T> Text(T&& t) { TextWriter(&result).bind(std::forward<T>(t)); }
    const QByteArray& utf8() const { return result; }
    operator    QByteArray() const { return result; }
};

// //////////////////////////////////////////////////////////////////////////
// Tests and Benchmarks

// Base dataset
static const char* ascii = "ascii characters are common in QDebug";
static QColor color(45,0,186);
static QVector<double> transform{1./3, 2./3, 1./3, 1.,
                                 2./3, 1./3, 2./3, 1.,
                                 1./3, 2./3, 1./3, 1.,
                                 0.  , 0.  , 0.  , 1.};
#else
class Text
{
    QByteArray result;
public:
    Text(Person& p) { result = QString("names:[%1 %2] height:%3 age:%4 phones[%5]:... comments:... children[%6]:...")
                                .arg(p.firstName).arg(p.lastName).arg(p.height).arg(p.age).arg(p.phones.size()).arg(p.children.size()).toUtf8(); }
    const QByteArray& utf8() const { return result; }
    operator    QByteArray() const { return result; }
};
#endif

static Person person{0,"John","Doe",1.75,18,{},"unicode is likely U+01 \x01 + U+1F \x1F + U+A4 ¤ U+B0 ° U+D8 Ø U+FF ÿ",QList<Person>()};
static const double PI=3.141592653589793;
static Phone phone {Phone::Home,"+44 1234567"};

#include <QtCore/qelapsedtimer.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
#include <QtCore/qbuffer.h>
#include <QtCore/qdatastream.h>
#include <QtCore/qdebug.h>
#include <QtCore/qjsonvalue.h>
#include <QtCore/qjsondocument.h>
#include <QtCore/qcborstream.h>

#include <QtGui/qcolor.h>

// NB: It is not possible to use QBENCHMARK to evaluate qDebug because it installs a specific handler

#ifndef NO_OTHER_CODE

#define GROUP(group) if (argc<=1 || strcmp(argv[1],group)==0) { \
    double previousTotal=0., groupTotal=0.; \
    bool groupWarmup; \
    do { \
    groupWarmup=(previousTotal<0.01 || std::abs(groupTotal-previousTotal)*100/previousTotal > 1.); \
    previousTotal=groupTotal; \
    groupTotal=0.; \
    if (previousTotal<0.01) { fprintf(results,"============"); fprintf(samples, "%-14s|================================================================================\n", group); } else fprintf(results,"%-12s",group);

#define GROUP_STOP \
    if (previousTotal<0.01) fprintf(results,"|total(usecs)|variation(%%)\n"); else fprintf(results,"|%12.1f|%5.1f\n", groupTotal, previousTotal>=0.01 ? std::abs(groupTotal-previousTotal)*100/previousTotal : 0); \
    } while (groupWarmup); }

static QElapsedTimer item;
#define START item.start(); for (int i=0; i<123; ++i)
#define STOP(label,result) { \
    auto usecs=double(item.nsecsElapsed())/1000/123; groupTotal+=usecs; \
    if (previousTotal<0.01) { fprintf(results,"|%14s", label); fprintf(samples, "%-14s|%s\n", label, qUtf8Printable(result)); } else fprintf(results,"|%14.1f", usecs); }

#else
#   define GROUP(group)
#   define GROUP_STOP
#   define START
#   define STOP(label,result) fprintf(samples, "%-14s|%s\n", label, qUtf8Printable(result));
#endif

//#define NO_MANUAL_CODE
#ifndef NO_MANUAL_CODE
inline bool qJsonDocumentWritePerson(QByteArray &ba, const Person &person)
{
    static QJsonDocument doc; // static to exclude ctor from benchmark

    QJsonObject p{
        {"names"   , QJsonArray{person.firstName, person.lastName} },
        {"height"  , person.height},
        {"age"     , person.age},
        {"comments", person.comments},
        {"children", QJsonArray()}}; // would need recursion
    QJsonArray phones;
    for (auto&& phone : person.phones) {
        phones.append(QJsonObject{
            {"type"  ,quint8(phone._t)},
            {"number",       phone._n }});
    }
    p["phones"] = phones;
    doc.setObject(p);
    ba = doc.toJson(QJsonDocument::Compact);
    return !ba.isEmpty();
}

inline bool qJsonDocumentReadPerson(QByteArray &ba, Person &person)
{
    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(ba, &error);
    if (error.error!=QJsonParseError::NoError) return false;
    if (!doc["names"].isArray()) return false;
    QJsonArray names = doc["names"].toArray();
    if (!(names.size()>0 && names[0].isString())) return false;
    person.firstName = names[0].toString();
    if (!(names.size()>1 && names[1].isString())) return false;
    person.lastName  = names[1].toString();
    if (!doc["height"  ].isDouble()) return false;
    person.height = doc["height"  ].toDouble();
    if (!doc["age"].isDouble()) return false;

    if (!doc["phones"].isArray()) return false;
    QJsonArray phones = doc["phones"].toArray();
    for (auto&& i : phones) {
        if (!i.isObject()) return false;
        QJsonObject phone = i.toObject();
        Phone p;
        if (!phone["type"].isDouble()) return false;
        p._t = Phone::Type(doc["type"].toDouble());
        if (!phone["number"].isString()) return false;
        p._n = doc["number"].toString();
    }

    person.age = doc["age"].toDouble();
    if (!doc["comments"].isString()) return false;
    person.comments = doc["comments"].toString();

    if (!doc["children"].isArray()) return false;
    // Would need recursion

    return true;
}

inline bool qCborStreamWritePerson(QIODevice *b, const Person &person)
{
    static QCborStreamWriter s((QIODevice *)nullptr); // static to exclude ctor from benchmark

    s.setDevice(b);
    s.startMap();
        s.append("names");s.startArray();
            s.append(person.firstName);
            s.append(person. lastName);
            s.endArray();
        s.append("height");s.append(person.height  );
        s.append("age"   );s.append(person.age     );
        int size = person.phones.size();
        s.append("phones");s.startArray(quint64(size));
            for (int i=0; i < size; i++) {
                s.startMap();
                    s.append("type"  );s.append(person.phones[i]._t);
                    s.append("number");s.append(person.phones[i]._n);
                    s.endMap();
            }
            s.endArray();
        s.append("comments");s.append(person.comments);
        s.append("children");s.startArray(quint64(person.children.size()));
            // Would need recursion
            s.endArray();
    s.endMap();
    return s.device()->errorString().isEmpty();
}

inline bool qCborStreamReadPerson(QIODevice *io, Person& p)
{
    static QCborStreamReader reader; // static to exclude ctor from benchmark

    bool parsed;
    reader.setDevice(io);
    reader.reset();
    if (reader.isMap() &&
        reader.enterContainer() &&
        reader.hasNext() &&
        reader.isString() &&
        reader.readString().data=="names" &&
        reader.readString().status==QCborStreamReader::EndOfString &&
        reader.isArray() &&
        reader.enterContainer() &&
        reader.hasNext() &&
        reader.isString())
        p.firstName=reader.readString().data;

    if (reader.readString().status==QCborStreamReader::EndOfString &&
        reader.hasNext() &&
        reader.isString())
        p.lastName=reader.readString().data;

    if (reader.readString().status==QCborStreamReader::EndOfString &&
        !reader.hasNext() &&
        reader.leaveContainer() &&
        reader.hasNext() &&
        reader.isString() &&
        reader.readString().data=="height" &&
        reader.readString().status==QCborStreamReader::EndOfString &&
        reader.isDouble())
        p.height=reader.toDouble();

    if (reader.next() &&
        reader.hasNext() &&
        reader.isString() &&
        reader.readString().data=="age" &&
        reader.readString().status==QCborStreamReader::EndOfString &&
        !reader.isUnsignedInteger() &&
        reader.isNegativeInteger())
        p.age=int(reader.toInteger());

    if (reader.next() &&
        reader.hasNext() &&
        reader.isString() &&
        reader.readString().data=="phones" &&
        reader.readString().status==QCborStreamReader::EndOfString &&
        reader.isArray() &&
        reader.enterContainer())
        while(reader.hasNext()) {
            Phone ph;
            if (reader.isMap() &&
                reader.enterContainer() &&
                reader.hasNext() &&
                reader.isString() &&
                reader.readString().data=="type" &&
                reader.readString().status==QCborStreamReader::EndOfString &&
                reader.isInteger())
                ph._t=Phone::Type(reader.toInteger());

            if (reader.next() &&
                reader.hasNext() &&
                reader.isString() &&
                reader.readString().data=="number" &&
                reader.readString().status==QCborStreamReader::EndOfString &&
                reader.isString())
                ph._n=reader.readString().data;

            if (reader.readString().status==QCborStreamReader::EndOfString &&
                !reader.hasNext() &&
                reader.leaveContainer())
                p.phones.append(ph);
        }

    if (reader.leaveContainer() &&
        reader.hasNext() &&
        reader.isString() &&
        reader.readString().data=="comments" &&
        reader.readString().status==QCborStreamReader::EndOfString &&
        reader.isString())
        p.comments=reader.readString().data;

    parsed =
        reader.readString().status==QCborStreamReader::EndOfString &&
        reader.hasNext() &&
        reader.isString() &&
        reader.readString().data=="children" &&
        reader.readString().status==QCborStreamReader::EndOfString &&
        reader.hasNext() &&
        reader.isArray() &&
        reader.enterContainer() &&
        !reader.hasNext() &&
        reader.leaveContainer() &&
        !reader.hasNext() &&
        reader.leaveContainer();

    return parsed;
}
#endif

void doGuiExample();

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Generated files
    FILE* results; fopen_s(&results, "../QBind/results.csv", "w");
    FILE* samples; fopen_s(&samples, "../QBind/samples.txt", "w");
    if (!(results && samples))
        return -1;

    // Temporary buffers
    QString s;
    QByteArray ba; ba.reserve(1000);
    QBuffer b(&ba); b.open(QIODevice::ReadWrite);
    QVariant v;
    QDataStream d(&b);

    QStringList errors;
    auto handler = [&errors](QIdentifierLiteral error, QVariant context) {
#ifndef NO_OTHER_CODE
        QByteArray ba;
        TextWriter(&ba).bind(context);
        errors.append(ba.append(':'));
#endif
        errors.append(error.latin1());
        return true;
    };

#ifndef NO_OTHER_CODE
    GROUP("builtin")//========================================================
    {
        START {
            s.resize(0);
            QDebug(&s)
                << 1.333333333333f
                << PI
                << ascii
                << false
                << color
                ;
        }
        STOP("QDebug",s)
        START {
            ba.resize(0);
            TextWriter(&ba)
                << 1.333333333333f
                << PI
                << ascii
                << false
                << color
                ;
        }
        STOP("Text",b.buffer())
        START {
            ba.resize(0);
            QJsonWriter(&ba).sequence()
                .bind(1.333333333333f)
                .bind(PI)
                .bind(ascii)
                .bind(false)
                .bind(color)
                .out();
        }
        STOP("Json",ba)
        START {
            ba.resize(0);
            QXmlStreamWriter w(&ba);
            QXmlWriter x(&w);
            // TODO x.setErrorFilter([](QIdentifierLiteral error, QString context){ return error == qBindUnexpectedEnd ? true : false; }); // to trigger a Q_ASSERT if .out() is forgotten
            x.sequence()
                .bind(1.333333333333f)
                .bind(PI)
                .bind(ascii)
                .bind(false)
                .bind(color)
                .out();
        }
        STOP("Xml",ba)
        START {
            v.clear();
            QVariantBuilder(&v).sequence()
                .bind(1.333333333333f)
                .bind(PI)
                .bind(ascii)
                .bind(false)
                .bind(color)
                .out();
        }
        STOP("Variant",Text(v).utf8())
        START {
            ba.resize(0);
            QCborWriter(&ba).value().sequence(5)
                .bind(1.333333333333f)
                .bind(PI)
                .bind(ascii)
                .bind(false)
                .bind(color)
                .out();
        }
        STOP("Cbor",ba.toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QCborStreamWriter s(&b);
            s.startArray(5);
                s.append(1.333333333333f);
                s.append(PI);
                s.append(ascii);
                s.append(false);
                s.startMap();
                    s.append("RGB");s.startArray();
                        s.append(quint8(color.red  ()));
                        s.append(quint8(color.green()));
                        s.append(quint8(color.blue ()));
                        s.endArray();
                    s.append("base");s.append(quint8(255));
                    s.endMap();
                s.endArray();
        }
        STOP("QCborStream",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream d(&b);
            QDataWriter(&d).sequence()
                .bind(1.333333333333f)
                .bind(PI)
                .bind(ascii)
                .bind(false)
                .bind(color)
                .out();
        }
        STOP("Data",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream(&b)
                    << 1.333333333333f
                    << PI
                    << ascii
                    << false
                    << color;
        }
        STOP("QDataStream",b.buffer().toHex())
        float f = 1.333333333333f; bool boolean = false;
        START {
            ba.resize(0);
            ba.append(reinterpret_cast<const char*>(&f      ),sizeof(f      )); // endianness must be specified externally
            ba.append(reinterpret_cast<const char*>(&PI     ),sizeof(PI     ));
            ba.append(                               ascii                   );
            ba.append(reinterpret_cast<const char*>(&boolean),sizeof(boolean));
            ba.append(reinterpret_cast<const char*>(&color  ),sizeof(color  )); // schema evolution must be specified externally
        }
        STOP("QByteArray",ba.toHex())

        QBindLater bindables[5];
        START {
            bindables[0] = 1.333333333333f;
            bindables[1] = PI;
            bindables[2] = ascii;
            bindables[3] = false;
            bindables[4] = color;
        }
        STOP("Bindables","")
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(bindables);
        }
        STOP("Bindables>Cbor",ba.toHex())
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(bindables);
        }
        STOP("Bindables>Json",ba)
    }
    GROUP_STOP
    GROUP("doubles")//========================================================
    {
        START {
            s.resize(0);
            QDebug d(&s);
            for (int i=0; i < 16; i++) {
            d << transform[i];
            }
        }
        STOP("QDebug",s)
        START {
            ba.resize(0);
            TextWriter(&ba).bind(transform);
        }
        STOP("Text",b.buffer())
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(transform);
        }
        STOP("Json",ba)
        START {
            ba.resize(0);
            QXmlStreamWriter w(&ba);
            QXmlWriter d(&w);
            d.bind(transform);
        }
        STOP("Xml",ba)
        START {
            v.clear();
            QVariantBuilder(&v).bind(transform);
        }
        STOP("Variant",Text(v).utf8())
        START {
            b.seek(0); b.buffer().resize(0);
            QCborWriter(&ba).bind(transform);
        }
        STOP("Cbor",ba.toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QCborStreamWriter s(&b);
            constexpr quint64 size = 16;
            s.startArray(size);
                for (quint8 i=0; i < size; i++) {
                    s.append(transform[i]);
                }
                s.endArray();
        }
        STOP("QCborStream",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream d(&b);
            QDataWriter(&d).value().bind(transform);
        }
        STOP("Data",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream data(&b);
            data << transform.size();
            for (int i=0; i < transform.size(); i++) {
                data << transform[i];
            }
        }
        STOP("QDataStream",b.buffer().toHex())
        QByteArray str; str.reserve(500);
        START {
            str.resize(0);
            int size=transform.size();
            str.append(reinterpret_cast<char*>(&size),sizeof(size));
            for (int i=0; i < size; i++) {
                str.append(reinterpret_cast<char*>(&transform[i]),sizeof(transform[i]));
            }
        }
        STOP("QByteArray",str.toHex())

        QBindLater bindable;
        START {
            bindable = transform;
        }
        STOP("Bindable","")
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(bindable);
        }
        STOP("Bindable>Cbor",ba.toHex())
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(bindable);
        }
        STOP("Bindable>Json",ba)
    }
    GROUP_STOP
    GROUP("Person")//=========================================================
    {
        START {
            s.resize(0);
            QDebug dbg(&s);
            dbg << person;
        }
        STOP("QDebug",s)
        START {
            ba.resize(0);
            TextWriter(&ba).bind(person);
        }
        STOP("Text",b.buffer())
#endif
#ifndef NO_QBIND_CODE
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(person);
        }
        STOP("Json",ba)
#endif
#ifndef NO_OTHER_CODE
        START {
            ba.resize(0);
            QXmlStreamWriter w(&ba);
            QXmlWriter d(&w);
            d.bind(person);
        }
        STOP("Xml",ba)
        START {
            v.clear();
            QVariantBuilder(&v).bind(person);
        }
        STOP("Variant",Text(v).utf8())
#endif
#ifndef NO_QBIND_CODE
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(person);
        }
        STOP("Cbor",ba.toHex())
#endif
#ifndef NO_MANUAL_CODE
        START {
            b.seek(0); b.buffer().resize(0);
            qCborStreamWritePerson(&b, person);
        }
        STOP("QCborStream",b.buffer().toHex())
#endif
#ifndef NO_OTHER_CODE
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream d(&b);
            QDataWriter(&d).value().bind(person);
        }
        STOP("Data",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream d(&b);
            d << person;
        }
        STOP("QDataStream",b.buffer().toHex())
        QByteArray str; str.reserve(100); int s;
        START {
            str.resize(0);
            str.append(reinterpret_cast<const char*>(person.firstName.constData()),person.firstName.size());// encoding must be specified externally
            str.append(reinterpret_cast<const char*>(person.lastName .constData()),person.lastName .size());
            str.append(reinterpret_cast<const char*>(&person.height),sizeof(person.height));                // endianness must be specified externally
            str.append(reinterpret_cast<const char*>(&person.age   ),sizeof(person.age   ));
            s=person.phones.size();
            str.append(reinterpret_cast<const char*>(&s),sizeof(s));
            for (int i=0; i<s; ++i) {
                str.append(reinterpret_cast<const char*>(&person.phones[i]._t),sizeof(person.phones[i]._t));str.append(qPrintable(person.phones[i]._n));
            }
            str.append(reinterpret_cast<const char*>(person.comments.constData()),person.comments.size());                                                           // byte order must be specified externally
            s=person.children.size();
            str.append(reinterpret_cast<const char*>(&s),sizeof(s));
            for (int i=0; i<s; ++i) {
                Q_ASSERT(false); // Would need recursion
            }
        }
        STOP("QByteArray",str.toHex())
#ifdef PROTOBUF
        std::string buf;
        pb::Person pb;
        pb.set_firstname("John");
        pb.set_lastname("Doe");
        pb.set_height(1.75);
        pb.set_age(18);
        pb.set_comments("unicode is likely U+01 \x01 + U+1F \x1F + U+A4 ¤ U+B0 ° U+D8 Ø U+FF ÿ");
        START {
            buf.resize(0);
            buf = pb.SerializeAsString();
        }
        STOP("protobuf",QByteArray::fromStdString(buf).toHex());
#endif
        START {
            ba.resize(0);
            qJsonDocumentWritePerson(ba, person);
        }
        STOP("QJsonDocument",ba)

        QBindLater bindable;
        START {
            bindable = person;
        }
        STOP("Bindable","")
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(bindable);
        }
        STOP("Bindable>Cbor",ba.toHex())
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(bindable);
        }
        STOP("Bindable>Json",ba)
    }
    GROUP_STOP
    GROUP("Phone")//=========================================================
    {
        START {
            s.resize(0);
            QDebug dbg(&s);
            dbg << phone;
        }
        STOP("QDebug",s)
        START {
            ba.resize(0);
            TextWriter(&ba).bind(phone);
        }
        STOP("Text",b.buffer())
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(phone);
        }
        STOP("Json",ba)
        START {
            ba.resize(0);
            QXmlStreamWriter w(&ba);
            QXmlWriter d(&w);
            d.bind(phone);
        }
        STOP("Xml",ba)
        START {
            v.clear();
            QVariantBuilder(&v).bind(phone);
        }
        STOP("Variant",Text(v).utf8())
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(phone);
        }
        STOP("Cbor",ba.toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QCborStreamWriter s(&b);
            s.startMap();
                s.append("type"  );s.append(phone._t);
                s.append("number");s.append(phone._n);
                s.endMap();
        }
        STOP("QCborStream",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream d(&b);
            QDataWriter(&d).value().bind(phone);
        }
        STOP("Data",b.buffer().toHex())
        START {
            b.seek(0); b.buffer().resize(0);
            QDataStream d(&b);
            d << phone;
        }
        STOP("QDataStream",b.buffer().toHex())
        QByteArray str; str.reserve(100);
        START {
            str.resize(0);
            str.append(reinterpret_cast<const char*>(&phone._t),sizeof(phone._t));str.append(qPrintable(phone._n));
        }
        STOP("QByteArray",str.toHex())

        QBindLater bindable;
        START {
            bindable = phone;
        }
        STOP("Bindable","")
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(bindable);
        }
        STOP("Bindable>Cbor",ba.toHex())
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(bindable);
        }
        STOP("Bindable>Json",ba)
    }
    GROUP_STOP
    GROUP("Person-Json")//====================================================
    {
#endif
        QBuffer json; json.open(QIODevice::ReadOnly);
        json.buffer() = "_{ \"names\": [ _\"John\" _, \"Doe\"] , \"age\"_:_null, \"height\":_1.75, \"phones\": [], \"_\":\"superfluous item\" _} ";
        //               1               15        23                   4042                58                                                102
        Person p;
        QBuffer roundtrip; roundtrip.setData(json.buffer()); roundtrip.open(QIODevice::ReadWrite);
#ifndef NO_OTHER_CODE
        QJsonValue jv;
        //---------------------------------------------------------------------
        START {
            json.seek(0); p = {}; errors.clear();
            QJsonReader r(&json);
            r.setErrorFilter(handler);
            r.bind(p);
        }
        STOP("Json>P",Text(p).utf8()+" | "+errors.join(", "))
        START {
            roundtrip.seek(0);
            QJsonWriter(&roundtrip).bind(p);
        }
        STOP("P>Json",QString(roundtrip.buffer()))
#endif
#ifndef NO_QBIND_CODE
        START {
            roundtrip.seek(0); p = {}; errors.clear();
            QJsonReader r(&roundtrip);
            r.setErrorFilter(handler);
            r.bind(p);
        }
        STOP("Json>P",Text(p).utf8()+" | "+errors.join(", "))
#endif
#ifndef NO_MANUAL_CODE
        //---------------------------------------------------------------------
        bool parsed;
        START {
            p = {};
            parsed = qJsonDocumentReadPerson(roundtrip.buffer(), p);
        }
        STOP("QJsonDocument>P",Text(p).utf8()+" | "+(parsed?"true":"false"))
#endif
#ifndef NO_OTHER_CODE
        //---------------------------------------------------------------------
        START {
            jv = QJsonValue();
            QJsonBuilder(&jv).bind(p);
        }
        STOP("P>JsonValue",QJsonDocument(jv.toObject()).toJson(QJsonDocument::Compact))
        START {
            p = {}; errors.clear();
            QJsonVisitor r(&jv);
            r.setErrorFilter(handler);
            r.bind(p);
        }
        STOP("JsonValue>P",Text(p).utf8()+" | "+errors.join(", "))
        //---------------------------------------------------------------------
        START {
            roundtrip.seek(0); jv = QJsonValue(); errors.clear();
            QJsonReader r(&roundtrip);
            r.setErrorFilter(handler);
            r.bind(jv);
        }
        STOP("Json>JsonValue",QJsonDocument(jv.toObject()).toJson(QJsonDocument::Compact)+" | "+errors.join(", "));
        START {
            ba.resize(0);
            QJsonWriter(&ba).bind(jv);
        }
        STOP("JsonValue>Json",ba);
        START {
            roundtrip.seek(0); ba.resize(0);
            QJsonReader r(&roundtrip);
            r.setErrorFilter(handler);
            r.bind(QCborWriter(&ba).value());
        }
        STOP("Json>Cbor",ba.toHex())
    }
    GROUP_STOP
    GROUP("Person-Cbor")//====================================================
    {
        QBuffer cbor; cbor.open(QIODevice::ReadOnly);
        cbor.buffer() = QByteArray::fromHex(
            "A5656E616D657382644A6F686E63446F6563616765F666686569676874F93F006670686F6E65738060707375706572666C756F7573206974656D");
//           { "names":    [ "John",   "Doe"], "age":null,"height":    1.75, "phones":     [],"":"superfluous item"              }
//                                                                                            41                               58
        Person p;
        QBuffer roundtrip; roundtrip.open(QIODevice::ReadWrite);
        QCborValue cv;
        bool parsed;
        //---------------------------------------------------------------------
        START {
            cbor.seek(0); p = {}; errors.clear();
            QCborReader r(&cbor);
            r.setErrorFilter(handler);
            r.bind(p);
        }
        STOP("Cbor>P",Text(p).utf8()+" | "+errors.join(", "))
        START {
            roundtrip.buffer().resize(0);
            QCborWriter(&roundtrip.buffer()).bind(p);
        }
        STOP("P>Cbor",QString(roundtrip.buffer().toHex()))
#endif
#ifndef NO_QBIND_CODE
        START {
            roundtrip.seek(0); p = {}; errors.clear();
            QCborReader r(&roundtrip);
            r.setErrorFilter(handler);
            r.bind(p);
        }
        STOP("Cbor>P",Text(p).utf8()+" | "+errors.join(", "))
        //---------------------------------------------------------------------
#endif
#ifndef NO_MANUAL_CODE
        START {
            p = {};
            parsed = qCborStreamReadPerson(&roundtrip, p);
        }
        STOP("QCborStream>P",Text(p).utf8()+" | "+(parsed?"true":"false"))
#endif
#ifndef NO_OTHER_CODE
        //---------------------------------------------------------------------
        START {
            cv = QCborValue();
            QCborBuilder(&cv).bind(p);
        }
        STOP("P>CborValue",QString(cv.toCbor().toHex()))
        START {
            p = {}; errors.clear();
            QCborVisitor r(&cv);
            r.setErrorFilter(handler);
            r.bind(p);
        }
        STOP("CborValue>P",Text(p).utf8()+" | "+errors.join(", "))
        //---------------------------------------------------------------------
        START {
            roundtrip.seek(0); cv = QCborValue(); errors.clear();
            QCborReader r(&roundtrip);
            r.setErrorFilter(handler);
            r.bind(cv);
        }
        STOP("Cbor>CborValue",cv.toDiagnosticNotation()+" | "+errors.join(", "));
        START {
            ba.resize(0);
            QCborWriter(&ba).bind(cv);
        }
        STOP("CborValue>Cbor",ba.toHex());
        //---------------------------------------------------------------------
        START {
            roundtrip.seek(0); b.seek(0); b.buffer().resize(0); errors.clear();
            QCborReader r(&roundtrip);
            r.setErrorFilter(handler);
            r.bind(QJsonWriter(&b).value());
        }
        STOP("Cbor>Json",b.buffer()+" | "+errors.join(", "))
    }
    GROUP_STOP
    GROUP("Person-Settings")//====================================================
    {
        QFileInfo iniFile(QDir::currentPath()+"/../QBind");
        QSettings::setPath(QSettings::IniFormat,QSettings::UserScope,iniFile.path());
        QSettings      ini(QSettings::IniFormat,QSettings::UserScope,"QBind","sample");
        QSettings      nat(                     QSettings::UserScope,"QBind","sample");
        //---------------------------------------------------------------------
        START {
            QSettingsWriter(&nat).bind(person);
        }
        STOP("P>Settings",Text(person).utf8())
        START {
            QSettingsReader r(&nat); errors.clear();
            r.setErrorFilter(handler);
            r.bind(person);
        }
        STOP("Settings>P",Text(person).utf8()+" | "+errors.join(", "))
        START {
            QSettingsWriter(&ini).bind(person);
        }
        STOP("P>Settings",iniFile.fileName())
        //---------------------------------------------------------------------
    }
    GROUP_STOP

    if (argc<=1 || strcmp(argv[1],"gui")==0) {
        doGuiExample();
    }
#endif
    return (fclose(samples)==0) &&/*anyway*/
           (fclose(results)==0) &&/*anyway*/
           QFile::copy("../QBind/results.csv", QString("../QBind/results-%1.csv").arg(QDir::current().dirName()).toLocal8Bit().constData());
}

#ifndef NO_OTHER_CODE
QSequence flatten(QList<Person>& ps, QSequence&& s) {
    for (auto&& p : ps) { // Iterating on ps only works for Write but flatten cannot be Read/Write anyway
        s = s
            .record()
                .bind("first name", p.firstName  )
                .bind("height(ft)", p.height*3.28)
                .out()
            .with(p.children, flatten);
    }
    return std::move(s);
};

void doGuiExample() {
    // Dataset adapted to different views
    person.lastName="";
    person.comments="";
    person.phones.append(phone);
    QList<Person> persons;
    persons << person << person;
    persons[0].age=42;
    persons[0].children.append(person);
    persons[0].children[0].firstName="Joe";
    persons[0].children[0].age=18;
    persons[0].children[0].height=1.82;
    persons[1].firstName="Jane";
    persons[1].age=41;
    persons[0].height=1.72;
    persons[1].phones.append(Phone{Phone::Office,"112"});

    QStringList errors;
    auto handler = [&errors](QIdentifierLiteral error, QVariant context){
        QByteArray ba;
        TextWriter(&ba).bind(context);
        errors.append(ba.append(':').append(error.latin1()));
        return true;
    };

    // Various possibilities to customize bind

#if 0
    // Design that works for Read/Write but requires several cumbersome functions
    QModelWriter<>(&customModel).sequence().forEach(persons, [](Person& p, QValue&& item)->QValueEnd {
        return item
            .record()
                .bind("first name", p.firstName)
                .sequence("phones").forEach(p.phones,
                    [](Phone& phone, QValue&& item) {
                        return item.bind(phone._n);
                    },
                    Phone::isOffice)
                    ; // automagically closed while cast to returned QValueEnd type
    });
#endif

    QDialog dlg;
    auto grid = new QGridLayout(&dlg); grid->setColumnStretch(0, 1); grid->setColumnStretch(1, 3);
    dlg.setLayout(grid);

    auto r0Label = new QLabel(&dlg); grid->addWidget(r0Label, 0, 0); r0Label->setWordWrap(true); r0Label->setMinimumWidth(100); r0Label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    auto r0  = new QTabWidget(&dlg); grid->addWidget(r0     , 0, 1);
    struct { QStandardItemModel* model; QAbstractItemView* widget; const char* name; QValLambda bind; } transformTabs[] = {
        { new QStandardItemModel(&dlg), new QTableView(&dlg), "bind(transform)"         , [&](QValue&& v) { return v                    .bind(transform); } },
        { new QStandardItemModel(&dlg), new QTableView(&dlg), "meta(qmSizes,\"4,3\")...", [&](QValue&& v) { return v.meta(qmSizes,"4,3").bind(transform); } },
    };
    for (auto&& t: transformTabs) { r0->addTab(t.widget, t.name); t.widget->setModel(t.model); }

    int currentTransformTab = -1;
    QObject::connect(r0, &QTabWidget::currentChanged, [&](int newTab) {
        QByteArray text;
        if (0<=currentTransformTab) {
            QModelReader<> reader(transformTabs[currentTransformTab].model, false);
            transformTabs[currentTransformTab].bind(reader.value());
            text = errors.join('\n').toUtf8().append('\n');
        }
        QJsonWriter(&text).bind(transform);
        r0Label->setText(QString::fromUtf8(text));

        transformTabs[newTab].model->clear();
        transformTabs[newTab].bind(QModelWriter<>(transformTabs[newTab].model, false).value());

        auto table = qobject_cast<QTableView*>(transformTabs[newTab].widget); table->resizeColumnsToContents(); table->resizeRowsToContents();
        currentTransformTab = newTab;
    });
    r0->setCurrentIndex(0);

    auto r1Label = new QLabel(&dlg); grid->addWidget(r1Label, 1, 0); r1Label->setWordWrap(true); r1Label->setMinimumWidth(100); r1Label->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    auto r1  = new QTabWidget(&dlg); grid->addWidget(r1     , 1, 1);
    struct { QStandardItemModel* model; QAbstractItemView* widget; const char* name; QValLambda bind; } personsTabs[] = {
        { new QStandardItemModel(&dlg), new QTableView(&dlg), "bind(persons)", [&](QValue&& v) {
            return v.bind(persons); } },

        // Using meta() to drive various custom bind
        { new QStandardItemModel(&dlg), new QTableView(&dlg), "meta(qmColumns,\"names,age\")...", [&](QValue&& v) {
            return v.meta(qmColumns,"names,age").bind(persons); } },

        { new QStandardItemModel(&dlg), new QTreeView (&dlg), "meta(qmChildren,\"children\")...", [&](QValue&& v) {
            return v.meta(qmChildren,"children").bind(persons); } },

        // Using view types (non-owning const& with a custom bind method)
        { new QStandardItemModel(&dlg), new QTreeView (&dlg), "bind(PersonsView{persons})", [&](QValue&& v) {
            return v.bind(PersonsView{persons}); } },

        // Using sequence comprehensions similar to Python (that does not work by default for Read/Write)
        { new QStandardItemModel(&dlg), new QTableView(&dlg), "sequence().with([&](...){for...", [&](QValue&& v) {
            return v.sequence().with([&](QSequence&& s) {
                for (auto&& person : persons) { // Read would require looping while !s.item()
                    s = s // To keep working with the active QCur
                        .record()
                            .item("first name")
                                .meta(qmColor, person.age >= 42 ? "green" : "blue")
                                .bind(person.firstName)
                            .item("office phone").with([&](QValue&& v) {
                                for (auto&& phone : person.phones) {
                                    if (phone._t == Phone::Office) {
                                        return v.bind(phone._n);
                                    }
                                }
                                return v.null();
                            })
                            .out();
                    }
                return std::move(s); // So caller stops calling QAbstractValue if user function was unable to keep track of the active QCur
            }).out();}},

        // The same design allows flattening trees into a sequence
        { new QStandardItemModel(&dlg), new QTableView(&dlg), "bind(PersonsTable{persons})", [&](QValue&& v) {
            return v.bind(PersonsTable(persons)); } },
    };
    for (auto&& t: personsTabs) { r1->addTab(t.widget, t.name); t.widget->setModel(t.model); }

    int currentPersonsTab = -1;
    QObject::connect(r1, &QTabWidget::currentChanged, [&](int newTab) {
        QByteArray text;
        if (0<=currentPersonsTab) {
            QModelReader<> reader(personsTabs[currentPersonsTab].model);
            reader.setErrorFilter(handler);
            personsTabs[currentPersonsTab].bind(reader.value());
            TextWriter(&text).bind(errors.join("\n"));
            text.append('\n');
        }
        QJsonWriter(&text).bind(persons);
        r1Label->setText(QString::fromUtf8(text));

        personsTabs[newTab].model->clear();
        personsTabs[newTab].bind(QModelWriter<>(personsTabs[newTab].model).value());

        auto table = qobject_cast<QTableView*>(personsTabs[newTab].widget); if (table) { table->resizeColumnsToContents(); table->resizeRowsToContents(); }
        auto tree  = qobject_cast< QTreeView*>(personsTabs[newTab].widget); if (tree ) for(int i=0; i<personsTabs[newTab].model->columnCount(); i++) tree->resizeColumnToContents(i);
        currentPersonsTab = newTab;
    });
    r1->setCurrentIndex(0);

    dlg.resize(960,360);
    dlg.exec();
}
#endif

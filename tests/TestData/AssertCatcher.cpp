/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/

#include <QtTest/qtest.h>
#include <QtCore/qglobal.h>
#include <QtCore/qlogging.h>
#include <QtCore/qdebug.h>

#include <AssertCatcher.h>

AssertCatcher* AssertCatcher::instance = nullptr;

#ifdef Q_CC_MSVC
int voidReportHook( int reportType, char* message, int* returnValue )
{
    Q_UNUSED(reportType)
    Q_UNUSED(message)
    Q_UNUSED(returnValue)
    return true; // Same as ignore.
}
#endif

AssertCatcher::AssertCatcher(unsigned short p_expectedTimes)
    : m_remainingTimes(p_expectedTimes)
#ifdef Q_CC_MSVC
    , m_savedReportHook(nullptr)
#endif
{
    m_savedMessageHandler = qInstallMessageHandler(handleMessage);
#ifdef Q_CC_MSVC
    m_savedReportMode = _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_WNDW);
    m_savedReportHook = _CrtSetReportHook(voidReportHook);
#endif
}


bool AssertCatcher::create(unsigned short p_expectedTimes,
                           QScopedPointer<AssertCatcher>& p_assertCatcher)
{
    Q_ASSERT(instance == nullptr);    //A Q_ASSERT in a "AssertCatcher" ?..
    if (instance != nullptr) { return false; }

    instance = new AssertCatcher(p_expectedTimes);
    p_assertCatcher.reset(instance);
    return true;
}

void AssertCatcher::privateHandleMessage(QtMsgType p_msgType,
                                         const QMessageLogContext& p_mesLogContext,
                                         const QString& p_message)
{
    if (p_msgType == QtFatalMsg)
    {
        --m_remainingTimes;
    }
    else
    {
        m_savedMessageHandler(p_msgType,p_mesLogContext,p_message);
    }
}

int AssertCatcher::getRemainingAsserts() const
{
    return m_remainingTimes;
}

AssertCatcher::~AssertCatcher()
{
    instance = nullptr;
    qInstallMessageHandler(m_savedMessageHandler);
#ifdef Q_CC_MSVC
    _CrtSetReportMode(_CRT_ERROR, m_savedReportMode);
    _CrtSetReportHook(m_savedReportHook);
#endif
    QVERIFY(m_remainingTimes == 0);
}


//---------------------------Statics ----------------------------//

void AssertCatcher::handleMessage(QtMsgType p_msgType, const QMessageLogContext& p_msgLogContext, const QString& p_message)
{
    if (instance)
    {
        instance->privateHandleMessage(p_msgType, p_msgLogContext, p_message);
    }
}

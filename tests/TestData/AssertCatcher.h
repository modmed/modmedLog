/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qmap.h>
#include <QtCore/qstring.h>
#include <QtCore/qscopedpointer.h>

#include <QtTest/qtest.h>

#ifdef QT_NO_DEBUG
#   define EXPECT_ASSERTS_IN_SCOPE(HOW_MANY) qt_noop()
#else
#   if defined(Q_CC_MSVC)
#       define EXPECT_ASSERTS_IN_SCOPE(HOW_MANY) \
QScopedPointer<AssertCatcher> testData_assertCatcher; \
AssertCatcher::create(HOW_MANY, testData_assertCatcher)
#   else
#       define EXPECT_ASSERTS_IN_SCOPE(HOW_MANY) QSKIP("Cannot check abort emission")
#   endif
#endif

class AssertCatcher
{
    int m_remainingTimes;
    QtMessageHandler m_savedMessageHandler;
#ifdef Q_CC_MSVC
    int m_savedReportMode;
    _CRT_REPORT_HOOK m_savedReportHook;
#endif

    AssertCatcher(unsigned short p_expectedTimes);

    void privateHandleMessage(QtMsgType pMsgType,
                              const QMessageLogContext& p_mesLogContext,
                              const QString& p_message);

public:

    int getRemainingAsserts() const;

    static AssertCatcher* instance;

    static bool create(unsigned short p_expectedTimes,
                       QScopedPointer<AssertCatcher>& p_assertCatcher);

    static void handleMessage(QtMsgType p_MsgType,
                              const QMessageLogContext&,
                              const QString& p_message);

    ~AssertCatcher();
};

/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include <modmed/data/BindState.h>

#include "IDataTester.h"

template<>
int IDataTester<Reader>::m_callOrder = 0;

template<>
ISequence<Reader>* IDataTester<Reader>::sequence(const ItmState<Reader>& dp)
{
    m_sequenceCall.push_back(m_callOrder++);
    m_sequenceState.push_back(&dp);
    return m_sequenceRetNullAfter-- > 0 ? this : nullptr;
}

template<>
IRecord<Reader>* IDataTester<Reader>::record (const ItmState<Reader>& dp)
{
    m_recordCall.push_back(m_callOrder++);
    m_recordState.push_back(&dp);
    return m_recordRetNullAfter-- > 0 ? this : nullptr;
}

template<>
bool IDataTester<Writer>::bindNumber(const ItmState<Writer>&, qlonglong& p_number)
{
    m_numberCall.push_back(m_callOrder++);
    m_longlongValues.push_back(p_number);
    return true;
}

template<>
bool IDataTester<Writer>::bindNumber(const ItmState<Writer>&, double& p_number)
{
    m_numberCall.push_back(m_callOrder++);
    m_doubleValues.push_back(p_number);
    return true;
}

template<>
bool IDataTester<Writer>::bindBoolean(const ItmState<Writer>&, bool& p_bool)
{
    m_booleanCall.push_back(m_callOrder++);
    m_boolValues.push_back(p_bool);
    return true;
}

template<>
bool IDataTester<Writer>::bindTimeStamp(const ItmState<Writer>&, QDateTime& p_datetime)
{
    m_timestampCall.push_back(m_callOrder++);
    m_timestampValues.push_back(p_datetime);
    return true;
}

template<>
bool IDataTester<Writer>::bindBytes(const ItmState<Writer>&, QByteArray& p_bytes)
{
    m_bytesCall.push_back(m_callOrder++);
    m_bytesValues.push_back(p_bytes);
    return true;
}

template<>
bool IDataTester<Reader>::bindNumber(const ItmState<Reader>&, qlonglong& p_number)
{
    m_numberCall.push_back(m_callOrder++);
    if (m_longlongValues.size() > 0)
    {
        p_number = m_longlongValues.front(); m_longlongValues.pop_front();
        return true;
    }
    return false;
}

template<>
bool IDataTester<Reader>::bindNumber(const ItmState<Reader>&, double& p_number)
{
    m_numberCall.push_back(m_callOrder++);
    if (m_doubleValues.size() > 0)
    {
        p_number = m_doubleValues.front(); m_doubleValues.pop_front();
        return true;
    }
    return false;
}

template<>
bool IDataTester<Reader>::bindBoolean(const ItmState<Reader>&, bool& p_bool)
{
    m_booleanCall.push_back(m_callOrder++);
    if (m_boolValues.size() > 0)
    {
        p_bool = m_boolValues.front(); m_boolValues.pop_front();
        return true;
    }
    return false;
}

template<>
bool IDataTester<Reader>::bindTimeStamp(const ItmState<Reader>&, QDateTime& p_timestamp)
{
    m_timestampCall.push_back(m_callOrder++);
    if (m_timestampValues.size() > 0)
    {
        p_timestamp = m_timestampValues.front();
        m_timestampValues.pop_front();
        return true;
    }
    return false;
}

template<>
bool IDataTester<Reader>::bindBytes(const ItmState<Reader>&, QByteArray& p_bytes)
{
    m_bytesCall.push_back(m_callOrder++);
    if (m_bytesValues.size() > 0)
    {
        p_bytes = m_bytesValues.front();
        m_bytesValues.pop_front();
        return true;
    }
    return false;
}

template<>
bool IDataTester<Reader>::null(const ItmState<Reader>& dp)
{
    m_nullCall.push_back(m_callOrder++);
    m_nullState.push_back(&dp);
    return m_null;
}

template<>
bool IDataTester<Reader>::text(const ItmState<Reader>& tp, const QString& s)
{
    m_textCall.push_back(m_callOrder++);
    m_textState.push_back(&tp);
    return s == m_textStrings.takeFirst();
}

template<>
bool IDataTester<Reader>::bindText(const ItmState<Reader>& tp,       QString& s)
{
    m_bindTextCall.push_back(m_callOrder++);
    m_bindTextState.push_back(&tp);
    if (m_bindTextStrings.isEmpty())
      return false;
    s = m_bindTextStrings.takeFirst();
    return m_bindText;
}

template<>
IItem<Reader>* IDataTester<Reader>::sequenceItem(const SeqState<Reader>& sp)
{
    m_sequenceItemCall.push_back(m_callOrder++);
    m_sequenceItemState.push_back(&sp);
    return m_sequenceItemRetNullAfter-- > 0 ? this : nullptr;
}

template<>
bool IDataTester<Reader>::  sequenceOut (const SeqState<Reader>& sp)
{
    m_sequenceOutCall.push_back(m_callOrder++);
    m_sequenceOutState.push_back(&sp);
    return m_sequenceOut;
}

template<>
IItem<Reader>* IDataTester<Reader>::recordItem(const RecState<Reader>& rp, QString& key)
{
    m_recordItemCall.push_back(m_callOrder++);
    if (m_recordItemKey.empty()) { return nullptr; }
    key = m_recordItemKey.takeFirst();
    m_recordItemState.push_back(&rp);
    return m_recordItemRetNull ? nullptr : this;
}

template<>
bool IDataTester<Reader>::  recordOut (const RecState<Reader>& rp)
{
    m_recordOutCall.push_back(m_callOrder++);
    m_recordOutState.push_back(&rp);
    return m_recordOut;
}

template<>
int IDataTester<Writer>::m_callOrder = 0;

template<>
ISequence<Writer>* IDataTester<Writer>::sequence(const ItmState<Writer>& dp)
{
    m_sequenceCall.push_back(m_callOrder++);
    m_sequenceState.push_back(&dp);
    return m_sequenceRetNullAfter-- > 0 ? this : nullptr;
}

template<>
IRecord<Writer>* IDataTester<Writer>::record (const ItmState<Writer>& dp)
{
    m_recordCall.push_back(m_callOrder++);
    m_recordState.push_back(&dp);
    return m_recordRetNullAfter-- > 0 ? this : nullptr;
}

template<>
bool IDataTester<Writer>::null(const ItmState<Writer>& dp)
{
    m_nullCall.push_back(m_callOrder++);
    m_nullState.push_back(&dp);
    return m_null;
}

template<>
bool IDataTester<Writer>::text(const ItmState<Writer>& tp, const QString& s)
{
    m_textCall.push_back(m_callOrder++);
    m_textState.push_back(&tp);
    m_textStrings.push_back(s);
    return m_text;
}

template<>
bool IDataTester<Writer>::bindText(const ItmState<Writer>& tp,       QString& s)
{
    m_bindTextCall.push_back(m_callOrder++);
    m_bindTextState.push_back(&tp);
    m_bindTextStrings.push_back(s);
    return m_bindText;
}

template<>
IItem<Writer>* IDataTester<Writer>::sequenceItem(const SeqState<Writer>& sp)
{
    m_sequenceItemCall.push_back(m_callOrder++);
    m_sequenceItemState.push_back(&sp);
    return m_sequenceItemRetNullAfter-- > 0 ? this : nullptr;
}

template<>
bool IDataTester<Writer>::  sequenceOut (const SeqState<Writer>& sp)
{
    m_sequenceOutCall.push_back(m_callOrder++);
    m_sequenceOutState.push_back(&sp);
    return m_sequenceOut;
}

template<>
IItem<Writer>* IDataTester<Writer>::recordItem(const RecState<Writer>& rp, QString& key)
{
    m_recordItemCall.push_back(m_callOrder++);
    m_recordItemKey.push_back(key);
    m_recordItemState.push_back(&rp);
    return m_recordItemRetNull ? nullptr : this;
}

template<>
bool IDataTester<Writer>::  recordOut (const RecState<Writer>& rp)
{
    m_recordOutCall.push_back(m_callOrder++);
    m_recordOutState.push_back(&rp);
    return m_recordOut;
}


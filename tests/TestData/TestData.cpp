/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include "TestData.h"

#include <cstdio>
#include <algorithm>

#include <QtTest/qtest.h>
#include <QtCore/qlogging.h>
#include <QtCore/qbuffer.h>
#include <QtCore/qlist.h>
#include <QtCore/qmap.h>
#include <QtCore/qtemporaryfile.h>
#include <QtCore/qtemporarydir.h>
#include <QtCore/qprocess.h>
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qhostaddress.h>

#include <modmed/data/Data.h>
#include <modmed/data/Bind.h>
#include <modmed/data/Bindable.h>
#include <modmed/data/JsonReader.h>
#include <modmed/data/JsonWriter.h>
#include <modmed/data/XmlWriter.h>
#include <modmed/data/Summary.h>
#include <modmed/data/CborWriter.h>
#include <modmed/data/StringResult.h>
#include <modmed/data/CborWriter.h>

#include "AssertCatcher.h"
#include "IDataTester.h"

using namespace modmed;
using namespace data;

template<Operator Op>
struct TestUserData
{
    TestUserData() :
        bindCalled(false),
        bindFromCalled(false),
        bindFromFail(false),
        bindToCalled(false),
        bindToFail(false),
        bindToString("validData"),
        receivedDataState(nullptr),
        haveBeenCopied(false) {}
    TestUserData(const TestUserData& u) { *this = u; u.haveBeenCopied = true; }
    bool bindCalled;
    bool bindFromCalled;
    bool bindFromFail;
    mutable bool bindToCalled;
    bool bindToFail;
    QString bindToString;
    BindState<Op>* receivedDataState;
    mutable bool haveBeenCopied;
};

namespace modmed {
namespace data {
template<>
struct Bind <Reader, TestUserData<Reader> >
{
    static void bind(Item<Reader> d, ::TestUserData<Reader>& userData)
    {
        userData.bindCalled = true;
        userData.receivedDataState = d.state().data();
        d.bind(userData.bindToString);
    }
};

template<>
struct Bind <Writer, TestUserData<Writer> >
{
    static void bind(Item<Writer> d, ::TestUserData<Writer>& userData)
    {
        userData.bindCalled = true;
        userData.receivedDataState = d.state().data();
        d.bind(userData.bindToString);
    }
};
} // data
} // modmed

void TestData::initTestCase()
{
}

void TestData::IItem_01()
{
    IDataTester<Writer> IDT;
    Itm<Result<Writer>> d = IDT.write();
    QVERIFY(!d.state()->parent());
}

void TestData::IItem_02()
{
    IDataTester<Reader> IDT;
    Itm<Result<Reader>> d = IDT.read();
    QVERIFY(!d.state()->parent());
}

void TestData::IItem_03()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    Itm<Result<Writer>> d = IDT.write(&err);
    QCOMPARE(d.state()->errors(), &err);
}

void TestData::IItem_04()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    Itm<Result<Reader>> d = IDT.read(&err);
    QCOMPARE(d.state()->errors(), &err);
}

template<Operator Op = Writer>
struct TestState : BindState<Op>
{
    TestState(IItem<Op>* impl, QSharedPointer<BindState<Op>> parent = QSharedPointer<BindState<Op>>()) : BindState<Op>(impl, parent) {}

    void setCurrent(bool c = true) { BindState<Op>::setCurrent(c); }
    void makeCurrent(QSharedPointer<BindState<Op>> p) { BindState<Op>::makeCurrent(p); }

    // Declare virtual pure methods
    QString toString() const { return ""; }
    void addError(const BindError&) const {}
};

void TestData::State_01()
{
    IDataTester<Writer> IDT;
    TestState<Writer> TP(&IDT);
    QVERIFY(!TP.isOk());
    QVERIFY(!TP);
}

void TestData::State_02()
{
    IDataTester<Writer> IDT;
    Item<Writer> d = IDT.write();
    auto r = d.record();
    auto s = r.sequence("key");
    auto i = s.item();
    QCOMPARE(r.state()->level(), 0u);
    QCOMPARE(s.state()->level(), 1u);
    QCOMPARE(i.state()->level(), 2u);
}

void TestData::State_03()
{
    IDataTester<Writer> IDT;
    QSharedPointer<TestState<Writer>> TP1(new TestState<Writer>(&IDT));
    QSharedPointer<TestState<Writer>> TP2(new TestState<Writer>(&IDT));
    TP1->setCurrent();
    QVERIFY(TP1->isOk());
    QVERIFY(!TP2->isOk());
    TP1->makeCurrent(TP2);
    QVERIFY(!TP1->isOk());
    QVERIFY(TP2->isOk());
    TP2->makeCurrent(QSharedPointer<TestState<Writer>>());
    QVERIFY(!TP2->isOk());
}

void TestData::State_04()
{
    IDataTester<Writer> IDT;
    auto s = IDT.write().sequence();
    auto r = s.record();
    auto i = r.item("key");
    r.state()->setAttachmentsDir("toto");
    QCOMPARE(s.state()->getAttachmentsDir(), QString());
    QCOMPARE(r.state()->getAttachmentsDir(), QString("toto"));
    QCOMPARE(i.state()->getAttachmentsDir(), QString("toto"));
}

void TestData::ItmState_02()
{
    IDataTester<Writer> IDT;
    const TestUserData<Writer> UD;
    Item<Writer> d = IDT.write();
    d.bind(UD);
    QVERIFY(UD.bindCalled);
    QVERIFY(UD.receivedDataState == d.state().data());
}

void TestData::ItmState_05()
{
    IDataTester<Writer> IDT;
    Item<Writer> d = IDT.write();
    auto s = d.sequence();
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceState.front(), d.state().data());
    QVERIFY(!d.state()->isOk());
    QVERIFY(s.state()->isOk());
}

void TestData::ItmState_06()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    IDT.m_sequenceRetNullAfter = 0;
    Item<Writer> d = IDT.write(&err);
    auto s = d.sequence();
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceState.front(), d.state().data());
    QVERIFY(d.state()->isOk());
    QVERIFY(!s.state()->isOk());
    QVERIFY(d.state()->errors()->find<FailSequence>());
    QVERIFY(s.state()->errors()->find<FailSequence>());
}

void TestData::ItmState_07()
{
    IDataTester<Writer> IDT;
    Item<Writer> d = IDT.write();
    auto r = d.record();
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordState.front(), d.state().data());
    QVERIFY(!d.state()->isOk());
    QVERIFY(r.state()->isOk());
}

void TestData::ItmState_08()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    IDT.m_recordRetNullAfter = 0;
    Item<Writer> d = IDT.write(&err);
    auto r = d.record();
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordState.front(), d.state().data());
    QVERIFY(d.state()->isOk());
    QVERIFY(!r.state()->isOk());
    QVERIFY(d.state()->errors()->find<FailRecord>());
    QVERIFY(r.state()->errors()->find<FailRecord>());
}

void TestData::ItmState_11()
{
    IDataTester<Writer> IDT;
    auto s = IDT.write().sequence();
    auto d = s.item();
    d.null();
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_nullState.front(), d.state().data());
    QVERIFY(!d.state()->isOk());
    QVERIFY(s.state()->isOk());
}

void TestData::ItmState_12()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    Item<Writer> d = IDT.write(&err);
    d.state()->addError(data::Unknown("plop", true));
    QVERIFY(d.state()->errors()->find<data::Unknown>());
}

void TestData::ItmState_13()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto s = IDT.write(&err).sequence();
    {
        auto d = s.item();
        d.state()->addError(data::Unknown("plop", true));
    }
    QVERIFY(s.state()->errors()->find<data::Unknown>());
}

void TestData::ItmState_17()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_null = false;
    Item<Reader> d = IDT.read(&err);
    d.null();
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_nullState.front(), d.state().data());
    QVERIFY(d.state()->isOk());
    QVERIFY(d.state()->errors()->find<FailNull>());
}

void TestData::ItmState_19()
{
    {
        IDataTester<Reader> IDT;
        IDT.m_null = true;
        auto s = IDT.read().sequence();
        auto d = s.item();
        QVERIFY(d.isNull());
        QCOMPARE(IDT.m_nullCall.size(), 1);
        QCOMPARE(IDT.m_nullState.front(), d.state().data());
        QVERIFY(d.state()->isOk());
    }
    {
        IDataTester<Reader> IDT;
        IDT.m_null = false;
        auto s = IDT.read().sequence();
        auto d = s.item();
        QVERIFY(!d.isNull());
        QCOMPARE(IDT.m_nullCall.size(), 1);
        QCOMPARE(IDT.m_nullState.front(), d.state().data());
        QVERIFY(d.state()->isOk());
    }
}

void TestData::SeqState_01()
{
    IDataTester<Writer> IDT;
    auto s = IDT.write().sequence();
    int idxBefore = s.state()->currentIndex();
    auto i = s.item();
    int idxAfter = s.state()->currentIndex();
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemState.front(), s.state().data());
    QVERIFY(!s.state()->isOk());
    QVERIFY(i.state()->isOk());
    QCOMPARE(idxBefore + 1, idxAfter);
}

void TestData::SeqState_02()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    IDT.m_sequenceItemRetNullAfter = 0;
    auto s = IDT.write(&err).sequence();
    int idxBefore = s.state()->currentIndex();
    auto i = s.item();
    int idxAfter = s.state()->currentIndex();
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemState.front(), s.state().data());
    QVERIFY(s.state()->isOk());
    QVERIFY(!i.state()->isOk());
    QCOMPARE(idxBefore + 1, idxAfter);
    auto error = s.state()->errors()->find<FailSequenceItem>();
    QVERIFY(error);
    QCOMPARE(error->getIdx(), idxBefore);
}

void TestData::SeqState_03()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceRetNullAfter = 2;
    Item<Writer> d = IDT.write();
    auto s1 = d.sequence();
    auto s2 = s1.sequence();
    auto s3 = s2.out();
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QCOMPARE(IDT.m_sequenceOutState.front(), s2.state().data());
    QVERIFY(!s2.state()->isOk());
    QCOMPARE(s1.state(), s3.state());
    QVERIFY(s1.state()->isOk());
}

void TestData::SeqState_04()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    IDT.m_sequenceRetNullAfter = 2;
    IDT.m_sequenceOut = false;
    Item<Writer> d = IDT.write(&err);
    auto s1 = d.sequence();
    auto s2 = s1.sequence();
    auto s3 = s2.out();
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QCOMPARE(IDT.m_sequenceOutState.front(), s2.state().data());
    QVERIFY(!s2.state()->isOk());
    QCOMPARE(s1.state(), s3.state());
    QVERIFY(s1.state()->isOk());
    QVERIFY(s2.state()->errors()->find<FailSequenceOut>());
}

void TestData::SeqState_05()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto s = IDT.write(&err).sequence();
    s.state()->addError(data::Unknown("plop", true));
    QVERIFY(s.state()->errors()->find<data::Unknown>());
}

void TestData::SeqState_06()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto s1 = IDT.write(&err).sequence();
    auto s2 = s1.sequence();
    s2.state()->addError(data::Unknown("plop", true));
    s2.out();
    QVERIFY(s1.state()->errors()->find<data::Unknown>());
}

void TestData::RecState_01()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto r = IDT.write(&err).record();
    int idxBefore = r.state()->currentIndex();
    QString keyBefore = r.state()->currentKey();
    auto i = r.item("");
    QString keyAfter = r.state()->currentKey();
    int idxAfter = r.state()->currentIndex();
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QVERIFY(r.state()->isOk());
    QVERIFY(!i.state()->isOk());
    QCOMPARE(idxBefore, idxAfter);
    QCOMPARE(keyBefore, keyAfter);
    QVERIFY(r.state()->errors()->find<FailRecordItemEmptyKey>());
}

void TestData::RecState_02()
{
    IDataTester<Writer> IDT;
    auto r = IDT.write().record();
    int idxBefore = r.state()->currentIndex();
    QVERIFY(r.state()->currentKey() != QString("key"));
    auto i = r.item("key");
    QCOMPARE(r.state()->currentKey(), QString("key"));
    int idxAfter = r.state()->currentIndex();
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_recordItemKey.size(), 1);
    QCOMPARE(IDT.m_recordItemState.front(), r.state().data());
    QCOMPARE(IDT.m_recordItemKey.front(), QString("key"));
    QVERIFY(!r.state()->isOk());
    QVERIFY(i.state()->isOk());
    QCOMPARE(idxBefore + 1, idxAfter);
}

void TestData::RecState_03()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    IDT.m_recordItemRetNull = true;
    auto r = IDT.write(&err).record();
    int idxBefore = r.state()->currentIndex();
    QVERIFY(r.state()->currentKey() != QString("key"));
    auto i = r.item("key");
    QCOMPARE(r.state()->currentKey(), QString("key"));
    int idxAfter = r.state()->currentIndex();
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_recordItemKey.size(), 1);
    QCOMPARE(IDT.m_recordItemState.front(), r.state().data());
    QCOMPARE(IDT.m_recordItemKey.front(), QString("key"));
    QVERIFY(r.state()->isOk());
    QVERIFY(!i.state()->isOk());
    QCOMPARE(idxBefore + 1, idxAfter);
    auto error = r.state()->errors()->find<FailRecordItem>();
    QVERIFY(error);
    QVERIFY(error->getKey() == "key");
}

void TestData::RecState_04()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_recordItemKey.push_back("key2");
    auto r = IDT.read(&err).record();
    int idxBefore = r.state()->currentIndex();
    QVERIFY(r.state()->currentKey() != QString("key"));
    auto i = r.item("key");
    QCOMPARE(r.state()->currentKey(), QString("key"));
    int idxAfter = r.state()->currentIndex();
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_recordItemState.front(), r.state().data());
    QVERIFY(r.state()->isOk());
    QVERIFY(!i.state()->isOk());
    QCOMPARE(idxBefore + 1, idxAfter);
    auto error = r.state()->errors()->find<FailRecordItemWrongKey>();
    QVERIFY(error);
    QVERIFY(error->getExpectedKey() == "key");
    QVERIFY(error->getReadKey() == "key2");
}

void TestData::RecState_05()
{
    IDataTester<Reader> IDT;
    IDT.m_recordItemKey.push_back("key");
    QString key;
    auto r = IDT.read().record();
    int idxBefore = r.state()->currentIndex();
    QVERIFY(r.state()->currentKey() != QString("key"));
    auto i = r.item(key);
    QCOMPARE(r.state()->currentKey(), QString("key"));
    int idxAfter = r.state()->currentIndex();
    QCOMPARE(idxBefore + 1, idxAfter);
    QCOMPARE(key, QString("key"));
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_recordItemState.front(), r.state().data());
    QVERIFY(!r.state()->isOk());
    QVERIFY(i.state()->isOk());
}

void TestData::RecState_06()
{
    IDataTester<Writer> IDT;
    IDT.m_recordRetNullAfter = 2;
    Item<Writer> d = IDT.write();
    auto r1 = d.record();
    auto r2 = r1.record("key");
    auto r3 = r2.out();
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QCOMPARE(IDT.m_recordOutState.front(), r2.state().data());
    QVERIFY(!r2.state()->isOk());
    QCOMPARE(r1.state(), r3.state());
    QVERIFY(r1.state()->isOk());
}

void TestData::RecState_07()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_recordOut = false;
    Item<Writer> d = IDT.write(&err);
    auto r1 = d.record();
    auto r2 = r1.record("key");
    auto r3 = r2.out();
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QCOMPARE(IDT.m_recordOutState.front(), r2.state().data());
    QVERIFY(!r2.state()->isOk());
    QCOMPARE(r1.state(), r3.state());
    QVERIFY(r1.state()->isOk());
    QVERIFY(r2.state()->errors()->find<FailRecordOut>());
}

void TestData::RecState_08()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto r = IDT.write(&err).record();
    r.state()->addError(data::Unknown("plop", true));
    QVERIFY(r.state()->errors()->find<data::Unknown>());
}

void TestData::RecState_09()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto s = IDT.write(&err).sequence();
    auto r = s.record();
    r.state()->addError(data::Unknown("plop", true));
    r.out();
    QVERIFY(s.state()->errors()->find<data::Unknown>());
}

void TestData::Itm_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 2;
    Itm<Seq<Seq<Result<Writer>>>> d = IDT.write().sequence().sequence().item();
    Itm<Result<Writer>> dt = d;
    QCOMPARE(d.state(), dt.state());
}

void TestData::Itm_02()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Itm<Result<Writer>> d = IDT.write();
    Result<Writer> e = d.null();
    Q_UNUSED(e);
}

void TestData::Itm_03()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Itm<Seq<Result<Writer>>> d = IDT.write().sequence().item();
    Seq<Result<Writer>> s = d.null();
}

void TestData::Itm_04()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Itm<Result<Writer>> d = IDT.write();
    Seq<Result<Writer>> s = d.sequence();
}

void TestData::Itm_05()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Itm<Result<Writer>> d = IDT.write();
    Rec<Result<Writer>> r = d.record();
}

void TestData::Itm_07()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    TestUserData<Writer> UD;
    Itm<Result<Writer>> d = IDT.write();
    Result<Writer> e = d.bind(UD);
    Q_UNUSED(e);
}

void TestData::Itm_08()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    TestUserData<Writer> UD;
    Itm<Seq<Result<Writer>>> d = IDT.write().sequence().item();
    Seq<Result<Writer>> s = d.bind(UD);
}

void TestData::Seq_01()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Seq<Result<Writer>> s = IDT.write().sequence();
    Result<Writer> e = s.out();
    Q_UNUSED(e);
}

void TestData::Seq_02()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Seq<Seq<Result<Writer>>> s1 = IDT.write().sequence().sequence();
    Seq<Result<Writer>> s2 = s1.out();
}

void TestData::Seq_03()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Seq<Result<Writer>> s = IDT.write().sequence();
    Itm<Seq<Result<Writer>>> i = s.item();
}

void TestData::Rec_01()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Rec<Result<Writer>> r = IDT.write().record();
    Result<Writer> e = r.out();
    Q_UNUSED(e);
}

void TestData::Rec_02()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Rec<Rec<Result<Writer>>> r1 = IDT.write().record().record("plop");
    Rec<Result<Writer>> r2 = r1.out();
}

void TestData::Rec_03()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Rec<Result<Writer>> r = IDT.write().record();
    QString key("key");
    Itm<Rec<Result<Writer>>> i = r.item(key);
}

void TestData::Rec_04()
{
    // The fact that this test compile means that it passes
    IDataTester<Writer> IDT;
    Rec<Result<Writer>> r = IDT.write().record();
    Itm<Rec<Result<Writer>>> i = r.item("key");
}

void TestData::Result_01()
{
    IDataTester<Writer> IDT;
    Item<Writer> d = IDT.write();
    auto res = d.null();
    QVERIFY(d.state()->errors() == res.errors());
}

void TestData::Result_02()
{
    IDataTester<Writer> IDT;
    BindErrors err1;
    BindErrors err2;
    auto te1 = IDT.write(&err1).null();
    auto te2 = IDT.write(&err2).null();
    auto cte(te1);
    QVERIFY(cte.errors() == te1.errors());
    cte = te2;
    QVERIFY(cte.errors() == te2.errors());
}

void TestData::Error_01()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    Item<Writer> d = IDT.write(&err);
    QCOMPARE(d.state()->errors()->size(), 0);
}

void TestData::Error_02()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    Item<Writer> d = IDT.write(&err);
    d.state()->addError(data::Unknown("plop", true));
    QCOMPARE(d.state()->errors()->size(), 1);
}

void TestData::Error_05()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    Item<Writer> d = IDT.write(&err);
    d.state()->addError(data::Unknown("plop", true));
    QVERIFY(d.state()->errors()->find<data::Unknown>());
}

void TestData::Error_06()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    Item<Writer> d = IDT.write(&err);
    d.state()->addError(data::Unknown("plop", true));
    QVERIFY(!d.state()->errors()->find<FailBind>());
}

void TestData::Error_07()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto d = IDT.write(&err);
    d.sequence().item().state()->addError(data::Unknown("plop", true));
    QVERIFY(d.state()->errors()->find<data::Unknown>());
}

void TestData::Error_08()
{
    IDataTester<Writer> IDT;
    BindErrors err;
    auto d = IDT.write(&err);
    d.record().item("key").state()->addError(data::Unknown("plop", true));
    QVERIFY(d.state()->errors()->find<data::Unknown>());
}

void TestData::Unknown_01()
{
    data::Unknown u("plop", true);
    QCOMPARE(u.toString(), QString("plop"));
    QVERIFY(u.isReader());
}

void TestData::Unknown_02()
{
    data::Unknown u("plop", true);
    auto e = u.clone();
    QCOMPARE(e.dynamicCast<data::Unknown>()->toString(), QString("plop"));
    QVERIFY(e.dynamicCast<data::Unknown>()->isReader());
}

void TestData::FailBind_01()
{
    data::FailBind u("plop", true);
    QCOMPARE(u.toString(), QString("plop"));
    QVERIFY(u.isReader());
}

void TestData::FailBind_02()
{
    data::FailBind u("plop", true);
    auto e = u.clone();
    QCOMPARE(e.dynamicCast<data::FailBind>()->toString(), QString("plop"));
    QVERIFY(e.dynamicCast<data::FailBind>()->isReader());
}

void TestData::FailBindFrom_01()
{
    FailBindFrom f("256", "char");
    QCOMPARE(f.toString(), QString("Fail bindFrom(\"256\", char) returned false"));
    QVERIFY(f.isReader());
}

void TestData::FailBindFrom_02()
{
    FailBindFrom f("256", "char");
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailBindFrom>()->toString(), QString("Fail bindFrom(\"256\", char) returned false"));
    QVERIFY(e.dynamicCast<FailBindFrom>()->isReader());
}

void TestData::FailNull_01()
{
    FailNull f(true);
    QCOMPARE(f.toString(), QString("Fail to read null"));
    QVERIFY(f.isReader());
}

void TestData::FailNull_02()
{
    FailNull f(false);
    QCOMPARE(f.toString(), QString("Fail to write null"));
    QVERIFY(!f.isReader());
}

void TestData::FailNull_03()
{
    FailNull f(false);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailNull>()->toString(), QString("Fail to write null"));
    QVERIFY(!e.dynamicCast<FailNull>()->isReader());
}

void TestData::FailSequence_01()
{
    FailSequence f(true);
    QCOMPARE(f.toString(), QString("Fail to read a sequence"));
    QVERIFY(f.isReader());
}

void TestData::FailSequence_02()
{
    FailSequence f(false);
    QCOMPARE(f.toString(), QString("Fail to write a sequence"));
    QVERIFY(!f.isReader());
}

void TestData::FailSequence_03()
{
    FailSequence f(false);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailSequence>()->toString(), QString("Fail to write a sequence"));
    QVERIFY(!e.dynamicCast<FailSequence>()->isReader());
}

void TestData::FailSequenceItem_01()
{
    FailSequenceItem f(true, 3);
    QCOMPARE(f.toString(), QString("Fail to read element 3"));
    QVERIFY(f.isReader());
    QCOMPARE(f.getIdx(), 3);
}

void TestData::FailSequenceItem_02()
{
    FailSequenceItem f(false, 4);
    QCOMPARE(f.toString(), QString("Fail to write element 4"));
    QVERIFY(!f.isReader());
    QCOMPARE(f.getIdx(), 4);
}

void TestData::FailSequenceItem_03()
{
    FailSequenceItem f(false, 4);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailSequenceItem>()->toString(), QString("Fail to write element 4"));
    QVERIFY(!e.dynamicCast<FailSequenceItem>()->isReader());
    QCOMPARE(e.dynamicCast<FailSequenceItem>()->getIdx(), 4);
}

void TestData::FailSequenceOut_01()
{
    FailSequenceOut f(true);
    QCOMPARE(f.toString(), QString("Fail to read a sequence out"));
    QVERIFY(f.isReader());
}

void TestData::FailSequenceOut_02()
{
    FailSequenceOut f(false);
    QCOMPARE(f.toString(), QString("Fail to write a sequence out"));
    QVERIFY(!f.isReader());
}

void TestData::FailSequenceOut_03()
{
    FailSequenceOut f(false);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailSequenceOut>()->toString(), QString("Fail to write a sequence out"));
    QVERIFY(!e.dynamicCast<FailSequenceOut>()->isReader());
}

void TestData::FailRecord_01()
{
    FailRecord f(true);
    QCOMPARE(f.toString(), QString("Fail to read a record"));
    QVERIFY(f.isReader());
}

void TestData::FailRecord_02()
{
    FailRecord f(false);
    QCOMPARE(f.toString(), QString("Fail to write a record"));
    QVERIFY(!f.isReader());
}

void TestData::FailRecord_03()
{
    FailRecord f(false);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailRecord>()->toString(), QString("Fail to write a record"));
    QVERIFY(!e.dynamicCast<FailRecord>()->isReader());
}

void TestData::FailRecordItem_01()
{
    FailRecordItem f(true, "key");
    QCOMPARE(f.toString(), QString("Fail to read field \"key\""));
    QVERIFY(f.isReader());
    QCOMPARE(f.getKey(), QString("key"));
}

void TestData::FailRecordItem_02()
{
    FailRecordItem f(false, "key");
    QCOMPARE(f.toString(), QString("Fail to write field \"key\""));
    QVERIFY(!f.isReader());
    QCOMPARE(f.getKey(), QString("key"));
}

void TestData::FailRecordItem_03()
{
    FailRecordItem f(false, "key");
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailRecordItem>()->toString(), QString("Fail to write field \"key\""));
    QVERIFY(!e.dynamicCast<FailRecordItem>()->isReader());
    QCOMPARE(e.dynamicCast<FailRecordItem>()->getKey(), QString("key"));
}

void TestData::FailRecordItemEmptyKey_01()
{
    FailRecordItemEmptyKey f(true);
    QCOMPARE(f.toString(), QString("Fail to read a record field with an empty key"));
    QVERIFY(f.isReader());
}

void TestData::FailRecordItemEmptyKey_02()
{
    FailRecordItemEmptyKey f(false);
    QCOMPARE(f.toString(), QString("Fail to write a record field with an empty key"));
    QVERIFY(!f.isReader());
}

void TestData::FailRecordItemEmptyKey_03()
{
    FailRecordItemEmptyKey f(false);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailRecordItemEmptyKey>()->toString(), QString("Fail to write a record field with an empty key"));
    QVERIFY(!e.dynamicCast<FailRecordItemEmptyKey>()->isReader());
}

void TestData::FailRecordItemWrongKey_01()
{
    FailRecordItemWrongKey f("expected", "read");
    QCOMPARE(f.toString(), QString("Try to read key \"expected\" but found key \"read\""));
    QVERIFY(f.isReader());
    QCOMPARE(f.getExpectedKey(), QString("expected"));
    QCOMPARE(f.getReadKey(), QString("read"));
}

void TestData::FailRecordItemWrongKey_02()
{
    FailRecordItemWrongKey f("expected", "read");
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailRecordItemWrongKey>()->toString(), QString("Try to read key \"expected\" but found key \"read\""));
    QVERIFY(e.dynamicCast<FailRecordItemWrongKey>()->isReader());
    QCOMPARE(e.dynamicCast<FailRecordItemWrongKey>()->getExpectedKey(), QString("expected"));
    QCOMPARE(e.dynamicCast<FailRecordItemWrongKey>()->getReadKey(), QString("read"));
}

void TestData::FailRecordOut_01()
{
    FailRecordOut f(true);
    QCOMPARE(f.toString(), QString("Fail to read a record out"));
    QVERIFY(f.isReader());
}

void TestData::FailRecordOut_02()
{
    FailRecordOut f(false);
    QCOMPARE(f.toString(), QString("Fail to write a record out"));
    QVERIFY(!f.isReader());
}

void TestData::FailRecordOut_03()
{
    FailRecordOut f(false);
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailRecordOut>()->toString(), QString("Fail to write a record out"));
    QVERIFY(!e.dynamicCast<FailRecordOut>()->isReader());
}

void TestData::FailText_01()
{
    FailText f(true, "txt");
    QCOMPARE(f.toString(), QString("Fail to read a constant text \"txt\""));
    QVERIFY(f.isReader());
    QCOMPARE(f.getText(), QString("txt"));
}

void TestData::FailText_02()
{
    FailText f(false, "txt");
    QCOMPARE(f.toString(), QString("Fail to write a constant text \"txt\""));
    QVERIFY(!f.isReader());
    QCOMPARE(f.getText(), QString("txt"));
}

void TestData::FailText_03()
{
    FailText f(false, "txt");
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailText>()->toString(), QString("Fail to write a constant text \"txt\""));
    QVERIFY(!e.dynamicCast<FailText>()->isReader());
    QCOMPARE(e.dynamicCast<FailText>()->getText(), QString("txt"));
}

void TestData::FailBindText_01()
{
    FailBindText f(true, "txt");
    QCOMPARE(f.toString(), QString("Fail to read a text data \"txt\""));
    QVERIFY(f.isReader());
    QCOMPARE(f.getText(), QString("txt"));
}

void TestData::FailBindText_02()
{
    FailBindText f(false, "txt");
    QCOMPARE(f.toString(), QString("Fail to write a text data \"txt\""));
    QVERIFY(!f.isReader());
    QCOMPARE(f.getText(), QString("txt"));
}

void TestData::FailBindText_03()
{
    FailBindText f(false, "txt");
    auto e = f.clone();
    QCOMPARE(e.dynamicCast<FailBindText>()->toString(), QString("Fail to write a text data \"txt\""));
    QVERIFY(!e.dynamicCast<FailBindText>()->isReader());
    QCOMPARE(e.dynamicCast<FailBindText>()->getText(), QString("txt"));
}

void TestData::Bind_Default_01()
{
    IDataTester<Writer> IDT;
    const TestUserData<Writer> UD;
    IDT.write().bind(UD);
    QVERIFY(UD.bindCalled);
}

void TestData::Bind_Default_03()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 2;
    TestUserData<Writer> UD[2];
    IDT.write().bind(UD);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_sequenceOutCall[0]);
    QVERIFY(UD[0].bindCalled);
    QVERIFY(UD[1].bindCalled);
}

void TestData::Bind_Default_04()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 2;
    const TestUserData<Writer> UD[2];
    IDT.write().bind(UD);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_sequenceOutCall[0]);
    QVERIFY(UD[0].bindCalled);
    QVERIFY(UD[1].bindCalled);
}

void TestData::Bind_Pointer_01()
{
    IDataTester<Writer> IDT;
    int* i = nullptr;
    IDT.write().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_bindTextCall.size(), 0);
    delete i;
}

void TestData::Bind_Pointer_02()
{
    IDataTester<Writer> IDT;
    int* i = new int(42);
    IDT.write().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
    QCOMPARE(IDT.m_longlongValues.front(), 42ll);
    delete i;
}

void TestData::Bind_Pointer_03()
{
    IDataTester<Reader> IDT;
    IDT.m_null = true;
    int* i = new int(42);
    IDT.read().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(i, (int*) nullptr);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    delete i;
}

void TestData::Bind_Pointer_04()
{
    IDataTester<Reader> IDT;
    IDT.m_null = false;
    IDT.m_longlongValues.push_back(42);
    int* i = nullptr;
    IDT.read().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 1);
    QCOMPARE(*i, 42);
    delete i;
}

void TestData::Bind_Pointer_05()
{
    IDataTester<Reader> IDT;
    IDT.m_null = false;
    int* i = new int(42);
    IDT.read().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 1);
    QCOMPARE(i, nullptr); // See #625
    delete i;
}

void TestData::Bind_Data_04()
{
    IDataTester<Reader> IDT1;
    IDataTester<Writer> IDT2;
    IDT1.m_sequenceRetNullAfter = 0;
    IDT1.m_recordRetNullAfter = 0;
    Item<Reader> src = IDT1.read();
    Item<Writer> dst = IDT2.write();
    src.bind(dst);
    QCOMPARE(IDT2.m_nullCall.size(), 1);
}

void TestData::Bind_Data_05()
{
    IDataTester<Reader> IDT1;
    IDataTester<Writer> IDT2;
    IDT1.m_null = false;
    IDT1.m_sequenceRetNullAfter = 1;
    IDT1.m_sequenceItemRetNullAfter = 2;
    IDT2.m_sequenceItemRetNullAfter = 2;
    IDT1.m_recordRetNullAfter = 0;
    IDT1.m_bindTextStrings.push_back(QString("")); IDT1.m_bindTextStrings.push_back(QString(""));
    Item<Reader> src = IDT1.read();
    Item<Writer> dst = IDT2.write();
    src.bind(dst);
    QCOMPARE(IDT2.m_sequenceCall.size(), 1);
    QCOMPARE(IDT2.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT2.m_bindTextCall.size(), 2);
    QCOMPARE(IDT2.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT2.m_sequenceCall[0] < IDT2.m_sequenceItemCall[0]);
    QVERIFY(IDT2.m_sequenceItemCall[0] < IDT2.m_bindTextCall[0]);
    QVERIFY(IDT2.m_bindTextCall[0] < IDT2.m_sequenceItemCall[1]);
    QVERIFY(IDT2.m_sequenceItemCall[1] < IDT2.m_bindTextCall[1]);
    QVERIFY(IDT2.m_bindTextCall[1] < IDT2.m_sequenceOutCall[0]);
}

void TestData::Bind_Data_06()
{
    IDataTester<Reader> IDT1;
    IDataTester<Writer> IDT2;
    IDT1.m_null = false;
    IDT1.m_sequenceRetNullAfter = 0;
    IDT1.m_recordRetNullAfter = 1;
    IDT1.m_recordItemKey.push_back(QString("k1")); IDT1.m_recordItemKey.push_back(QString("k2"));
    IDT1.m_bindTextStrings.push_back(QString("")); IDT1.m_bindTextStrings.push_back(QString(""));
    Item<Reader> src = IDT1.read();
    Item<Writer> dst = IDT2.write();
    src.bind(dst);
    QCOMPARE(IDT2.m_recordCall.size(), 1);
    QCOMPARE(IDT2.m_recordItemCall.size(), 2);
    QCOMPARE(IDT2.m_bindTextCall.size(), 2);
    QCOMPARE(IDT2.m_recordOutCall.size(), 1);
    QVERIFY(IDT2.m_recordCall[0] < IDT2.m_recordItemCall[0]);
    QVERIFY(IDT2.m_recordItemCall[0] < IDT2.m_bindTextCall[0]);
    QVERIFY(IDT2.m_bindTextCall[0] < IDT2.m_recordItemCall[1]);
    QVERIFY(IDT2.m_recordItemCall[1] < IDT2.m_bindTextCall[1]);
    QVERIFY(IDT2.m_bindTextCall[1] < IDT2.m_recordOutCall[0]);
}

void TestData::Bind_Data_07()
{
    IDataTester<Reader> IDT1;
    IDataTester<Writer> IDT2;
    IDT1.m_null = false;
    IDT1.m_sequenceRetNullAfter = 0;
    IDT1.m_recordRetNullAfter = 0;
    IDT1.m_bindTextStrings.push_back(QString(""));
    Item<Reader> src = IDT1.read();
    Item<Writer> dst = IDT2.write();
    src.bind(dst);
    QCOMPARE(IDT2.m_bindTextCall.size(), 1);
}

#define TEST_BIND_HEX_READER_SUCCESS_2(type, value, T) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_bindTextStrings.push_back(QString(T)); \
        type c; \
        Hex h(c); \
        auto res = IDT.read(&err).bind(h); \
        QCOMPARE(c, (type) value); \
        QVERIFY(res.noError()); \
    }

#define TEST_BIND_HEX_READER_SUCCESS(type, value) TEST_BIND_HEX_READER_SUCCESS_2(type, value, #value)

#define TEST_BIND_HEX_READER_FAILURE(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_bindTextStrings.push_back(QString(#value)); \
        type c; \
        Hex h(c); \
        auto res = IDT.read(&err).bind<Hex>(h); \
        QCOMPARE(res.errors()->size(), 1); \
        QVERIFY(res.errors()->find<FailBindFrom>()); \
        QCOMPARE(res.errors()->find<FailBindFrom>()->getRead(), QString(#value)); \
    }

#define TEST_BIND_HEX_WRITER_2(type, value, T) \
    { \
        IDataTester<Writer> IDT; \
        BindErrors err; \
        auto res = IDT.write(&err).bind(Hex((type)value)); \
        QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString(T)); \
        QVERIFY(res.noError()); \
    }

#define TEST_BIND_HEX_WRITER(type, value) TEST_BIND_HEX_WRITER_2(type, value, #value)

void TestData::Bind_Hex_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(Hex(10));
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_Hex_02()
{
    TEST_BIND_HEX_READER_SUCCESS(bool, 0x0);
    TEST_BIND_HEX_READER_SUCCESS(bool, 0x1);
    TEST_BIND_HEX_READER_FAILURE(bool, "plop");

    TEST_BIND_HEX_READER_SUCCESS(uint8_t, 0x1C);
    TEST_BIND_HEX_READER_SUCCESS(uint8_t, 0xFF);
    TEST_BIND_HEX_READER_FAILURE(uint8_t, "plop");

    TEST_BIND_HEX_READER_SUCCESS(uint16_t, 0xABCD);
    TEST_BIND_HEX_READER_SUCCESS(uint16_t, 0xFFFF);
    TEST_BIND_HEX_READER_FAILURE(uint16_t, "plop");

    TEST_BIND_HEX_READER_SUCCESS(uint32_t, 0xABCDEF01);
    TEST_BIND_HEX_READER_SUCCESS(uint32_t, 0xFFFFFFFF);
    TEST_BIND_HEX_READER_FAILURE(uint32_t, "plop");

    TEST_BIND_HEX_READER_SUCCESS(uint64_t, 0xABCDEF0123456789);
    TEST_BIND_HEX_READER_SUCCESS(uint64_t, 0xFFFFFFFFFFFFFFFF);
    TEST_BIND_HEX_READER_FAILURE(uint64_t, "plop");
}

void TestData::Bind_Hex_03()
{
    TEST_BIND_HEX_WRITER(bool, 0x0);
    TEST_BIND_HEX_WRITER(bool, 0x1);

    TEST_BIND_HEX_WRITER(uint8_t, 0x1c);
    TEST_BIND_HEX_WRITER(uint8_t, 0xff);

    TEST_BIND_HEX_WRITER(uint16_t, 0xabcd);
    TEST_BIND_HEX_WRITER(uint16_t, 0xffff);

    TEST_BIND_HEX_WRITER(uint32_t, 0xabcdef01);
    TEST_BIND_HEX_WRITER(uint32_t, 0xffffffff);

    TEST_BIND_HEX_WRITER(uint64_t, 0xabcdef0123456789);
    TEST_BIND_HEX_WRITER(uint64_t, 0xffffffffffffffff);

    int i;
    TEST_BIND_HEX_WRITER_2(int*, &i, "0x" + QString::number((uintptr_t)&i, 16).toLower());
}

#define TEST_BIND_BOOL_READER_SUCCESS(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_boolValues.push_back(value); \
        type c; \
        QVERIFY(IDT.read(&err).bind(c).noError()); \
        QCOMPARE(c, (type) value); \
    }


#define TEST_BIND_BOOL_READER_FAILURE(type) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        type c; \
        auto res = IDT.read(&err).bind<type>(c); \
        QCOMPARE(res.errors()->size(), 1); \
        QVERIFY(res.errors()->find<FailBind>()); \
    }

#define TEST_BIND_BOOL_WRITER(type, value) \
    { \
        IDataTester<Writer> IDT; \
        BindErrors err; \
        auto res = IDT.write(&err).bind<type>(value); \
        QCOMPARE(IDT.m_boolValues.takeFirst(), value); \
        QVERIFY(res.noError()); \
    }

void TestData::Bind_Bool_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(true);
    QCOMPARE(IDT.m_booleanCall.size(), 1);
}

void TestData::Bind_Bool_02()
{
    TEST_BIND_BOOL_READER_SUCCESS(bool, true)
    TEST_BIND_BOOL_READER_SUCCESS(bool, false)
    TEST_BIND_BOOL_READER_FAILURE(bool)
}

void TestData::Bind_Bool_03()
{
    TEST_BIND_BOOL_WRITER(bool, true)
    TEST_BIND_BOOL_WRITER(bool, false)
}

#define TEST_BIND_NUM_READER_SUCCESS(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_longlongValues.push_back(value); \
        type c; \
        QVERIFY(IDT.read(&err).bind(c).noError()); \
        QCOMPARE(c, (type) value); \
    }

#define TEST_BIND_NUM_READER_FAILURE(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_longlongValues.push_back(value); \
        type c; \
        auto res = IDT.read(&err).bind<type>(c); \
        QCOMPARE(res.errors()->size(), 1); \
        QVERIFY(res.errors()->find<FailBind>()); \
    }

#define TEST_BIND_NUM_READER_FAILURE_STR(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_bindTextStrings.push_back(QString(#value)); \
        type c; \
        auto res = IDT.read(&err).bind<type>(c); \
        QCOMPARE(res.errors()->size(), 1); \
        QVERIFY(res.errors()->find<FailBindFrom>()); \
    }

#define TEST_BIND_NUM_WRITER(type, value) \
    { \
        IDataTester<Writer> IDT; \
        BindErrors err; \
        QVERIFY(IDT.write(&err).bind<type>(value).noError()); \
        QCOMPARE(IDT.m_longlongValues.takeFirst(), value); \
    }

void TestData::Bind_Char_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<char>(0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_Char_02()
{
    TEST_BIND_NUM_READER_SUCCESS(char, -128)
    TEST_BIND_NUM_READER_SUCCESS(char, 127)
    TEST_BIND_NUM_READER_FAILURE(char, 128)
    TEST_BIND_NUM_READER_FAILURE(char, -129)
}

void TestData::Bind_Char_03()
{
    TEST_BIND_NUM_WRITER(char, 42)
}

void TestData::Bind_UnsignedChar_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<unsigned char>(0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_UnsignedChar_02()
{
    TEST_BIND_NUM_READER_SUCCESS(unsigned char, 0)
    TEST_BIND_NUM_READER_SUCCESS(unsigned char, 255)
    TEST_BIND_NUM_READER_FAILURE(unsigned char, 256)
    TEST_BIND_NUM_READER_FAILURE(unsigned char, -1)
}

void TestData::Bind_UnsignedChar_03()
{
    TEST_BIND_NUM_WRITER(unsigned char, 42)
}

void TestData::Bind_Short_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<short>(0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_Short_02()
{
    TEST_BIND_NUM_READER_SUCCESS(short, -32768)
    TEST_BIND_NUM_READER_SUCCESS(short, 32767)
    TEST_BIND_NUM_READER_FAILURE(short, 32768)
    TEST_BIND_NUM_READER_FAILURE(short, -32769)
}

void TestData::Bind_Short_03()
{
    TEST_BIND_NUM_WRITER(short, 42)
}

void TestData::Bind_UnsignedShort_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<unsigned short>(0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_UnsignedShort_02()
{
    TEST_BIND_NUM_READER_SUCCESS(unsigned short, 0)
    TEST_BIND_NUM_READER_SUCCESS(unsigned short, 65535)
    TEST_BIND_NUM_READER_FAILURE(unsigned short, 65536)
    TEST_BIND_NUM_READER_FAILURE(unsigned short, -1)
}

void TestData::Bind_UnsignedShort_03()
{
    TEST_BIND_NUM_WRITER(unsigned short, 42)
}

void TestData::Bind_Int_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<int>(0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

QT_WARNING_DISABLE_MSVC(4146)

void TestData::Bind_Int_02()
{
    TEST_BIND_NUM_READER_SUCCESS(int, -2147483648ll)
    TEST_BIND_NUM_READER_SUCCESS(int, 2147483647ll)
    TEST_BIND_NUM_READER_FAILURE(int, 2147483648ll)
    TEST_BIND_NUM_READER_FAILURE(int, -2147483649ll)
}

void TestData::Bind_Int_03()
{
    TEST_BIND_NUM_WRITER(int, 42)
}

void TestData::Bind_UnsignedInt_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<unsigned int>(0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_UnsignedInt_02()
{
    TEST_BIND_NUM_READER_SUCCESS(unsigned int, 0)
    TEST_BIND_NUM_READER_SUCCESS(unsigned int, 4294967295)
    TEST_BIND_NUM_READER_FAILURE(unsigned int, 4294967296)
    TEST_BIND_NUM_READER_FAILURE(unsigned int, -1)
}

void TestData::Bind_UnsignedInt_03()
{
    TEST_BIND_NUM_WRITER(unsigned int, 42)
}

void TestData::Bind_Long_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<long>(0l);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

#ifdef Q_OS_WIN
#define MAX_LONG_STR 2147483647ll
#define MIN_LONG_STR -2147483648ll
#define MAX_LONG_STR_PLUS1 2147483648ll
#define MIN_LONG_STR_MINUS1 -2147483649ll
#else
#define MAX_LONG_STR 9223372036854775807
#define MIN_LONG_STR -9223372036854775808
#define MAX_LONG_STR_PLUS1 9223372036854775808
#define MIN_LONG_STR_MINUS1 -9223372036854775809
#endif
void TestData::Bind_Long_02()
{
    TEST_BIND_NUM_READER_SUCCESS(long, MIN_LONG_STR)
    TEST_BIND_NUM_READER_SUCCESS(long, MAX_LONG_STR)
    TEST_BIND_NUM_READER_FAILURE_STR(long, MAX_LONG_STR_PLUS1)
    TEST_BIND_NUM_READER_FAILURE_STR(long, MIN_LONG_STR_MINUS1)
}

void TestData::Bind_Long_03()
{
    TEST_BIND_NUM_WRITER(long, 42)
}

void TestData::Bind_UnsignedLong_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<unsigned long>(0ul);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

#ifdef Q_OS_WIN
#define MAX_ULONG_STR 4294967295
#define MAX_ULONG_STR_PLUS1 4294967296
#else
#define MAX_ULONG_STR 18446744073709551615
#define MAX_ULONG_STR_PLUS1 18446744073709551616
#endif
void TestData::Bind_UnsignedLong_02()
{
    TEST_BIND_NUM_READER_SUCCESS(unsigned long, 0)
    TEST_BIND_NUM_READER_SUCCESS(unsigned long, MAX_ULONG_STR)
    TEST_BIND_NUM_READER_FAILURE_STR(unsigned long, MAX_ULONG_STR_PLUS1)
    TEST_BIND_NUM_READER_FAILURE_STR(unsigned long, -1)
}

void TestData::Bind_UnsignedLong_03()
{
    TEST_BIND_NUM_WRITER(unsigned long, 42)
}

void TestData::Bind_LongLong_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<long long>(0ll);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_LongLong_02()
{
    TEST_BIND_NUM_READER_SUCCESS(long long, -9223372036854775808ULL)
    TEST_BIND_NUM_READER_SUCCESS(long long, 9223372036854775807LL)
    TEST_BIND_NUM_READER_FAILURE_STR(long long, 9223372036854775808)
    TEST_BIND_NUM_READER_FAILURE_STR(long long, -9223372036854775809)
}

void TestData::Bind_LongLong_03()
{
    TEST_BIND_NUM_WRITER(long long, 42)
}

void TestData::Bind_UnsignedLongLong_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<unsigned long long>(0ull);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_UnsignedLongLong_02()
{
    TEST_BIND_NUM_READER_SUCCESS(unsigned long long, 0ull)
    TEST_BIND_NUM_READER_SUCCESS(unsigned long long, 18446744073709551615ull)
    TEST_BIND_NUM_READER_FAILURE_STR(unsigned long long, 18446744073709551616)
    TEST_BIND_NUM_READER_FAILURE_STR(unsigned long long, -1)
}

void TestData::Bind_UnsignedLongLong_03()
{
    TEST_BIND_NUM_WRITER(unsigned long long, 42)
}

#define TEST_BIND_FLT_READER_SUCCESS(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_doubleValues.push_back(value); \
        type c; \
        auto res = IDT.read(&err).bind(c); \
        QCOMPARE(c, (type) value); \
        QVERIFY(res.noError()); \
    }

#define TEST_BIND_FLT_READER_FAILURE(type, value) \
    { \
        IDataTester<Reader> IDT; \
        BindErrors err; \
        IDT.m_bindTextStrings.push_back(QString(#value)); \
        type c; \
        auto res = IDT.read(&err).bind<type>(c); \
        QCOMPARE(res.errors()->size(), 1); \
        QVERIFY(res.errors()->find<FailBindFrom>()); \
    }

#define TEST_BIND_FLT_WRITER(type, value) \
    { \
        IDataTester<Writer> IDT; \
        BindErrors err; \
        auto res = IDT.write(&err).bind<type>(value); \
        QCOMPARE(IDT.m_doubleValues.takeFirst(), value); \
        QVERIFY(res.noError()); \
    }

void TestData::Bind_Float_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<float>(0.f);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_Float_02()
{
    TEST_BIND_FLT_READER_SUCCESS(float, -1.25e+10f)
    TEST_BIND_FLT_READER_SUCCESS(float, 127.f)
    TEST_BIND_FLT_READER_FAILURE(float, plop)
}

void TestData::Bind_Float_03()
{
    TEST_BIND_FLT_WRITER(float, 42.4242424242424242f)
}

void TestData::Bind_Double_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<double>(0.);
    QCOMPARE(IDT.m_numberCall.size(), 1);
}

void TestData::Bind_Double_02()
{
    TEST_BIND_FLT_READER_SUCCESS(double, -1.25e+10)
    TEST_BIND_FLT_READER_SUCCESS(double, 127.)
    TEST_BIND_FLT_READER_FAILURE(double, plop)
}

void TestData::Bind_Double_03()
{
    TEST_BIND_FLT_WRITER(double, 42.4242424242424242)
}

void TestData::Bind_CharArray_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind("");
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_CharArray_02()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back(QString("plop"));
    char s[5];
    auto err = IDT.read().bind<char[5]>(s);
    QCOMPARE(QString(s), QString("plop"));
    QVERIFY(err.noError());
}

void TestData::Bind_CharArray_03()
{
    IDataTester<Writer> IDT;
    auto res = IDT.write().bind<char[5]>("plop");
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("plop"));
    QVERIFY(res.noError());
}

void TestData::Bind_String_01()
{
    IDataTester<Writer> IDT;
    std::string s = "plop";
    IDT.write().bind(s);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString("plop"));
}

void TestData::Bind_String_02()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back(QString("plop"));
    std::string s;
    IDT.read().bind(s);
    QCOMPARE(s, std::string("plop"));
}

void TestData::Bind_String_03()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back(QString(QChar(0x03A9)) + QChar('A')); // <Omega>A
    std::string s;
    IDT.read().bind(s);
#ifdef Q_OS_UNIX
    QCOMPARE(s, (QString(QChar(0x03A9)) + 'A').toStdString());
#else
    QCOMPARE(s, std::string("\x01A""A"));
#endif
}

void TestData::Bind_WString_01()
{
    IDataTester<Writer> IDT;
    std::wstring s = L"plop";
    IDT.write().bind(s);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString("plop"));
}

void TestData::Bind_WString_02()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back(QString("plop"));
    std::wstring s;
    IDT.read().bind(s);
    QCOMPARE(s, std::wstring(L"plop"));
}

void TestData::Bind_Array_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    std::array<int, 3> v = {{1, 2, 3}};
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
}

void TestData::Bind_Array_02()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    std::array<int, 3> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(v[0], 1);
    QCOMPARE(v[1], 2);
    QCOMPARE(v[2], 3);
}

void TestData::Bind_Vector_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    std::vector<int> v = {{1}, {2}, {3}};
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
}

void TestData::Bind_Vector_02()
{
    IDataTester<Writer> IDT;
    std::vector<int> v = {};
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_Vector_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    std::vector<int> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_Vector_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    std::vector<int> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(v[0], 1);
    QCOMPARE(v[1], 2);
    QCOMPARE(v[2], 3);
}

void TestData::Bind_Vector_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    std::vector<int> v = {{1}, {2}, {3}};
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(v.empty());
}

void TestData::Bind_Map_01()
{
    IDataTester<Writer> IDT;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 2;
    std::map<int, int> m;
    m[1] = 2;
    m[3] = 4;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_recordCall.size(), 2);
    QCOMPARE(IDT.m_recordItemCall.size(), 4);
    QCOMPARE(IDT.m_recordOutCall.size(), 2);
    QCOMPARE(IDT.m_numberCall.size(), 4);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);

    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);

    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_recordCall[1]);
    QVERIFY(IDT.m_recordCall[1] < IDT.m_recordItemCall[2]);
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_recordItemCall[3]);
    QVERIFY(IDT.m_recordItemCall[3] < IDT.m_numberCall[3]);
    QVERIFY(IDT.m_numberCall[3] < IDT.m_recordOutCall[1]);

    QVERIFY(IDT.m_recordOutCall[1] < IDT.m_sequenceOutCall[0]);

    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 4);
}

void TestData::Bind_Map_02()
{
    IDataTester<Writer> IDT;
    std::map<int, int> m;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_Map_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    std::map<int, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_Map_04()
{
    IDataTester<Reader> IDT;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 1;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2);
    IDT.m_recordItemKey.push_back("key");
    IDT.m_recordItemKey.push_back("value");
    std::map<int, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);

    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);
    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceItemCall[1]);

    QCOMPARE(m[1], 2);
}

void TestData::Bind_Map_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    std::map<int, int> m = { {{1}, {2}}, {{3}, {4}} };
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(m.empty());
}

void TestData::Bind_StringMap_01()
{
    IDataTester<Writer> IDT;
    std::map<std::string, int> m;
    m["k1"] = 1;
    m["k2"] = 2;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 2);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_recordItemKey.takeFirst(), QString("k1"));
    QCOMPARE(IDT.m_recordItemKey.takeFirst(), QString("k2"));
}

void TestData::Bind_StringMap_02()
{
    IDataTester<Writer> IDT;
    std::map<std::string, int> m;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordOutCall.front());
}

void TestData::Bind_StringMap_03()
{
    IDataTester<Reader> IDT;
    IDT.m_recordRetNullAfter = 0;
    std::map<std::string, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 0);
}

void TestData::Bind_StringMap_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2);
    IDT.m_recordItemKey.push_back("k1");
    IDT.m_recordItemKey.push_back("k2");
    std::map<std::string, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordItemCall[2]);
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_recordOutCall[0]);
    QCOMPARE(m["k1"], 1);
    QCOMPARE(m["k2"], 2);
}

void TestData::Bind_StringMap_05()
{
    IDataTester<Reader> IDT;
    IDT.m_recordItemRetNull = true;
    std::map<std::string, int> m = { {"k1", 1}, {"k2", 2} };
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordItemCall.front());
    QVERIFY(IDT.m_recordItemCall.front() < IDT.m_recordOutCall.front());
    QVERIFY(m.empty());
}

void TestData::Bind_List_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    std::list<int> v = { {1}, {2}, {3} };
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
}

void TestData::Bind_List_02()
{
    IDataTester<Writer> IDT;
    std::list<int> v = {};
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_List_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    std::list<int> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_List_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    std::list<int> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(v.front(), 1); v.pop_front();
    QCOMPARE(v.front(), 2); v.pop_front();
    QCOMPARE(v.front(), 3);
}

void TestData::Bind_List_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    std::list<int> v = { {1}, {2}, {3} };
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(v.empty());
}

void TestData::Bind_Set_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    std::set<int> s = { {1}, {2}, {3} };
    IDT.write().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QVERIFY(IDT.m_longlongValues.contains(1));
    QVERIFY(IDT.m_longlongValues.contains(2));
    QVERIFY(IDT.m_longlongValues.contains(3));
}

void TestData::Bind_Set_02()
{
    IDataTester<Writer> IDT;
    std::set<int> s = {};
    IDT.write().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_Set_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    std::set<int> s;
    IDT.read().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_Set_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    std::set<int> s;
    IDT.read().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QVERIFY(s.find(1) != s.end());
    QVERIFY(s.find(2) != s.end());
    QVERIFY(s.find(3) != s.end());
}

void TestData::Bind_Set_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    std::set<int> s = { {1}, {2}, {3} };
    IDT.read().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(s.empty());
}

void TestData::Bind_QString_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind<QString>("");
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_QString_02()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back("plop");
    QString s;
    IDT.read().bind(s);
    QCOMPARE(s, QString("plop"));
}

void TestData::Bind_QString_03()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QString("plop"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("plop"));
}

void TestData::Bind_QDate_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QDate(2015, 01, 31));
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_QDate_02()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("2015-01-31");
    QDate d;
    auto res = IDT.read(&err).bind(d);
    QCOMPARE(d, QDate(2015, 1, 31));
    QVERIFY(res.noError());
}

void TestData::Bind_QDate_03()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("invalid");
    QDate d;
    auto res = IDT.read(&err).bind(d);
    QCOMPARE(res.errors()->size(), 1);
    QVERIFY(res.errors()->find<FailBindFrom>());
}

void TestData::Bind_QDate_04()
{
    QDate d(2015, 1, 31);
    IDataTester<Writer> IDT;
    IDT.write().bind(d);
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString("2015-01-31"));
}

void TestData::Bind_QDate_05()
{
    QDate d;
    IDataTester<Writer> IDT;
    IDT.write().bind(d);
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString(""));
}

void TestData::Bind_QTime_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QTime(23,42));
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_QTime_02()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("23:42:42.666");
    QTime t;
    auto res = IDT.read(&err).bind(t);
    QCOMPARE(t, QTime(23, 42, 42, 666));
    QVERIFY(res.noError());
}

void TestData::Bind_QTime_03()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("invalid");
    QTime t;
    auto res = IDT.read(&err).bind(t);
    QCOMPARE(res.errors()->size(), 1);
    QVERIFY(res.errors()->find<FailBindFrom>());
}

void TestData::Bind_QTime_04()
{
    QTime t(23, 42, 42, 666);
    IDataTester<Writer> IDT;
    IDT.write().bind(t);
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString("23:42:42.666"));
}

void TestData::Bind_QTime_05()
{
    QTime t;
    IDataTester<Writer> IDT;
    IDT.write().bind(t);
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString(""));
}

void TestData::Bind_QUuid_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QUuid::createUuid());
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_QUuid_02()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("{67C8770B-44F1-410A-AB9A-F9B5446F13EE}");
    QUuid u;
    auto res = IDT.read(&err).bind(u);
    QCOMPARE(u, QUuid(0x67c8770b, 0x44f1, 0x410a, 0xab, 0x9a, 0xf9, 0xb5, 0x44, 0x6f, 0x13, 0xee));
    QVERIFY(res.noError());
}

void TestData::Bind_QUuid_03()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("invalid");
    QUuid u;
    auto res = IDT.read(&err).bind(u);
    QCOMPARE(res.errors()->size(), 1);
    QVERIFY(res.errors()->find<FailBindFrom>());
}

void TestData::Bind_QUuid_04()
{
    QUuid u(0x67c8770b, 0x44f1, 0x410a, 0xab, 0x9a, 0xf9, 0xb5, 0x44, 0x6f, 0x13, 0xee);
    IDataTester<Writer> IDT;
    IDT.write().bind(u);
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
    QCOMPARE(IDT.m_bindTextStrings.front(), QString("{67c8770b-44f1-410a-ab9a-f9b5446f13ee}"));
}

void TestData::Bind_QDateTime_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QDateTime(QDate(2015, 1, 31), QTime(23, 42, 42, 666), Qt::OffsetFromUTC, 30600));
    QCOMPARE(IDT.m_timestampCall.size(), 1);
}

void TestData::Bind_QDateTime_02()
{
    QDateTime d1(QDate(2015, 1, 31), QTime(23, 42, 42, 666), Qt::OffsetFromUTC, 30600);
    QDateTime d2(QDate(2015, 1, 31), QTime(23, 42, 42, 666), Qt::OffsetFromUTC, -30600);
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_timestampValues.push_back(d1);
    IDT.m_timestampValues.push_back(d2);
    QDateTime t;
    auto res = IDT.read(&err).bind(t);
    // Keep this line because QTest compare QDateTime considering the offset but does not print it when fails
    QCOMPARE(t.offsetFromUtc(), d1.offsetFromUtc());
    QCOMPARE(t, d1);
    QVERIFY(res.noError());

    res = IDT.read(&err).bind(t);
    QCOMPARE(t.offsetFromUtc(), d2.offsetFromUtc());
    QCOMPARE(t, d2);
    QVERIFY(res.noError());
}

void TestData::Bind_QDateTime_03()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("invalid");
    QDateTime t;
    auto res = IDT.read(&err).bind(t);
    QCOMPARE(res.errors()->size(), 1);
    QVERIFY(res.errors()->find<FailBindFrom>());
}

void TestData::Bind_QDateTime_04()
{
    QDateTime t(QDate(2015, 1, 31), QTime(23, 42, 42, 666), Qt::OffsetFromUTC, 30600);
    IDataTester<Writer> IDT;
    IDT.write().bind(t);
    QCOMPARE(IDT.m_timestampValues.takeFirst(), t);

    t = QDateTime(QDate(2015, 1, 31), QTime(23, 42, 42, 666), Qt::OffsetFromUTC, -30600);
    IDT.write().bind(t);
    QCOMPARE(IDT.m_timestampValues.takeFirst(), t);
}

void TestData::Bind_QDateTime_05()
{
    QDateTime t;
    IDataTester<Writer> IDT;
    IDT.write().bind(t);
    QCOMPARE(IDT.m_timestampCall.size(), 1);
    QVERIFY(IDT.m_timestampValues.front().isNull());
}

void TestData::Bind_QMsgType_01()
{
    IDataTester<Writer> IDT;
    QtMsgType t = QtDebugMsg;
    IDT.write().bind(t);
    QCOMPARE(IDT.m_bindTextCall.size(), 1);
}

void TestData::Bind_QMsgType_02()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back("DEBUG");
    IDT.m_bindTextStrings.push_back("WARNING");
    IDT.m_bindTextStrings.push_back("CRITICAL");
    IDT.m_bindTextStrings.push_back("EMERGENCY");
    IDT.m_bindTextStrings.push_back("INFORMATIONAL");
    QtMsgType t;
    IDT.read().bind(t);
    QCOMPARE(t, QtDebugMsg);
    IDT.read().bind(t);
    QCOMPARE(t, QtWarningMsg);
    IDT.read().bind(t);
    QCOMPARE(t, QtCriticalMsg);
    IDT.read().bind(t);
    QCOMPARE(t, QtFatalMsg);
    IDT.read().bind(t);
    QCOMPARE(t, QtInfoMsg);
}

void TestData::Bind_QMsgType_03()
{
    IDataTester<Reader> IDT;
    BindErrors err;
    IDT.m_bindTextStrings.push_back("invalid");
    QtMsgType t;
    auto res = IDT.read(&err).bind(t);
    QCOMPARE(res.errors()->size(), 1);
    QVERIFY(res.errors()->find<FailBindFrom>());
}

void TestData::Bind_QMsgType_04()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QtDebugMsg);
    IDT.write().bind(QtWarningMsg);
    IDT.write().bind(QtCriticalMsg);
    IDT.write().bind(QtFatalMsg);
    IDT.write().bind(QtInfoMsg);
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("DEBUG"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("WARNING"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("CRITICAL"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("EMERGENCY"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("INFORMATIONAL"));
}

void TestData::Bind_QSharedPointer_01()
{
    IDataTester<Writer> IDT;
    QSharedPointer<int> i(nullptr);
    IDT.write().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
}

void TestData::Bind_QSharedPointer_02()
{
    IDataTester<Writer> IDT;
    QSharedPointer<int> i(new int(42));
    IDT.write().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 0);
    QCOMPARE(IDT.m_numberCall.size(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 42);
}

void TestData::Bind_QSharedPointer_03()
{
    IDataTester<Reader> IDT;
    IDT.m_null = true;
    QSharedPointer<int> i(new int(42));
    IDT.read().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(i, QSharedPointer<int>());
    QCOMPARE(IDT.m_numberCall.size(), 0);
}

void TestData::Bind_QSharedPointer_04()
{
    IDataTester<Reader> IDT;
    IDT.m_null = false;
    IDT.m_longlongValues.push_back(42);
    QSharedPointer<int> i(nullptr);
    IDT.read().bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 1);
    QCOMPARE(*i, int(42));
}

void TestData::Bind_QSharedPointer_05()
{
    IDataTester<Reader> IDT;
    IDT.m_null = false;
    IDT.m_bindText = false;
    BindErrors err;
    QSharedPointer<int> i(new int(42));
    int* iPtr = i.data();
    auto res = IDT.read(&err).bind(i);
    QCOMPARE(IDT.m_nullCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 1);
    QVERIFY(iPtr != i.data()); // #625: new int if we change Bind<Reader,QSharedPointer>
    QCOMPARE(res.errors()->size(), 1);
    QVERIFY(res.errors()->find<FailBind>());
}

void TestData::Bind_QList_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    QList<int> l = { 1, 2, 3 };
    IDT.write().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
}

void TestData::Bind_QList_02()
{
    IDataTester<Writer> IDT;
    QList<int> l = {};
    IDT.write().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QList_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QList<int> l;
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QList_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    QList<int> l;
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(l[0], 1);
    QCOMPARE(l[1], 2);
    QCOMPARE(l[2], 3);
}

void TestData::Bind_QList_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QList<int> l = { 1, 2, 3 };
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(l.empty());
}

void TestData::Bind_QStringList_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    QStringList l; l.push_back("1"); l.push_back("2"); l.push_back("3");
    IDT.write().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_bindTextCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_bindTextCall[0]);
    QVERIFY(IDT.m_bindTextCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_bindTextCall[1]);
    QVERIFY(IDT.m_bindTextCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_bindTextCall[2]);
    QVERIFY(IDT.m_bindTextCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("1"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("2"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("3"));
}

void TestData::Bind_QStringList_02()
{
    IDataTester<Writer> IDT;
    QStringList l = {};
    IDT.write().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QStringList_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QStringList l;
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QStringList_04()
{
    IDataTester<Reader> IDT;
    IDT.m_bindTextStrings.push_back(QString("1")); IDT.m_bindTextStrings.push_back(QString("2")); IDT.m_bindTextStrings.push_back(QString("3"));
    IDT.m_sequenceItemRetNullAfter = 3;
    QStringList l;
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_bindTextCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_bindTextCall[0]);
    QVERIFY(IDT.m_bindTextCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_bindTextCall[1]);
    QVERIFY(IDT.m_bindTextCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_bindTextCall[2]);
    QVERIFY(IDT.m_bindTextCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(l[0], QString("1"));
    QCOMPARE(l[1], QString("2"));
    QCOMPARE(l[2], QString("3"));
}

void TestData::Bind_QStringList_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QStringList l; l.push_back("1"); l.push_back("2"); l.push_back("3");
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_bindTextCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(l.empty());
}

void TestData::Bind_QLinkedList_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    QLinkedList<int> l; l.push_back(1); l.push_back(2); l.push_back(3);
    IDT.write().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
}

void TestData::Bind_QLinkedList_02()
{
    IDataTester<Writer> IDT;
    QLinkedList<int> l = {};
    IDT.write().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QLinkedList_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QLinkedList<int> l;
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QLinkedList_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    QLinkedList<int> l;
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(l.takeFirst(), 1);
    QCOMPARE(l.takeFirst(), 2);
    QCOMPARE(l.takeFirst(), 3);
}

void TestData::Bind_QLinkedList_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QLinkedList<int> l; l.push_back(1); l.push_back(2); l.push_back(3);
    IDT.read().bind(l);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(l.empty());
}

void TestData::Bind_QVector_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    QVector<int> v; v.push_back(1); v.push_back(2); v.push_back(3);
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
}

void TestData::Bind_QVector_02()
{
    IDataTester<Writer> IDT;
    QVector<int> v = {};
    IDT.write().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QVector_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QVector<int> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QVector_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    QVector<int> v;
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(v[0], 1);
    QCOMPARE(v[1], 2);
    QCOMPARE(v[2], 3);
}

void TestData::Bind_QVector_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QVector<int> v; v.push_back(1); v.push_back(2); v.push_back(3);
    IDT.read().bind(v);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(v.empty());
}

void TestData::Bind_QSet_01()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    QSet<int> s = { 1, 2, 3 };
    IDT.write().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceOutCall[0]);
    QVERIFY(IDT.m_longlongValues.contains(1));
    QVERIFY(IDT.m_longlongValues.contains(2));
    QVERIFY(IDT.m_longlongValues.contains(3));
}

void TestData::Bind_QSet_02()
{
    IDataTester<Writer> IDT;
    QSet<int> s = {};
    IDT.write().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QSet_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QSet<int> s;
    IDT.read().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QSet_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2); IDT.m_longlongValues.push_back(3);
    IDT.m_sequenceItemRetNullAfter = 3;
    QSet<int> s;
    IDT.read().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 4);
    QCOMPARE(IDT.m_numberCall.size(), 3);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_sequenceItemCall[3]);
    QVERIFY(IDT.m_sequenceItemCall[3] < IDT.m_sequenceOutCall[0]);
    QVERIFY(s.contains(1));
    QVERIFY(s.contains(2));
    QVERIFY(s.contains(3));
}

void TestData::Bind_QSet_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QSet<int> s = { 1, 2, 3 };
    IDT.read().bind(s);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(s.empty());
}

void TestData::Bind_QStringMap_01()
{
    IDataTester<Writer> IDT;
    QMap<QString, int> m;
    m["k1"] = 1;
    m["k2"] = 2;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 2);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
}

void TestData::Bind_QStringMap_02()
{
    IDataTester<Writer> IDT;
    QMap<QString, int> m;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordOutCall.front());
}

void TestData::Bind_QStringMap_03()
{
    IDataTester<Reader> IDT;
    IDT.m_recordRetNullAfter = 0;
    QMap<QString, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 0);
}

void TestData::Bind_QStringMap_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2);
    IDT.m_recordItemKey.push_back("k1");
    IDT.m_recordItemKey.push_back("k2");
    QMap<QString, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordItemCall[2]);
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_recordOutCall[0]);
    QCOMPARE(m["k1"], 1);
    QCOMPARE(m["k2"], 2);
}

void TestData::Bind_QStringMap_05()
{
    IDataTester<Reader> IDT;
    IDT.m_recordItemRetNull = true;
    QMap<QString, int> m = { {"k1", 1}, {"k2", 2} };
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordItemCall.front());
    QVERIFY(IDT.m_recordItemCall.front() < IDT.m_recordOutCall.front());
    QVERIFY(m.empty());
}

void TestData::Bind_QMap_01()
{
    IDataTester<Writer> IDT;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 2;
    QMap<int, int> m;
    m[1] = 2;
    m[3] = 4;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_recordCall.size(), 2);
    QCOMPARE(IDT.m_recordItemCall.size(), 4);
    QCOMPARE(IDT.m_recordOutCall.size(), 2);
    QCOMPARE(IDT.m_numberCall.size(), 4);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);

    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);

    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_recordCall[1]);
    QVERIFY(IDT.m_recordCall[1] < IDT.m_recordItemCall[2]);
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_recordItemCall[3]);
    QVERIFY(IDT.m_recordItemCall[3] < IDT.m_numberCall[3]);
    QVERIFY(IDT.m_numberCall[3] < IDT.m_recordOutCall[1]);

    QVERIFY(IDT.m_recordOutCall[1] < IDT.m_sequenceOutCall[0]);

    QCOMPARE(IDT.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 3);
    QCOMPARE(IDT.m_longlongValues.takeFirst(), 4);
}

void TestData::Bind_QMap_02()
{
    IDataTester<Writer> IDT;
    QMap<int, int> m;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QMap_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QMap<int, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QMap_04()
{
    IDataTester<Reader> IDT;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 1;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2);
    IDT.m_recordItemKey.push_back("key");
    IDT.m_recordItemKey.push_back("value");
    QMap<int, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);

    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);
    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceItemCall[1]);

    QCOMPARE(m[1], 2);
}

void TestData::Bind_QMap_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QMap<int, int> m; m[1]=2; m[3]=4;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(m.empty());
}

void TestData::Bind_QStringHash_01()
{
    IDataTester<Writer> IDT;
    QHash<QString, int> m;
    m["k1"] = 1;
    m["k2"] = 2;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 2);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);

    auto indexOfK1 = std::find(IDT.m_recordItemKey.cbegin(), IDT.m_recordItemKey.cend(), "k1");
    QVERIFY(indexOfK1 != IDT.m_recordItemKey.cend());
    auto indexOfK2 = std::find(IDT.m_recordItemKey.cbegin(), IDT.m_recordItemKey.cend(), "k2");
    QVERIFY(indexOfK2 != IDT.m_recordItemKey.cend());

    auto s0 = IDT.m_longlongValues.takeFirst();
    auto s1 = IDT.m_longlongValues.takeFirst();
    // QHash does not preserve insertion order so, the 2 bound key+value strings may be inverted
    QVERIFY(  (s0 == 1 && s1 == 2)
            ||(s0 == 2 && s1 == 1));
}

void TestData::Bind_QStringHash_02()
{
    IDataTester<Writer> IDT;
    QHash<QString, int> m;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordOutCall.front());
}

void TestData::Bind_QStringHash_03()
{
    IDataTester<Reader> IDT;
    IDT.m_recordRetNullAfter = 0;
    QHash<QString, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 0);
}

void TestData::Bind_QStringHash_04()
{
    IDataTester<Reader> IDT;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2);
    IDT.m_recordItemKey.push_back("k1");
    IDT.m_recordItemKey.push_back("k2");
    QHash<QString, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 3);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordItemCall[2]);
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_recordOutCall[0]);
    QCOMPARE(m["k1"], 1);
    QCOMPARE(m["k2"], 2);
}

void TestData::Bind_QStringHash_05()
{
    IDataTester<Reader> IDT;
    IDT.m_recordItemRetNull = true;
    QHash<QString, int> m = { {"k1", {1}}, {"k2", {2}} };
    IDT.read().bind(m);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordItemCall.front());
    QVERIFY(IDT.m_recordItemCall.front() < IDT.m_recordOutCall.front());
    QVERIFY(m.empty());
}

void TestData::Bind_QHash_01()
{
    IDataTester<Writer> IDT;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 2;
    QHash<int, int> m;
    m[1] = 2;
    m[3] = 4;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_recordCall.size(), 2);
    QCOMPARE(IDT.m_recordItemCall.size(), 4);
    QCOMPARE(IDT.m_recordOutCall.size(), 2);
    QCOMPARE(IDT.m_numberCall.size(), 4);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);

    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);

    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_recordCall[1]);
    QVERIFY(IDT.m_recordCall[1] < IDT.m_recordItemCall[2]);
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_numberCall[2]);
    QVERIFY(IDT.m_numberCall[2] < IDT.m_recordItemCall[3]);
    QVERIFY(IDT.m_recordItemCall[3] < IDT.m_numberCall[3]);
    QVERIFY(IDT.m_numberCall[3] < IDT.m_recordOutCall[1]);

    QVERIFY(IDT.m_recordOutCall[1] < IDT.m_sequenceOutCall[0]);

    auto s0 = IDT.m_longlongValues.takeFirst();
    auto s1 = IDT.m_longlongValues.takeFirst();
    auto s2 = IDT.m_longlongValues.takeFirst();
    auto s3 = IDT.m_longlongValues.takeFirst();
    // QHash does not preserve insertion order so, the 2 bound key+value strings may be inverted
    QVERIFY(  (s0 == 1 && s1 == 2 && s2 == 3 && s3 == 4)
            ||(s0 == 3 && s1 == 4 && s2 == 1 && s3 == 2));
}

void TestData::Bind_QHash_02()
{
    IDataTester<Writer> IDT;
    QHash<int, int> m;
    IDT.write().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceOutCall.front());
}

void TestData::Bind_QHash_03()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceRetNullAfter = 0;
    QHash<int, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 0);
}

void TestData::Bind_QHash_04()
{
    IDataTester<Reader> IDT;
    IDT.m_recordRetNullAfter = 2;
    IDT.m_sequenceItemRetNullAfter = 1;
    IDT.m_longlongValues.push_back(1); IDT.m_longlongValues.push_back(2);
    IDT.m_recordItemKey.push_back("key");
    IDT.m_recordItemKey.push_back("value");
    QHash<int, int> m;
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 2);
    QCOMPARE(IDT.m_recordCall.size(), 1);
    QCOMPARE(IDT.m_recordItemCall.size(), 2);
    QCOMPARE(IDT.m_recordOutCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 2);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);

    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_numberCall[0]);
    QVERIFY(IDT.m_numberCall[0] < IDT.m_recordItemCall[1]);
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_numberCall[1]);
    QVERIFY(IDT.m_numberCall[1] < IDT.m_recordOutCall[0]);
    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceItemCall[1]);

    QCOMPARE(m[1], 2);
}

void TestData::Bind_QHash_05()
{
    IDataTester<Reader> IDT;
    IDT.m_sequenceItemRetNullAfter = 0;
    QHash<int, int> m = { {1, 2}, {3, 4} };
    IDT.read().bind(m);
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
    QCOMPARE(IDT.m_sequenceItemCall.size(), 1);
    QCOMPARE(IDT.m_numberCall.size(), 0);
    QCOMPARE(IDT.m_sequenceOutCall.size(), 1);
    QVERIFY(IDT.m_sequenceCall.front() < IDT.m_sequenceItemCall.front());
    QVERIFY(IDT.m_sequenceItemCall.front() < IDT.m_sequenceOutCall.front());
    QVERIFY(m.empty());
}

#define JSON
#include "TestWriterCommon.cpp"
#undef JSON

void TestData::JsonWriter_01()
{
    QBuffer b;
    b.open(QBuffer::ReadWrite);
    JsonWriter writer(&b);
    QVERIFY(!writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    QCOMPARE(QString(b.buffer()), QString("[{\"key\":42}\n]"));
}

void TestData::JsonWriter_02()
{
    QBuffer b;
    b.open(QIODevice::ReadWrite);
    JsonWriter writer(&b, "\"plop\"");
    QVERIFY(writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    QCOMPARE(QString(b.buffer()), QString("{\"document\":\"plop\",\"data\":\n[{\"key\":42}\n]\n}"));
}

void TestData::JsonWriter_03()
{
    QTemporaryDir tmpDir;
    QString tmpFilename = tmpDir.path() + "/JsonWriter_03.json";
    FILE* f;
#ifdef Q_CC_MSVC
    QVERIFY(!fopen_s(&f, tmpFilename.toLocal8Bit(), "w+"));
#else
    f = fopen(tmpFilename.toLocal8Bit(), "w+");
#endif
    QVERIFY(f);
    JsonWriter writer(f);
    QVERIFY(!writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    std::fseek(f, 0, 0);
    QTextStream s(f);
    QCOMPARE(s.readAll(), QString("[{\"key\":42}\n]"));
    std::fclose(f);
}

void TestData::JsonWriter_04()
{
    QTemporaryDir tmpDir;
    QString tmpFilename = tmpDir.path() + "/JsonWriter_04.json";
    FILE* f;
#ifdef Q_CC_MSVC
    QVERIFY(!fopen_s(&f, tmpFilename.toLocal8Bit(), "w+"));
#else
    f = fopen(tmpFilename.toLocal8Bit(), "w+");
#endif
    QVERIFY(f);
    JsonWriter writer(f, "\"plop\"");
    QVERIFY(writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    std::fseek(f, 0, 0);
    QTextStream s(f);
    QCOMPARE(s.readAll(), QString("{\"document\":\"plop\",\"data\":\n[{\"key\":42}\n]\n}"));
    std::fclose(f);
}

void TestData::JsonWriter_05()
{
    QString s;
    JsonWriter writer(&s);
    QVERIFY(!writer.isDocument());
    writer.write().sequence().record().item("key").bind(42);
    writer.flush();
    QCOMPARE(s, QString("[{\"key\":42}\n]"));
}

void TestData::JsonWriter_06()
{
    QString s;
    JsonWriter writer(&s, "\"plop\"");
    QVERIFY(writer.isDocument());
    writer.write().sequence().record().item("key").bind(42);
    writer.flush();
    QCOMPARE(s, QString("{\"document\":\"plop\",\"data\":\n[{\"key\":42}\n]\n}"));
}

void TestData::JsonWriter_07()
{
    QString s("\\\"\b\f\n\r\t\x6");
    QString res;
    JsonWriter(&res).write().bind(s);
    QCOMPARE(res, QString("\"\\\\\\\"\\b\\f\\n\\r\\t\\u0006\""));
}

void TestData::JsonWriter_08()
{
    {
        QString s;
        JsonWriter indent0(&s, QString(), 0, 0);
        indent0.write().sequence()
            .record().bind("i", 1).bind("i", 2).out()
            .record().bind("i", 1).bind("i", 2);
        indent0.flush();
        QCOMPARE(s, QString("[{\"i\":1,\"i\":2},{\"i\":1,\"i\":2}]"));
    }
    {
        QString s;
        JsonWriter indent1(&s, QString(), 1);
        indent1.write().sequence()
            .record().bind("i", 1).bind("i", 2).out()
            .record().bind("i", 1).bind("i", 2);
        indent1.flush();
        QCOMPARE(s, QString("[{\"i\":1\t,\"i\":2}\n,{\"i\":1\t,\"i\":2}\n]"));
    }
}

#define XML
#include "TestWriterCommon.cpp"
#undef XML

void TestData::XmlWriter_01()
{
    QBuffer b;
    b.open(QBuffer::ReadWrite);
    XmlWriter writer(&b);
    QVERIFY(!writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    QCOMPARE(QString(b.buffer()), QString("<s><r><t type=\"integer\" key=\"key\">42</t></r>\n</s>"));
}

void TestData::XmlWriter_02()
{
    QBuffer b;
    b.open(QIODevice::ReadWrite);
    XmlWriter writer(&b, "<!-- plop -->");
    QVERIFY(writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    QCOMPARE(QString(b.buffer()), QString("<?xml version=\"1.0\" encoding=\"") + writer.codecName() + "\"?>\n<!-- plop -->\n<data>\n<s><r><t type=\"integer\" key=\"key\">42</t></r>\n</s>\n</data>");
}

void TestData::XmlWriter_03()
{
    QTemporaryDir tmpDir;
    QString tmpFilename = tmpDir.path() + "/XmlWriter_03.xml";
    FILE* f;
#ifdef Q_CC_MSVC
    QVERIFY(!fopen_s(&f, tmpFilename.toLocal8Bit(), "w+"));
#else
    f = fopen(tmpFilename.toLocal8Bit(), "w+");
#endif
    QVERIFY(f);
    XmlWriter writer(f);
    QVERIFY(!writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    std::fseek(f, 0, 0);
    QTextStream s(f);
    QCOMPARE(s.readAll(), QString("<s><r><t type=\"integer\" key=\"key\">42</t></r>\n</s>"));
    std::fclose(f);
}

void TestData::XmlWriter_04()
{
    QTemporaryDir tmpDir;
    QString tmpFilename = tmpDir.path() + "/XmlWriter_04.xml";
    FILE* f;
#ifdef Q_CC_MSVC
    QVERIFY(!fopen_s(&f, tmpFilename.toLocal8Bit(), "w+"));
#else
    f = fopen(tmpFilename.toLocal8Bit(), "w+");
#endif
    QVERIFY(f);
    XmlWriter writer(f, "<!-- plop -->");
    QVERIFY(writer.isDocument());
    writer.write().sequence().record().bind("key", 42);
    writer.flush();
    std::fseek(f, 0, 0);
    QTextStream s(f);
    QCOMPARE(s.readAll(), QString("<?xml version=\"1.0\" encoding=\"") + writer.codecName() + "\"?>\n<!-- plop -->\n<data>\n<s><r><t type=\"integer\" key=\"key\">42</t></r>\n</s>\n</data>");
    std::fclose(f);
}

void TestData::XmlWriter_05()
{
    QString s;
    XmlWriter writer(&s);
    QVERIFY(!writer.isDocument());
    writer.write().sequence().record().item("key").bind(42);
    writer.flush();
    QCOMPARE(s, QString("<s><r><t type=\"integer\" key=\"key\">42</t></r>\n</s>"));
}

void TestData::XmlWriter_06()
{
    QString s;
    XmlWriter writer(&s, "<!-- plop -->");
    QVERIFY(writer.isDocument());
    writer.write().sequence().record().item("key").bind(42);
    writer.flush();
    QCOMPARE(s, QString("<?xml version=\"1.0\" encoding=\"") + writer.codecName() + "\"?>\n<!-- plop -->\n<data>\n<s><r><t type=\"integer\" key=\"key\">42</t></r>\n</s>\n</data>");
}

void TestData::XmlWriter_07()
{
    QString s = QString::fromWCharArray(L"&\"'<>\t\n\r\ue000\x6");
    BindErrors err;
    QString res;
    auto bindRes= XmlWriter(&res).write(&err).bind(s);
    QCOMPARE(res, QString("<t>&amp;&quot;&apos;&lt;&gt;&#x9;&#xa;&#xd;&#xe000; </t>"));
    QVERIFY(bindRes.errors()->find<InvalidXmlChar>());
    QVERIFY(bindRes.errors()->find<InvalidXmlChar>()->getInvalidChar() == QChar('\x6'));
}

/// <summary>Only use IANA registered QTextCodec</summary>
void TestData::XmlWriter_08()
{
    QString s;
    QVERIFY(QTextCodec::codecForName(qPrintable(XmlWriter(&s).codecName()))->mibEnum() >= 3);
}

#define SUMMARY
#include "TestWriterCommon.cpp"
#undef SUMMARY

void TestData::Summary_01()
{
    QBuffer b;
    b.open(QBuffer::ReadWrite);
    Summary writer(&b);
    writer.write().record().sequence("k1").out().bind("k2", 42);
    writer.flush();
    QCOMPARE(QString(b.buffer()), QString("{k1:[] | k2:42}"));
}

void TestData::Summary_02()
{
    QTemporaryDir tmpDir;
    QString tmpFilename = tmpDir.path() + "/Summary_02.xml";
    FILE* f;
#ifdef Q_CC_MSVC
    QVERIFY(!fopen_s(&f, tmpFilename.toLocal8Bit(), "w+"));
#else
    f = fopen(tmpFilename.toLocal8Bit(), "w+");
#endif
    QVERIFY(f);
    Summary writer(f);
    writer.write().record().sequence("k1").out().bind("k2", 42);
    writer.flush();
    std::fseek(f, 0, 0);
    QTextStream s(f);
    QCOMPARE(s.readAll(), QString("{k1:[] | k2:42}"));
    std::fclose(f);
}

void TestData::Summary_03()
{
    QString s;
    Summary writer(&s);
    writer.write().record().sequence("k1").out().item("k2").bind(42);
    writer.flush();
    QCOMPARE(s, QString("{k1:[] | k2:42}"));
}

void TestData::Summary_04()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(0, 10);
    writer.write().sequence().bind(42);
    writer.flush();
    QCOMPARE(s, QString("[]"));
}

void TestData::Summary_05()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(1, 1);
    writer.write().sequence().bind(42);
    writer.flush();
    QCOMPARE(s, QString("[42]"));
}

void TestData::Summary_06()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(1, 1);
    writer.write().sequence().bind(42).bind(43).bind(44);
    writer.flush();
    QCOMPARE(s, QString("[42 | 2 more]"));
}

void TestData::Summary_07()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(1, 2);
    writer.write().sequence().bind(42).bind(43).bind(44);
    writer.flush();
    QCOMPARE(s, QString("[42 | 1 more | 44]"));
}

void TestData::Summary_08()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(1, 3);
    writer.write().sequence().bind(42).bind(43).bind(44);
    writer.flush();
    QCOMPARE(s, QString("[42 | 43 | 44]"));
}

void TestData::Summary_09()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(1, 3);
    writer.write().sequence().bind(42).bind(43).bind(44).bind(45);
    writer.flush();
    QCOMPARE(s, QString("[42 | 43 | 1 more | 45]"));
}

void TestData::Summary_10()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(2, 2);
    writer.write().sequence().bind(42).bind(43).bind(44);
    writer.flush();
    QCOMPARE(s, QString("[42 | 43 | 44]"));
}

void TestData::Summary_11()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(2, 2);
    writer.write().sequence().sequence().bind(42).bind(43).bind(44);
    writer.flush();
    QCOMPARE(s, QString("[[42 | 1 more | 44]]"));
}

void TestData::Summary_12()
{
    QString s;
    Summary writer(&s);
    writer.setLevelOfDetails(1, 10);
    writer.write().sequence()
    .record().bind("k", "42").out()
    .sequence().bind(42).out()
    .bind(42);
    writer.flush();
    QCOMPARE(s, QString("[{} | [] | 42]"));
}

#define CBOR
#include "TestWriterCommon.cpp"
#undef CBOR

void TestData::CborWriterUnsignedIntegers_01()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(00);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x00\xFF", 6));
}

void TestData::CborWriterUnsignedIntegers_02()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(23);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x17\xFF", 6));
}

void TestData::CborWriterUnsignedIntegers_03()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(24);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x18\x18\xFF", 7));
}

void TestData::CborWriterUnsignedIntegers_04()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(255);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x18\xFF\xFF", 7));
}

void TestData::CborWriterUnsignedIntegers_05()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(256);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x19\x01\x00\xFF", 8));
}

void TestData::CborWriterUnsignedIntegers_06()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(65535);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x19\xFF\xFF\xFF", 8));
}

void TestData::CborWriterUnsignedIntegers_07()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(65536);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x1A\x00\x01\x00\x00\xFF", 10));
}

void TestData::CborWriterUnsignedIntegers_08()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(4294967295);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x1A\xFF\xFF\xFF\xFF\xFF", 10));
}

void TestData::CborWriterUnsignedIntegers_09()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(4294967296);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x1B\x00\x00\x00\x01\x00\x00\x00\x00\xFF", 14));
}

void TestData::CborWriterNegativeIntegers_01()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-1);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x20\xFF", 6));
}

void TestData::CborWriterNegativeIntegers_02()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-24);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x37\xFF", 6));
}

void TestData::CborWriterNegativeIntegers_03()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-25);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x38\x18\xFF", 7));
}

void TestData::CborWriterNegativeIntegers_04()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-256);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x38\xFF\xFF", 7));
}

void TestData::CborWriterNegativeIntegers_05()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-257);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x39\x01\x00\xFF", 8));
}

void TestData::CborWriterNegativeIntegers_06()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-65536);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x39\xFF\xFF\xFF", 8));
}

void TestData::CborWriterNegativeIntegers_07()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-65537);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x3A\x00\x01\x00\x00\xFF", 10));
}

void TestData::CborWriterNegativeIntegers_08()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-4294967296);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x3A\xFF\xFF\xFF\xFF\xFF", 10));
}

void TestData::CborWriterNegativeIntegers_09()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-4294967297);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\x3B\x00\x00\x00\x01\x00\x00\x00\x00\xFF", 14));
}

void TestData::CborWriterFloatingPoint_01()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(3.14);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xFB\x40\x09\x1E\xB8\x51\xEB\x85\x1F\xFF", 14));
}

void TestData::CborWriterFloatingPoint_02()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(-3.14);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xFB\xC0\x09\x1E\xB8\x51\xEB\x85\x1F\xFF", 14));
}

void TestData::CborWriterFloatingPoint_03()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(100000.0);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xFB\x40\xF8\x6A\x00\x00\x00\x00\x00\xFF", 14));
}

void TestData::CborWriterFloatingPoint_04()
{
    QBuffer buf;
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(5.5);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xFB\x40\x16\x00\x00\x00\x00\x00\x00\xFF", 14));
}

void TestData::CborWriterTimeStamp_01()
{
    QBuffer buf;
    QDateTime timestamp = QDateTime::fromString(QString("2013-11-12T00:12:56+01:00"), Qt::ISODate);
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(timestamp);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xC0\x78\x19\x32\x30\x31\x33\x2D\x31\x31\x2D\x31\x32\x54\x30\x30\x3A\x31\x32\x3A\x35\x36\x2B\x30\x31\x3A\x30\x30\xFF", 33));
}

void TestData::CborWriterTimeStamp_02()
{
    QBuffer buf;
    QDateTime timestamp = QDateTime::fromString(QString("2013-11-12T00:12:56Z"), Qt::ISODate);
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(timestamp);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xC0\x78\x19\x32\x30\x31\x33\x2D\x31\x31\x2D\x31\x32\x54\x30\x30\x3A\x31\x32\x3A\x35\x36\x2B\x30\x30\x3A\x30\x30\xFF", 33));
}

void TestData::CborWriterTimeStamp_03()
{
    QBuffer buf;
    QDateTime timestamp = QDateTime::fromString(QString("2013-11-12T00:12:56+00:00"), Qt::ISODate);
    if (!buf.open(QIODevice::WriteOnly))
           return;
    CborWriter writer(&buf);
    QVERIFY(!writer.isDocument());
    writer.write()
            .sequence()
            .bind(timestamp);
    QCOMPARE(buf.buffer(), QByteArray("\xD9\xD9\xF7\x9F\xC0\x78\x19\x32\x30\x31\x33\x2D\x31\x31\x2D\x31\x32\x54\x30\x30\x3A\x31\x32\x3A\x35\x36\x2B\x30\x30\x3A\x30\x30\xFF", 33));
}

void TestData::Bindable_01()
{
    IDataTester<Writer> IDT;
    BindableData BD;
    BD.write().null();
    BD.bind(IDT.write());
    QCOMPARE(IDT.m_nullCall.size(), 1);
}

void TestData::Bindable_02()
{
    IDataTester<Writer> IDT;
    BindableData BD;
    BD.write().sequence();
    BD.bind(IDT.write());
    QCOMPARE(IDT.m_sequenceCall.size(), 1);
}

void TestData::Bindable_03()
{
    IDataTester<Writer> IDT;
    BindableData BD;
    BD.write().record();
    BD.bind(IDT.write());
    QCOMPARE(IDT.m_recordCall.size(), 1);
}

void TestData::Bindable_05()
{
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    IDT.m_sequenceRetNullAfter = 2;
    BindableData BD;
    BD.write().sequence()
    .null()
    .sequence().out()
    .record();
    BD.bind(IDT.write());
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_nullCall[0]);
    QVERIFY(IDT.m_nullCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_sequenceCall[1]);
    QVERIFY(IDT.m_sequenceCall[1] < IDT.m_sequenceOutCall[0]);
    QVERIFY(IDT.m_sequenceOutCall[0] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_recordCall[0]);
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordOutCall[0]);
    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_sequenceOutCall[1]);
}

void TestData::Bindable_06()
{
    IDataTester<Writer> IDT;
    IDT.m_recordRetNullAfter = 2;
    BindableData BD;
    BD.write().record()
    .null("k1")
    .sequence("k2").out()
    .record("k3");
    BD.bind(IDT.write());
    QVERIFY(IDT.m_recordCall[0] < IDT.m_recordItemCall[0]);
    QCOMPARE(IDT.m_recordItemKey.takeFirst(), QString("k1"));
    QVERIFY(IDT.m_recordItemCall[0] < IDT.m_nullCall[0]);
    QVERIFY(IDT.m_nullCall[0] < IDT.m_recordItemCall[1]);
    QCOMPARE(IDT.m_recordItemKey.takeFirst(), QString("k2"));
    QVERIFY(IDT.m_recordItemCall[1] < IDT.m_sequenceCall[0]);
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceOutCall[0]);
    QVERIFY(IDT.m_sequenceOutCall[0] < IDT.m_recordItemCall[2]);
    QCOMPARE(IDT.m_recordItemKey.takeFirst(), QString("k3"));
    QVERIFY(IDT.m_recordItemCall[2] < IDT.m_recordCall[1]);
    QVERIFY(IDT.m_recordCall[1] < IDT.m_recordOutCall[0]);
    QVERIFY(IDT.m_recordOutCall[0] < IDT.m_recordOutCall[1]);
}

void TestData::Bindable_08()
{
    TestUserData<Writer> t[3];
    t[0].bindToString = "42";
    t[1].bindToString = "43";
    t[2].bindToString = "44";
    IDataTester<Writer> IDT;
    IDT.m_sequenceItemRetNullAfter = 3;
    BindableData BD;
    BD.write().bind(t);
    QVERIFY(!t[0].bindCalled);
    QVERIFY(!t[1].bindCalled);
    QVERIFY(!t[2].bindCalled);
    BD.bind(IDT.write());
    QVERIFY(IDT.m_sequenceCall[0] < IDT.m_sequenceItemCall[0]);
    QVERIFY(IDT.m_sequenceItemCall[0] < IDT.m_bindTextCall[0]);
    QVERIFY(IDT.m_bindTextCall[0] < IDT.m_sequenceItemCall[1]);
    QVERIFY(IDT.m_sequenceItemCall[1] < IDT.m_bindTextCall[1]);
    QVERIFY(IDT.m_bindTextCall[1] < IDT.m_sequenceItemCall[2]);
    QVERIFY(IDT.m_sequenceItemCall[2] < IDT.m_bindTextCall[2]);
    QVERIFY(IDT.m_bindTextCall[2] < IDT.m_sequenceOutCall[0]);
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("42"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("43"));
    QCOMPARE(IDT.m_bindTextStrings.takeFirst(), QString("44"));
}

void TestData::Bindable_10()
{
    BindableData BD;
    BD.write().text(" ");
    IDataTester<Writer> IDT;
    BD.bind(IDT.write());
    QCOMPARE(IDT.m_textStrings.takeFirst(), QString(" "));
}

void TestData::JsonReader_01()
{
    QTextStream s("null");
    JsonReader reader(&s);
    QVERIFY(reader.read().null().noError());
}

void TestData::JsonReader_02()
{
    QTextStream s("[");
    JsonReader reader(&s);
//    QVERIFY(reader.read().sequence().state()->noError());
}

void TestData::JsonReader_03()
{
    QTextStream s("[]");
    JsonReader reader(&s);
    QVERIFY(reader.read().sequence().out().noError());
}

void TestData::JsonReader_04()
{
    QTextStream s("{");
    JsonReader reader(&s);
//    QVERIFY(reader.read().record().state()->noError());
}

void TestData::JsonReader_05()
{
    QTextStream s("{}");
    JsonReader reader(&s);
    QVERIFY(reader.read().record().out().noError());
}

void TestData::JsonReader_06()
{
    QTextStream s("\"");
    JsonReader reader(&s);
//    QVERIFY(reader.read().message().state()->noError());
}

void TestData::JsonReader_07()
{
    QTextStream s("\"\"");
    JsonReader reader(&s);
    QVERIFY(reader.read().text("").noError());
}

void TestData::JsonReader_08()
{
    QTextStream s("[null\n,[]\n,{}\n,\"\"\n]");
    JsonReader reader(&s);
//    QVERIFY(reader.read().sequence()
//            .null()
//            .sequence().out()
//            .record().out()
//            .message().out().state()->noError());
}

void TestData::JsonReader_09()
{
    QTextStream s("{\"k1\":null\n,\"k2\":[]\n,\"k3\":{}\n,\"k4\":\"\"\n}");
    JsonReader reader(&s);
//    QVERIFY(reader.read().record()
//            .null("k1")
//            .sequence("k2").out()
//            .record("k3").out()
//            .message("k4").out().state()->noError());
}

void TestData::Bind_QLocale_01()
{
    IDataTester<Writer> IDT;
    IDT.write().bind(QLocale(QLocale::French, QLocale::Canada));
    QVERIFY(IDT.m_recordCall.front() < IDT.m_recordItemCall.front());
}

void TestData::Bind_QLocale_02()
{
    QLocale loc(QLocale::French, QLocale::Canada);
}

void TestData::Bind_QCoreApplication_01()
{
    // TODO ?
    //QVERIFY(data::JsonString().write().bind(QCoreApplication::instance()));
}

void TestData::Bind_QHostInfo_01()
{
    QHostInfo info(1);
    info.setHostName("TEST_NAME");
    QList<QHostAddress> listAddresses;
    QHostAddress aHostAddress("192.0.1.0");
    listAddresses.push_back(aHostAddress);
    info.setAddresses(listAddresses);
    QString aLocalDomainName = info.localDomainName();
    QString aLocalHostName = info.localHostName();
    QCOMPARE((QString)data::JsonString().write().bind(info), QString("{\"addresses\":[\"%1\"]\n,\"host_name\":\"TEST_NAME\"\n,\"local_domain_name\":\"%2\"\n,\"local_host_name\":\"%3\"\n}").arg(aHostAddress.toString()).arg(aLocalDomainName).arg(aLocalHostName));
}


void TestData::Bind_QVersionNumber_01()
{
    QVersionNumber v1(6, 4, 8);
    QCOMPARE((QString)data::JsonString().write().bind(v1), QString("\"6.4.8\""));
}

void TestData::Bind_QProcessEnvironment_01()
{
    QProcess aProcessTest;
    QProcessEnvironment aProcessEnv = aProcessTest.processEnvironment();
    QCOMPARE(aProcessEnv.toStringList().size(), 0);
    aProcessEnv.insert("TESTDATA_PROCESSENV", "Inserted");
    QCOMPARE(aProcessEnv.toStringList().size(), 1);
    QCOMPARE(aProcessEnv.contains("TESTDATA_PROCESSENV"), true);
    QCOMPARE((QString)data::JsonString().write().bind(aProcessEnv), QString("{\"TESTDATA_PROCESSENV\":\"Inserted\"\n}"));
    aProcessEnv.remove("TESTDATA_PROCESSENV");
    QCOMPARE(aProcessEnv.toStringList().size(), 0);
    QCOMPARE(aProcessEnv.contains("TESTDATA_PROCESSENV"), false);
    QCOMPARE((QString)data::JsonString().write().bind(aProcessEnv), QString("{\n}"));
}

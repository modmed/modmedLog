/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qobject.h>

class TestData : public QObject
{
    Q_OBJECT

    Q_SLOT void initTestCase();

    Q_SLOT void IItem_01();
    Q_SLOT void IItem_02();
    Q_SLOT void IItem_03();
    Q_SLOT void IItem_04();

    Q_SLOT void State_01();
    Q_SLOT void State_02();
    Q_SLOT void State_03();
    Q_SLOT void State_04();

    Q_SLOT void ItmState_02();
    Q_SLOT void ItmState_05();
    Q_SLOT void ItmState_06();
    Q_SLOT void ItmState_07();
    Q_SLOT void ItmState_08();
    Q_SLOT void ItmState_11();
    Q_SLOT void ItmState_12();
    Q_SLOT void ItmState_13();
    Q_SLOT void ItmState_17();
    Q_SLOT void ItmState_19();

    Q_SLOT void SeqState_01();
    Q_SLOT void SeqState_02();
    Q_SLOT void SeqState_03();
    Q_SLOT void SeqState_04();
    Q_SLOT void SeqState_05();
    Q_SLOT void SeqState_06();

    Q_SLOT void RecState_01();
    Q_SLOT void RecState_02();
    Q_SLOT void RecState_03();
    Q_SLOT void RecState_04();
    Q_SLOT void RecState_05();
    Q_SLOT void RecState_06();
    Q_SLOT void RecState_07();
    Q_SLOT void RecState_08();
    Q_SLOT void RecState_09();

    Q_SLOT void Itm_01();
    Q_SLOT void Itm_02();
    Q_SLOT void Itm_03();
    Q_SLOT void Itm_04();
    Q_SLOT void Itm_05();
    Q_SLOT void Itm_07();
    Q_SLOT void Itm_08();

    Q_SLOT void Seq_01();
    Q_SLOT void Seq_02();
    Q_SLOT void Seq_03();

    Q_SLOT void Rec_01();
    Q_SLOT void Rec_02();
    Q_SLOT void Rec_03();
    Q_SLOT void Rec_04();

    Q_SLOT void Result_01();
    Q_SLOT void Result_02();

    Q_SLOT void Error_01();
    Q_SLOT void Error_02();
    Q_SLOT void Error_05();
    Q_SLOT void Error_06();
    Q_SLOT void Error_07();
    Q_SLOT void Error_08();

    Q_SLOT void Unknown_01();
    Q_SLOT void Unknown_02();

    Q_SLOT void FailBind_01();
    Q_SLOT void FailBind_02();

    Q_SLOT void FailBindFrom_01();
    Q_SLOT void FailBindFrom_02();

    Q_SLOT void FailNull_01();
    Q_SLOT void FailNull_02();
    Q_SLOT void FailNull_03();

    Q_SLOT void FailSequence_01();
    Q_SLOT void FailSequence_02();
    Q_SLOT void FailSequence_03();

    Q_SLOT void FailSequenceItem_01();
    Q_SLOT void FailSequenceItem_02();
    Q_SLOT void FailSequenceItem_03();

    Q_SLOT void FailSequenceOut_01();
    Q_SLOT void FailSequenceOut_02();
    Q_SLOT void FailSequenceOut_03();

    Q_SLOT void FailRecord_01();
    Q_SLOT void FailRecord_02();
    Q_SLOT void FailRecord_03();

    Q_SLOT void FailRecordItem_01();
    Q_SLOT void FailRecordItem_02();
    Q_SLOT void FailRecordItem_03();

    Q_SLOT void FailRecordItemEmptyKey_01();
    Q_SLOT void FailRecordItemEmptyKey_02();
    Q_SLOT void FailRecordItemEmptyKey_03();

    Q_SLOT void FailRecordItemWrongKey_01();
    Q_SLOT void FailRecordItemWrongKey_02();

    Q_SLOT void FailRecordOut_01();
    Q_SLOT void FailRecordOut_02();
    Q_SLOT void FailRecordOut_03();

    Q_SLOT void FailText_01();
    Q_SLOT void FailText_02();
    Q_SLOT void FailText_03();

    Q_SLOT void FailBindText_01();
    Q_SLOT void FailBindText_02();
    Q_SLOT void FailBindText_03();

    Q_SLOT void Bind_Default_01();
    Q_SLOT void Bind_Default_03();
    Q_SLOT void Bind_Default_04();

    Q_SLOT void Bind_Pointer_01();
    Q_SLOT void Bind_Pointer_02();
    Q_SLOT void Bind_Pointer_03();
    Q_SLOT void Bind_Pointer_04();
    Q_SLOT void Bind_Pointer_05();

    Q_SLOT void Bind_Data_04();
    Q_SLOT void Bind_Data_05();
    Q_SLOT void Bind_Data_06();
    Q_SLOT void Bind_Data_07();

    Q_SLOT void Bind_Hex_01();
    Q_SLOT void Bind_Hex_02();
    Q_SLOT void Bind_Hex_03();

    Q_SLOT void Bind_Bool_01();
    Q_SLOT void Bind_Bool_02();
    Q_SLOT void Bind_Bool_03();

    Q_SLOT void Bind_Char_01();
    Q_SLOT void Bind_Char_02();
    Q_SLOT void Bind_Char_03();

    Q_SLOT void Bind_UnsignedChar_01();
    Q_SLOT void Bind_UnsignedChar_02();
    Q_SLOT void Bind_UnsignedChar_03();

    Q_SLOT void Bind_Short_01();
    Q_SLOT void Bind_Short_02();
    Q_SLOT void Bind_Short_03();

    Q_SLOT void Bind_UnsignedShort_01();
    Q_SLOT void Bind_UnsignedShort_02();
    Q_SLOT void Bind_UnsignedShort_03();

    Q_SLOT void Bind_Int_01();
    Q_SLOT void Bind_Int_02();
    Q_SLOT void Bind_Int_03();

    Q_SLOT void Bind_UnsignedInt_01();
    Q_SLOT void Bind_UnsignedInt_02();
    Q_SLOT void Bind_UnsignedInt_03();

    Q_SLOT void Bind_Long_01();
    Q_SLOT void Bind_Long_02();
    Q_SLOT void Bind_Long_03();

    Q_SLOT void Bind_UnsignedLong_01();
    Q_SLOT void Bind_UnsignedLong_02();
    Q_SLOT void Bind_UnsignedLong_03();

    Q_SLOT void Bind_LongLong_01();
    Q_SLOT void Bind_LongLong_02();
    Q_SLOT void Bind_LongLong_03();

    Q_SLOT void Bind_UnsignedLongLong_01();
    Q_SLOT void Bind_UnsignedLongLong_02();
    Q_SLOT void Bind_UnsignedLongLong_03();

    Q_SLOT void Bind_Float_01();
    Q_SLOT void Bind_Float_02();
    Q_SLOT void Bind_Float_03();

    Q_SLOT void Bind_Double_01();
    Q_SLOT void Bind_Double_02();
    Q_SLOT void Bind_Double_03();

    Q_SLOT void Bind_CharArray_01();
    Q_SLOT void Bind_CharArray_02();
    Q_SLOT void Bind_CharArray_03();

    Q_SLOT void Bind_String_01();
    Q_SLOT void Bind_String_02();
    Q_SLOT void Bind_String_03();

    Q_SLOT void Bind_WString_01();
    Q_SLOT void Bind_WString_02();

    Q_SLOT void Bind_Array_01();
    Q_SLOT void Bind_Array_02();

    Q_SLOT void Bind_Vector_01();
    Q_SLOT void Bind_Vector_02();
    Q_SLOT void Bind_Vector_03();
    Q_SLOT void Bind_Vector_04();
    Q_SLOT void Bind_Vector_05();

    Q_SLOT void Bind_Map_01();
    Q_SLOT void Bind_Map_02();
    Q_SLOT void Bind_Map_03();
    Q_SLOT void Bind_Map_04();
    Q_SLOT void Bind_Map_05();

    Q_SLOT void Bind_StringMap_01();
    Q_SLOT void Bind_StringMap_02();
    Q_SLOT void Bind_StringMap_03();
    Q_SLOT void Bind_StringMap_04();
    Q_SLOT void Bind_StringMap_05();

    Q_SLOT void Bind_List_01();
    Q_SLOT void Bind_List_02();
    Q_SLOT void Bind_List_03();
    Q_SLOT void Bind_List_04();
    Q_SLOT void Bind_List_05();

    Q_SLOT void Bind_Set_01();
    Q_SLOT void Bind_Set_02();
    Q_SLOT void Bind_Set_03();
    Q_SLOT void Bind_Set_04();
    Q_SLOT void Bind_Set_05();

    Q_SLOT void Bind_QString_01();
    Q_SLOT void Bind_QString_02();
    Q_SLOT void Bind_QString_03();

    Q_SLOT void Bind_QDate_01();
    Q_SLOT void Bind_QDate_02();
    Q_SLOT void Bind_QDate_03();
    Q_SLOT void Bind_QDate_04();
    Q_SLOT void Bind_QDate_05();

    Q_SLOT void Bind_QTime_01();
    Q_SLOT void Bind_QTime_02();
    Q_SLOT void Bind_QTime_03();
    Q_SLOT void Bind_QTime_04();
    Q_SLOT void Bind_QTime_05();

    Q_SLOT void Bind_QUuid_01();
    Q_SLOT void Bind_QUuid_02();
    Q_SLOT void Bind_QUuid_03();
    Q_SLOT void Bind_QUuid_04();

    Q_SLOT void Bind_QDateTime_01();
    Q_SLOT void Bind_QDateTime_02();
    Q_SLOT void Bind_QDateTime_03();
    Q_SLOT void Bind_QDateTime_04();
    Q_SLOT void Bind_QDateTime_05();

    Q_SLOT void Bind_QMsgType_01();
    Q_SLOT void Bind_QMsgType_02();
    Q_SLOT void Bind_QMsgType_03();
    Q_SLOT void Bind_QMsgType_04();

    Q_SLOT void Bind_QSharedPointer_01();
    Q_SLOT void Bind_QSharedPointer_02();
    Q_SLOT void Bind_QSharedPointer_03();
    Q_SLOT void Bind_QSharedPointer_04();
    Q_SLOT void Bind_QSharedPointer_05();

    Q_SLOT void Bind_QList_01();
    Q_SLOT void Bind_QList_02();
    Q_SLOT void Bind_QList_03();
    Q_SLOT void Bind_QList_04();
    Q_SLOT void Bind_QList_05();

    Q_SLOT void Bind_QStringList_01();
    Q_SLOT void Bind_QStringList_02();
    Q_SLOT void Bind_QStringList_03();
    Q_SLOT void Bind_QStringList_04();
    Q_SLOT void Bind_QStringList_05();

    Q_SLOT void Bind_QLinkedList_01();
    Q_SLOT void Bind_QLinkedList_02();
    Q_SLOT void Bind_QLinkedList_03();
    Q_SLOT void Bind_QLinkedList_04();
    Q_SLOT void Bind_QLinkedList_05();

    Q_SLOT void Bind_QVector_01();
    Q_SLOT void Bind_QVector_02();
    Q_SLOT void Bind_QVector_03();
    Q_SLOT void Bind_QVector_04();
    Q_SLOT void Bind_QVector_05();

    Q_SLOT void Bind_QSet_01();
    Q_SLOT void Bind_QSet_02();
    Q_SLOT void Bind_QSet_03();
    Q_SLOT void Bind_QSet_04();
    Q_SLOT void Bind_QSet_05();

    Q_SLOT void Bind_QStringMap_01();
    Q_SLOT void Bind_QStringMap_02();
    Q_SLOT void Bind_QStringMap_03();
    Q_SLOT void Bind_QStringMap_04();
    Q_SLOT void Bind_QStringMap_05();

    Q_SLOT void Bind_QMap_01();
    Q_SLOT void Bind_QMap_02();
    Q_SLOT void Bind_QMap_03();
    Q_SLOT void Bind_QMap_04();
    Q_SLOT void Bind_QMap_05();

    Q_SLOT void Bind_QStringHash_01();
    Q_SLOT void Bind_QStringHash_02();
    Q_SLOT void Bind_QStringHash_03();
    Q_SLOT void Bind_QStringHash_04();
    Q_SLOT void Bind_QStringHash_05();

    Q_SLOT void Bind_QHash_01();
    Q_SLOT void Bind_QHash_02();
    Q_SLOT void Bind_QHash_03();
    Q_SLOT void Bind_QHash_04();
    Q_SLOT void Bind_QHash_05();

    Q_SLOT void JsonWriterCommon_01();
    Q_SLOT void JsonWriterCommon_02();
    Q_SLOT void JsonWriterCommon_03();
    Q_SLOT void JsonWriterCommon_05();
    Q_SLOT void JsonWriterCommon_06();
    Q_SLOT void JsonWriterCommon_08();

    Q_SLOT void JsonWriter_01();
    Q_SLOT void JsonWriter_02();
    Q_SLOT void JsonWriter_03();
    Q_SLOT void JsonWriter_04();
    Q_SLOT void JsonWriter_05();
    Q_SLOT void JsonWriter_06();
    Q_SLOT void JsonWriter_07();
    Q_SLOT void JsonWriter_08(); // p_maxIndentLevel

    Q_SLOT void XmlWriterCommon_01();
    Q_SLOT void XmlWriterCommon_02();
    Q_SLOT void XmlWriterCommon_03();
    Q_SLOT void XmlWriterCommon_05();
    Q_SLOT void XmlWriterCommon_06();
    Q_SLOT void XmlWriterCommon_08();

    Q_SLOT void XmlWriter_01();
    Q_SLOT void XmlWriter_02();
    Q_SLOT void XmlWriter_03();
    Q_SLOT void XmlWriter_04();
    Q_SLOT void XmlWriter_05();
    Q_SLOT void XmlWriter_06();
    Q_SLOT void XmlWriter_07();
    Q_SLOT void XmlWriter_08();

    Q_SLOT void SummaryCommon_01();
    Q_SLOT void SummaryCommon_02();
    Q_SLOT void SummaryCommon_03();
    Q_SLOT void SummaryCommon_05();
    Q_SLOT void SummaryCommon_06();
    Q_SLOT void SummaryCommon_08();

    Q_SLOT void Summary_01();
    Q_SLOT void Summary_02();
    Q_SLOT void Summary_03();
    Q_SLOT void Summary_04();
    Q_SLOT void Summary_05();
    Q_SLOT void Summary_06();
    Q_SLOT void Summary_07();
    Q_SLOT void Summary_08();
    Q_SLOT void Summary_09();
    Q_SLOT void Summary_10();
    Q_SLOT void Summary_11();
    Q_SLOT void Summary_12();

    Q_SLOT void CborWriterCommon_01();
    Q_SLOT void CborWriterCommon_02();
    Q_SLOT void CborWriterCommon_03();
    Q_SLOT void CborWriterCommon_05();
    Q_SLOT void CborWriterCommon_06();
    Q_SLOT void CborWriterCommon_08();

    Q_SLOT void CborWriterUnsignedIntegers_01();
    Q_SLOT void CborWriterUnsignedIntegers_02();
    Q_SLOT void CborWriterUnsignedIntegers_03();
    Q_SLOT void CborWriterUnsignedIntegers_04();
    Q_SLOT void CborWriterUnsignedIntegers_05();
    Q_SLOT void CborWriterUnsignedIntegers_06();
    Q_SLOT void CborWriterUnsignedIntegers_07();
    Q_SLOT void CborWriterUnsignedIntegers_08();
    Q_SLOT void CborWriterUnsignedIntegers_09();

    Q_SLOT void CborWriterNegativeIntegers_01();
    Q_SLOT void CborWriterNegativeIntegers_02();
    Q_SLOT void CborWriterNegativeIntegers_03();
    Q_SLOT void CborWriterNegativeIntegers_04();
    Q_SLOT void CborWriterNegativeIntegers_05();
    Q_SLOT void CborWriterNegativeIntegers_06();
    Q_SLOT void CborWriterNegativeIntegers_07();
    Q_SLOT void CborWriterNegativeIntegers_08();
    Q_SLOT void CborWriterNegativeIntegers_09();

    Q_SLOT void CborWriterFloatingPoint_01();
    Q_SLOT void CborWriterFloatingPoint_02();
    Q_SLOT void CborWriterFloatingPoint_03();
    Q_SLOT void CborWriterFloatingPoint_04();

    Q_SLOT void CborWriterTimeStamp_01();
    Q_SLOT void CborWriterTimeStamp_02();
    Q_SLOT void CborWriterTimeStamp_03();

    Q_SLOT void Bindable_01();
    Q_SLOT void Bindable_02();
    Q_SLOT void Bindable_03();
    Q_SLOT void Bindable_05();
    Q_SLOT void Bindable_06();
    Q_SLOT void Bindable_08();
    Q_SLOT void Bindable_10();

    Q_SLOT void JsonReader_01();
    Q_SLOT void JsonReader_02();
    Q_SLOT void JsonReader_03();
    Q_SLOT void JsonReader_04();
    Q_SLOT void JsonReader_05();
    Q_SLOT void JsonReader_06();
    Q_SLOT void JsonReader_07();
    Q_SLOT void JsonReader_08();
    Q_SLOT void JsonReader_09();

    Q_SLOT void Bind_QLocale_01();
    Q_SLOT void Bind_QLocale_02();

    Q_SLOT void Bind_QCoreApplication_01();

    Q_SLOT void Bind_QHostInfo_01();

    Q_SLOT void Bind_QVersionNumber_01();

    Q_SLOT void Bind_QProcessEnvironment_01();
};

QT += testlib core network
QT -= gui

TEMPLATE = app
TARGET = TestData
CONFIG += c++11 console debug_and_release
CONFIG -= app_bundle

INCLUDEPATH += ../../include

CONFIG(debug, debug|release) {
    DESTDIR = $$_PRO_FILE_PWD_/bin/Debug
} else {
    DESTDIR = $$_PRO_FILE_PWD_/bin/Release
}

CONFIG(debug, debug|release) {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Debug
} else {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Release
}

CONFIG(debug, debug|release) { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Debug }
else { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Release }

CONFIG(release, debug|release) { DEFINES += QT_MESSAGELOGCONTEXT }

LIBS += -lmodmedLog

HEADERS += \
    TestData.h \
    AssertCatcher.h \
    IDataTester.h

SOURCES += \
    main.cpp \
    TestData.cpp \
    TestWriterCommon.cpp \
    AssertCatcher.cpp \
    IDataTester.cpp \

include(../../modmedLog.pri)

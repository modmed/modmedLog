/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/

#include <iostream>

#define TEST_WRITER_COMMON_FUNC(CLASS, NB) CLASS ## Common_ ## NB
#define TEST_WRITER_COMMON(CLASS, NB) void TestData::TEST_WRITER_COMMON_FUNC(CLASS, NB)

#undef EXPECTED
#undef TESTED_CLASS
#undef TESTED_CONTENT
#undef TESTED_CONTENT_GETTER
#undef RW_MODE

#ifdef JSON
#define EXPECTED(json, xml, summary, cbor) QString expected = json;
#define TESTED_CLASS JsonWriter
#else
# ifdef XML
# define EXPECTED(json, xml, summary, cbor) QString expected = xml;
# define TESTED_CLASS XmlWriter
# else
#  ifdef SUMMARY
#  define EXPECTED(json, xml, summary, cbor) QString expected = summary;
#  define TESTED_CLASS Summary
#  else
#   ifdef CBOR
#   define EXPECTED(json, xml, summary, cbor) QByteArray expected = cbor;
#   define TESTED_CLASS CborWriter
#   endif
#  endif
# endif
#endif

#ifdef CBOR
# define TESTED_CONTENT(res) QBuffer res; res.open(QIODevice::WriteOnly);
# define TESTED_CONTENT_GETTER(res) res.buffer()
#else
#  define TESTED_CONTENT(res) QTextStream res(new QString);
#  define TESTED_CONTENT_GETTER(res) *res.string()
#endif

TEST_WRITER_COMMON(TESTED_CLASS, 01)()
{
    EXPECTED("null", "<n />", "null", "\xD9\xD9\xF7\xF6");
    TESTED_CONTENT(res);
    TESTED_CLASS writer(&res);
    writer.write().null();
    QCOMPARE(TESTED_CONTENT_GETTER(res), expected);
}

TEST_WRITER_COMMON(TESTED_CLASS, 02)()
{
    EXPECTED("[\n]", "<s>\n</s>", "[]", "\xD9\xD9\xF7\x9F\xFF");
    TESTED_CONTENT(res);
    TESTED_CLASS writer(&res);
    writer.write().sequence();
    QCOMPARE(TESTED_CONTENT_GETTER(res), expected);
}

TEST_WRITER_COMMON(TESTED_CLASS, 03)()
{
    EXPECTED("{\n}", "<r>\n</r>", "{}", "\xD9\xD9\xF7\xBF\xFF");
    TESTED_CONTENT(res);
    TESTED_CLASS writer(&res);
    writer.write().record();
    QCOMPARE(TESTED_CONTENT_GETTER(res), expected);
}

TEST_WRITER_COMMON(TESTED_CLASS, 05)()
{
    EXPECTED("[null\n,[]\n,{}\n]",
             "<s><n />\n   <s></s>\n   <r></r>\n</s>",
             "[null | [] | {}]",
             "\xD9\xD9\xF7\x9F\xF6\x9F\xFF\xBF\xFF\xFF");
    TESTED_CONTENT(res);
    TESTED_CLASS writer(&res);
    writer.write().sequence()
    .null()
    .sequence().out()
    .record();
    QCOMPARE(TESTED_CONTENT_GETTER(res), expected);
}

TEST_WRITER_COMMON(TESTED_CLASS, 06)()
{
    EXPECTED("{\"k1\":null\n,\"k2\":[]\n,\"k3\":{}\n}",
             "<r><n key=\"k1\" />\n   <s key=\"k2\"></s>\n   <r key=\"k3\"></r>\n</r>",
             "{k1:null | k2:[] | k3:{}}",
             "\xD9\xD9\xF7\xBF\x62\x6B\x31\xF6\x62\x6B\x32\x9F\xFF\x62\x6B\x33\xBF\xFF\xFF"); // FIX this to remove empty message at end
    TESTED_CONTENT(res);
    TESTED_CLASS writer(&res);
    writer.write().record()
    .null("k1")
    .sequence("k2").out()
    .record("k3");
    QCOMPARE(TESTED_CONTENT_GETTER(res), expected);
}

TEST_WRITER_COMMON(TESTED_CLASS, 08)()
{
    EXPECTED("[true\n,false\n]",
             "<s><t type=\"boolean\">true</t>\n   <t type=\"boolean\">false</t>\n</s>",
             "[true | false]",
             "\xD9\xD9\xF7\x9F\xF5\xF4\xFF");
    TESTED_CONTENT(res);
    TESTED_CLASS writer(&res);
    writer.write().sequence().bind(true).bind(false);
    QCOMPARE(TESTED_CONTENT_GETTER(res), expected);
}


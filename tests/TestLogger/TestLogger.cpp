/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#include "TestLogger.h"

#include <QtTest/qtest.h>
#include <QtCore/QTimer>
#include <QtCore/QThread>

Q_LOGGING_CATEGORY(TestCategory, "test")

using namespace modmed;
using namespace data;
using namespace log;

void TestLogger::initTestCase()
{
    TO.reset(new TestingOutput());
}

void TestLogger::init()
{
    TO->clearAll();
    modmed::log::LogManager::getInstance().setOutput(TO.data());
}

void TestLogger::LogMessageMacroTags_01()
{
    // Test action LogMessage macro
    auto m = LogMessage() << log::ACTION_CREATE;
    QCOMPARE(m.format(),QByteArray("#Create"));
    m = LogMessage() << log::ACTION_COPY;
    QCOMPARE(m.format(),QByteArray("#Copy"));
    m = LogMessage() << log::ACTION_MOVE;
    QCOMPARE(m.format(),QByteArray("#Move"));
    m = LogMessage() << log::ACTION_REMOVE;
    QCOMPARE(m.format(),QByteArray("#Remove"));

    m = LogMessage() << log::ACTION_OPEN;
    QCOMPARE(m.format(),QByteArray("#Open"));
    m = LogMessage() << log::ACTION_READ;
    QCOMPARE(m.format(),QByteArray("#Read"));
    m = LogMessage() << log::ACTION_WRITE;
    QCOMPARE(m.format(),QByteArray("#Write"));
    m = LogMessage() << log::ACTION_EXECUTE;
    QCOMPARE(m.format(),QByteArray("#Execute"));
    m = LogMessage() << log::ACTION_ACCESS;
    QCOMPARE(m.format(),QByteArray("#Access"));
    m = LogMessage() << log::ACTION_CLOSE;
    QCOMPARE(m.format(),QByteArray("#Close"));

    m = LogMessage() << log::ACTION_LOGIN;
    QCOMPARE(m.format(),QByteArray("#Login"));
    m = LogMessage() << log::ACTION_ALLOW;
    QCOMPARE(m.format(),QByteArray("#Allow"));
    m = LogMessage() << log::ACTION_BLOCK;
    QCOMPARE(m.format(),QByteArray("#Block"));
    m = LogMessage() << log::ACTION_LOGOUT;
    QCOMPARE(m.format(),QByteArray("#Logout"));

    m = LogMessage() << log::ACTION_ALLOCATE;
    QCOMPARE(m.format(),QByteArray("#Allocate"));
    m = LogMessage() << log::ACTION_INITIALIZE;
    QCOMPARE(m.format(),QByteArray("#Initialize"));
    m = LogMessage() << log::ACTION_LOCK;
    QCOMPARE(m.format(),QByteArray("#Lock"));
    m = LogMessage() << log::ACTION_UNLOCK;
    QCOMPARE(m.format(),QByteArray("#Unlock"));
    m = LogMessage() << log::ACTION_FREE;
    QCOMPARE(m.format(),QByteArray("#Free"));

    m = LogMessage() << log::ACTION_BIND;
    QCOMPARE(m.format(),QByteArray("#Bind"));
    m = LogMessage() << log::ACTION_CONNECT;
    QCOMPARE(m.format(),QByteArray("#Connect"));
    m = LogMessage() << log::ACTION_DOWNLOAD;
    QCOMPARE(m.format(),QByteArray("#Download"));
    m = LogMessage() << log::ACTION_UPLOAD;
    QCOMPARE(m.format(),QByteArray("#Upload"));
    m = LogMessage() << log::ACTION_DISCONNECT;
    QCOMPARE(m.format(),QByteArray("#Disconnect"));

    m = LogMessage() << log::ACTION_START;
    QCOMPARE(m.format(),QByteArray("#Start"));
    m = LogMessage() << log::ACTION_SUSPEND;
    QCOMPARE(m.format(),QByteArray("#Suspend"));
    m = LogMessage() << log::ACTION_RESUME;
    QCOMPARE(m.format(),QByteArray("#Resume"));
    m = LogMessage() << log::ACTION_STOP;
    QCOMPARE(m.format(),QByteArray("#Stop"));

    m = LogMessage() << log::ACTION_INSTALL;
    QCOMPARE(m.format(),QByteArray("#Install"));
    m = LogMessage() << log::ACTION_UPDATE;
    QCOMPARE(m.format(),QByteArray("#Update"));
    m = LogMessage() << log::ACTION_UPGRADE;
    QCOMPARE(m.format(),QByteArray("#Upgrade"));
    m = LogMessage() << log::ACTION_UNINSTALL;
    QCOMPARE(m.format(),QByteArray("#Uninstall"));

    m = LogMessage() << log::ACTION_TRACE;
    QCOMPARE(m.format(),QByteArray("#Trace"));
}

void TestLogger::LogMessageMacroTags_02()
{
    // Test object LogMessage macro
    auto m = LogMessage() << log::OBJECT_ACCOUNT;
    QCOMPARE(m.format(),QByteArray("#Account"));
    m = LogMessage() << log::OBJECT_APP;
    QCOMPARE(m.format(),QByteArray("#App"));
    m = LogMessage() << log::OBJECT_CONNECTION;
    QCOMPARE(m.format(),QByteArray("#Connection"));
    m = LogMessage() << log::OBJECT_DRIVER;
    QCOMPARE(m.format(),QByteArray("#Driver"));
    m = LogMessage() << log::OBJECT_EMAIL;
    QCOMPARE(m.format(),QByteArray("#Email"));
    m = LogMessage() << log::OBJECT_FILE;
    QCOMPARE(m.format(),QByteArray("#File"));
    m = LogMessage() << log::OBJECT_HOST;
    QCOMPARE(m.format(),QByteArray("#Host"));
    m = LogMessage() << log::OBJECT_IPV4;
    QCOMPARE(m.format(),QByteArray("#Ipv4"));
    m = LogMessage() << log::OBJECT_IPV6;
    QCOMPARE(m.format(),QByteArray("#Ipv6"));
    m = LogMessage() << log::OBJECT_LIBRARY;
    QCOMPARE(m.format(),QByteArray("#Library"));
    m = LogMessage() << log::OBJECT_MEMORY;
    QCOMPARE(m.format(),QByteArray("#Memory"));
    m = LogMessage() << log::OBJECT_PROCESS;
    QCOMPARE(m.format(),QByteArray("#Process"));
    m = LogMessage() << log::OBJECT_SYSTEM;
    QCOMPARE(m.format(),QByteArray("#System"));
    m = LogMessage() << log::OBJECT_THREAD;
    QCOMPARE(m.format(),QByteArray("#Thread"));

    m = LogMessage() << log::OBJECT_ASSUMPTION;
    QCOMPARE(m.format(),QByteArray("#Assumption"));
    m = LogMessage() << log::OBJECT_REQUIREMENT;
    QCOMPARE(m.format(),QByteArray("#Requirement"));
}

void TestLogger::LogMessageMacroTags_03()
{
    // Test status LogMessage macro
    auto m = LogMessage() << log::STATUS_ONGOING;
    QCOMPARE(m.format(),QByteArray("#Ongoing"));
    m = LogMessage() << log::STATUS_CANCEL;
    QCOMPARE(m.format(),QByteArray("#Cancel"));
    m = LogMessage() << log::STATUS_FINISHED;
    QCOMPARE(m.format(),QByteArray("#Finished"));
    m = LogMessage() << log::STATUS_FAILURE;
    QCOMPARE(m.format(),QByteArray("#Failure"));
    m = LogMessage() << log::STATUS_SUCCESS;
    QCOMPARE(m.format(),QByteArray("#Success"));
    m = LogMessage() << log::STATUS_UNKNOWN;
    QCOMPARE(m.format(),QByteArray("#Unknown"));
}

void TestLogger::LogEventMacro_01()
{
    mDebug() << "debug"; int dline = __LINE__;
    mInfo() << "info"; int iline = __LINE__;
    mWarning() << "warning"; int wline = __LINE__;
    mCritical() << "critical"; int cline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 4);
    QCOMPARE(TO->m_eventDataList[0].category(), "default");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("debug"));

    QCOMPARE(TO->m_eventDataList[1].category(), "default");
    QCOMPARE(TO->m_eventDataList[1].type(), QtInfoMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), iline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("info"));

    QCOMPARE(TO->m_eventDataList[2].category(), "default");
    QCOMPARE(TO->m_eventDataList[2].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), wline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("warning"));

    QCOMPARE(TO->m_eventDataList[3].category(), "default");
    QCOMPARE(TO->m_eventDataList[3].type(), QtCriticalMsg);
    QCOMPARE(TO->m_eventDataList[3].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[3].line(), cline);
    QCOMPARE(TO->m_eventDataList[3].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[3].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[3].format(),QByteArray("critical"));
}

void TestLogger::LogEventMacro_02()
{
    mCDebug(TestCategory) << "debug"; int dline = __LINE__;
    mCInfo(TestCategory) << "info"; int iline = __LINE__;
    mCWarning(TestCategory) << "warning"; int wline = __LINE__;
    mCCritical(TestCategory) << "critical"; int cline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 4);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("debug"));

    QCOMPARE(TO->m_eventDataList[1].category(), "test");
    QCOMPARE(TO->m_eventDataList[1].type(), QtInfoMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), iline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("info"));

    QCOMPARE(TO->m_eventDataList[2].category(), "test");
    QCOMPARE(TO->m_eventDataList[2].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), wline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("warning"));

    QCOMPARE(TO->m_eventDataList[3].category(), "test");
    QCOMPARE(TO->m_eventDataList[3].type(), QtCriticalMsg);
    QCOMPARE(TO->m_eventDataList[3].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[3].line(), cline);
    QCOMPARE(TO->m_eventDataList[3].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[3].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[3].format(),QByteArray("critical"));
}

void TestLogger::LogEventMacro_03()
{
    int dline;
    for (int i=0; i<250; ++i)
    {
        mCDebugModulo(TestCategory, 100) << i;  dline = __LINE__;
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);

    QCOMPARE(TO->m_eventDataList[1].category(), "test");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 1);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 100);

    QCOMPARE(TO->m_eventDataList[2].category(), "test");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 1);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 200);
}

void TestLogger::LogEventMacro_04()
{
    int dline;
    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    int i = 0;
    for (double elapsed(0); elapsed<10*ms; elapsed=t.nsecsElapsed())
    {
        mCDebugNSecs(TestCategory, 5) << i; dline = __LINE__;
        i += 5;
        QThread::msleep(3); // slightly faster than sampling
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);

    QCOMPARE(TO->m_eventDataList[1].category(), "test");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 1);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 5);

    QCOMPARE(TO->m_eventDataList[2].category(), "test");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 1);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 10);
}

void TestLogger::LogEventMacro_05()
{
    double result = 0.5;
    int dline;
    {
        mCDebugOut(TestCategory) << result; dline = __LINE__;
        result = 3.14;
    }
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_doubleValues.takeFirst(), 3.14);
}

void TestLogger::LogEventMacroExpr_01()
{
    int testNumber = 2048;
    mDebug() << mExpr(testNumber); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "default");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("testNumber %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber);
}

void TestLogger::LogEventMacroExpr_02()
{
    int testNumber1 = 1;
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    mDebug() << mExpr9(testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "default");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroTrace_01()
{
    int testNumber1 = 1;
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    mCTrace9(TestCategory, testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroTrace_02()
{
    int dline;
    int testNumber1 = 0;
    int testNumber2 = 0;
    int testNumber3 = 0;
    int testNumber4 = 0;
    int testNumber5 = 0;
    int testNumber6 = 0;
    int testNumber7 = 0;
    int testNumber8 = 0;
    int testNumber9 = 0;
    {
        mCTraceOut9(TestCategory, testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        testNumber1 = 1;
        testNumber2 = 2;
        testNumber3 = 3;
        testNumber4 = 4;
        testNumber5 = 5;
        testNumber6 = 6;
        testNumber7 = 7;
        testNumber8 = 8;
        testNumber9 = 9;
    }
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Finished testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 9);
}

void TestLogger::LogEventMacroTrace_03()
{
    QTimer* testTimer1 = new QTimer(this);
    int number1 = 1;
    mCTraceQObject2(TestCategory, testTimer1, number1); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace QTimer %s objectName %s number1 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 3;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_bindTextCall.size(), 2);
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), QString("0x%1").arg(quintptr(testTimer1), 0, 16));
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), testTimer1->objectName());
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), number1);
}

void TestLogger::LogEventMacroTrace_04()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    for (int i=0; i<250; ++i)
    {
        mCTraceModulo9(TestCategory, 100, i, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9);  dline = __LINE__;
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);

    QCOMPARE(TO->m_eventDataList[1].category(), "test");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 2;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 2);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 100);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), testNumber2);

    QCOMPARE(TO->m_eventDataList[2].category(), "test");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 2);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 200);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), testNumber2);
}

void TestLogger::LogEventMacroTrace_05()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    int i = 0;
    for (double elapsed(0); elapsed<10*ms; elapsed=t.nsecsElapsed())
    {
        mCTraceNSecs9(TestCategory, 5, i, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        i += 5;
        QThread::msleep(3); // slightly faster than sampling
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);

    QCOMPARE(TO->m_eventDataList[1].category(), "test");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 2;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 2);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 5);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), testNumber2);

    QCOMPARE(TO->m_eventDataList[2].category(), "test");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 2);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 10);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), testNumber2);
}

void TestLogger::LogEventMacroAssumption_01()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    int res = 0;
    for (int i = 1; i < 3; ++i)
    {
        mCAssumption9(TestCategory, res < 1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        res += i;
    }

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Assumption #Failure res < 1 testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 8;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroRequirement_01()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    int res = 0;
    for (int i = 1; i < 3; ++i)
    {
        mCRequirement9(TestCategory, res < 1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        res += i;
    }

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "test");
    QCOMPARE(TO->m_eventDataList[0].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Requirement #Failure res < 1 testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 8;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

M_UNIT_LOG_CATEGORY("unitTestCategory")

void TestLogger::LogEventMacroSULC_01()
{
    mUDebug() << "debug"; int dline = __LINE__;
    mUInfo() << "info"; int iline = __LINE__;
    mUWarning() << "warning"; int wline = __LINE__;
    mUCritical() << "critical"; int cline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 4);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("debug"));

    QCOMPARE(TO->m_eventDataList[1].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[1].type(), QtInfoMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), iline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("info"));

    QCOMPARE(TO->m_eventDataList[2].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[2].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), wline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("warning"));

    QCOMPARE(TO->m_eventDataList[3].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[3].type(), QtCriticalMsg);
    QCOMPARE(TO->m_eventDataList[3].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[3].line(), cline);
    QCOMPARE(TO->m_eventDataList[3].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[3].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[3].format(),QByteArray("critical"));
}

void TestLogger::LogEventMacroSULC_02()
{
    int dline;
    for (int i=0; i<250; ++i)
    {
        mUDebugModulo(100) << i;  dline = __LINE__;
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);

    QCOMPARE(TO->m_eventDataList[1].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 1);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 100);

    QCOMPARE(TO->m_eventDataList[2].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 1);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 200);
}

void TestLogger::LogEventMacroSULC_03()
{
    int dline;
    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    int i = 0;
    for (double elapsed(0); elapsed<10*ms; elapsed=t.nsecsElapsed())
    {
        mUDebugNSecs(5) << i; dline = __LINE__;
        i += 5;
        QThread::msleep(4); // slightly faster than sampling
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);

    QCOMPARE(TO->m_eventDataList[1].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 1);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 5);

    QCOMPARE(TO->m_eventDataList[2].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 1);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 10);
}

void TestLogger::LogEventMacroSULC_04()
{
    double result = 0.5;
    int dline;
    {
        mUDebugOut() << result; dline = __LINE__;
        result = 3.14;
    }
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_doubleValues.takeFirst(), 3.14);
}

void TestLogger::LogEventMacroSULCTrace_01()
{
    int testNumber1 = 1;
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    mUTrace9(testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroSULCTrace_02()
{
    int testNumber1 = 0;
    int testNumber2 = 0;
    int testNumber3 = 0;
    int testNumber4 = 0;
    int testNumber5 = 0;
    int testNumber6 = 0;
    int testNumber7 = 0;
    int testNumber8 = 0;
    int testNumber9 = 0;
    int dline;
    {
        mUTraceOut9(testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        testNumber1 = 1;
        testNumber2 = 2;
        testNumber3 = 3;
        testNumber4 = 4;
        testNumber5 = 5;
        testNumber6 = 6;
        testNumber7 = 7;
        testNumber8 = 8;
        testNumber9 = 9;
    }
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Finished testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroSULCTrace_03()
{
    QTimer* testTimer1 = new QTimer(this);
    int number1 = 1;
    mUTraceQObject2(testTimer1, number1); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace QTimer %s objectName %s number1 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 3;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_bindTextCall.size(), 2);
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), QString("0x%1").arg(quintptr(testTimer1), 0, 16));
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), testTimer1->objectName());
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), number1);
}

void TestLogger::LogEventMacroSULCTrace_04()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    for (int i=0; i<250; ++i)
    {
        mUTraceModulo9(100, i, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9);  dline = __LINE__;
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);

    QCOMPARE(TO->m_eventDataList[1].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 2;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 2);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 100);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), testNumber2);

    QCOMPARE(TO->m_eventDataList[2].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 2);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 200);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), testNumber2);
}

void TestLogger::LogEventMacroSULCTrace_05()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    int i = 0;
    for (double elapsed(0); elapsed<10*ms; elapsed=t.nsecsElapsed())
    {
        mUTraceNSecs9(5, i, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        i += 5;
        QThread::msleep(3); // slightly faster than sampling
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);

    QCOMPARE(TO->m_eventDataList[1].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 2;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 2);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 5);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), testNumber2);

    QCOMPARE(TO->m_eventDataList[2].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 2);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 10);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), testNumber2);
}

void TestLogger::LogEventMacroSULCAssumption_01()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    int res = 0;
    for (int i = 1; i < 3; ++i)
    {
        mUAssumption9(res < 1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        res += i;
    }

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Assumption #Failure res < 1 testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 8;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroSULCRequirement_01()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    int res = 0;
    for (int i = 1; i < 3; ++i)
    {
        mURequirement9(res < 1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        res += i;
    }

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "unitTestCategory");
    QCOMPARE(TO->m_eventDataList[0].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Requirement #Failure res < 1 testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 8;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

M_NAMESPACE_LOG_CATEGORY()

void TestLogger::LogEventMacroNLC_01()
{
    mNDebug() << "debug"; int dline = __LINE__;
    mNInfo() << "info"; int iline = __LINE__;
    mNWarning() << "warning"; int wline = __LINE__;
    mNCritical() << "critical"; int cline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 4);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("debug"));

    QCOMPARE(TO->m_eventDataList[1].category(), "");
    QCOMPARE(TO->m_eventDataList[1].type(), QtInfoMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), iline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("info"));

    QCOMPARE(TO->m_eventDataList[2].category(), "");
    QCOMPARE(TO->m_eventDataList[2].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), wline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("warning"));

    QCOMPARE(TO->m_eventDataList[3].category(), "");
    QCOMPARE(TO->m_eventDataList[3].type(), QtCriticalMsg);
    QCOMPARE(TO->m_eventDataList[3].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[3].line(), cline);
    QCOMPARE(TO->m_eventDataList[3].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[3].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[3].format(),QByteArray("critical"));
}

void TestLogger::LogEventMacroNLC_02()
{
    int dline;
    for (int i=0; i<250; ++i)
    {
        mNDebugModulo(100) << i;  dline = __LINE__;
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);

    QCOMPARE(TO->m_eventDataList[1].category(), "");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 1);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 100);

    QCOMPARE(TO->m_eventDataList[2].category(), "");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 1);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 200);
}

void TestLogger::LogEventMacroNLC_03()
{
    int dline;
    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    int i = 0;
    for (double elapsed(0); elapsed<10*ms; elapsed=t.nsecsElapsed())
    {
        mNDebugNSecs(5) << i; dline = __LINE__;
        i += 5;
        QThread::msleep(4); // slightly faster than sampling
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);

    QCOMPARE(TO->m_eventDataList[1].category(), "");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 1);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 5);

    QCOMPARE(TO->m_eventDataList[2].category(), "");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 1);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 10);
}

void TestLogger::LogEventMacroNLC_04()
{
    double result = 0.5;
    int dline;
    {
        mNDebugOut() << result; dline = __LINE__;
        result = 3.14;
    }
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("%s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_doubleValues.takeFirst(), 3.14);
}

void TestLogger::LogEventMacroNLCTrace_01()
{
    int testNumber1 = 1;
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    mNTrace9(testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroNLCTrace_02()
{
    int testNumber1 = 0;
    int testNumber2 = 0;
    int testNumber3 = 0;
    int testNumber4 = 0;
    int testNumber5 = 0;
    int testNumber6 = 0;
    int testNumber7 = 0;
    int testNumber8 = 0;
    int testNumber9 = 0;
    int dline;
    {
        mNTraceOut9(testNumber1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        testNumber1 = 1;
        testNumber2 = 2;
        testNumber3 = 3;
        testNumber4 = 4;
        testNumber5 = 5;
        testNumber6 = 6;
        testNumber7 = 7;
        testNumber8 = 8;
        testNumber9 = 9;
    }
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Finished testNumber1 %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber1);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroNLCTrace_03()
{
    QTimer* testTimer1 = new QTimer(this);
    int number1 = 1;
    mNTraceQObject2(testTimer1, number1); int dline = __LINE__;

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace QTimer %s objectName %s number1 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 3;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 1);
    QCOMPARE(IDT0.m_bindTextCall.size(), 2);
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), QString("0x%1").arg(quintptr(testTimer1), 0, 16));
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), testTimer1->objectName());
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), number1);
}

void TestLogger::LogEventMacroNLCTrace_04()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    for (int i=0; i<250; ++i)
    {
        mNTraceModulo9(100, i, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9);  dline = __LINE__;
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);

    QCOMPARE(TO->m_eventDataList[1].category(), "");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 2;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 2);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 100);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), testNumber2);

    QCOMPARE(TO->m_eventDataList[2].category(), "");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 2;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 2);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 200);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), testNumber2);
}

void TestLogger::LogEventMacroNLCTrace_05()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    QElapsedTimer t;
    t.start();
    const int ms = 1000000;
    int i = 0;
    for (double elapsed(0); elapsed<10*ms; elapsed=t.nsecsElapsed())
    {
        mNTraceNSecs9(5, i, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        i += 5;
        QThread::msleep(3); // slightly faster than sampling
    }

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 9;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 9);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), 0);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);

    QCOMPARE(TO->m_eventDataList[1].category(), "");
    QCOMPARE(TO->m_eventDataList[1].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[1].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[1].line(), dline);
    QCOMPARE(TO->m_eventDataList[1].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[1].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 9;
    IDT1.write().bind(TO->m_eventDataList[1].args());
    QCOMPARE(IDT1.m_numberCall.size(), 9);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), 5);
    QCOMPARE(IDT1.m_longlongValues.takeFirst(), testNumber2);

    QCOMPARE(TO->m_eventDataList[2].category(), "");
    QCOMPARE(TO->m_eventDataList[2].type(), QtDebugMsg);
    QCOMPARE(TO->m_eventDataList[2].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[2].line(), dline);
    QCOMPARE(TO->m_eventDataList[2].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[2].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("#Trace i %s testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 9;
    IDT2.write().bind(TO->m_eventDataList[2].args());
    QCOMPARE(IDT2.m_numberCall.size(), 9);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), 10);
    QCOMPARE(IDT2.m_longlongValues.takeFirst(), testNumber2);
}

void TestLogger::LogEventMacroNLCAssumption_01()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    int res = 0;
    for (int i = 1; i < 3; ++i)
    {
        mNAssumption9(res < 1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        res += i;
    }

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Assumption #Failure res < 1 testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 8;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

void TestLogger::LogEventMacroNLCRequirement_01()
{
    int testNumber2 = 2;
    int testNumber3 = 3;
    int testNumber4 = 4;
    int testNumber5 = 5;
    int testNumber6 = 6;
    int testNumber7 = 7;
    int testNumber8 = 8;
    int testNumber9 = 9;

    int dline;
    int res = 0;
    for (int i = 1; i < 3; ++i)
    {
        mNRequirement9(res < 1, testNumber2, testNumber3, testNumber4, testNumber5, testNumber6, testNumber7, testNumber8, testNumber9); dline = __LINE__;
        res += i;
    }

    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].category(), "");
    QCOMPARE(TO->m_eventDataList[0].type(), QtWarningMsg);
    QCOMPARE(TO->m_eventDataList[0].file(), __FILE__);
    QCOMPARE(TO->m_eventDataList[0].line(), dline);
    QCOMPARE(TO->m_eventDataList[0].function(), Q_FUNC_INFO);
    QCOMPARE(TO->m_eventDataList[0].threadId(), QByteArray::number((qint64)QThread::currentThreadId()));
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("#Trace #Requirement #Failure res < 1 testNumber2 %s testNumber3 %s testNumber4 %s testNumber5 %s testNumber6 %s testNumber7 %s testNumber8 %s testNumber9 %s"));
    // Verify args
    IDataTester<Writer> IDT0;
    IDT0.m_sequenceItemRetNullAfter = 8;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_numberCall.size(), 8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber2);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber3);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber4);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber5);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber6);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber7);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber8);
    QCOMPARE(IDT0.m_longlongValues.takeFirst(), testNumber9);
}

namespace _ {
M_NAMESPACE_LOG_CATEGORY()
void mNDebug_01()
{
    mNDebug() << "category should be _";
}
namespace namespace_test {
M_NAMESPACE_LOG_CATEGORY()
void mNDebug_01()
{
    mNDebug() << "category should be _.namespace_test";
}
namespace namespace_test2 {
M_NAMESPACE_LOG_CATEGORY(QtWarningMsg)
void mNDebug_01()
{
    mNDebug() << "category should be _.namespace_test.namespace_test2";
}
}
}
}

void TestLogger::mNamespaceLogCategory_01()
{
    QCOMPARE(QString(                     mNamespaceLogCategory().categoryName()), QString(""));
    QCOMPARE(QString(                   ::mNamespaceLogCategory().categoryName()), QString(""));
    QCOMPARE(QString(                  _::mNamespaceLogCategory().categoryName()), QString("_"));
    QCOMPARE(QString(::_::namespace_test::mNamespaceLogCategory().categoryName()), QString("_.namespace_test"));
    QCOMPARE(QString(::_::namespace_test::namespace_test2::mNamespaceLogCategory().categoryName()), QString("_.namespace_test.namespace_test2"));
}

void TestLogger::mNamespaceLogCategory_02()
{
    QCOMPARE(   mNamespaceLogCategory().isEnabled(QtDebugMsg), true);
    QCOMPARE(_::mNamespaceLogCategory().isEnabled(QtDebugMsg), true);
    QCOMPARE(::_::namespace_test::mNamespaceLogCategory().isEnabled(QtDebugMsg), true);
    QCOMPARE(::_::namespace_test::namespace_test2::mNamespaceLogCategory().isEnabled(QtDebugMsg), false);
}

void TestLogger::mNDebug_01()
{
    _::mNDebug_01();
    _::namespace_test::mNDebug_01();
    _::namespace_test::namespace_test2::mNDebug_01();

    QCOMPARE(TO->m_eventDataList.size(), 2);
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("category should be _"));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("category should be _.namespace_test"));
}

void TestLogger::mDebug_01()
{
    // mDebug(...)-like macros must be fully-flexible expressions
    (false ? mCritical() : false ? mWarning() : false ? mInfo() : mDebug()) << "stream";
    (false ? mCritical("format") : false ? mWarning("format") : false ? mInfo("format") : mDebug("format"));

    // mDebug(...)-like macros must be fully-flexible expressions even when disabled
    (MODMED_NO_MESSAGE_MACRO()) << "stream";

    QVERIFY2(true, "compile-time test");
}

void TestLogger::mDebug_02()
{
    mCDebug(TestCategory(), "clap clip clap % s in the middle");
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("clap clip clap % s in the middle"));
}

void TestLogger::mDebug_03()
{
    qDebug() << "plooo-hooop";
    mDebug() << "pliii-hiiip";

    auto prevHandler = qInstallMessageHandler(modmed::log::LogManager::qtMessageHandler);

    qDebug() << "plop";
    mDebug() << "plip";

    QCOMPARE(TO->m_eventDataList.size(), 3);
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("pliii-hiiip"));
    QCOMPARE(TO->m_eventDataList[1].format(),QByteArray("plop"));
    QCOMPARE(TO->m_eventDataList[2].format(),QByteArray("plip"));

    qInstallMessageHandler(prevHandler);
}

class LoggerThread : public QThread
{
    void run() {
        auto id = quint64(currentThreadId());
        while(!stop) {
            mDebug()<<"stream" << id ;
            mDebug("format %lld", id);
        }
    }
public:
    bool stop = false;
};

void TestLogger::mDebug_04()
{
    modmed::log::ConsoleLogWriter writer(false); // Use stderr output for testing purpose
    modmed::log::LogManager::getInstance().setOutput(&writer);

    QElapsedTimer t; LoggerThread lt1, lt2;
    lt1.setPriority(QThread::HighPriority);
    lt1.setPriority(QThread::HighPriority);
    t.start(); lt1.start(); lt2.start();
    while (t.elapsed() < 60*1000) // msec
    {
        QThread::msleep(1000); // msec
        qDebug()<<".";
    }
    lt1.stop = lt2.stop = true;
    QVERIFY(lt1.wait(1000));
    QVERIFY(lt2.wait(1000));
    QThread::msleep(1000);
}

void TestLogger::mDebug_05()
{
    char aWord[5] = "word";
    char aPhrase[7] = "phrase";
    mDebug("a %s and a %s and another %s", aWord, aPhrase, "word");
    QCOMPARE(TO->m_eventDataList.size(), 1);
    QCOMPARE(TO->m_eventDataList[0].format(),QByteArray("a %s and a %s and another %s"));

    IDataTester<Writer> IDT0;
    QCOMPARE(TO->m_eventDataList[0].args().size(), 3);
    IDT0.m_sequenceItemRetNullAfter = 3;
    IDT0.write().bind(TO->m_eventDataList[0].args());
    QCOMPARE(IDT0.m_bindTextCall.size(), 3);
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), QString("word"));
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), QString("phrase"));
    QCOMPARE(IDT0.m_bindTextStrings.takeFirst(), QString("word"));

    IDataTester<Writer> IDT1;
    IDT1.m_sequenceItemRetNullAfter = 3;
    IDT1.write().bind(TO->m_eventDataList[0].argTypes());
    QCOMPARE(IDT1.m_bytesCall.size(), 3);
    QCOMPARE(QString(IDT1.m_bytesValues.takeFirst()), QString(""));
    QCOMPARE(QString(IDT1.m_bytesValues.takeFirst()), QString(""));
    QCOMPARE(QString(IDT1.m_bytesValues.takeFirst()), QString(""));

    IDataTester<Writer> IDT2;
    IDT2.m_sequenceItemRetNullAfter = 3;
    IDT2.write().bind(TO->m_eventDataList[0].argNames());
    QCOMPARE(IDT2.m_bytesCall.size(), 3);
    QCOMPARE(QString(IDT2.m_bytesValues.takeFirst()), QString("aWord"));
    QCOMPARE(QString(IDT2.m_bytesValues.takeFirst()), QString("aPhrase"));
    QCOMPARE(QString(IDT2.m_bytesValues.takeFirst()), QString(""));
}

void TestLogger::LogEvent_01()
{
    static LogEvent a(QtDebugMsg, "", 0, ""), b(QtWarningMsg, "", 0, "");
    QCOMPARE(a.id(), 0);
    QCOMPARE(b.id(), 0);
    a.log(); b.log();
    QVERIFY(a.id() != 0);
    QVERIFY(b.id() != 0);
    QVERIFY(a.id() != b.id());
}

void TestLogger::LogEventFormat_01()
{
    LogEvent e(QtDebugMsg);
    LogMessage m =
        e.message(LogMessage::NoOptions
                 ,"2,true,1.23f,QString(\"a message can contain free text with spaces at the end\")"
                 ,"free text number %s text with atomic data types %s %s %s"
                 ,2
                 ,true
                 ,1.23f
                 ,QString("a message can contain free text with spaces at the end")
                 );
    QCOMPARE(m.format(), QByteArray("free text number %s text with atomic data types %s %s %s"));
    QCOMPARE(m.args().size(), 4);
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[0]), QString("2"));
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[1]), QString("true"));
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[2]), data::JsonString().write().bind(1.23f).toString());
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[3]), QString("\"a message can contain free text with spaces at the end\""));
}

void TestLogger::LogEventFormat_02()
{
    QLoggingCategory& category = *QLoggingCategory::defaultCategory();
    category.setEnabled(QtDebugMsg, false);
    LogEvent e(QtDebugMsg, nullptr, 0, nullptr, category);
    LogMessage m =
        e.message(LogMessage::NoOptions
                 ,"2,true,1.23f,QString(\"a message can contain free text with spaces at the end\")"
                 ,"free text number %s text with atomic data types %s %s %s"
                 ,2
                 ,true
                 ,1.23f
                 ,QString("a message can contain free text with spaces at the end")
                 );
    QCOMPARE(m.format(), QByteArray(""));
    QCOMPARE(m.args  ().size(), 0);
}

void TestLogger::LogMessage_01()
{
    auto m = // variable kept for test purposes only
        LogMessage()
        << "free text"
        << "number"
        << 2
        << "text with atomic data types"
        << true
        << 1.23f
        << QString("a message can contain free text with spaces at the end")
        ;
    QCOMPARE(m.format(),QByteArray("free text number %s text with atomic data types %s %s %s"));
    QCOMPARE(m.args  ().size(), 4);
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[0]), QString("2"));
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[1]), QString("true"));
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[2]), data::JsonString().write().bind(1.23f).toString());
    QCOMPARE((QString)data::JsonString().write().bind(m.args()[3]), QString("\"a message can contain free text with spaces at the end\""));
}

void TestLogger::LogMessageArgTypes_01()
{
    LogEvent e(QtDebugMsg);
    LogMessage m = e.message() << "No arg_types";
    QCOMPARE(m.format(), QByteArray("No arg_types"));
    QCOMPARE(m.args().size(), 0);
    QCOMPARE(m.argTypes().size(), 0);
}

void TestLogger::LogMessageArgTypes_02()
{
    LogEvent e(QtDebugMsg);

    bool aBoolean = false;
    int anInt = 42;
    double aDecimal = 3.14;
    QByteArray aByte = "blabla";
    QDateTime aTimestamp = QDateTime::currentDateTime();
    QString another = "blibli";

    LogMessage m = e.message() << "Test arg_types" << aBoolean << anInt << aDecimal << aByte << aTimestamp << another;
    QCOMPARE(m.format(), QByteArray("Test arg_types %s %s %s %s %s %s"));

    QCOMPARE(m.args().size(), m.argTypes().size());
    QCOMPARE(m.argTypes().size(), 6);

    QCOMPARE(m.argTypes()[0], QByteArray("_Boolean"));
    QCOMPARE(m.argTypes()[1], QByteArray("_Integer"));
    QCOMPARE(m.argTypes()[2], QByteArray("_Decimal"));
    QCOMPARE(m.argTypes()[3], QByteArray("_Bytes"));
    QCOMPARE(m.argTypes()[4], QByteArray("_TimeStamp"));
    QCOMPARE(m.argTypes()[5], QByteArray(""));
}

void TestLogger::LogMessageArgNames_01()
{
    LogEvent e(QtDebugMsg);
    LogMessage m = e.message() << "No argNames";
    QCOMPARE(m.format(), QByteArray("No argNames"));
    QCOMPARE(m.args().size(), 0);
    QCOMPARE(m.argNames().size(), 0);
}

void TestLogger::LogMessageArgNames_02()
{
    LogEvent e(QtDebugMsg);

    bool aBoolean = false;
    int anInt = 42;
    double aDecimal = 3.14;
    QByteArray aByte = "blabla";
    QDateTime aTimestamp = QDateTime::currentDateTime();
    QString another = "blibli";

    LogMessage m = e.message() << "Test arg_types" << aBoolean << mExpr5(anInt, aDecimal, aByte, aTimestamp, another) << aBoolean << "end test";
    QCOMPARE(m.format(), QByteArray("Test arg_types %s anInt %s aDecimal %s aByte %s aTimestamp %s another %s %s end test"));

    QCOMPARE(m.args().size(), m.argTypes().size());
    QCOMPARE(m.argTypes().size(), 7);

    QCOMPARE(m.argTypes()[0], QByteArray("_Boolean"));
    QCOMPARE(m.argTypes()[1], QByteArray("_Integer"));
    QCOMPARE(m.argTypes()[2], QByteArray("_Decimal"));
    QCOMPARE(m.argTypes()[3], QByteArray("_Bytes"));
    QCOMPARE(m.argTypes()[4], QByteArray("_TimeStamp"));
    QCOMPARE(m.argTypes()[5], QByteArray(""));
    QCOMPARE(m.argTypes()[6], QByteArray("_Boolean"));

    QCOMPARE(m.args().size(), m.argNames().size());
    QCOMPARE(m.argNames().size(), 7);

    QCOMPARE(m.argNames()[0], QByteArray(""));
    QCOMPARE(m.argNames()[1], QByteArray("anInt"));
    QCOMPARE(m.argNames()[2], QByteArray("aDecimal"));
    QCOMPARE(m.argNames()[3], QByteArray("aByte"));
    QCOMPARE(m.argNames()[4], QByteArray("aTimestamp"));
    QCOMPARE(m.argNames()[5], QByteArray("another"));
    QCOMPARE(m.argNames()[6], QByteArray(""));
}

void TestLogger::ConsoleLogWriter_01()
{
    RUN_IN_LOGGER_EXE
    // ConsoleLogWriter always log ENDL before writing another trace
    QCOMPARE_IN_LOGGER_EXE_OUT(ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("_|default[TestRunner::ConsoleLo| test mDebug" ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("i|default[TestRunner::ConsoleLo| test mInfo" ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("?|default[TestRunner::ConsoleLo| test mWarning" ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("!|default[TestRunner::ConsoleLo| test mCritical" ENDL);
    // No ENDL because last one log
    QCOMPARE_IN_LOGGER_EXE_OUT("_|default[TestRunner::ConsoleLo| test mDebug Final");
}

void TestLogger::ConsoleLogWriter_02()
{
    RUN_IN_LOGGER_EXE
    QCOMPARE_IN_LOGGER_EXE_OUT("Les sanglots longs" ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("_|default[TestRunner::ConsoleLo| test mDebugDes violons" ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("i|default[TestRunner::ConsoleLo| test mInfoDe l'automne" ENDL);
    QCOMPARE_IN_LOGGER_EXE_OUT("?|default[TestRunner::ConsoleLo| test mWarning");
}

void TestLogger::LogWriter_01()
{
    QFile tempFile(QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".json");
    QVERIFY(tempFile.open(QIODevice::WriteOnly|QIODevice::Truncate|QIODevice::Text));
    log::LogWriter<data::JsonWriter> FO(&tempFile);
    log::LogManager::getInstance().setOutput(&FO);

    qint64 sizeBefore = tempFile.size();
    tempFile.seek(sizeBefore);
    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();
    QVERIFY(tempFile.size() > sizeBefore);
    tempFile.close();

    QVERIFY(tempFile.open(QIODevice::ReadOnly|QIODevice::Text));
    QTextStream o(&tempFile);
    QStringList sl = o.readLine().split("\t,");
    QCOMPARE(sl.size(), 14);
    QRegExp re("\\[\\{\"_elapsed_s\":[0-9]+(\\.[0-9]+)");
    QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("\"_timestamp\":\"\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\"");
    QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("\"_severity\":\"\\d{1}\"");
    QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("\"_category\":(\")*[a-zA-Z0-9]*(\")*");
    QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern(QString("\"_function\":\"[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\"");
    QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("\"_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern("\"_count\":[0-9]+");
    QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    QFileInfo fileInfo(__FILE__);
    re.setPattern(QString("\"_path\":\"[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\"");
    QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("\"_line\":[A-Z0-9]+");
    QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    re.setPattern("\"_thread_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("\"_format\":\"test %s\"");
    QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("\"_args\":\\[42\\]");
    QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("\"_arg_types\":\\[\"_Integer\"\\]");
    QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("\"_arg_names\":\\[\"\"\\]\\}");
    QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));

    QCOMPARE(o.readLine(), QString("]"));

    tempFile.remove();
}

void TestLogger::LogWriter_02()
{
    RUN_IN_LOGGER_EXE

    QStringList sl = QString(p.readLine()).split("\t,");
    QCOMPARE(sl.size(), 14);
    QRegExp re("\\[\\{\"_elapsed_s\":[0-9]+(\\.[0-9]+)");
    QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("\"_timestamp\":\"\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\"");
    QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("\"_severity\":\"\\d{1}\"");
    QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("\"_category\":(\")*[a-zA-Z0-9]*(\")*");
    QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern(QString("\"_function\":\"[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\"");
    QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("\"_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern("\"_count\":[0-9]+");
    QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
////    QFileInfo fileInfo(__FILE__);
////    re.setPattern(QString("\"_path\":\"[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\"");
////    QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("\"_line\":[A-Z0-9]+");
    QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    re.setPattern("\"_thread_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("\"_format\":\"test %s\"");
    QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("\"_args\":\\[42\\]");
    QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("\"_arg_types\":\\[\"_Integer\"\\]");
    QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
#ifdef Q_OS_WIN
    re.setPattern("\"_arg_names\":\\[\"\"\\]\\}\\r\\n");
#else
    re.setPattern("\"_arg_names\":\\[\"\"\\]\\}\\n");
#endif
    QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));

    QCOMPARE(QString(p.readLine()), QString("]"));
}

void TestLogger::LogWriter_03()
{
    QString slw;
    log::LogWriter<data::JsonWriter> FO(&slw);
    log::LogManager::getInstance().setOutput(&FO);

    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();

    QTextStream o(&slw);
    QStringList sl = o.readLine().split("\t,");
    QCOMPARE(sl.size(), 14);
    QRegExp re("\\[\\{\"_elapsed_s\":[0-9]+(\\.[0-9]+)");
    QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("\"_timestamp\":\"\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\"");
    QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("\"_severity\":\"\\d{1}\"");
    QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("\"_category\":(\")*[a-zA-Z0-9]*(\")*");
    QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern(QString("\"_function\":\"[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\"");
    QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("\"_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern("\"_count\":[0-9]+");
    QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    QFileInfo fileInfo(__FILE__);
    re.setPattern(QString("\"_path\":\"[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\"");
    QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("\"_line\":[A-Z0-9]+");
    QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    re.setPattern("\"_thread_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("\"_format\":\"test %s\"");
    QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("\"_args\":\\[42\\]");
    QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("\"_arg_types\":\\[\"_Integer\"\\]");
    QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("\"_arg_names\":\\[\"\"\\]\\}");
    QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));

    QCOMPARE(o.readLine(), QString("]"));
}


void TestLogger::LogWriter_04()
{
    QString slw;
    QTextStream ttslw(&slw);
    log::LogWriter<data::JsonWriter> FO(&ttslw);
    log::LogManager::getInstance().setOutput(&FO);

    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();

    QStringList sl = ttslw.readLine().split("\t,");
    QCOMPARE(sl.size(), 14);
    QRegExp re("\\[\\{\"_elapsed_s\":[0-9]+(\\.[0-9]+)");
    QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("\"_timestamp\":\"\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\"");
    QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("\"_severity\":\"\\d{1}\"");
    QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("\"_category\":(\")*[a-zA-Z0-9]*(\")*");
    QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern(QString("\"_function\":\"[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\"");
    QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("\"_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern("\"_count\":[0-9]+");
    QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    QFileInfo fileInfo(__FILE__);
    re.setPattern(QString("\"_path\":\"[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\"");
    QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("\"_line\":[A-Z0-9]+");
    QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    re.setPattern("\"_thread_id\":\"[a-zA-Z0-9]+\"");
    QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("\"_format\":\"test %s\"");
    QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("\"_args\":\\[42\\]");
    QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("\"_arg_types\":\\[\"_Integer\"\\]");
    QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("\"_arg_names\":\\[\"\"\\]\\}");
    QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));

    QCOMPARE(ttslw.readLine(), QString("]"));
}

void TestLogger::LogWriter_05()
{
    QFile tempFile(QDir::tempPath() + "/" + QUuid::createUuid().toString() + ".xml");
    QVERIFY(tempFile.open(QIODevice::WriteOnly|QIODevice::Truncate|QIODevice::Text));
    log::LogWriter<data::XmlWriter> FO(&tempFile);
    log::LogManager::getInstance().setOutput(&FO);

    qint64 sizeBefore = tempFile.size();
    tempFile.seek(sizeBefore);
    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();
    QVERIFY(tempFile.size() > sizeBefore);
    tempFile.close();

    QVERIFY(tempFile.open(QIODevice::ReadOnly|QIODevice::Text));
    QTextStream o(&tempFile);
    QStringList sl = o.readLine().split("><");
    QCOMPARE(sl.size(), 24);
    QRegExp re("\\<s"); QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("r"); QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("t key=\"_elapsed_s\"\\>[0-9]+(\\.[0-9]+)\\<\\/t"); QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("t key=\"_timestamp\"\\>\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\\<\\/t"); QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern("t key=\"_severity\"\\>\\d{1}\\<\\/t"); QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("t type=\"integer\" key=\"_category\"\\>(\")*[a-zA-Z0-9]*(\")*\\<\\/t"); QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern(QString("t key=\"_function\"\\>[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    re.setPattern("t key=\"_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("t type=\"integer\" key=\"_count\"\\>[0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    QFileInfo fileInfo(__FILE__);
    re.setPattern(QString("t key=\"_path\"\\>[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\\<\\/t"); QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("t type=\"integer\" key=\"_line\"\\>[A-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("t key=\"_thread_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("t key=\"_format\"\\>test %s\\<\\/t"); QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("s key=\"_args\""); QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));
    re.setPattern("t type=\"integer\"\\>42\\<\\/t"); QVERIFY2(re.exactMatch(sl[14]), qPrintable(sl[14]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[15]), qPrintable(sl[15]));
    re.setPattern("s key=\"_arg_types\""); QVERIFY2(re.exactMatch(sl[16]), qPrintable(sl[16]));
    re.setPattern("t\\>_Integer\\<\\/t"); QVERIFY2(re.exactMatch(sl[17]), qPrintable(sl[17]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[18]), qPrintable(sl[18]));
    re.setPattern("s key=\"_arg_names\""); QVERIFY2(re.exactMatch(sl[19]), qPrintable(sl[19]));
    re.setPattern("t"); QVERIFY2(re.exactMatch(sl[20]), qPrintable(sl[20]));
    re.setPattern("\\/t"); QVERIFY2(re.exactMatch(sl[21]), qPrintable(sl[21]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[22]), qPrintable(sl[22]));
    re.setPattern("\\/r\\>"); QVERIFY2(re.exactMatch(sl[23]), qPrintable(sl[23]));

    QCOMPARE(o.readLine(), QString("</s>"));

    tempFile.remove();
}

void TestLogger::LogWriter_06()
{
    RUN_IN_LOGGER_EXE

    QStringList sl = QString(p.readLine()).split("><");
    QCOMPARE(sl.size(), 24);
    QRegExp re("\\<s"); QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("r"); QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("t key=\"_elapsed_s\"\\>[0-9]+(\\.[0-9]+)\\<\\/t"); QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("t key=\"_timestamp\"\\>\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\\<\\/t"); QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern("t key=\"_severity\"\\>\\d{1}\\<\\/t"); QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("t type=\"integer\" key=\"_category\"\\>(\")*[a-zA-Z0-9]*(\")*\\<\\/t"); QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern(QString("t key=\"_function\"\\>[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    re.setPattern("t key=\"_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("t type=\"integer\" key=\"_count\"\\>[0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
////    QFileInfo fileInfo(__FILE__);
////    re.setPattern(QString("t key=\"_path\"\\>[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\\<\\/t"); //QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
////    QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    re.setPattern("t type=\"integer\" key=\"_line\"\\>[A-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("t key=\"_thread_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("t key=\"_format\"\\>test %s\\<\\/t"); QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("s key=\"_args\""); QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));
    re.setPattern("t type=\"integer\"\\>42\\<\\/t"); QVERIFY2(re.exactMatch(sl[14]), qPrintable(sl[14]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[15]), qPrintable(sl[15]));
    re.setPattern("s key=\"_arg_types\""); QVERIFY2(re.exactMatch(sl[16]), qPrintable(sl[16]));
    re.setPattern("t\\>_Integer\\<\\/t"); QVERIFY2(re.exactMatch(sl[17]), qPrintable(sl[17]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[18]), qPrintable(sl[18]));
    re.setPattern("s key=\"_arg_names\""); QVERIFY2(re.exactMatch(sl[19]), qPrintable(sl[19]));
    re.setPattern("t"); QVERIFY2(re.exactMatch(sl[20]), qPrintable(sl[20]));
    re.setPattern("\\/t"); QVERIFY2(re.exactMatch(sl[21]), qPrintable(sl[21]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[22]), qPrintable(sl[22]));
#ifdef Q_OS_WIN
    re.setPattern("\\/r\\>\\r\\n");
#else
    re.setPattern("\\/r\\>\\n");
#endif
    QVERIFY2(re.exactMatch(sl[23]), qPrintable(sl[23]));

    QCOMPARE(QString(p.readLine()), QString("</s>"));
}

void TestLogger::LogWriter_07()
{
    QString slw;
    log::LogWriter<data::XmlWriter> FO(&slw);
    log::LogManager::getInstance().setOutput(&FO);

    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();

    QTextStream o(&slw);
    QStringList sl = o.readLine().split("><");
    QCOMPARE(sl.size(), 24);
    QRegExp re("\\<s"); QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("r"); QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("t key=\"_elapsed_s\"\\>[0-9]+(\\.[0-9]+)\\<\\/t"); QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("t key=\"_timestamp\"\\>\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\\<\\/t"); QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern("t key=\"_severity\"\\>\\d{1}\\<\\/t"); QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("t type=\"integer\" key=\"_category\"\\>(\")*[a-zA-Z0-9]*(\")*\\<\\/t"); QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern(QString("t key=\"_function\"\\>[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    re.setPattern("t key=\"_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("t type=\"integer\" key=\"_count\"\\>[0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    QFileInfo fileInfo(__FILE__);
    re.setPattern(QString("t key=\"_path\"\\>[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\\<\\/t"); QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("t type=\"integer\" key=\"_line\"\\>[A-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("t key=\"_thread_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("t key=\"_format\"\\>test %s\\<\\/t"); QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("s key=\"_args\""); QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));
    re.setPattern("t type=\"integer\"\\>42\\<\\/t"); QVERIFY2(re.exactMatch(sl[14]), qPrintable(sl[14]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[15]), qPrintable(sl[15]));
    re.setPattern("s key=\"_arg_types\""); QVERIFY2(re.exactMatch(sl[16]), qPrintable(sl[16]));
    re.setPattern("t\\>_Integer\\<\\/t"); QVERIFY2(re.exactMatch(sl[17]), qPrintable(sl[17]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[18]), qPrintable(sl[18]));
    re.setPattern("s key=\"_arg_names\""); QVERIFY2(re.exactMatch(sl[19]), qPrintable(sl[19]));
    re.setPattern("t"); QVERIFY2(re.exactMatch(sl[20]), qPrintable(sl[20]));
    re.setPattern("\\/t"); QVERIFY2(re.exactMatch(sl[21]), qPrintable(sl[21]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[22]), qPrintable(sl[22]));
    re.setPattern("\\/r\\>"); QVERIFY2(re.exactMatch(sl[23]), qPrintable(sl[23]));

    QCOMPARE(o.readLine(), QString("</s>"));
}


void TestLogger::LogWriter_08()
{
    QString slw;
    QTextStream ttslw(&slw);
    log::LogWriter<data::XmlWriter> FO(&ttslw);
    log::LogManager::getInstance().setOutput(&FO);

    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();

    QStringList sl = ttslw.readLine().split("><");
    QCOMPARE(sl.size(), 24);
    QRegExp re("\\<s"); QVERIFY2(re.exactMatch(sl[0]), qPrintable(sl[0]));
    re.setPattern("r"); QVERIFY2(re.exactMatch(sl[1]), qPrintable(sl[1]));
    re.setPattern("t key=\"_elapsed_s\"\\>[0-9]+(\\.[0-9]+)\\<\\/t"); QVERIFY2(re.exactMatch(sl[2]), qPrintable(sl[2]));
    re.setPattern("t key=\"_timestamp\"\\>\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[+-]\\d{2}:\\d{2}\\<\\/t"); QVERIFY2(re.exactMatch(sl[3]), qPrintable(sl[3]));
    re.setPattern("t key=\"_severity\"\\>\\d{1}\\<\\/t"); QVERIFY2(re.exactMatch(sl[4]), qPrintable(sl[4]));
    re.setPattern("t type=\"integer\" key=\"_category\"\\>(\")*[a-zA-Z0-9]*(\")*\\<\\/t"); QVERIFY2(re.exactMatch(sl[5]), qPrintable(sl[5]));
    re.setPattern(QString("t key=\"_function\"\\>[a-zA-Z0-9_:() ]+(") + QString(__FUNCTION__).split("::").takeLast() + ")[a-zA-Z__0-9() ]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[6]), qPrintable(sl[6]));
    re.setPattern("t key=\"_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[7]), qPrintable(sl[7]));
    re.setPattern("t type=\"integer\" key=\"_count\"\\>[0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[8]), qPrintable(sl[8]));
    QFileInfo fileInfo(__FILE__);
    re.setPattern(QString("t key=\"_path\"\\>[..\\a-zA-Z0-9]+(") + fileInfo.fileName() + ")\\<\\/t"); QVERIFY2(re.exactMatch(sl[9]), qPrintable(sl[9]));
    re.setPattern("t type=\"integer\" key=\"_line\"\\>[A-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[10]), qPrintable(sl[10]));
    re.setPattern("t key=\"_thread_id\"\\>[a-zA-Z0-9]+\\<\\/t"); QVERIFY2(re.exactMatch(sl[11]), qPrintable(sl[11]));
    re.setPattern("t key=\"_format\"\\>test %s\\<\\/t"); QVERIFY2(re.exactMatch(sl[12]), qPrintable(sl[12]));
    re.setPattern("s key=\"_args\""); QVERIFY2(re.exactMatch(sl[13]), qPrintable(sl[13]));
    re.setPattern("t type=\"integer\"\\>42\\<\\/t"); QVERIFY2(re.exactMatch(sl[14]), qPrintable(sl[14]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[15]), qPrintable(sl[15]));
    re.setPattern("s key=\"_arg_types\""); QVERIFY2(re.exactMatch(sl[16]), qPrintable(sl[16]));
    re.setPattern("t\\>_Integer\\<\\/t"); QVERIFY2(re.exactMatch(sl[17]), qPrintable(sl[17]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[18]), qPrintable(sl[18]));
    re.setPattern("s key=\"_arg_names\""); QVERIFY2(re.exactMatch(sl[19]), qPrintable(sl[19]));
    re.setPattern("t"); QVERIFY2(re.exactMatch(sl[20]), qPrintable(sl[20]));
    re.setPattern("\\/t"); QVERIFY2(re.exactMatch(sl[21]), qPrintable(sl[21]));
    re.setPattern("\\/s"); QVERIFY2(re.exactMatch(sl[22]), qPrintable(sl[22]));
    re.setPattern("\\/r\\>"); QVERIFY2(re.exactMatch(sl[23]), qPrintable(sl[23]));

    QCOMPARE(ttslw.readLine(), QString("</s>"));
}

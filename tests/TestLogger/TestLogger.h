/****************************************************************************
 * **
 * ** Copyright (C) 2016 MinMaxMedical.
 * ** All rights reserved.
 * ** Contact: MinMaxMedical <InCAS@MinMaxMedical.com>
 * **
 * ** This file is part of the modmedLog module.
 * **
 * ** $QT_BEGIN_LICENSE:BSD$
 * ** You may use this file under the terms of the BSD license as follows:
 * **
 * ** "Redistribution and use in source and binary forms, with or without
 * ** modification, are permitted provided that the following conditions are
 * ** met:
 * **   * Redistributions of source code must retain the above copyright
 * **     notice, this list of conditions and the following disclaimer.
 * **   * Redistributions in binary form must reproduce the above copyright
 * **     notice, this list of conditions and the following disclaimer in
 * **     the documentation and/or other materials provided with the
 * **     distribution.
 * **   * Neither the name of MinMaxMedical S.A.S. and its Subsidiary(-ies) nor
 * **     the names of its contributors may be used to endorse or promote
 * **     products derived from this software without specific prior written
 * **     permission.
 * **
 * ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 * ** $QT_END_LICENSE$
 * **
 * ****************************************************************************/
#pragma once

#include <QtCore/qobject.h>

#include <modmed/data/Bind.h>
#include <modmed/data/JsonWriter.h>
#include <modmed/data/XmlWriter.h>

#include <modmed/log/LogEvent.h>
#include <modmed/log/LogMessage.h>
#include <modmed/log/LogManager.h>
#include <modmed/log/LogWriter.h>

#include <../Common/TestingOutput.h>
#include <../Common/IDataTester.h>
#include <TestingExe.h>

using namespace modmed;
using namespace log;

class TestLogger : public QObject
{
    Q_OBJECT

    Q_SLOT void initTestCase();
    Q_SLOT void init();

    Q_SLOT void LogMessageMacroTags_01(); // Test action LogMessage macro
    Q_SLOT void LogMessageMacroTags_02(); // Test object LogMessage macro
    Q_SLOT void LogMessageMacroTags_03(); // Test status LogMessage macro

    //Log with right severity and context (line, file, function, thread, time)
    Q_SLOT void LogEventMacro_01(); // Qt derivative macro
    Q_SLOT void LogEventMacro_02(); // Qt derivative macro + category
    Q_SLOT void LogEventMacro_03(); // Qt derivative macro + modulo
    Q_SLOT void LogEventMacro_04(); // Qt derivative macro + Nsecs
    Q_SLOT void LogEventMacro_05(); // Qt derivative macro + Out

    Q_SLOT void LogEventMacroExpr_01(); // Expr macro
    Q_SLOT void LogEventMacroExpr_02(); // Expr 2->9 macro

    Q_SLOT void LogEventMacroTrace_01(); // Trace macro + category
    Q_SLOT void LogEventMacroTrace_02(); // Trace macro + out
    Q_SLOT void LogEventMacroTrace_03(); // Trace macro + qobject
    Q_SLOT void LogEventMacroTrace_04(); // Trace macro + modulo
    Q_SLOT void LogEventMacroTrace_05(); // Trace macro + nsecs

    Q_SLOT void LogEventMacroAssumption_01(); // Assumption macro + category
    Q_SLOT void LogEventMacroRequirement_01(); // Requirement macro + category

    //mSoftwareUnitLogCategory
    Q_SLOT void LogEventMacroSULC_01(); // Qt derivative macro
    Q_SLOT void LogEventMacroSULC_02(); // Qt derivative macro + modulo
    Q_SLOT void LogEventMacroSULC_03(); // Qt derivative macro + Nsecs
    Q_SLOT void LogEventMacroSULC_04(); // Qt derivative macro + Out

    Q_SLOT void LogEventMacroSULCTrace_01(); // Trace macro + category
    Q_SLOT void LogEventMacroSULCTrace_02(); // Trace macro + out
    Q_SLOT void LogEventMacroSULCTrace_03(); // Trace macro + qobject
    Q_SLOT void LogEventMacroSULCTrace_04(); // Trace macro + modulo
    Q_SLOT void LogEventMacroSULCTrace_05(); // Trace macro + nsecs

    Q_SLOT void LogEventMacroSULCAssumption_01(); // Assumption macro + category
    Q_SLOT void LogEventMacroSULCRequirement_01(); // Requirement macro + category

    //mNamespaceLogCategory
    Q_SLOT void LogEventMacroNLC_01(); // Qt derivative macro
    Q_SLOT void LogEventMacroNLC_02(); // Qt derivative macro + modulo
    Q_SLOT void LogEventMacroNLC_03(); // Qt derivative macro + Nsecs
    Q_SLOT void LogEventMacroNLC_04(); // Qt derivative macro + Out

    Q_SLOT void LogEventMacroNLCTrace_01(); // Trace macro + category
    Q_SLOT void LogEventMacroNLCTrace_02(); // Trace macro + out
    Q_SLOT void LogEventMacroNLCTrace_03(); // Trace macro + qobject
    Q_SLOT void LogEventMacroNLCTrace_04(); // Trace macro + modulo
    Q_SLOT void LogEventMacroNLCTrace_05(); // Trace macro + nsecs

    Q_SLOT void LogEventMacroNLCAssumption_01(); // Assumption macro + category
    Q_SLOT void LogEventMacroNLCRequirement_01(); // Requirement macro + category

    // Namespace category test
    Q_SLOT void mNamespaceLogCategory_01(); // Fill categoryName
    Q_SLOT void mNamespaceLogCategory_02(); // Check isEnabled ignore lower QtMsgType given to macro

    Q_SLOT void mNDebug_01(); //check output according to namespace and isEnabled level

    Q_SLOT void mDebug_01(); // Fully-flexible expressions
    Q_SLOT void mDebug_02(); // Encodage % no crash
    Q_SLOT void mDebug_03(); // Message hangler log qt and ours traces
    Q_SLOT void mDebug_04(); // Thread safety by storing only const LogEventData in the shared LogEvent
    Q_SLOT void mDebug_05(); // Old C-style trace

    Q_SLOT void LogEvent_01(); // Test ID
    Q_SLOT void LogEventFormat_01(); //Cannot test before LogEvent.message(...) returns LogMessage
    Q_SLOT void LogEventFormat_02(); //Disable category
    Q_SLOT void LogMessage_01();

    Q_SLOT void LogMessageArgTypes_01(); // No args
    Q_SLOT void LogMessageArgTypes_02(); // Others

    Q_SLOT void LogMessageArgNames_01(); // No args
    Q_SLOT void LogMessageArgNames_02(); // Others

    Q_SLOT void ConsoleLogWriter_01(); // Test basic output and ENDL
    Q_SLOT void ConsoleLogWriter_02(); // macro always start line but can have other text at the end

    Q_SLOT void LogWriter_01(); // Write in Json QIODevice (QFile)
    Q_SLOT void LogWriter_02(); // Write in Json File *
    Q_SLOT void LogWriter_03(); // Write in Json QString
    Q_SLOT void LogWriter_04(); // Write in Json QTextStream
    Q_SLOT void LogWriter_05(); // Write in Xml QIODevice (QFile)
    Q_SLOT void LogWriter_06(); // Write in Xml File *
    Q_SLOT void LogWriter_07(); // Write in Xml QString
    Q_SLOT void LogWriter_08(); // Write in Xml QTextStream

    QSharedPointer<TestingOutput> TO;
};

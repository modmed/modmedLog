QT += testlib core
QT -= gui

TEMPLATE = app
TARGET = TestLogger
CONFIG += c++11 console debug_and_release
CONFIG -= app_bundle

INCLUDEPATH += ../../include

CONFIG(debug, debug|release) { DESTDIR = $$_PRO_FILE_PWD_/bin/Debug }
else { DESTDIR = $$_PRO_FILE_PWD_/bin/Release }

CONFIG(debug, debug|release) {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Debug
} else {
  LIBS += -L$$_PRO_FILE_PWD_/../../lib/Release
}

CONFIG(debug, debug|release) { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Debug }
else { unix:QMAKE_RPATHDIR += $$_PRO_FILE_PWD_/../../lib/Release }

CONFIG(release, debug|release) { DEFINES += QT_MESSAGELOGCONTEXT }

LIBS += -lmodmedLog

HEADERS += \
    TestLogger.h \
    ../Common/TestingOutput.h \
    ../Common/IDataTester.h \
    TestingExe.h

SOURCES += \
    main.cpp \
    TestLogger.cpp \
    ../Common/TestingOutput.cpp \
    ../Common/IDataTester.cpp

include(../../modmedLog.pri)

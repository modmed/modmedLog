#ifdef Q_OS_WIN
#define ENDL "\r\n"
#define REMOVE_COLOR_CHAR(x) x
#define EXE_EXT ".exe"
#else
#define ENDL "\n"
#define REMOVE_COLOR_CHAR(x) x.remove(QRegExp("\e\\[\\d*(;\\d*)*m"))
#define EXE_EXT
#endif

#define QCOMPARE_IN_LOGGER_EXE_OUT(expected) \
    QCOMPARE(REMOVE_COLOR_CHAR(QString(p.readLine())), QString(expected));

#define RUN_IN_LOGGER_EXE \
    QString testLogger_exe = QCoreApplication::applicationDirPath() + "/../../../TestLogger_exe/bin/" + QFileInfo(QCoreApplication::applicationDirPath()).baseName() + "/TestLogger_exe" EXE_EXT; \
    QProcess p; \
    p.setReadChannel(QProcess::StandardError); \
    QString funcName = __FUNCTION__; \
    funcName = funcName.mid(funcName.lastIndexOf(':') + 1); \
    p.start(testLogger_exe + " " + funcName); \
    QVERIFY2(p.waitForFinished(), qPrintable(QString("Could not start/find %1").arg(testLogger_exe))); \
    if (p.exitCode() != 0) \
    { \
    qCritical() << "exitCode" << p.exitCode(); \
    QFAIL(p.readAllStandardError().replace('\r', ' ')); \
    }

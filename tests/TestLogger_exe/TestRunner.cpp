#include "TestRunner.h"

#include <modmed/log/LogEvent.h>
#include <modmed/log/LogMessage.h>
#include <modmed/log/LogManager.h>
#include <modmed/log/ConsoleLogWriter.h>
#include <modmed/log/LogWriter.h>
#include <modmed/data/JsonWriter.h>
#include <modmed/data/XmlWriter.h>

using namespace modmed;
using namespace data;
using namespace log;

#define TEST_FAILURE testSucceeded = false; emit quit(); return;
#define TEST_SUCCESS testSucceeded = true; emit quit(); return;

void TestRunner::run(QString testName)
{
    qApp->processEvents();

    QMetaObject::invokeMethod(this, "init", Qt::QueuedConnection);
    if (!QMetaObject::invokeMethod(this, testName.toLatin1(), Qt::QueuedConnection))
    {
        QTextStream(stderr) << "Test " << testName << " is not implemented";
        TEST_FAILURE;
    }
}

void TestRunner::init()
{
    TO->clearAll();
}

void TestRunner::ConsoleLogWriter_01()
{
    mDebug() << "test mDebug";
    mInfo() << "test mInfo";
    mWarning() << "test mWarning";
    mCritical() << "test mCritical";
    mDebug() << "test mDebug Final";

    emit quit();
}

void TestRunner::ConsoleLogWriter_02()
{
    QTextStream out(stderr);

    out << "Les sanglots longs";
    out.flush();
    mDebug() << "test mDebug";
    out << "Des violons";
    out.flush();
    mInfo() << "test mInfo";
    out << "De l'automne";
    out.flush();
    mWarning() << "test mWarning";

    emit quit();
}


M_NAMESPACE_LOG_CATEGORY()
void TestRunner::LogWriter_02()
{
    log::LogWriter<data::JsonWriter> FO(stderr);
    log::LogManager::getInstance().setOutput(&FO);

    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();

    emit quit();
}

void TestRunner::LogWriter_06()
{
    log::LogWriter<data::XmlWriter> FO(stderr);
    log::LogManager::getInstance().setOutput(&FO);

    int anArg = 42;
    mNDebug() << "test" << anArg;
    FO.close();

    emit quit();
}

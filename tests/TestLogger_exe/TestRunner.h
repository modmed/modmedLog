#pragma once

#include <QtCore/qobject.h>

#include <../Common/TestingOutput.h>

using namespace modmed;
using namespace log;

struct TestRunner : public QObject
{
    Q_OBJECT
public:
    TestRunner() : testSucceeded(true) {}

    Q_SLOT void run (QString testName);
    Q_SLOT void init();

    Q_SLOT void ConsoleLogWriter_01();
    Q_SLOT void ConsoleLogWriter_02();

    Q_SLOT void LogWriter_02();
    Q_SLOT void LogWriter_06();

    Q_SIGNAL void quit();

    bool testSucceeded;

    QSharedPointer<TestingOutput> TO;
};

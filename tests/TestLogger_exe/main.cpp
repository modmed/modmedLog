#include <QtCore/qcoreapplication.h>
#include <QtCore/qstring.h>

#include <TestRunner.h>

#include <modmed/log/LogManager.h>
#include <modmed/log/ConsoleLogWriter.h>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    TestRunner testRunner;
    testRunner.TO.reset(new TestingOutput());

    if (QString(argv[1]) == "ConsoleLogWriter_01" || QString(argv[1]) == "ConsoleLogWriter_02")
    {
      modmed::log::ConsoleLogWriter writer(true); // Use stderr output for testing purpose
      modmed::log::LogManager::getInstance().setOutput(&writer);
    }

    QMetaObject::invokeMethod(&testRunner, "run", Qt::QueuedConnection, Q_ARG(QString, QString(argv[1])));

    app.connect(&testRunner, SIGNAL(quit()), SLOT(quit()));
    app.exec();

    return !testRunner.testSucceeded;
}
